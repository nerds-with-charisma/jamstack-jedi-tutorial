import React from "react"
import renderer from "react-test-renderer"

import HeaderLogo from "../src/components/Header/HeaderLogo"

describe("Header", () => {
  it("renders correctly", () => {
    const tree = renderer
      .create(<HeaderLogo />)
      .toJSON()
    expect(tree).toMatchSnapshot()
  })
})