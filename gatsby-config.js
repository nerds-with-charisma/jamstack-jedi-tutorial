module.exports = {
  /* Your site config here */
  siteMetadata: {
    siteUrl: `https://jamstack-jedi.nerdswithcharisma.com`,
  },
  plugins: [
    // 'gatsby-plugin-eslint',
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `Nerds With Charisma`,
        short_name: `NerdsWithCharisma`,
        start_url: `/`,
        background_color: `#733791`,
        theme_color: `#733791`,
        // Enables "Add to Homescreen" prompt and disables browser UI (including back button)
        // see https://developers.google.com/web/fundamentals/web-app-manifest/#display
        display: `standalone`,
        icon: `src/images/favicon.ico`, // This path is relative to the root of the site.
      },
    },
    {
      resolve: `gatsby-plugin-google-analytics`,
      options: {
        trackingId: "UA-141701162-1",
      },
    },
    {
      resolve: 'gatsby-plugin-robots-txt',
      options: {
        host: 'https://jamstack-jedi.nerdswithcharisma.com',
        sitemap: 'https://jamstack-jedi.nerdswithcharisma.com/sitemap.xml',
        policy: [{ userAgent: '*', allow: '/' }]
      }
    },
    {
      resolve: "gatsby-transformer-remark",
      options: {
        plugins: [
          {
            resolve:"@weknow/gatsby-remark-codepen",
            options: {
              theme: "dark",
              height: 400
            }
          },
          `gatsby-plugin-transition-link`
        ]
      }
    },
    `gatsby-plugin-sass`,
    `gatsby-plugin-offline`,
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-sitemap`,
    {
      resolve: `gatsby-plugin-google-analytics`,
      options: {
        trackingId: "UA-141701162-1",
        siteSpeedSampleRate: 10,
      },
    },
    {
      resolve: `gatsby-plugin-canonical-urls`,
      options: {
        siteUrl: `https://jamstack-jedi.nerdswithcharisma.com`,
      },
    },
  ],
}
