import React from 'react';

export default () => (
  <div id="fourOhFour"
    style={{
      padding: 0,
      margin: 0,
      width: '100%',
      height: '100%',
      display: 'flex',
      textAlign: 'center',
      alignItems: 'center',
      fontFamily: 'Roboto',
      letterSpacing: 0,
      fontStyle: 'normal',
      textRendering: 'optimizeLegibility',
      fontSize: 20,
      lineHeight: 1.4,
      color: '#484848',
    }}
  >
    <div style={{ flex: 1 }}>
      <h1 style={{
        fontSize: 200,
        margin: 0,
        left: 0,
        color: '#fff',
        fontWeight: 'bold',
        textAlign: 'left',
        position: 'fixed',
        top: 0,
        left: 0,
        lineHeight: '146px',
      }}>
        {'404'}
      </h1>
      <strong className="font--light" style={{ fontSize: 32, position: 'fixed', bottom: 0, right: 20, lineHeight: '20px' }}>
        {'WE\'RE ALL OUT OF PAGES'}
      </strong>
      <br />
      <br />
      <a href="/" style={{
        background: '#fff',
        fontSize: 24,
        borderRadius: 300,
        padding: '10px 65px',
        color: '#2c2c2c',
        fontWeight: 'bold',
        border: 'none',
        boxShadow: '0 4px 15px rgba(0, 0, 0, 0.2)',
        transition: 'all 0.3s linear',
        margin: '20px 0',
        width: 350,
        maxWidth: '100%',
        display: 'inline-block',
        clear: 'both',
       }}>
        {'GO HOME'}
      </a>
    </div>
  </div>
);
