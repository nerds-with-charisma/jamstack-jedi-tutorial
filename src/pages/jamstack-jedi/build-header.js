import React from 'react';
import AniLink from '../../components/Common/AniLink';

import Layout from '../../components/layout';

import Markdown from '../../components/Common/markdown';
import YouTube from '../../components/Common/YouTube';

const HeaderBuild = () => (
  <Layout
    title="Header Component"
    author={{ gravatar: '//0.gravatar.com/avatar/b5eff8d62adf3ff34e67f153781d27c4' }}
    gitRepo="https://gitlab.com/nerds-with-charisma/jamstack-jedi/blob/master/build-header"
    time={12}
    description={null}
    keywords={null}
  >
    <strong>Some Interesting Heading!</strong>
    <br />
    <br />

    <p>
      We're done with our hero and now it's time to work on our header and navigation. We'll start laying out the header elements and then we can move on to some interactivity to have the menu slide in.
    </p>

    <p>
      First lets create our initial files we'll need:
    </p>

    <Markdown
      rel="Terminal"
      language="terminal"
      md={`  mkdir src/components/header

  > src/components/header/Header.js`}
    />

    <p>
      And lets open up our new <strong>Header.js</strong> file and add some placeholder content:
    </p>

    <Markdown
      rel="Header.js"
      language="javascript"
      md={`  import React from 'react';

  const Header = () => (
    <header>
      <h1>Header</h1>
    </header>
  );

  export default Header;
  `}
    />

    <p>
      And import it into <strong>index.js</strong>:
    </p>

    <Markdown
      rel="index.js"
      language="javascript"
      md={`  ...
      import Header from '../components/header/Header';
  ...

  <Header />
  <Hero supportsWebP={supportsWebP} />
  ...`}
    />

    <img src="/images/jamstack-jedi/header--init.png" class="jamstack--img" alt="Header Init" />

    <hr />

    <p>
      Now lets add some content, we can split what will be inside our header into 2 components, the logo and the menu, so lets make those 2 files:
    </p>

    <Markdown
      rel="Terminal"
      language="terminal"
      md={`  > src/components/header/Logo.js

  > src/components/header/Nav.js`}
    />

    <p>
      Lets start by importing them into <strong>Header.js</strong>:
    </p>

    <Markdown
      rel="Header.js"
      language="javascript"
      md={`  import React from 'react';

  import Logo from './Logo';
  import Nav from './Nav';

  const Header = () => (
    <header>
      <Nav />
      <Logo />
    </header>
  );

  export default Header;`}
    />

    <p>
      Here's our initial layout for each component:
    </p>

    <Markdown
      rel="Logo.js"
      language="javascript"
      md={`  import React from 'react';

  import LogoSvg from '../../images/logo.svg';

  const Logo = () => (
    <a
      href="/"
      id="logo"
      className="font--light transition--all"
    >
      <img
        src={LogoSvg}
        alt="TODO"
      />
    </a>
  );

  export default Logo;`}
    />

    <Markdown
      rel="Nav.js"
      language="javascript"
      md={`  import React from 'react';

  const Nav = () => (
    <section id="headerNav">
      <button
        className="font--light"
        type="button"
      >
        <span>
          <i class="fas fa-bars">
            <span className="ir">
              Menu
            </span>
          </i>
        </span>
      </button>
    </section>
  );

  export default Nav;`}
    />

    <p>
      You can grab the logo svg from our <a href="https://gitlab.com/nerds-with-charisma/jamstack-jedi/blob/master/build-header/src/images/logo.svg" target="_blank">GitLab repo</a>, and place it in <b>/src/images/</b>
    </p>

    <p>
      Nothing fancy in here yet, so lets quickly tackle a few things we still need to do. First we need to handle the styling. Then we need to pull in some dynamic data, then we need to build the actual menu itself. We'll cover the first 2 items in this chapter.
    </p>

    <hr />

    <p>
      Styling shouldn't be too bad, lets make another scss file called <strong>_header.scss</strong> and import it into <strong>main.scss</strong>
    </p>

    <Markdown
      rel="Terminal"
      language="terminal"
      md={`  > src/styles/_header.scss`}
    />

    <Markdown
      rel="main.scss"
      language="scss"
      md={`  ...
  @import 'hero.scss';
  @import 'header.scss';`}
    />

    <p>
      And lets style the header, comments inline as usual:
    </p>

    <Markdown
      rel="_header.scss"
      language="scss"
      md={`  header {
    position: fixed;  /* doc our header at the top */
    width: 100%; /* make sure it stretches all the way across */
    left: 0; /* start at (0,0) */
    top: 0;
    z-index: 99999; /* keep it above pretty much everything */

    #headerNav {
      button {
        position: absolute; /* position our menu button on the left */
        left: 15px;
        top: 30px;
        border: none; /* remove the border from the button */
        font-size: 40px; /* make it larger */
      }
    }

    #logo {
      position: absolute; /* position our logo on the right */
      right: 30px;
      top: 45px;

      img {
        height: 20px; /* shrink our svg down a little bit */
      }
    }
  }`}
    />

    <img src="/images/jamstack-jedi/header--first-styles.png" class="jamstack--img" alt="Header Styles" />

    <p>
      We're looking pretty good, except we notice our <strong>.ir</strong> didn't work here, and it's because of our positioning, so we need to fix this by adding one rule to our .ir style, open up <strong>base.scss</strong> and add this line to our .ir class:
    </p>

    <Markdown
      rel="base.scss"
      language="scss"
      md={`  .ir {
    text-indent: -9999px;
    font-size: 10px;
    position: absolute;
  }`}
    />

    <p>
      That gets us where we want, and it honestly looks good on mobile as is!
    </p>

    <img src="/images/jamstack-jedi/header--mobile.png" class="jamstack--img" alt="Header Mobile" />

    <hr />

    <p>
      Now we want to pull in some dynamic data so that we can add an alt attribute to our logo.
    </p>

    <p>
      Open up <strong>gatsby-config.js</strong> and find the old <strong>title</strong> node, and modify it to our site's name:
    </p>

    <Markdown
      rel="gatsby-config.js"
      language="javascript"
      md={`  title: \`Nerds With Charisma\`,`}
    />

    <p>
      Just like before we need to pull it in via our GraphQl query and pass it as props to our header and again to our logo. Open up <strong>index.js</strong> and lets add to our query to get the site's title and pass it as a prop:
    </p>

    <Markdown
      rel="index.js"
      language="javascript"
      md={`  ...
  siteMetadata {
    title
    ...

    ...
    <Header title={data.site.siteMetadata.title} />
  ...`}
    />

    <p>
     Pass it to our <strong>Logo</strong> component:
    </p>

    <Markdown
      rel="Header.js"
      language="javascript"
      md={`  import React from 'react';
  import { PropTypes } from 'prop-types';
  import Logo from './Logo';
  import Nav from './Nav';

  const Header = ({ title }) => (
    <header>
      <Nav />
      <Logo title={title} />
    </header>
  );

  Header.propTypes = {
    title: PropTypes.string.isRequired,
  };

  export default Header;`}
    />

    <p>
      And use it:
    </p>

    <Markdown
      rel="Header.js"
      language="javascript"
      md={`  import React from 'react';
  import { PropTypes } from 'prop-types';

  import LogoSvg from '../../images/logo.svg';

  const Logo = ({ title }) => (
    <a
      href="/"
      id="logo"
      className="font--light transition--all"
    >
      <img
        src={LogoSvg}
        alt={\`\${title} Logo\`}
      />
    </a>
  );

  Logo.propTypes = {
    title: PropTypes.string.isRequired,
  };

  export default Logo;`}
    />

    <p>
      That finishes up everything we need to do for our header outside of the menu itself, so lets jump on that in the next chapter.
    </p>

    <AniLink to="jamstack-jedi/build-header-2" className="nextBtn">Build the Menu &raquo;</AniLink>
    <br />
  </Layout>
);

export default HeaderBuild;
