import React from 'react';

import AniLink from '../../components/Common/AniLink';
import Layout from '../../components/layout';
import Markdown from '../../components/Common/markdown';

const HostingNetlify = () => (
  <Layout
    title="Netlify"
    author={{ gravatar: '//0.gravatar.com/avatar/b5eff8d62adf3ff34e67f153781d27c4' }}
    gitRepo={null}
    time={15}
    description="How to host a website on Netlify."
    keywords="Netlify Hosting, hosting a site on Netlify, Git deployment to Netlify, Jamstack deployment"
  >
    <strong>Netlify Hosting</strong>
    <br />
    <br />

    <p>
      <b>
        Note: Only go through this section if you are going to host your website on Netlify. If you are going to host with an FTP service like HostGator or goDaddy check <AniLink to="jamstack-jedi/hosting-ftp">FTP chapter</AniLink>!!!!
      </b>
    </p>

    <p className="tldr">
      FTP hosting is easy, which is great for beginners, but it's also error prone and not very safe. You have the backup of Git incase you did something incorrect, but you still have to manually copy all files to your server.
    </p>

    <p className="tldr">
      What if we could link up our GitLab repo to our live site? Every time we commit something to our master branch, we could trigger a deployment to production. With Netlify, this is possible, as well as <u>FREE</u>! Netlify has paid plans as well if you outgrow the free plan.
    </p>

    <p className="tldr">
      Once you link Netlify with your Git repo, all you have to do is commit to the master branch, and Netlify will auto-trigger a build. Your new changes will be deployed live, within minutes. And if you want to roll back, that is also super simple.
    </p>

    <p>
      To get started with Netlify you'll need an account. Signup for one and once you're logged in you'll see your team's dashboard. Click <b>New site from Git</b>
    </p>

    <img src="/images/jamstack-jedi/netlify--gitlab.png" class="jamstack--img" alt="Netlify New Site Setup" />

    <p>
      Choose <b>GitLab</b>, and it will ask you to authorize Netlify to be able to access GitLab. Once logged in, choose the repo you made for coming soon.
    </p>

    <img src="/images/jamstack-jedi/netlify--choose-repo.png" class="jamstack--img" alt="Netlify Choose Repo" />

    <p>
      For now, you can leave the settings as default and click <b>Deploy Site</b>.
    </p>

    <p>
      Once deployed, you can visit your site at the custom Netlify domain they have setup. For example, ours is <a href="https://blissful-cori-69cc22.netlify.com/" target="_blank">https://blissful-cori-69cc22.netlify.com/</a>
    </p>

    <img src="/images/jamstack-jedi/netlify--first-deploy.png" class="jamstack--img" alt="Netlify First Deploy" />

    <p id="anchor--domain-hosting-connection">
      Next we need to setup our domain, click <b>Setup a custom domain</b> and enter your purchased domain name (if you buy the domain from Netlify they will handle all this for you).
    </p>

    <p>
      Here you'll receive instructions on linking your website to your domain. You'll want to grab the <b>Nameservers from Netlify</b> and add them to HostGator (or wherever you purchased your domain).
    </p>

    <p>
      Login to your HostGator admin and choose <b>Domains</b>, find your domain, and clock the <b>More</b> button to show your domains settings.
    </p>

    <img src="/images/jamstack-jedi/netlify--domain.png" class="jamstack--img" alt="Netlify connecting domains" />

    <p>
      You should see a <b>Nameservers</b> option, click <b>Change</b> and add your Netlify nameservers here (choose manual if it's set to automatic). Finally click <b>Save Name Servers</b>. Now you have to wait until they propagate through the internet.
    </p>

    <img src="/images/jamstack-jedi/netlify--nameservers.png" class="jamstack--img" alt="Netlify Nameservers" />

    <p id="anchor--https">
      Propagation can take anywhere from a few min, to 24 hours. If after 24 hours, your website is not resolving, I would suggest contacting both your hosting company and the domain registrar's support teams.
    </p>

    <p>
      Once your site propagates and you can pull it up with your domain, you should click the <b>Enable HTTPS</b> button in Netlify (under <b>Settings > Domain Management > HTTPS</b>) to get your free SSL Cert applied.
    </p>

    <p className="tldr">
      I've deployed our site to a sub-domain, since our portfolio sits on our main domain, you can view it at <a href="https://coming-soon.nerdswithcharisma.com/" target="_blank">https://coming-soon.nerdswithcharisma.com/</a>.
    </p>

    <hr />

    <p>
      Now, for the coolest part of Netlify. If we make a change, and push to our git repo, everything should begin building and deploy automatically.
    </p>

    <p className="tldr">
      To confirm everything is working as expected, lets open up our coming soon project again. Lets follow our Git workflow and perform:
    </p>

    <Markdown
      rel="Terminal"
      language="terminal"
      md={
        `  git pull`
      }
    />

    <p>
      Now lets open up index.html and make a change. Lets change the email us button to say:
    </p>

    <Markdown
      rel="index.html"
      language="html"
      md={
        `  <a href="mailto:briandausman@gmail.com?subject=Nerds%20With%20Charisma%20Inquiry">
    Send Us An Email
  </a>`
      }
    />

    <p>
      Lets stage our changes:
    </p>

    <Markdown
      rel="Terminal"
      language="terminal"
      md={
        `  git add .`
      }
    />

    <p>
      Commit them:
    </p>

    <Markdown
      rel="Terminal"
      language="terminal"
      md={
        `  git commit -m "update email button text"`
      }
    />

    <p>
      And finally, push them to GitLab:
    </p>

    <Markdown
      rel="Terminal"
      language="terminal"
      md={
        `  git push`
      }
    />

    <p>
      If you navigate to Netlify's admin, and choose <b>Deploys</b>, you should see a new deployment:
    </p>

    <img src="/images/jamstack-jedi/netlify--second-deploy.png" class="jamstack--img" alt="Netlify Second Deploy" />

    <p>
      And if you refresh our site, you should see the change reflected almost instantly!
    </p>

    <img src="/images/jamstack-jedi/netlify--second-deploy-site.png" class="jamstack--img" alt="Netlify Second Deploy Site" />

    <p className="tldr">
      Feel free to leave your site as is, or make more changes and push them out via the Git workflow. We have one more thing to cover before wrapping up our coming soon page, and that is Javascript.
    </p>

    <AniLink to="jamstack-jedi/javascript-101" className="nextBtn">Javascript 101 &raquo;</AniLink>
    <br />
  </Layout>
);

export default HostingNetlify;
