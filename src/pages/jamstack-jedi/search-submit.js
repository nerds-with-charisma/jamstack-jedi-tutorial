import React from 'react';
import AniLink from '../../components/Common/AniLink';

import Layout from '../../components/layout';

const SearchSubmit = () => (
  <Layout
    title="Search Engines"
    author={{ gravatar: '//0.gravatar.com/avatar/b5eff8d62adf3ff34e67f153781d27c4' }}
    gitRepo={null}
    time={0}
    description="Submit your website to search engines"
    keywords="Search engine submission, submit to yahoo, submit to bing, submit to duckduckgo"
  >
  <strong>Submit!</strong>
  <br />
  <br />

  <p>
    Most of our traffic will come from Google, but it doesn't hurt to submit our site to other websites like Bing, Yahoo, and DuckDuckGo.
  </p>

  <p>
    <b>DuckDuckGo &amp; Yahoo</b>
    <br />
    Fortunately, DuckDuckGo &amp; Yahoo will automatically index your site once it's submit to Google and Bing. So we don't have to do anything for them!
  </p>

  <hr />

  <p>
    <b id="anchor--bing">Bing</b>
    <br />
    To submit our site to Bing, which is Microsoft's search engine, we need to signup for the <b>Bing Webmaster Tools</b> which is free, just like GSC.
  </p>

  <p>
    Head to the <a id="anchor--bing-webmaster" href="https://www.bing.com/toolbox/webmaster" target="_blank">BWT login page</a> and sign in, you can use your existing Microsoft or Google account to sign in.
  </p>

  <p>
    Once you're signed in, you'll see an option at the top to import your site from Google Search Console. Since we've already setup GSC, lets do that. Click <b>Import</b> then <b>Continue to Search Console</b>. You will be asked to sign in to your Google account again.
  </p>

  <p>
    When the auth screen comes up, choose <b>Allow</b>. Check the domain you want to import if it isn't already, and click <b>Import</b>, click <b>Done</b> once it's successful.
  </p>

  <p>
    That's it for Bing, feel free to modify any of the imported content, but we should be good with what we had in GSC.
  </p>

  <hr />

  <p>
    Now that our site is submit to all the major search engines, within a few days we should start to see our site show up when we search by our company name.
  </p>

  <p>
    You can also see where your website ranks by keyword with <a id="anchor--serp-ranker" href="https://smallseotools.com/keyword-position/" target="_blank">Small SEO Tools</a> search ranking checker.
  </p>

  <p>
    That's all there is to submitting our site to search engines. These last few chapters have been pretty easy (but pretty important). In the last chapter of the SEO Part 2 chapter we'll cover a more complex task with "schema markup", and how it will help our site.
  </p>

  <AniLink to="jamstack-jedi/schema-markup" className="nextBtn">Schema Markup &raquo;</AniLink>
  <br />
  </Layout>
);

export default SearchSubmit;
