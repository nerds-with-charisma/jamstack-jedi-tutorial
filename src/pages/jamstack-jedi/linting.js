import React from 'react';
import AniLink from '../../components/Common/AniLink';

import Layout from '../../components/layout';
import Markdown from '../../components/Common/markdown';

const Linting = () => (
  <Layout
    title="Linting"
    author={{ gravatar: '//0.gravatar.com/avatar/b5eff8d62adf3ff34e67f153781d27c4' }}
    gitRepo="https://gitlab.com/nerds-with-charisma/jamstack-jedi/tree/master/linting"
    time={45}
    description="Use linting via esLint and AirB&B to keep your code clean and tidy"
    keywords="esLint GatsbyJs, AirBnb GatsbyJs, Linting GatsbyJs"
  >
    <strong>The Lint Roller</strong>
    <br />
    <br />

    <p>
      Linting...or keeping your code clean, concise, and consistent is a really important part of being a developer, in my opinion. It prevents errors, keeps a certain standard to the code you'll maintain, and makes sure all devs on a team follow a similar style.
    </p>

    <p>
      We probably should have done this to start, because now that we've coded a LOT of code, we'll have a LOT of errors. But linting is not required, and it's tedious, and it's not something I wanted to introduce to start this project.
    </p>

    <p>
      The AirBnB standard is, again in my opinion, the best standard for linting, it makes the most sense to me. It's simply a set of rules that the AirBnB team deemed the "best" way of developing in-house and shared it with the world. You can <a id="anchor--airbnb-standard" href="https://github.com/airbnb/javascript" target="_blank">read all about the standards here</a>.
    </p>

    <p>
      Similar to other plugins we've worked with, there is also an esLint <a id="anchor--eslint-plugin" href="https://www.gatsbyjs.org/packages/gatsby-plugin-eslint/" target="_blank">Gatsby plugin</a> that we can use that will run with every compile, so we'll install all the deps for that and we'll get started fixing (a lot of) errors.
    </p>

    <p>
      First we need to install our deps:
    </p>

    <Markdown
      rel="Terminal"
      language="terminal"
      md={
      `  npm install --save-dev gatsby-plugin-eslint

  npm install --save-dev eslint eslint-loader

  npm install --save-dev babel-eslint eslint-plugin-import eslint-plugin-react

  npm install --save-dev eslint-config-airbnb eslint-plugin-import eslint-plugin-jsx-a11y eslint-plugin-react

  > .eslintrc`}
    />

    <p>
      The eslintrc file is a configuration file that will tell esLint what we want to do as far as rules, and files, and what not.
    </p>

    <p>
      Now that we have everything bootstrap'd we need to tell Gatsby to use this plugin, so head over to <strong>gatsby-config.js</strong> and add the following to the plugins section:
    </p>

    <Markdown
      rel="gatsby-config.js"
      language="javascript"
      md={
      `  'gatsby-plugin-eslint',`}
    />

    <p>
      Next open up the <strong>.eslintrc</strong> file and lets add our config:
    </p>

    <Markdown
      rel=".eslintrc"
      language="javascript"
      md={
      `  {
    "parser": "babel-eslint",
    "plugins": [
      "react"
    ],
    "env": {
      "browser": true,
      "node": true
    },
    "rules": {
      "strict": 0,
      "no-console": 0,
      "react/forbid-prop-types": 0,
      "react/jsx-props-no-spreading": 0,
      "react/jsx-uses-vars": 2,
      "react/prefer-es6-class": [2, "always"],
      "react/jsx-filename-extension": [1, { "extensions": [".js", ".jsx"] }],
      "react/jsx-indent": [2, 2],
      "react/jsx-key": 2,
      "no-unused-vars": 2,
      "react/jsx-curly-brace-presence": 0,
      "react/no-danger": 0
    },
    "extends": "airbnb"
  }`}
    />

    <p>
      We tell the linter we want to use babel to interpret our fancy ES6 code, and that we're going to be using React. We put browser and node in the env node because we will prevent some errors that would pop up otherwise.
    </p>

    <p>
      At the bottom we have extends "airbnb" because we want to use their standards. Above that we have our custom rules, which are just my personal preference. Feel free to look them up and modify how you see fit.
    </p>

    <p>
      Finally, we start up our server and we're going to get a lot of compile errors, so buckle up and lets get fixing. Once you run <strong>gatsby develop</strong> you're going to get a lot of errors in a lot of pages, I find the best way to handle it is to start at the bottom and fix each file one by one, save after you've fixed everything in that file and let it re-run. Just keep working your way up until you're done.
    </p>

    <p>
      We'll walk through a few files to help you get the gist, then we'll link to our git repo with all the changes we made. I would suggest you handle everything on your own if you can, but if you get stuck you can reference the repo to see what did to fix it.
    </p>

    <p>
      The first page we'll clean up is our <strong>index.js</strong> file, we see there's about 36 errors, so lets fix those. Save after each so that we can get updated line numbers. The first says "pageContext" is missing from our props validate, well that's because we didn't add props validation to this file:
    </p>

    <Markdown
      rel="index.js"
      language="javascript"
      md={
      `  ...
  import { PropTypes } from 'prop-types';

  ...
  IndexPage.propTypes = {
    pageContext: PropTypes.object.isRequired,
  };

  export default IndexPage;`}
    />

    <p>
      If you notice, it gives a line number right before the validation, looking at the next issue, "18:11  error There should be no space after {'\'{\''}". This tells us that there's something wrong with line 18, if we look at it, we've added an extra space in there that, according to our linting standards, shouldn't be there.
    </p>

    <Markdown
    rel="index.js"
    language="javascript"
    md={
    `  ...
  query={graphql\`
    ...`}
  />

  <p>
    Next up, line 106 is too long, which we can easily fix by making it a curly braced function instead of one line:
  </p>

  <Markdown
    rel="index.js"
    language="javascript"
    md={
    `  ...
  if (pageContext.currentProjectToOpen) {
    setCurrentProjectToOpen(pageContext.currentProjectToOpen);
  }
    ...`}
  />

  <p>
    The next few lines (114-117) are from our geolocation funciton, we missed some parens, semis, and commas, so lets add those and clean up a handful of issues:
  </p>

  <Markdown
    rel="index.js"
    language="javascript"
    md={
    `  ...
  const geolocation = new Promise((resolve, reject) => {
    navigator.geolocation.getCurrentPosition(
      (position) => { resolve(position.coords); },
      (error) => { reject(error); },
    );
  }).catch((error) => error);
    ...`}
  />

  <p>
    Next we have a really good catch by the linter, we've used the same variable name in the same scope, which could result in an error or unexpected results if we're not careful. Lets update "data" to something more specific for this result like "geo" and clean up the next few linting issues related to these lines:
  </p>

  <Markdown
    rel="index.js"
    language="javascript"
    md={
    `  ...
  geolocation.then((geo) => {
    ...
    const geocoder = new window.google.maps.Geocoder();
    geocoder.geocode({
      location: {
        lat: geo.latitude,
        lng: geo.longitude,
      },
    }, (results, status) => {
  ...`}
  />

  <p>
    Next we find another variable naming issue, we've already used "city" for our state var, so we need to use something more specific on line 130, we'll also make it return right away to appease the linter:
  </p>

  <Markdown
    rel="index.js"
    language="javascript"
    md={
    `  ...
    const cityFound = results[0].address_components.filter((obj) => obj.types.includes('locality'))[0].long_name;
    setCity(cityFound);
  ...`}
  />

  <p>
    Jump down to 142 and it says that the "hasShown" var never changes so we should be using const instead of let, also we have too many spaces after our semi:
  </p>

  <Markdown
    rel="index.js"
    language="javascript"
    md={
    `  ...
  const hasShown = document.cookie.match(new RegExp('(^| )showLeavingMessage=([^;]+)')); // set a flag by checking for cookie named "showLeavingMessage"
  ...`}
  />

  <p>
    We have the same spaces issue on the new 142 and 150:
  </p>

  <Markdown
    rel="index.js"
    language="javascript"
    md={
    `  ...
  const isMac = (window.navigator.platform === 'MacIntel'); // check if it's a mac, if not assume windows
  ...
  console.log('woah mrs osx, don\'t leave'); // do your functionality here
  ...`}
  />

  <p>
    151 says we used double quotes, when strings should use single quotes:
  </p>

  <Markdown
    rel="index.js"
    language="javascript"
    md={
    `  ...
  document.cookie = 'showLeavingMessage=false'; // trigger our flag so next time it won't bother the user
  ...`}
  />

  <p>
    We're down to single digit issues in this file now, 154 is too long, we can just remove or shorten our comment. 156 is also using double quotes, so lets fix those two:
  </p>

  <Markdown
    rel="index.js"
    language="javascript"
    md={
    `  ...
  if (isMac !== true && (mouseX < window.innerWidth - 30) && mouseY < 30) { // windows
    console.log('easy mr windows, don\\'t leave'); // do your functionality here
    document.cookie = 'showLeavingMessage=false'; // trigger our flag so next time it won't bother the user
  }
  ...`}
  />

  <p>
    159, we missed an indent, so add a tab or 2 spaces here to fix that. This example shows how linting can keep your code nice and clean, you can't incorrectly indent even!
  </p>

  <Markdown
    rel="index.js"
    language="javascript"
    md={
    `  ...
  });
  ...`}
  />

  <p>
    Lines 171 and 174 are too long, we can fix this by putting our props on their own lines:
  </p>

  <Markdown
    rel="index.js"
    language="javascript"
    md={
    `  ...
  <Header
    title={data.site.siteMetadata.title}
    navigation={data.site.siteMetadata.navigation}
  />
  ...
  <Portfolio
    currentProjectToOpen={currentProjectToOpen}
    portfolioData={data.site.siteMetadata.portfolioData}
  />
  ...`}
  />

  <p>
    The next few issues on line 190 are indenting related so we can quickly fix those:
  </p>

  <Markdown
    rel="index.js"
    language="javascript"
    md={
    `  ...
          </Layout>
        );
      }
    }
  />
);
  ...`}
  />

  <p>
    And just like that, this file is clean!
  </p>

  <hr />

  <p>
    From here on out you should fix the issues on your own, we'll discuss a few special cases like in <strong>schema-markup.js</strong>. There's lots of errors in this file, but we don't need to correct these as they're a special case of the required syntax for the schema markup, so we can ignore this file completely. There's a few ways we could do this, the easiest being adding a comment to the top of the file telling the linter to ignore this file:
  </p>

  <Markdown
    rel="schema-markup.js"
    language="javascript"
    md={
    `  /* eslint-disable */
  const context = "http://schema.org";
  ...`}
  />

  <p>
    Next we see an error about using array indexing in a map loop in <strong>service-skills.js</strong>, this one is kind of a tricky one. To use simply "i" as a key is bad, but we're using the index to make a unique key based on the item plus the index. Incase 2 items have the same name.
  </p>

  <p>
    At this point, you have 2 options, you can remove the index and hope the keys with the item as the key don't ever match up or remove the validation from <strong>.eslintrc</strong>. We're going to assume that the keys will always be different and if not we'll only see a warning.
  </p>

  <Markdown
    rel="services-skilss.js"
    language="javascript"
    md={
    `  ...
  { services.map((service) => (
    <div key={\`\${service}\`} className="col-12 col-md-6 col-lg-4 lh-md">
      <strong>
        {service.title}
      </strong>

      <hr className="shorty opacity5" />

      { service.items.map((item) => (
        <React.Fragment key={\`\${item}\`}>
          {item}
          <br />
        </React.Fragment>
      ))}
    </div>
  ))}
  ...`}
  />

  <hr />

  <p>
    Take the time to go through each file and fix any issues, this shouldn't take much more than a half hour. Once completed make sure all functionality is still working by running through your app again (maybe another chapter on Jest testing should be in our future?).
  </p>

  <p>
    If you get stuck at all, you can check <a href="https://gitlab.com/nerds-with-charisma/jamstack-jedi/tree/master/linting" target="_blank">our repo</a> to compare your code vs our fixes.
  </p>

  <p>
    At this point, we're effectively done coding! We've created a complete portfolio covering everything we need to from the layout and styling, frameworks, seo, linting, and a whole lot. This was a long process and hopefully you're satisfied with the result.
  </p>

  <p>
    If you find any issues or have any comments, go ahead and send us an email. There may also be extra chapters added in the future but as of right now, I think we're in a pretty good place.
  </p>

  <p>
    Hopefully you enjoyed this and you learned something. The most important things to do when learning how to become a good developer is to code...code, code, code. And analyize, figure out the business use-cases for why and how you do something. You don't have to know all the syntaxical non-sense so long as you have the problem solving skills to know how and why you'd do it.
  </p>

  <p>
    Commit your files and watch as your website builds and deploys your final portfolio! Don't forget to watch your SEO, check your heatmaps, and run your A/B tests to constently be improving things.
  </p>

  <p>
    That wraps up everything we wanted to cover but we have one more bonus chapter! What if you wanted to use a service like <a href="https://www.contentful.com/" target="_blank">Contentful</a> to delivery your content? We can totally do that...head on to the LAST chapter to find out how.
  </p>

  <AniLink to="jamstack-jedi/contentful" className="nextBtn">Dynamic content from Contentful &raquo;</AniLink>
    <br />
  </Layout>
);

export default Linting;
