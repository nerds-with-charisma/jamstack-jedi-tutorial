import React from 'react';
import AniLink from '../../components/Common/AniLink';

import Layout from '../../components/layout';
import Markdown from '../../components/Common/markdown';

const Contentful = () => (
  <Layout
    title="Contentful"
    author={{ gravatar: '//0.gravatar.com/avatar/b5eff8d62adf3ff34e67f153781d27c4' }}
    gitRepo="https://gitlab.com/nerds-with-charisma/jamstack-jedi/tree/master/contentful"
    time={45}
    description="Get data from Contentful for GatsbyJs. Dynamic data from a headless CMS."
    keywords="Contentful, GatsbyJs, GatsbyJs Headless CMS, GatsbyJs contentful"
  >
    <p>
      Contentful is a great service that offers an excellent free plan to function as a headless CMS. This allows publishers who don't know code to add content then publish it. As devs we can then consume it and render it dynamically.
    </p>

    <p>
      Go ahead and <a href="https://www.contentful.com/sign-up/" target="_blank">sign-up</a> for a Contentful account and follow the wizard to get used to the admin dashboard. Once you're all set, lets create our first space by clicking <b>Create Space</b>, choose the <b>Free</b> option.
    </p>

    <img src="/images/jamstack-jedi/contentful--free.png" class="jamstack--img" alt="Contentful Free Tier" />

    <p>
      Name the space and choose <b>Create an Empty Space</b>. <b>Confirm</b> your space and you'll be taken to the <b>Space home</b>.
    </p>

    <p>
      We've got a space, now we need to determine the structure of our content that we'll be pulling in. Hint, this will look very similar to our portfolioData nodes in <b>gatsby-config.js</b> file. From the nav at the top, choose <b>Content Model</b> then <b>Add content type</b>.
    </p>

    <p>
      Name the content type, we'll be using contentful to pull in our portfolio data, so we'll call this <b>Portfolio</b>, Contentful will auto-populate our <b>Api Identifier</b> as a url friendly value, and you can add a <b>Description</b> if you'd like.
    </p>

    <p>
      We now need to define our portfolio model. If we open up <b>gatsby-config.js</b> and scroll down to our <b>portfolioData</b> node, we see the structure we need to mirror:
    </p>

    <Markdown
      rel="Portfolio JSON Structure"
      language="javascript"
      md={
      `  {
      about: string
      src: string/image
      alt: string
      type: string
      images: [string/image]
      id: id
      website: string
      hero: string/image
      tech: string
      launchDate: string
      browser: string/image,
      sort: int
  },`}
    />

    <p>
      We can choose to either host our images on Contentful or use a string to represent where our images are stored on our website. Since we've already got our images on our server, we'll go with strings. But image hosting on Contentful has some advantages, mainly business users could upload data themselves instead of asking you to do it. But this is our website, so we should be ok to upload our own photos to our own server.
    </p>

    <p>
      Jump back over to Contentful and click <b>Add Field</b> button from our Portfolio content model page. First we need to do <b>rich text</b>, as we want to have our about section present HTML, so choose that. Call it <b>about</b> and hit <b>Create</b>.
    </p>

    <p>
      Do the same for every node in our object: src = text, alt = text, type = text, images = text (check "list"), id will automatically be created for us, website = text, hero = text, tech = text, launchDate = text, browser = text, sort = number (select integer).
    </p>

    <img src="/images/jamstack-jedi/contentful--model.png" class="jamstack--img" alt="Contentful Model" />

    <p>
      Click <b>Save</b>. Now that our model is created, we need to add content, which we'll be pulling directly from our <b>gatsby-config.js</b> file. Open that up and lets get copy/pasting. Head to <b>Content</b> in Contentful, then <b>Add Portfolio</b>. For the "images" node, remove the quotes before you save.
    </p>

    <img src="/images/jamstack-jedi/contentful--copy.png" class="jamstack--img" alt="Contentful Content Generation" />

    <p>
      Do this for each of our items in portfolioData, hitting <b>Publish</b> after you've filled out each.
    </p>

    <p>
      Once you're done, we need to tell Gatsby we're going to pull in our data from Contentful. In order to do that we need an access token from Contentful to get permission to use the api.
    </p>

    <p>
      By default, Contentful has created an example api key for us, you can find it by navigating to <b>Settings > API Keys</b>. Click on the example api and feel free to rename it, and then keep this page up for later.
    </p>

    <p>
      Head back to our code and we need to install some deps that will allow Gatsby to pull in our data from contentful each build using the <a id="anchor--gatsby-contentful" href="https://www.gatsbyjs.org/packages/gatsby-source-contentful/" target="_blank">Gatsby Contentful Plugin</a>:
    </p>

    <Markdown
      rel="terminal"
      language="terminal"
      md={
      `  npm install --save gatsby-source-contentful`}
    />

    <p>
      Next we need to tell Gatsby about our API key, but we don't want to just pass this as a string in our Javascript, so we'll use an <b>.env</b> file to do this. If we put it directly into our Javascript anyone could see it and we'd be checking it into our GitLab, which isn't ideal.
    </p>

    <p>
      To do that, we need to install another package and create the <b>.env</b> file:
    </p>

    <Markdown
      rel="terminal"
      language="terminal"
      md={
      `  npm install --save dotenv

  > .env`}
    />

    <p>
      Open up the .env file and add two lines for our space id and access token that we got from Contentful:
    </p>

    <Markdown
      rel=".env"
      language="javascript"
      md={
      `  CONTENTFUL_SPACE_ID=xxxxxxxxxx
  CONTENTFUL_DELIVERY_ACCESS_TOKEN=yyyyyyyyy`}
    />

    <p>
      Next we need to setup the plugin in <b>gatsby-config.js</b> in the plugins node, at the top import the dotenv package then add the following to our plugins config:
    </p>

    <Markdown
      rel="gatsby-config.js"
      language="javascript"
      md={
      `  require('dotenv').config({ path: '.env' });
  ...
  {
    resolve: \`gatsby-source-contentful\`,
    options: {
      spaceId: process.env.CONTENTFUL_SPACE_ID,
      accessToken: process.env.CONTENTFUL_DELIVERY_ACCESS_TOKEN,
    },
  },
  ...`}
    />

    <p>
      <b>NOTE:</b> If you get a build error, see the note at the bottom of this page.
    </p>

    <p>
      This will pull our keys from the .env file, which we won't check in because it's already in our <b>.gitignore</b> file. We'll also configure <b>Netlify</b> to use these variables server side later.
    </p>

    <p>
      Now we want to pull in our data from Contentful (with a fallback to gatsby-config.js). We'll do this in <b>index.js</b> since everything's already setup for us, we can pull it in by adding another query above our site query:
    </p>

    <Markdown
      rel="index.js"
      language="javascript"
      md={
      `  allContentfulPortfolio(sort: {fields: sort}) {
    edges {
      node {
        alt
          src
          type
          website
          about {
            content {
              content {
                value
              }
            }
          }
          images
          hero
          tech
          launchDate
          sort
          browser
      }
    }
  }

  site {
  ...

  <Portfolio
    currentProjectToOpen={currentProjectToOpen}
    portfolioData={data.site.siteMetadata.portfolioData}
    dynamicPortfolioData={data.allContentfulPortfolio.edges}
  />
  ...`}
    />

    <p>
      Now flip over to <b>Portfolio.js</b> and we'll do a few things, first we'll declair our new prop, then we'll render both our dynamic and gatsby-config data:
    </p>

    <Markdown
      rel="Portfolio.js"
      language="javascript"
      md={
      `  ...
  const Portfolio = ({ dynamicPortfolioData, portfolioData, currentProjectToOpen }) => {
  ...
  <div className="masonry">
    { dynamicPortfolioData.map((item) => (
      <PortfolioItem key={item.node.alt} item={item.node} setItem={setItem} />
    ))}

    { portfolioData.portfolioData.sort((a, b) => a.sort < b.sort).map((item) => (
      <PortfolioItem key={item.alt} item={item} setItem={setItem} />
    ))}
  </div>
  ...`}
    />

    <p>
      Now our portfolio data should be doubling up. We'll add a flag inside of <b>gatsby-config.js</b> to tell our portfolio if we should display the data from Contentful or gatsby-config:
    </p>

    <Markdown
      rel="gatsby-config.js"
      language="javascript"
      md={
      `  ...
  siteMetadata: {
    useContentful: true,
  ...`}
    />

    <p>
      Pull that in to <b>index.js</b> and pass it down:
    </p>

    <Markdown
      rel="index.js"
      language="javascript"
      md={
      `  ...
  site {
    siteMetadata {
      useContentful
  ...
      <Portfolio
        currentProjectToOpen={currentProjectToOpen}
        portfolioData={data.site.siteMetadata.portfolioData}
        dynamicPortfolioData={data.allContentfulPortfolio.edges}
        useContentful={data.site.siteMetadata.useContentful}
      />
  ...`}
    />

    <p>
      Pull it into <b>Portfolio.js</b> then use it to determine which data we should show:
    </p>

    <Markdown
      rel="Portfolio.js"
      language="javascript"
      md={
      `  ...
  const Portfolio = ({
    dynamicPortfolioData,
    portfolioData,
    currentProjectToOpen,
    useContentful,
  }) => {
    ...
    { useContentful === true && dynamicPortfolioData.map((item) => (
      <PortfolioItem key={item.node.alt} item={item.node} setItem={setItem} />
    ))}

    { useContentful !== true
      && portfolioData.portfolioData.sort((a, b) => a.sort < b.sort).map((item) => (
        <PortfolioItem key={item.alt} item={item} setItem={setItem} />
      ))}
    ...
    useContentful: PropTypes.bool.isRequired,
  };

  export default Portfolio;
  ...`}
    />

  <p>
    Now, if you toggle the flag to false, you should see the data from gatsby-config. If you open up one of our portfolio items, you'll see one issue however. The about section is mapped differently in Contentful than what we were expecting. So we can add a check to render whichever node is found:
  </p>

  <Markdown
      rel="portoflio-single.js"
      language="javascript"
      md={
      `  ...
  <PortfolioWhat
    about={(item.about.content) ? item.about.content[0].content[0].value : item.about}
    tech={item.tech}
  />
  ...`}
    />

    <p>
      Now that everything is rendering correctly, we still need to do one thing before we commit and deploy our code. If we did that now, Netlify wouldn't be allowed to access our Contentful API. So we need to put our env variables into Netlify as well. Head over to your <b>Netlify Dashboard</b> and choose <b>Deploys > Deploy Settings</b>.
    </p>

    <p>
      Scroll down to <b>Environment</b> and click <b>Edit Variables</b>, add your Contentful key/value pairs here:
    </p>

    <img src="/images/jamstack-jedi/contentful--api-keys.png" class="jamstack--img" alt="Contentful Api Keys" />

    <p>
      Click <b>Save</b>, head back to your project and commit and deploy everything. Fingers crossed, all goes well and you're now pulling in data from Contentful!
    </p>

    <p>
      <b>Note:</b> if you have a problem and you're getting an error from Contentful, upload an image under <b>Media</b> for some reason we've experienced the Contentful API doesn't seem to initialize the first time until some form of media was uploaded.
    </p>

    <p id="anchor--contentful-webhooks">
      One final thing that might be handy for you, is that anytime you publish a change to <b>Contentful</b> we can have <b>Netlify</b> trigger a build so that our newest content is pulled in.  To do that, head back to our <b>Netlify Dash</b> and choose <b>Settings</b> from the top menu.
    </p>

    <p>
      Choose <b>Build &amp; Deploy</b>, and scroll down to <b>Build Hooks</b>. Click <b>Add Build Hook</b> and name it, we'll call it "NWC Auto-Build". Choose the branch you want to build, in our case it's <b>master</b>. Click <b>Save</b> and they will give you a web-hook url. Copy this, and head back to <b>Contentful's Admin</b>. From the dropdown at the top, choose <b>Settings > Webhooks</b>. On the right hand side, they have some pre-defined templates we can use, we want the <b>Netlify</b> webhook, so choose <b>Add</b> next to Netlify. Paste the web hook url into the input and click <b>Create webhook</b>. Head back to your project, make a change anywhere and go through our git commit process to trigger another build and make sure our contentful code gets to prod.
    </p>

    <p>
      Once in prod, lets test our new web hook. Make a change to one of your portfolio items, and publish the change. Once published, you should see a new deployment kick off, if you have Slack notifications setup. Pretty sweet! Once the build is done, whatever change you made should be auto deployed to prodcution. Totally awesome!
    </p>

    <p>
      That's it. We've reached the end! Congrats on making it all the way through, hopefully you learned something and are now able to apply that to your own project. If you find any errors or have any questions, feel free to <a href="mailto:briandausman@gmail.com?subject=Jamstack Jedi Inquiry">shoot us an email</a>. Thanks for sticking with us and I hope you enjoyed this series.
    </p>

    <br />
  </Layout>
);

export default Contentful;
