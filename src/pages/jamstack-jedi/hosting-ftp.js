import React from 'react';

import AniLink from '../../components/Common/AniLink';
import Layout from '../../components/layout';
import Markdown from '../../components/Common/markdown';

const HostingFTP = () => (
  <Layout
    title="Hosting: FTP"
    author={{ gravatar: '//0.gravatar.com/avatar/b5eff8d62adf3ff34e67f153781d27c4' }}
    gitRepo={null}
    time={10}
    description="How to host a website on FTP."
    keywords="FTP Hosting, hosting a site on ftp, Cyberduck, File Transfer Protocol"
  >
    <strong>FTP Hosting</strong>
    <br />
    <br />

    <p>
      <b>
        Note: Only go through this section if you are going to host your website on a shared hosting plan such as HostGator via FTP. If you are going to host with another service like Netlify (which is our suggestion) go to the <AniLink to="jamstack-jedi/hosting-netlify">Netlify</AniLink> chapter!
      </b>
    </p>

    <p className="tldr">
      If you choose to go with hosting from the same place you purchased the domain, then you won't have to set anything up. If you chose a service like HostGator or goDaddy, make sure the server's fit your needs. They're typically PHP servers, but you can configure or change the hosting type, but it will also change the price. In this tutorial we'll be building a standard HTML app with a framework called Gatsby. So it doesn't really matter where we host. There's no special language being used (Like PHP, .NET, JAVA, Ruby).
    </p>

    <p className="tldr">
      Since there's no domain name setup with these hosting solutions, we can get right into getting our coming soon page onto the server. If you ended up hosting somewhere other than the place you bought your domain you can read up on <a href="https://www.hostgator.com/help/article/what-are-my-name-servers" target="_blank">Nameservers</a> and come back here after you've setup your domain to point to your hosting server.
    </p>

    <p>
      To get your files up to your server so everyone can see them, you need to download and install an FTP client. I suggest <a id="anchor--cyberduck" href="https://cyberduck.io/" target="_blank">Cyber Duck</a>. Download and install the client and you'll be presented with the homescreen.
    </p>

    <p>
      Make sure you're on the "bookmark" tab, and choose the <b>+</b> plus icon at the bottom.
    </p>

    <img src="/images/jamstack-jedi/hosting--cyberduck.png" class="jamstack--img" alt="Cyber Duck bookmark" />

    <p>
      When you hit the plus icon, you'll have to input your credentials for your server:
    </p>

    <img src="/images/jamstack-jedi/hosting--ftp-login.png" class="jamstack--img" alt="Cyber Duck bookmark" />

    <p className="tldr">
      You should have received your FTP credentials when you signed up in an email. If you didn't or you lost them, you can find them by logging into your cPanel (Usually your domain.com/cpanel), and going to <b>FTP Accounts</b>. Scroll down to the FTP account you want to login with, and click <b>Configure FTP Client</b>. This will provide you with the username, server, and port you'll need to put into Cyber Duck.
    </p>

    <p className="tldr">
      You can also change your password at the <b>Change Password</b> link. Enter all that info into your FTP client, and double click the connection. If you entered your credentials correctly, you should be brought to your server's root directory.
    </p>

    <p>
      For some hosting providers, you need to enter into a specific folder to add your files. In HostGator's case, this is the <b>public_html</b> directory. Double click this to enter into the directory.
    </p>

    <p>
      Head back to our local project with our Coming Soon page, and drag &amp; drop your files into the public_html directory (Note: you might notice that I am in public_html/qa, this is because I already have hosting files on my site and do not want to overwrite them).
    </p>

    <p className="tldr">
      FTP is basically a remote folder structure, you can drag/drop, edit, delete, and move files on this remote directory. You can crate sub-folders as well that are accessible from the root. So for example, if you make a directory called "blog", and put files in that directory, you could access it from yourDomain.com/blog/
    </p>

    <img src="/images/jamstack-jedi/hosting--ftp-upload.png" class="jamstack--img" alt="Cyber Duck upload" />

    <p>
      Now, if we go to your website URL, you should see your files served to everyone on the web!
    </p>

    <img src="/images/jamstack-jedi/hosting--ftp-published.png" class="jamstack--img" alt="Published" />

    <p>
      One last thing to do before we finish up our FTP section is to enable a security certificate. If you're using HostGator this is very simple, as they give you a free SSL Certificate when you buy hosting and a domain from them.
    </p>

    <p>
      Head back over to your Cyber Duck and choose the option to see hidden files.
    </p>

    <img src="/images/jamstack-jedi/hosting--show-hidden-files.png" class="jamstack--img" alt="CyberDuck show hidden files" />

    <p>
      Lets create a new file via CyberDuck (you could also create this in the project on your machine, commit it to Git, and manually push it up. But since this file is not needed locally, and could potentially be overwritten, we'll just create it on the server).
    </p>

    <p>
      Go to <b>actions</b> > <b>New File</b> and name this new file <b>.htaccess</b>.
    </p>

    <img src="/images/jamstack-jedi/hosting--htaccess.png" class="jamstack--img" alt="CyberDuck htaccess" />

    <p id="anchor--htaccess-https">
      Highlight this new file and choose <b>Edit</b>. This will open the file on your computer (most likely in VS Code, if it does not, change your default editor in CyberDuck's settings). Add the following to the <b>.htaccess</b> file:
    </p>

    <Markdown
      rel=".htaccess"
      language="terminal"
      md={
        `  # Always use https for secure connections
  # Replace 'www.example.com' with your domain name
  RewriteEngine On
  RewriteCond %{SERVER_PORT} 80
  RewriteRule ^(.*)$ https://www.example.com/$1 [R=301,L]`
      }
    />

    <p>
      Save the file, and head back to your website. Refresh the page, and you should be redirected to your fancy new secure website.
    </p>

    <img src="/images/jamstack-jedi/hosting--ssl.png" class="jamstack--img" alt="HostGator SSL" />

    <p className="tldr">
      That's it, you're all set! We're now done with the Front-End 101 portion of this tutorial. You've now got a coming soon page to let your users know that your portfolio will be coming soon!
    </p>

    <p>
      <b>Helpful Links:</b> <a id="anchor--hosting-godaddy-link" href="https://www.youtube.com/user/godaddy" target="_blank">goDaddy's Youtube channel</a>, <a id="anchor--hosting-hostgator-link" href="https://www.youtube.com/user/hostgator/videos" target="_blank">HostGator's Youtube channel</a>,
    </p>

    <AniLink to="jamstack-jedi/javascript-101" className="nextBtn">Javascript 101 &raquo;</AniLink>
    <br />
  </Layout>
);

export default HostingFTP;
