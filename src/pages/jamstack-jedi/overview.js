import React from 'react';

import AniLink from '../../components/Common/AniLink';
import Layout from '../../components/layout';

const Overview = () => {
  return (
    <Layout
      title="Overview"
      author={{ gravatar: '//0.gravatar.com/avatar/b5eff8d62adf3ff34e67f153781d27c4' }}
      gitRepo={null}
      time={6}
      description="Get the basic terminology on becoming a front-end developer and learn what technologies you'll learn."
      keywords="Learn ReactJs, Learn HTML, Learn Sass, Learn Native Javascript, Learn GatsbyJs, Learn Node, Learn NPM, Learn Git"
    >
      <strong>
        If You Build It...
      </strong>
      <br />
      <br />
      <p>
        At the end of all this, what will you have to show off? You'll have built your own portfolio to show off your work and grow your skillset, based off <i>our</i> portfolio website, <a href="https://nerdswithcharisma.com" target="_blank">nerdswithcharisma.com</a>. Before we build the actual portfolio, we'll build a "coming soon" page that you can throw up on your server while you're working on your full blown portfolio. This will help you build your basic understanding of HTML and CSS, laying the foundation to move on and build something more complex later on.
      </p>
      <p className="tldr">
        Why build a portfolio? It's simple, it's clean, it's easy to understand what's going on, and we can use a lot of modern tools and tricks. While you're building, feel free to follow along, but at the end of the day, make the portfolio yours. Experiment, change colors, change the layout, do whatever you need to to make it <b>YOUR</b> portfolio, not ours. You can think of what we're showing you as a template to be extended, expanded, and play around to make it unique.
      </p>
      <br />
      <strong>
        How We Stack Up?
      </strong>
      <br />
      <br />
      <p>
        We'll be using a lot of things, prepare for a word dump: HTML, CSS, Sass, Javascript, React, GatsbyJs, Node &amp; NPM, Netlify, esLint, and more.
      </p>

      <p className="tldr">
        HTML(5) - Will be our markup or our scaffolding. I like metaphors, so if you think of our portfolio as a house, then the HTML would be the foundation and the studs. It is the layout of our project and will determine where things go later on. We'll be using the most recent specification called HTML5 which brings some neat additions to the previous versions of HTML which came out in 2014.
      </p>

      <p className="tldr">
        CSS(3) - CSS stands for cascading style sheets, which are a list of "selectors" or tag/naming conventions that allow us to add styles to our website. For example, we can change colors, font-sizes, backgrounds, and so much more. It's the basis of how our site will be decorated visually. If the HTML is the skeleton structure of a house, css are the walls, paintings, curtains, and furniture that make it look unique.
      </p>

      <p className="tldr">
        Sass - Later we'll cover Sass, which is a CSS pre-processor. What's that?! A pre-processor lets us write something in one language (Sass) then compiles it down to another (CSS). Sass allows us to do everything CSS does then adds on top of it allowing us to use things like variables, loops, and mixins (we'll talk about all these things later). Sass is probably the most popular pre-processor but some others are Less and Stylus. If CSS is our design sense of our metaphorical house then Sass would be like hiring a professional designer to help us make decisions faster and easier.
      </p>

      <p className="tldr">
        Javascript - Javascript will allow us to add interactivity to our site. User clicks, keyboard inputs, showing/hiding elements on the page will all be handled by Javascript. In our house example, Javascript would be our doors, windows, smart hubs, lights, etc...anything that does something fancy will probably be done via JS.
      </p>

      <p className="tldr">
       React - React is a Javascript framework, meaning someone wrote a bunch of code to make our life easier so that we don't have to re-invent the wheel every time we want to code. React has a certain style to it, like Angular or Vue have their own style, called "syntax". This is just a fancy way of saying how the code is arranged and formed. React can be thought of as our neighbor coming over to help wire up a new light or fan in our house. We might be able to do it, but they've done this before so they bring some knowledge that will help make things easier, they also brought their own tools!
      </p>

      <p className="tldr">
       GatsbyJs - Gatsby is a React framework that takes the heavy lifting out of our setup. Gatsby will handle all the fancy stuff like page routing, building, server side rendering, and much much more. If React is our neighbor coming to help with the house, then Gatsby is a professional we hired to do the same thing. They make things go much faster.
      </p>

      <p className="tldr">
        Node &amp; NPM - Node is the server we'll be using (technically Gatsby sets this up) to serve our site locally. NPM (node package manager) is the dependancy manager for Node. A "dep" is simply a package of code that we can utilize that someone else wrote. We won't get too far down the rabbit hole on this one right now.
      </p>

      <p className="tldr">
        Hosting - We'll be using Netlify for our example hosting because it's awesome. There's other options, sure, and we'll cover very briefly standard FTP hosting. Hosting, going back to our house metaphor, can be thought of as our plot of land we build our house on and the street address for our house. Some hosting company (Netlify in our case) give us a partition on their server's hard drive with an address that we're able to connect to and put our code up there for the world to see. We then connect that to a domain name so that people can easily find us on the crowded world wide web.
      </p>

      <p className="tldr">
       esLint - esLint we'll get into later in the chapters, will be a way of keeping our code clean and standardized. They're the association for our house, we can't do anything they don't allow in the bilaws. esLint will yell at us if we make a syntax error, or make a variable that we don't use. Essentially it'll make sure our code is more error-proof. You might get annoyed with it at times, but trust me, it is 100% worth it.
      </p>

      <p className="tldr">
        Git &amp; GitLab - Git is a version control system, or an insurance policy on your house. Say you're renting out your house, there's people coming and going all the time. What if someone breaks something?! Git will be our log of who did what and when. If someone broke somethings, we can "show our insurance" and they'll replace the damaged item exactly how it was before. GitLab is the insurance company. We keep all our code with them in the event something happens, we can come crawling to them to fix it. You could do Git entirely on your computer, but what if your ACTUAL house burns down and your computer doesn't make it? You're out of luck then, so better to use a company outside of your physical home.
      </p>

      <p className="tldr">
        We'll also be tackling a lot of marketing and SEO stuff like: Social integrations, Google Analytics, Google My Business, Google Tag Manager, server configs, so on and so forth.
      </p>

      <p className="tldr">
        Additionally we'll cover some optimizations like: accessibility, page speed, conversion/signup, and UX audits.
      </p>

      <p className="tldr">
        Throughout I'll be supplying deep-dive recommendations such as videos and books. One resource for a lot of technical books is <a href="https://www.academia.edu">https://www.academia.edu</a>, be sure to check there, or your library if you don't want to drop any cash on book. I'll try to provide free resources whenever possible, but sometimes you get what you pay for. If all else fails, you can get used copies fairly cheap on Amazon.
      </p>

      <p>
        A few things to pay attention to, along with the TLDR icons at the top right we'll also include links to a GitLab <i className="fab fa-gitlab" /> repo so that you can compare your code to ours next to the TLDR button. You should hand code everything, in our opinion. It's the best way to learn, but feel free to reference and even copy really long bits of code that you feel comfortable with.
      </p>

      <p className="tldr">
        If all that sounds good, lets talk about how we'll be coding and what exactly a "code editor" is.
      </p>

      <AniLink to="jamstack-jedi/ide" className="nextBtn">
        Picking out and Setting Up Your Editor &raquo;
      </AniLink>
    </Layout>
  );
};

export default Overview;
