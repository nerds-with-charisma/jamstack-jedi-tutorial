import React from 'react';
import AniLink from '../../components/Common/AniLink';

import Layout from '../../components/layout';
import Markdown from '../../components/Common/markdown';

const Meta = () => (
  <Layout
    title="Metadata"
    author={{ gravatar: '//0.gravatar.com/avatar/b5eff8d62adf3ff34e67f153781d27c4' }}
    gitRepo="https://gitlab.com/nerds-with-charisma/jamstack-jedi/tree/master/seo-sitemap"
    time={12}
    description="GatsbyJs, create a custom Favicon, color mobile address bar, and setup open graph tags."
    keywords="Custom favicon.ico, favicon.ico, GatsbyJs favicon, mobile color address bar, theme-color address bar, open graph meta tags, open graph how to"
  >

  <strong>That's Meta</strong>

  <br />
  <br />

  <p>
    In this section, we're going to add to our <b>SEO</b> component. Right now we just have some basic meta data, but there's lots more we can add that will be helpful to both users and bots.
  </p>

  <p>
    Gatsby by default already provides us a lot of the standard meta data such as the <a href="https://www.w3.org/International/questions/qa-html-language-declarations" target="_blank">language</a>, the <a href="https://www.w3schools.com/tags/att_meta_charset.asp" target="_blank">charset</a>, and the <a href="" target="_blank">viewport</a>.
  </p>

  <p>
    We've also specified a few common ones in our <b>gatsby-config.js</b> file like <b>title, keywords &amp; description</b>. But there's a few more we want to specify before proceeding.
  </p>

  <p>
    In the first part of this section we'll add a favicon, color our address bar on mobile, add a canonical tag, and finally add some <b>open Graph</b> tags.
  </p>

  <p>
    First the favicon, we created this in the <AniLink to="coming-soon-html">coming soon HTML</AniLink> portion, all we have to do is place that <a href="https://gitlab.com/nerds-with-charisma/jamstack-jedi/blob/master/coming-soon-css-final/favicon.ico" target="_blank">favicon.ico</a> in our <b>static</b> directory. Once there, Gatsby will know to automatically use it.
  </p>

  <img src="/images/jamstack-jedi/meta--favicon.png" class="jamstack--img" alt="Favicon" />

  <hr />

  <p>
    Next, you may have noticed that recently some websites have colored their address bars to match their brand. Open up your phone and head over to a site like <b>target.com</b> and you'll notice the address bar is the "target red".
  </p>

  <img src="/images/jamstack-jedi/meta--address-bar.png" class="jamstack--img" alt="Color Address Bar" />

  <p>
    Doing this neat little customization is actually super easy. We can add a meta tag to our head section with a color code. Open up <b>gatsby-config.js</b> and add another object below "keywords":
  </p>

  <Markdown
    rel="gatsby-config.js"
    language="javascript"
    md={`  ...
  meta: [
    {
      name: 'description',
      content: 'Nerds With Charisma online portfolio coming soon!',
    },
    {
      name: 'keywords',
      content: 'Brian Dausman, Nerds With Charisma',
    },
    {
      name: 'theme-color',
      content: '#9012fe',
    },
  ],
    ...`}
  />

  <p>
    Now refresh our page in a simulator like <b>browser stack</b>, <b>android studio</b>, or <b>X-Code</b> and you'll see the address bar is our custom color.
  </p>

  <img src="/images/jamstack-jedi/meta--custom-address.png" class="jamstack--img" alt="Color Address Bar" />

  <hr />

  <p>
    Next we want to add a <a id="anchor--canonical" href="https://moz.com/learn/seo/canonicalization" target="_blank">canonical</a> url meta tag. And, you guessed it, Gatsby has <a id="anchor--gatsby-canonical" href="https://www.gatsbyjs.org/packages/gatsby-plugin-canonical-urls/" target="_blank">a plugin</a> for this. So lets install and configure that:
  </p>

  <Markdown
    rel="Terminal"
    language="terminal"
    md={`  npm install --save gatsby-plugin-canonical-urls`}
  />

  <p>
    We need to configure it in <b>gatsby-config.js</b> and it's very similar to the way we did the Gatsby robots plugin configuration, so lets add our code just below that:
  </p>

  <Markdown
    rel="gatsby-config.js"
    language="javascript"
    md={`  ...
  {
    resolve: \`gatsby-plugin-canonical-urls\`,
    options: {
      siteUrl: \`https://nerdswithcharisma.com\`,
    },
  },
  ...`}
/>

<p>
  Fire up your server and lets take a look in the source to confirm everything is working.
</p>

<img src="/images/jamstack-jedi/seo--canonical.png" class="jamstack--img" alt="Canonical tag" />

<p>
  If we open up a portfolio item and refresh the page, we'll see it updates to our portfolio url.
</p>

<img src="/images/jamstack-jedi/seo--canonical-2.png" class="jamstack--img" alt="Canonical tag" />

<hr />

<p>
  The last thing we want to do in this meta chapter is add some <b>Open Graph</b> tags. Open Graph tags allow you to dictacte what your content will look like if someone shares your page on Facebook.
</p>

<p>
  You can <a id="anchor--open-graph" href="https://ogp.me/" target="_blank">read more about OG</a>, and decide which tags best suite your site. No matter which tags you choose, there are four that must be on the page and those are: title, type, image, and url.
</p>

<p>
  Lets add our OG to a node in <b>gatsby-config.js</b> just below our meta:
</p>

<Markdown
    rel="gatsby-config.js"
    language="javascript"
    md={`  ...
  og: {
    title: 'Nerds With Charisma',
    site_name: 'Nerds With Charisma',
    url: 'https://nerdswithcharisma.com',
    type: 'website',
    description: 'Nerds With Charisma is a cutting edge digital media agency specializing in awesome websites.',
    image: 'https://nerdswithcharisma.com/images/nwc-tile.jpg',
  },
  ...`}
/>

<p>
  Then we need to import it into our <b>index.js</b> query below our meta:
</p>

<Markdown
    rel="Index.js"
    language="javascript"
    md={`  ...
  og {
    title
    site_name
    url
    type
    description
    image
  }
  ...`}
/>

<p>
  Then pass it to our <b>SEO</b> component:
</p>

<Markdown
    rel="index.js"
    language="javascript"
    md={`  ...
  <SEO
    title={data.site.siteMetadata.homepageData.title}
    lang={data.site.siteMetadata.homepageData.lang}
    meta={data.site.siteMetadata.homepageData.meta}
    og={data.site.siteMetadata.homepageData.og}
  />
  ...`}
/>

<p>
  Then head into <b>SEO.js</b> and we'll loop through all our nodes in our object:
</p>

<Markdown
    rel="index.js"
    language="javascript"
    md={`  ...
  function SEO({ title, lang, meta, og }) {
  ...
  <Helmet
    htmlAttributes={{ lang }}
    title={title}
    meta={meta}
  >
    { Object.keys(og).map((key, value) => (
      <meta property={\`og:\${key}\`} content={og[key]} key={value} />
    ))}
  ...`}
/>

<p>
  Now, anytime we want to add an OG meta tag, we simply have to add it to <b>gatsby-config.js</b> and add it to our <b>index.js</b> query. It'll loop through and populate automatically.
</p>

<p>
  You can verify everything looks good with this <a id="anchor--og-plugin" href="https://chrome.google.com/webstore/detail/open-graph-preview/ehaigphokkgebnmdiicabhjhddkaekgh?hl=en" target="_blank">Chrome Plugin</a>, it's not perfect but it gives you a preview of what your listing will look like.
</p>

<p>
  That wraps up our first SEO section. We'll take a small break from SEO to jump into some optimizations and audits in the next part.
</p>

  <AniLink to="jamstack-jedi/optimize-overview" className="nextBtn">Lets Optimize &raquo;</AniLink>
  <br />
  </Layout>
);

export default Meta;
