import React from 'react';
import AniLink from '../../components/Common/AniLink';

import Layout from '../../components/layout';

import Markdown from '../../components/Common/markdown';
import YouTube from '../../components/Common/YouTube';

const StyleLibrary = () => (
  <Layout
    title="Style Library"
    author={{ gravatar: '//0.gravatar.com/avatar/b5eff8d62adf3ff34e67f153781d27c4' }}
    gitRepo="https://gitlab.com/nerds-with-charisma/jamstack-jedi/tree/master/style-library"
    time={20}
    description="How to make your own style library that can replace Bootstrap. A lightweight style library."
    keywords="Style Library, Replace Bootstrap, Lightweight Bootstrap, Bootstrap Grid"
  >
    <strong>Own Your Style</strong>
    <br />
    <br />

    <p>
      This chapter will be a fairly long one, and if you're comfortable with Sass and CSS and don't feel like creating your own style library, feel free to use it directly from our <a href="https://gitlab.com/nerds-with-charisma/nerds-style-sass/" target="_blank">NPM repo</a>. You can follow the instructions in the repo to install it and use it, however, it may change over time from what we have here.
    </p>

    <p>
      First lets talk a little bit about structure, our main entry point will be <b>main.scss</b>, then we will make individual scss files for each type of styling. For example, we will have a typography file to handle all text related styles.
    </p>

    <p>
      So lets start by making all the empty files we'll need:
    </p>

    <Markdown
      rel="Terminal"
      language="terminal"
      md={
        `  cd src/styles

  touch alerts.scss base.scss borders.scss buttons.scss colors.scss forms.scss grid.scss helpers.scss images.scss normalize.scss opacity.scss reset.scss tables.scss typography.scss variables.scss

  cd ../../`}
    />

    <p>
      We created lots of files here, but they're all pretty self explanitory. We'll cover what each is when we tackle it, I'll include inline comments for anything that needs farther clarification.
    </p>

    <p>
      There's a few important files that we need to fill out first, before we tackle the generic helpers specifically base, colors, and variables.
    </p>

    <p>
      The one file that all other files will depend on is <b>variables.scss</b> so lets start with that:
    </p>

    <Markdown
      rel="variables.scss"
      language="scss"
      md={
        `  $text: #484848;       /* standard text color */

  $dark: #000;          /* our actual black */
  $offDark: #2c2c2c;    /* our not actual black */
  $light: #fff;         /* our actual white */
  $offLight: #f7f7f7;   /* our not actual white */
  $grey: #d7d7d7;       /* grey, mostly borders */

  $primary: #9012FE;    /* our primary color */
  $primaryAlt: #C212FE; /* our alt primary color, accent */
  $cta: #F20028;        /* our secondary, or call to action color */
  $ctaAlt: #ff0084;     /* our alt cta color, accent */
  $success: #85DD33;    /* successful things */
  $successAlt: #d2ff52; /* our alt success color */
  $info: #4dd8ea;       /* info color */
  $infoAlt: #33d671;    /* info alt color */
  $error: #F20028;      /* error color */
  $errorAlt: #ff0084;   /* error alt color */
  $links: #f233b9;      /* our STANDARD link color */
  $linksHover: #02e2ca; /* our hover link color */

  /* put all our colors here, we'll use this for looping */
  $color_names: text, dark, offDark, light, offLight, grey, primary, primaryAlt, cta, ctaAlt, success, successAlt, info, infoAlt, error, errorAlt, links, linksHover;
  $color_vars: $text, $dark, $offDark, $light, $offLight, $grey, $primary, $primaryAlt, $cta, $ctaAlt, $success, $successAlt, $info, $infoAlt, $error, $errorAlt, $links, $linksHover;

  /* border radius */
  $borderRadiusXS: 3px;
  $borderRadiusSM: 5px;
  $borderRadiusMD: 10px;
  $borderRadiusLG: 300px;

  $gridColumns: 12;       /* how many cols show on desktop */
  $gridGap: 10px;         /* the space between our grid cols */`}
    />

    <p>
      If you want to change or customize any colors or default spacings, this would be the place to do it. If you notice we have a color_names and color_vars variable. We'll use these later to loop through our colors and create classes automatically based off these variables. If you add a new color, don't forget to add it here.
    </p>

    <hr />

    <p>
      Next we'll tackle the other important files, again comments inline, but you don't need to type those in your files:
    </p>

    <Markdown
      rel="base.scss"
      language="scss"
      md={
        `  /* set our base text styles for the generic body */
  body {
    font-family: 'Roboto', "system";
    letter-spacing: 0;
    font-style: normal;
    text-rendering: optimizeLegibility;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    color: $text;
    font-size: 20px;
    line-height: 1.4;
  }

  /* style our default links */
  a {
    text-decoration: none;
    color: $links;
    transition: all 0.4s linear; /* this will add a nice animation to our links */

    &:hover {
      color: $linksHover;
      opacity: 1;
    }
  }

  /* these two change the color when someone highlights text on the screen */
  ::selection {
    background: $primary; /* WebKit/Blink Browsers */
  }
  ::-moz-selection {
    background: $primary; /* Gecko Browsers */
  }`}
    />

    <p>
      Nothing special is going on here with the exception of using some of our variables from the previous file like <b>$text, $links, $linksHover</b>. We also make a <b>::selection</b> property, this will make the background of the highlighted area when a user clicks &amp; drags our primary color from variables.scss. Try highlighting this page and you'll see the text is highlighted in purple.
    </p>

    <hr />

    <Markdown
      rel="colors.scss"
      language="scss"
      md={
        `  @each $name in $color_names {
    $i: index($color_names, $name);

    /* backgrounds */
    .bg--#{$name} {
      background: nth($color_vars, $i);
    }

    /* fonts */
    .font--#{$name} {
      color: nth($color_vars, $i);
    }

    /* borders */
    .bordered--#{$name} {
      border-color: nth($color_vars, $i);
    }
  }`}
    />

    <p>
      This is our first complex SCSS file, we're performing an "each" loop, remember the <b>color_names</b> variables we created earlier? Now we're telling Sass to loop through each of those, we're creating 3 specific color helpers. One for backgrounds, one for fonts, and another for borders.
    </p>

    <p>
      We're going to create classes for each of those that will output a prefix followed by two dashes, then the color name we gave it in <b>variables.scss</b>. For example, we'll have an output of classes like <b>.bg--primary, .font--primary, .bordered--primary</b>. This will happen for each color in our loop.
    </p>

    <hr />

    <p>
      Now, to use these styles, we have to import them in our <b>main.scss</b> file:
    </p>

    <Markdown
      rel="main.scss"
      language="scss"
      md={
        `  @import 'variables.scss';

  @import 'base.scss';
  @import 'colors.scss';`}
    />

    <p>
      Now restart your server and we should see our styles being applied.
    </p>

    <img src="/images/jamstack-jedi/sass--main.png" class="jamstack--img" alt="Sass style imports" />

    <hr />

    <p>
      Next we'll start filling in each individual style file:
    </p>

    <p>
      Alerts are helpers for displaying errors or messaging to the user
    </p>

    <Markdown
      rel="alerts.scss"
      language="scss"
      md={
        `  /* a docked alerter up at the top right corner */
  /* use a custom html <alert> tag with one of the support classes */

  alert {
      z-index: 999;
      cursor: pointer;
      position: fixed;
      right: 15px;
      top: -150px;
      padding: 8px;
      border-radius: $borderRadiusLG;
      padding-left: 60px;
      overflow: hidden;
      color: #fff;
      background: red;
      box-shadow: 0 5px 15px rgba(0,0,0,0.2);
      transition: all 0.3s;

      &.active {
        top: 15px;
      }

      &:hover {
        box-shadow: 0 3px 8px rgba(0,0,0,0.3);
      }
  }

  alert.success {
    background: $success;
    background: linear-gradient(135deg, $successAlt 0%, $success 100%);
  }


  alert.info {
    background: $info;
    background: linear-gradient(135deg, $info 0%, $infoAlt 100%);
  }


  alert.error {
    background: $error;
    background: linear-gradient(135deg, $error 0%, $errorAlt 100%);
  }`}
    />

    <p>
     Borders have helpers for styling our borders like line thickness, corner radius, positioning of the borders and styles our horizontal rule:
    </p>

    <Markdown
      rel="borders.scss"
      language="scss"
      md={
        `  .bordered { border: 1px solid $grey; }
  .bordered--none { border: none; }

  .bordered---top, .bordered--bottom, .bordered--right, .bordered--left { border: none; }
  .bordered--top { border-top: 1px solid $grey; }
  .bordered--bottom { border-bottom: 1px solid $grey; }
  .bordered--right { border-right: 1px solid $grey; }
  .bordered--left { border-left: 1px solid $grey; }

  .bordered--xs { border-width: 2px; }
  .bordered--sm { border-width: 5px; }
  .bordered--md { border-width: 10px; }
  .bordered--lg { border-width: 25px; }

  .radius--xs { border-radius: 3px; }
  .radius--sm { border-radius: 5px; }
  .radius--md { border-radius: 10px; }
  .radius--lg { border-radius: 300px; }

  hr  {
    border: none;
    height: 0;
    border-bottom: 1px solid $grey;

    &.shorty {
      width: 60px;
    }
  }`}
    />

    <p>
      Buttons will style our base button look, we can combined a lot of these to make the perfect button for almost anything.
    </p>

    <p>
      At the end, you'll see we have another loop for our color_names, just like we did in our colors file.
    </p>

    <Markdown
      rel="buttons.scss"
      language="scss"
      md={
        `  button:focus,
  .btn {
    outline: none;
  }

  button,
  [type="submit"],
  .btn {
    border: 1px solid $grey;
    padding: 7px 20px;
    background: none;
    color: $light;
    transition: all 0.3s linear;
  }

  .round {
    border-radius: $borderRadiusLG;
  }

  .noBorder {
    border: 0;
  }

  .noShadow {
    text-shadow: none;
  }

  @each $name in $color_names {
    $i: index($color_names, $name);

    button.#{$name},
    [type="submit"].#{$name},
    .btn.#{$name} {
      border-color: nth($color_vars, $i);
      text-shadow: 0 1px 2px nth($color_vars, $i);
      background: nth($color_vars, $i);
      background: linear-gradient(
        135deg,
        nth($color_vars, $i) 0%,
        lighten(nth($color_vars, $i), 10%) 100%
      );
    }
  }`}
    />

    <p>
      Our forms are all pretty minimally styled, this will give them a nice clean look, almost like a reset:
    </p>

    <Markdown
      rel="forms.scss"
      language="scss"
      md={
        `  input[type=text], input[type=email], input[type=password], input[type=number], textarea {
    border: 1px solid $grey;
    padding: 6px;
    margin-bottom: 15px;
  }

  input.block, textarea.block {
    width: 99%;
  }

  label.radio, label.checkbox {
    overflow: hidden;
    position: relative;
    left: 0;
    top: 0;
    display: inline-block;
    padding-left: 30px;
    line-height: 15px;
    margin-right: 15px;
  }

  input[type="radio"], input[type="checkbox"] {
    position: absolute;
    left: -20px;
  }

  input[type="radio"]::before, input[type="checkbox"]::before {
    font-family: 'Font Awesome\ 5 Free';
    display: inline-block;
    position: absolute;
    width: 19px;
    height :19px;
    left: 30px;
    top: 10px;
    margin: -11px 10px 0 0;
    vertical-align: middle;
    content: '\f111';
    cursor: pointer;
    opacity: 0.7;
    font-size: 15px;
    color: $borderRadiusLG;
  }

  input[type="checkbox"]::before {
    content: '\f0c8';
  }

  .radio:hover input::before, .checkbox:hover input::before {
    color: $primary;
  }

  .radio input:checked::before {
    opacity: 1;
    color: $primary;
    content: '\f192';
  }

  .checkbox input:checked::before {
    opacity: 1;
    color: $primary;
    content: '\f14a';
  }`}
    />

    <p>
      Grid is next, and this is an important one, as it will be what we use to layout our page. It's very similar to the Bootstrap grid, in that it's 12 columns, but it's much smaller in size data wise, our styles are only a few KB. We'll go through the grid when we implement it, as well as how to stack it for mobile, tablet, and desktop.
    </p>

    <Markdown
      rel="grid.scss"
      language="scss"
      md={
        `  .container {
    width: 98%;
    margin: auto;
  }

  .container-xs {
    width: 25%;
    margin: auto;
  }

  .container-sm {
    width: 50%;
    margin: auto;
  }

  .container-md {
    width: 75%;
    margin: auto;
  }

  .grid {
    display: grid;
    grid-template-columns: repeat($gridColumns, 1fr);
    grid-gap: $gridGap;
  }

  /* base mobile grid */
  .col-1 { grid-column: auto / span 1; }
  .col-2 { grid-column: auto / span 2; }
  .col-3 { grid-column: auto / span 3; }
  .col-4 { grid-column: auto / span 4; }
  .col-5 { grid-column: auto / span 5; }
  .col-6 { grid-column: auto / span 6; }
  .col-7 { grid-column: auto / span 7; }
  .col-8 { grid-column: auto / span 8; }
  .col-9 { grid-column: auto / span 9; }
  .col-10 { grid-column: auto / span 10; }
  .col-11 { grid-column: auto / span 11; }
  .col-12 { grid-column: auto / span 12; }

  /* base tablet grid */
  @media only screen and (min-width: 768px) and (max-width: 1024px) {
    .col-md-1 { grid-column: auto / span 1; }
    .col-md-2 { grid-column: auto / span 2; }
    .col-md-3 { grid-column: auto / span 3; }
    .col-md-4 { grid-column: auto / span 4; }
    .col-md-5 { grid-column: auto / span 5; }
    .col-md-6 { grid-column: auto / span 6; }
    .col-md-7 { grid-column: auto / span 7; }
    .col-md-8 { grid-column: auto / span 8; }
    .col-md-9 { grid-column: auto / span 9; }
    .col-md-10 { grid-column: auto / span 10; }
    .col-md-11 { grid-column: auto / span 11; }
    .col-md-12 { grid-column: auto / span 12; }
  }

  /* base desktop grid */
  @media only screen and (min-width: 1025px) {
    .col-lg-1 { grid-column: auto / span 1; }
    .col-lg-2 { grid-column: auto / span 2; }
    .col-lg-3 { grid-column: auto / span 3; }
    .col-lg-4 { grid-column: auto / span 4; }
    .col-lg-5 { grid-column: auto / span 5; }
    .col-lg-6 { grid-column: auto / span 6; }
    .col-lg-7 { grid-column: auto / span 7; }
    .col-lg-8 { grid-column: auto / span 8; }
    .col-lg-9 { grid-column: auto / span 9; }
    .col-lg-10 { grid-column: auto / span 10; }
    .col-lg-11 { grid-column: auto / span 11; }
    .col-lg-12 { grid-column: auto / span 12; }
  }`}
    />

    <p>
      Next are our helpers, which are utility styles that really don't belong anywhere else.  They will help us with spacing on the page, positioning, as well as what should be shown on different devices.
    </p>

    <Markdown
      rel="helpers.scss"
      language="scss"
      md={
        `  /* animations */
  .transition--all {
      transition: all 0.2s;
  }

  /* padding */
  .padding-none { padding: 0; }
  .padding-xs { padding: 2px; }
  .padding-sm { padding: 5px; }
  .padding-md { padding: 15px; }
  .padding-lg { padding: 30px; }

  /* margins */
  .margin-auto { margin: auto; }
  .margin-none { margin: 0; }
  .margin-xs { margin-bottom: 2px; }
  .margin-sm { margin-bottom: 5px; }
  .margin-md { margin-bottom: 15px; }
  .margin-lg { margin-bottom: 30px; }

  /* positions */
  .position--relative { position: relative; }
  .position--absolute { position: absolute; }

  /* floats */
  .float-right{ float: right; }
  .float-left{ float: left; }
  .clear{ clear: both; }

  /* display */
  .block {
      display: block;
      width: 100%;
  }

  .no--wrap {
      white-space: nowrap;
  }

  .lh-xs { line-height: 0.5em; }
  .lh-sm { line-height: 1em; }
  .lh-md { line-height: 2em; }
  .lh-lg { line-height: 3em; }

  @media only screen and (max-width: 767px) {
      .hidden-xs { display: none; }
  }

  @media only screen and (min-width: 768px) and (max-width: 1023px) {
      .hidden-sm { display: none; }
  }

  @media only screen and (min-width: 1024px) and (max-width: 1299px) {
      .hidden-md { display: none; }
  }

  @media only screen and (min-width: 1300px) {
      .hidden-lg { display: none; }
  }

  .hide { display: none; }`}
    />

    <p>
      Images are next, not much going on here other than a responsive helper. This will make sure our images fit on smaller screens without running off the page.
    </p>

    <Markdown
      rel="images.scss"
      language="scss"
      md={
        `  .responsive { max-width: 100%; }`}
    />

    <p>
      Normalize is a standardized html reset to make all browsers play nice. You can read more about it <a id="anchor--css-normalize" href="https://github.com/necolas/normalize.css" target="_blank">here</a>, you could also include this as one of your deps, but since I use it in almost every project I pull it in locally. Incase you didn't notice, you can click "Copy" at the top right of any markdown box and it will copy the content to your clipboard, so you don't have to type something like this from scratch.
    </p>

    <Markdown
      rel="normalize.scss"
      language="scss"
      md={
        `  /*! normalize.css v8.0.0 | MIT License | github.com/necolas/normalize.css */

  /* Document
      ========================================================================== */

  /**
   * 1. Correct the line height in all browsers.
   * 2. Prevent adjustments of font size after orientation changes in iOS.
   */

    html {
      line-height: 1.15; /* 1 */
      -webkit-text-size-adjust: 100%; /* 2 */
    }

    /* Sections
        ========================================================================== */

    /**
     * Remove the margin in all browsers.
     */

    body {
      margin: 0;
    }

    /**
     * Correct the font size and margin on h1 elements within section and
     * 'article' contexts in Chrome, Firefox, and Safari.
     */

    h1 {
      font-size: 2em;
      margin: 0.67em 0;
    }

    /* Grouping content
        ========================================================================== */

    /**
     * 1. Add the correct box sizing in Firefox.
     * 2. Show the overflow in Edge and IE.
     */

    hr {
      box-sizing: content-box; /* 1 */
      height: 0; /* 1 */
      overflow: visible; /* 2 */
    }

    /**
     * 1. Correct the inheritance and scaling of font size in all browsers.
     * 2. Correct the odd em font sizing in all browsers.
     */

    pre {
      font-family: monospace, monospace; /* 1 */
      font-size: 1em; /* 2 */
    }

    /* Text-level semantics
        ========================================================================== */

    /**
     * Remove the gray background on active links in IE 10.
     */

    a {
      background-color: transparent;
    }

    /**
     * 1. Remove the bottom border in Chrome 57-
     * 2. Add the correct text decoration in Chrome, Edge, IE, Opera, and Safari.
     */

    abbr[title] {
      border-bottom: none; /* 1 */
      text-decoration: underline; /* 2 */
      text-decoration: underline dotted; /* 2 */
    }

    /**
     * Add the correct font weight in Chrome, Edge, and Safari.
     */

    b,
    strong {
      font-weight: bolder;
    }

    /**
     * 1. Correct the inheritance and scaling of font size in all browsers.
     * 2. Correct the odd em font sizing in all browsers.
     */

    code,
    kbd,
    samp {
      font-family: monospace, monospace; /* 1 */
      font-size: 1em; /* 2 */
    }

    /**
     * Add the correct font size in all browsers.
     */

    small {
      font-size: 80%;
    }

    /**
     * Prevent 'sub' and 'sup' elements from affecting the line height in
     * all browsers.
     */

    sub,
    sup {
      font-size: 75%;
      line-height: 0;
      position: relative;
      vertical-align: baseline;
    }

    sub {
      bottom: -0.25em;
    }

    sup {
      top: -0.5em;
    }

    /* Embedded content
        ========================================================================== */

    /**
     * Remove the border on images inside links in IE 10.
     */

    img {
      border-style: none;
    }

    /* Forms
        ========================================================================== */

    /**
     * 1. Change the font styles in all browsers.
     * 2. Remove the margin in Firefox and Safari.
     */

    button,
    input,
    optgroup,
    select,
    textarea {
      font-family: inherit; /* 1 */
      font-size: 100%; /* 1 */
      line-height: 1.15; /* 1 */
      margin: 0; /* 2 */
    }

    /**
     * Show the overflow in IE.
     * 1. Show the overflow in Edge.
     */

    button,
    input { /* 1 */
      overflow: visible;
    }

    /**
     * Remove the inheritance of text transform in Edge, Firefox, and IE.
     * 1. Remove the inheritance of text transform in Firefox.
     */

    button,
    select { /* 1 */
      text-transform: none;
    }

    /**
     * Correct the inability to style clickable types in iOS and Safari.
     */

    button,
    [type="button"],
    [type="reset"],
    [type="submit"] {
      -webkit-appearance: button;
    }

    /**
     * Remove the inner border and padding in Firefox.
     */

    button::-moz-focus-inner,
    [type="button"]::-moz-focus-inner,
    [type="reset"]::-moz-focus-inner,
    [type="submit"]::-moz-focus-inner {
      border-style: none;
      padding: 0;
    }

    /**
     * Restore the focus styles unset by the previous rule.
     */

    button:-moz-focusring,
    [type="button"]:-moz-focusring,
    [type="reset"]:-moz-focusring,
    [type="submit"]:-moz-focusring {
      outline: 1px dotted ButtonText;
    }

    /**
     * Correct the padding in Firefox.
     */

    fieldset {
      padding: 0.35em 0.75em 0.625em;
    }

    /**
     * 1. Correct the text wrapping in Edge and IE.
     * 2. Correct the color inheritance from fieldset elements in IE.
     * 3. Remove the padding so developers are not caught out when they zero out
     *    fieldset elements in all browsers.
     */

    legend {
      box-sizing: border-box; /* 1 */
      color: inherit; /* 2 */
      display: table; /* 1 */
      max-width: 100%; /* 1 */
      padding: 0; /* 3 */
      white-space: normal; /* 1 */
    }

    /**
     * Add the correct vertical alignment in Chrome, Firefox, and Opera.
     */

    progress {
      vertical-align: baseline;
    }

    /**
     * Remove the default vertical scrollbar in IE 10+.
     */

    textarea {
      overflow: auto;
    }

    /**
     * 1. Add the correct box sizing in IE 10.
     * 2. Remove the padding in IE 10.
     */

    [type="checkbox"],
    [type="radio"] {
      box-sizing: border-box; /* 1 */
      padding: 0; /* 2 */
    }

    /**
     * Correct the cursor style of increment and decrement buttons in Chrome.
     */

    [type="number"]::-webkit-inner-spin-button,
    [type="number"]::-webkit-outer-spin-button {
      height: auto;
    }

    /**
     * 1. Correct the odd appearance in Chrome and Safari.
     * 2. Correct the outline style in Safari.
     */

    [type="search"] {
      -webkit-appearance: textfield; /* 1 */
      outline-offset: -2px; /* 2 */
    }

    /**
     * Remove the inner padding in Chrome and Safari on macOS.
     */

    [type="search"]::-webkit-search-decoration {
      -webkit-appearance: none;
    }

    /**
     * 1. Correct the inability to style clickable types in iOS and Safari.
     * 2. Change font properties to inherit in Safari.
     */

    ::-webkit-file-upload-button {
      -webkit-appearance: button; /* 1 */
      font: inherit; /* 2 */
    }

    /* Interactive
        ========================================================================== */

    /*
      * Add the correct display in Edge, IE 10+, and Firefox.
      */

    details {
      display: block;
    }

    /*
      * Add the correct display in all browsers.
      */

    summary {
      display: list-item;
    }

    /* Misc
        ========================================================================== */

    /**
     * Add the correct display in IE 10+.
     */

    template {
      display: none;
    }

    /**
     * Add the correct display in IE 10.
     */

    [hidden] {
      display: none;
    }
  `}
    />

    <p>
      Opacity will change the alpha channel of an element, 0 being no opacity or invisible and 9 being 90% opacity, or almost entirely visible (simply leave this class off for 100% opacity).
    </p>

    <Markdown
      rel="opacity.scss"
      language="scss"
      md={
        `  .opacity0 { opacity: 0; }
  .opacity1 { opacity: 0.1; }
  .opacity2 { opacity: 0.2; }
  .opacity3 { opacity: 0.3; }
  .opacity4 { opacity: 0.4; }
  .opacity5 { opacity: 0.5; }
  .opacity6 { opacity: 0.6; }
  .opacity7 { opacity: 0.7; }
  .opacity8 { opacity: 0.8; }
  .opacity9 { opacity: 0.9; }`}
    />

    <p>
      Reset is just like normalize, I like to add this simple one as well:
    </p>

    <Markdown
      rel="reset.scss"
      language="scss"
      md={
        `  /* Find out more about this reset here          */
  /* https://alligator.io/css/minimal-css-reset/  */

  html {
    box-sizing: border-box;
    font-size: 16px;
  }

  *, *:before, *:after {
    box-sizing: inherit;
  }

  body, h1, h2, h3, h4, h5, h6, p, ol, ul {
    margin: 0;
    padding: 0;
    font-weight: normal;
  }

  ol, ul {
    list-style: none;
  }

  img {
    max-width: 100%;
    height: auto;
  }`}
    />

    <p>
      Tables has just basic styling for a table, almost a table reset:
    </p>

    <Markdown
      rel="tables.scss"
      language="scss"
      md={
        `  table {
    border-collapse: collapse;
    width: 100%;
    max-width: 100%;
    margin-bottom: 15px;
  }

  th {
    vertical-align: bottom;
    border-bottom: 2px solid $grey !important;
  }

  th, td {
    padding: 5px;
    vertical-align: top;
    border-left: none;
    border-right: none;
    border-bottom: 1px solid $grey;
  }

  .table-bordered {
    border: 1px solid $grey;
  }

  .table-striped tbody tr:nth-of-type(odd) {
    background-color: $offLight;
  }

  .table-hover tbody tr {
    background-color: $offLight;
  }`}
    />

    <p>
      And finally, typography. Where we reset our headings, have a for loop to make classes to change our font sizes from 1 to 200, and then alignment helpers:
    </p>

    <Markdown
      rel="typography.scss"
      language="scss"
      md={
        `  h1, h2, h3, h4, h5, h6 { margin: 0; }

  @for $i from 1 through 200 {
    .font--#{$i} { font-size: #{$i}px; }
  }

  .text-center { text-align: center; }
  .text-right { text-align: right; }
  .text-left { text-align: left; }`}
    />

    <p>
      Now we just need to import all these files into our <b>main.scss</b> file:
    </p>

    <Markdown
      rel="main.scss"
      language="scss"
      md={
        `  @import 'variables.scss'; /* customize all your colors in this file, keep it first */
  @import 'reset.scss';     /* a super simple reset */

  @import 'base.scss';      /* includes all our base styles like body fonts, etc.. */
  @import 'borders.scss';   /* any border styles, including hr */
  @import 'buttons.scss';   /* all our button styles */
  @import 'colors.scss';    /* the loops for all our predefined colors */
  @import 'forms.scss';     /* Form elements like inputs, text area, radios, etc */
  @import 'grid.scss';      /* our css grid layout, mimics the bootstrap grid */
  @import 'helpers.scss';   /* some helper styles or common things that really don't belong anywhere else */
  @import 'images.scss';    /* image styles */
  @import 'opacity.scss';   /* alpha / opacity helpers */
  @import 'tables.scss';    /* basic table styles, if you work with tables a lot, you'll want to customize */
  @import 'typography.scss';/* font and typography styles */`}
    />

    <p>
      Notice we have our variables first, that is so that all other files can use them. And then the reset is next since that will be the first thing used by the browser. Everything else will inherit down.
    </p>

    <hr />

    <p>
     Phew, that was a lot...Restarting your server and then viewing the page won't change what we see much, because we're not using any styles really at this point.
    </p>

    <p>
      One last thing we need to do, is import some fonts. If you noticed we are trying to use a font called "Roboto" and another icon font called "Font Awesome" (which you should be familiar with already).
    </p>

    <p>
      We can do that by adding them via React Helmet to our <b>seo.js</b> file, note that we have an "onload" attribute, this will only make this a "real" style sheet once the page loads (sneaky sneaky) to prevent blocking the rendering of the rest of our page:
    </p>

    <Markdown
      rel="seo.js"
      language="javascript"
      md={
        `  ...
  <Helmet
    htmlAttributes={{ lang }}
    title={title}
    meta={meta}
  >
    <link
      rel="alternate stylesheet"
      href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900"
      onload="this.rel='stylesheet'"
    />
    <link
      rel="alternate stylesheet"
      href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
      integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr"
      crossOrigin="anonymous"
      onload="this.rel='stylesheet'"
    />
  </Helmet>
  ...`}
    />

    <p>
      If you notice, we changed the Helmet component from a self-closing tag to a non-self-closing, then in-between we added a link to the Roboto font and Font Awesome CDN (content delivery network) sources.
    </p>

    <p>
      Now that we have all our initializations finished, we're ready to start building. If you had any problems, or skipped this section you can grab the source above from our GitLab repo:
    </p>

    <p>
      In the next chapter we need to cover one more thing before getting on to building our layout, pulling in data via GraphQl.
    </p>

    <AniLink to="jamstack-jedi/graphql-101" className="nextBtn">Dive into Some GraphQl Basics &raquo;</AniLink>
    <br />
  </Layout>
);

export default StyleLibrary;
