import React from 'react';
import AniLink from '../../components/Common/AniLink';

import Layout from '../../components/layout';

const Optimize = () => (
  <Layout
    title="Optimize"
    author={{ gravatar: '//0.gravatar.com/avatar/b5eff8d62adf3ff34e67f153781d27c4' }}
    gitRepo={null}
    time={2}
    description="Learn how to optimize your website for speed. Speed optimization with Gatsby."
    keywords={null}
  >

  <strong>
    Sooooooo Slooooooooow
  </strong>
  <br />
  <br />

  <p>
    Our site is coming along nicely. We have it totally built out and have added our base SEO requirements. Before we deploy it to production and replace our coming soon page, we should take a moment to check performance.
  </p>

  <p>
    Page speed and accessibility play a large role in SEO rankings as well as conversions. Think about it, if you're site takes too long, you'll drive people away. They won't wait for a 2mb background image to load while they're on their phone. So we can optimize everything as much as we can so that it loads snappy on all devices.
  </p>

  <p>
    Similarly, accessibility should be as top-notch as we can make it. By accessibility I mean the standard that has been handed down to allow users with disabilities, such as blindness, to still be able to navigate and interact with your site. <a id="anchor--accessibility" href="https://www.w3.org/WAI/fundamentals/accessibility-intro/" target="_blank">Read more about accessibility</a> on the <b>w3</b> website.
  </p>

  <p>
    While building our site, we should have been keeping both speed and accessibility in mind. So hopefully when we run our tests, everything is in pretty good shape. If not, we'll tackle everything one by one until we have a site that is performant as possible.
  </p>

  <p>
    To do this, we'll use a few different tools. First for accessibility, we can use a <a id="anchor--chrome-accessibility-plugin" href="https://chrome.google.com/webstore/detail/wave-evaluation-tool/jbbplnpkjmmeebjpijfedlgcdilocofh?hl=en-US" target="_blank">Chrome plugin</a> that will analyze our site for potential issues and display them along with a description of the issue on the left hand side.
  </p>

  <p>
    For page speed, we'll use a combination of <b>Chrome's Devtools</b> mainly the <b>network panel</b> and the <b>Lighthouse Audits</b> to find out where any potential bottle-knecks might be.
  </p>

  <p>
    Lets jump right into the <b>Wave Accessibility</b> audit and fix any issues it discovers.
  </p>

  <AniLink to="jamstack-jedi/opti-wave" className="nextBtn">Wave Accessibility &raquo;</AniLink>
  <br />
  </Layout>
);

export default Optimize;
