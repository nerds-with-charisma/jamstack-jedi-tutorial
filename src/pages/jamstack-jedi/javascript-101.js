import React from 'react';
import AniLink from '../../components/Common/AniLink';

import Layout from '../../components/layout';
import Markdown from '../../components/Common/markdown';

const Javascript101 = () => (
  <Layout
    title="Javascript 101"
    author={{ gravatar: '//0.gravatar.com/avatar/b5eff8d62adf3ff34e67f153781d27c4' }}
    gitRepo="https://gitlab.com/nerds-with-charisma/jamstack-jedi/tree/master/javascript-101"
    time={22}
    description="Learn Javascript, natively, one of the first steps to becoming a front-end developer. Javascript basics."
      keywords="Learn Javascript, Javascript Basics, Get better at Javascript, Javascript 101, Intro to Javascript"
  >
    <strong>The Tip of the Iceberg</strong>
    <br />
    <br />
    <p className="tldr">
      Javascript can be thought of as the interaction with our website. Right now everything is static in our coming soon example. But what if we wanted to have some interaction, or pull in some dynamic data, or hide an element on click?
    </p>

    <p className="tldr">
      All these sort of tasks can be accomplished with JavaScript. Plus, recently JS usage has seen a huge increase thanks to thinks like node.js, which allows you to write a backend with javascript.
    </p>

    <p>
      Before continuing on, I'd suggest you check out <a id="anchor--javascript-and-jquery" href="https://www.amazon.com/JavaScript-JQuery-Interactive-Front-End-Development/dp/1118531647" target="_blank">JavaScript and JQuery: Interactive Front-End Web Development</a>, by Jon Duckett. It's a great primer into the building blocks of JS. I would suggest only reading until page 429 then skipping to page 527, as we won't be covering jQuery as it's a dying framework.
      {/* https://www.academia.edu/35451488/Javascript_and_jquery_interactive_jon_duckett_www.ebook_dl.com_ */}
    </p>

    <p id="anchor--javascript-beginner-videos">
      There are a lot of video courses on JS as well, I would suggest checking out as many or as little as you'd like until you feel comfortable with Native Javascript. Here's a list of suggestions:
      <br />
      <a href="https://www.youtube.com/watch?v=W6NZfCO5SIk" target="_blank">JavaScript Tutorial for Beginners - Programming with Mosh</a>
      <br />
      <a href="https://www.youtube.com/watch?v=PkZNo7MFNFg" target="_blank">Learn JavaScript - Full Course for Beginners - freeCodeCamp.org</a>
      <br />
      <a href="https://www.youtube.com/watch?v=hdI2bqOjy3c" target="_blank">JavaScript Crash Course For Beginners - Traversy Media</a>
      <br />
      <a href="https://www.youtube.com/watch?v=VuN8qwZoego&list=PLu8EoSxDXHP6CGK4YVJhL_VWetA865GOH" target="_blank">Javascript 30 - Wes Bos</a>
    </p>

    <hr />

    <strong>Dynamic Dates</strong>
    <br />
    <p className="tldr">
      To finish up our coming soon page, we'll add a little interactivity to the page. If you're satisfied with the coming soon page and are eager to get into building our full portfolio feel free to skip this section and move right on to the next chapter.
    </p>

    <p>
      The first thing some of you might have noticed when we started was that we have the date in our footer copyright section. But it's hardcoded, which is fine for about 1 year...then it'll be incorrect and we'll need to update it again manually.
    </p>

    <p className="tldr">
      To avoid having to manually update the date, we can use javascript to print the current year in the footer.
    </p>

    <p>
      In order to do that we'll have to do a few things:
    </p>

    <ul className="bulleted">
      <li>Wrap the date text in the footer with a tag of some sort that we can target with an id</li>
      <li>Write some JS to store that tag as a variable so we can effect it</li>
      <li>Creat a JS variable with the current year in it</li>
      <li>Replace the text in the footer with the value from our year variable.</li>
    </ul>

    <p>
      We'll write all our javascript in a new file and link to it from index.html, so lets create that file called "scripts.js"
    </p>

    <Markdown
      rel="Terminal"
      language="terminal"
      md={
        `  > scripts.js`
      }
    />

    <p>
      Open up index.html and lets link our JS file, we'll load this just before the closing body tag so that it doesn't block loading of the page.
    </p>

    <Markdown
      rel="index.html"
      language="html"
      md={
        `  ...
      <footer>
        &copy; Copyright Nerds With Charisma &bull; 2019
      </footer>
    </main>
    <script src="scripts.js"></script>
  </body>
  ...`
      }
    />

    <p>
      Lets test if it worked, open up <b>scripts.js</b>, add the following line, save the file and lets refresh our local page.
    </p>

    <Markdown
      rel="scripts.js"
      language="javascript"
      md={
        `  alert('Working good!');`
      }
    />

    <img src="/images/jamstack-jedi/javascript--first-alert.png" class="jamstack--img" alt="Javascript first alert" />

    <p>
      We see our alert fire when we refresh. If we filter in the <b>network panel</b> we also see our script loaded with a success (200) status code.
    </p>

    <p className="tldr">
      Great, we've now got our JS file linked up correctly, lets step through our steps one by one, I'll be adding comments above each line to indicate which line does what, you do not need to add the comments yourself.
    </p>

    <p>
      First we need to wrap the date in something we can grab onto with JS, head over to index.html and add the following:
    </p>

    <Markdown
      rel="index.html"
      language="html"
      md={
        `  <footer>
    &copy; Copyright Nerds With Charisma &bull; <span id="copyrightDate"></span>
  </footer>`
      }
    />

    <p>
      We've replaced the hardcoded date with a span that we can grab later with an id of "copyrightDate". Now lets head on over to our <b>scripts.js</b> file. Remove the alert we added and add the following:
    </p>

    <Markdown
      rel="scripts.js"
      language="javascript"
      md={
        `  // store that tag as a variable so we can effect it
  var dateElement = document.getElementById('copyrightDate');

  // get the current year and store it to a variable
  var currentYear = new Date().getFullYear();

  // replace the text of our dateElement with the current year
  dateElement.textContent = currentYear;`
      }
    />

    <p>
      Save and update our file, and we should see the new date coming in, but now it's dynamic. Once the next year comes, the date will automatically reflect that!
    </p>

    <img src="/images/jamstack-jedi/coming-soon-css-final.png" class="jamstack--img" alt="Coming Soon Final" />

    <hr />

    <p>
      Next lets update our headline (Websites. Apps. Seo. Better.) to loop through text every few seconds rather than being static. So it'll say something like "Websites Better." then a few seconds later we'll swap it out to "Apps Better.", etc...
    </p>

    <p>
      Here's what we'll need to accomplish this:
    </p>

    <ul className="bulleted">
      <li>Change our h2 structure in index.html</li>
      <li>Grab the h2 element so we can interact with it later</li>
      <li>Write an array of words that we'll cycle through and store in a variable</li>
      <li>Replace the content of our h2 with a random slot from our array every time the interval runs with a reusable function</li>
      <li>Call the function on page load</li>
      <li>Create a timer that will perform every 3 seconds (called an interval)</li>
    </ul>

    <p>
      To get started, lets hop back to <b>index.html</b> and modify the h2 a tad:
    </p>

    <Markdown
      rel="index.html"
      language="html"
      md={
        `  <section id="copy-wrapper">
    <h2 id="rotatingText"></h2>
    <h2>
      Better.
    </h2>
    <h3>
      Our Portfolio Is Coming Soon
    </h3>
  </section>`
      }
    />

    <p>
      Now lets jump back to our <b>scripts.js</b> file and add this code below our date code. Again, I will comment things inline as we perform each step.
    </p>

    <Markdown
      rel="scripts.js"
      language="javascript"
      md={
        `  // grab the h2 element and store it in a variable
  var rotatingTextElement = document.getElementById('rotatingText');

  // write an array of words to swap out
  var rotatingTextArray = ['Websites.', 'Apps.', 'SEO.', 'Development.', 'Mobile.'];

  // function for replacing our h2 content
  function replaceRotatingText() {
    // replace the h2 content with a random number from our array
    rotatingTextElement.textContent = rotatingTextArray[Math.floor(Math.random() * rotatingTextArray.length)];
  }

  // call the function on page load
  replaceRotatingText();

  // call the function every 3 seconds after page load
  setInterval(function() {
    replaceRotatingText();
  }, 3000);`
      }
    />

    <p className="tldr">
      The first part we simply get the element and save it to var like we did with the date.
    </p>

    <p className="tldr">
      We then create an array of words to cycle through, you can add as many words as you'd like here, and we'll cycle through them dynamically.
    </p>

    <p>
      Next we write our actual function for replacing the text of our h2. We are setting our h2 to the value of rotatingTextArray:
    </p>

    <Markdown
      rel="scripts.js"
      language="javascript"
      md={
        `  rotatingTextArray[Math.floor(Math.random() * rotatingTextArray.length)]`
      }
    />

    <p>
      Instead of calling a specific number, like "rotatingTextArray[2]" and getting "SEO.", we want a random number from the array. So we perform a built in JS function called <b>Math.floor</b> which which will round to the closest or next closest whole number we get from <b>Math.random() * rotatingTextArray.length</b>. <b>Math.random()</b> is also a JS function that gives us a random number, and then multiply by the length or our array to make sure the maximum number returned is from <b>Math.floor()</b> is not higher than the number of items in our array.
    </p>

    <p className="tldr">
      After our function declaration, we call the function immedietly so that it runs on page load.
    </p>

    <p className="tldr">
      Finally we have our interval, which we call a function inside, and run it every 3000 miliseconds (or 3 seconds). Inside the interval function, we call our replace text function again, which will randomly swap out our h2 every 3 seconds.
    </p>

    <hr />

    <p>
      That's all we'll cover in this chapter. I'll leave you with a few exercises to do to get more comfortable with Javasript. All the files are located in our <a href="https://gitlab.com/nerds-with-charisma/jamstack-jedi/tree/master/javascript-101" target="_blank">GitLab repo</a> so you can check your work. (The scripts are inline at the bottom of each file: tip-calculator.html, superscript-cents.html, creditcard-types.html).
    </p>

    <p>
      The first challenge is to build a tip calculator. You should be able to take a total amount (in two decimal places), multiply it by a percentage and output the value to either an alert or the console.
    </p>

    <p>
      The second challenge would be to take the value of a div with just a string of "$25.00", and convert it so that it displays the cents in super script like this, $25.<sup>00</sup>
    </p>

    <p>
      Finally, you should be able to identify credit cards type by the card number a user types in. You should be able to determine when they're typing if it's a Visa, Mastercard, Amex, or other.
    </p>

    <p className="tldr">
      If you can accomplish those tasks, you should be good enough to move on to the next section, as we've just wrapped up the <b>Front-End 101</b> portion of these lessons.
    </p>

    <p>
      Lets wrap up our coming soon project in the next chapter.
    </p>


    <AniLink to="jamstack-jedi/wrapping-up-101" className="nextBtn">Wrapping Up 101 &raquo;</AniLink>
    <br />
  </Layout>
);

export default Javascript101;
