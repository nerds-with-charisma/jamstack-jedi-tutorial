import React from 'react';
import AniLink from '../../components/Common/AniLink';

import Layout from '../../components/layout';

import Markdown from '../../components/Common/markdown';

const GTM = () => (
  <Layout
    title="Google Tag Manager"
    author={{ gravatar: '//0.gravatar.com/avatar/b5eff8d62adf3ff34e67f153781d27c4' }}
    gitRepo="https://gitlab.com/nerds-with-charisma/jamstack-jedi/tree/master/seo-gtm"
    time={8}
    description="Learn Google Tag Manager, how to setup google tag manager. Google Tag Manager basics."
    keywords="Learn Google Tag Manager, Google Tag Manager Basics, Get better at Google Tag Manager, Google Tag Manager 101, Intro to Google Tag Manager"
  >

  <strong>
    Tag, You're It
  </strong>

  <br />
  <br />

  <p>
    Google Tag Manager, or <b>GTM</b> from here on out, is simply a place to put our marketing scripts (or any scripts really) and allows us (or less tech savy people) the ability to publish a script without ever touching the code.
  </p>

  <p>
    I also want to point out that GTM, although a great tool, can have some adverse effects. You can easily bloat your code and up your load times if you don't know what you're doing. Fortunately, GTM is pretty good about allowing us to control how and when tags fire. But you should always ask yourself if you <u>NEED</u> to add something or are you adding it just to add it.
  </p>

  <p>
    We can install GTM manually, or we can use a Gatsby plugin that will do it all for us. Before we decide on that, we need to signup for a GMT account, which is no big deal if you already have a Google account and you signed up for Google Analytics in the <b>Coming Soon</b> section.
  </p>

  <p>
    Head on over to <a id="anchor--google-tag-manager" href="https://tagmanager.google.com/#/home" target="_blank">tagmanager.google.com</a> and sign-up or sign-in, once you do we'll create our container.
  </p>

  <p>
    Once you're logged in, click the <b>Create Account</b> button. <b>Name</b> your account, we'll call ours "Nerds With Charisma", choose your country of origin and if you'd like to send usage stats to Google, and then set a <b>container name</b>, we'll use "nerdswithcharisma.com". Finally choose your <b>platform</b>, we're making a website, so choose "Web" and hit <b>Create</b>
  </p>

  <img src="/images/jamstack-jedi/gtm--create.png" class="jamstack--img" alt="GTM create account" />

  <p>
    Accept the Terms of Service and GTM will give you some <b>tracking code</b>. If we were going to setup GTM manually, we'd want to paste these snippets into our <b>index.html</b> file. But since Gatsby has a plugin for GTM, we really just need the <b>GTM-ID</b>, which should looks something like, "GTM-NP7BCNC". So copy that and place it somewhere for later.
  </p>

  <p>
    Now, we want to install the <b>gatsby-plugin-google-tagmanager</b> plugin to automatically configure our GTM tags. You can <a id="anchor--gatsby-plugin-gtm" href="https://www.gatsbyjs.org/packages/gatsby-plugin-google-tagmanager/" target="_blank">read about it here</a>.  First we need to install it, so if your server is running, kill the process and run the following command:
  </p>

  <Markdown
    rel="Terminal"
    language="terminal"
    md={`  npm install --save gatsby-plugin-google-tagmanager`}
  />

  <p>
    Add the following code to the <b>plugins</b> node, just below the "gatsby-plugin-sharp" node:
  </p>

  <Markdown
    rel="gatsby-config.js"
    language="javascript"
    md={`  ...
  {
    resolve: 'gatsby-plugin-google-tagmanager',
    options: {
      id: 'YOUR_GOOGLE_TAGMANAGER_ID',
      includeInDevelopment: true,
    },
  },
  ...`}
  />

  <p>
    Replace YOUR_GOOGLE_TAGMANAGER_ID with the GTM id you copied earlier. If you lost it or forgot to copy the id, you can go to GTM and navigate to <b>Admin</b> > <b>Install Google Tag Manager</b> under container settings to find it.
  </p>

  <p>
    We're setting "includeInDevelopment" to true, so that we can work with it locally. You can turn it to false if you'd like in production, but we don't really see a need to. Totally up to you.
  </p>

  <p>
    Restart your server and open up devtools, go to <b>network</b> and filter by <b>gtm</b>. We should see a GTM script loading, but there might be a problem for you. It might be 404'ing or "not found". This happens when a GTM container is first created, as there's nothing to serve up, it doesn't have any scripts generated, and it throws a 404 error.
  </p>

  <img src="/images/jamstack-jedi/gtm--404.png" class="jamstack--img" alt="GTM 404" />

  <p>
    We can both fix this, and prove GTM is working by making a test script and firing off some Javascript. Go to GMT and choose <b>Tags</b>, then <b>New</b>.
  </p>

  <p>
    Name it "Test" and click inside the <b>Tag Configuration</b> panel. Choose <b>Custom HTML</b>, then click into the HTML and add the following script:
  </p>

  <Markdown
    rel="Google Tag Manager"
    language="html"
    md={`  <script>
    console.log('GTM is up and running!');
  </script>`}
  />

  <p>
    Next click the <b>Trigger</b> panel, and choose <b>All Pages</b>. Click <b>Save</b>.
  </p>

  <img src="/images/jamstack-jedi/gtm--first.png" class="jamstack--img" alt="GTM First Script" />

  <p>
    Next, click <b>Submit</b>, fill in the fields with a title and description and hit <b>Publish</b> and wait for GTM to finish thinking. Once the snippet has been published, head back to our portfolio and refresh the page.
  </p>

  <p>
    Now, instead of red and 404'ing, we should see the status of gtm as 200 and black. If we open up our console, we should also see our "GTM is up and running!" console log.
  </p>

  <img src="/images/jamstack-jedi/gtm--firing.png" class="jamstack--img" alt="GTM First Tag Firing" />

  <p>
    Very cool, you can also preview changes without publishing them. Head on over to GTM, and lets edit our <b>Test</b> tag to say something else, like "This is only a test". <b>Save</b> it, and instead of hitting "submit", hit <b>Preview</b>.
  </p>

  <p>
    The page should refresh and you'll see a large orange box, letting you know that we're in <b>preview</b> mode. Any time you make a change but don't want to deploy to production, you can use this mode. Simply hit <b>Refresh</b> in the orange box to push any of your new changes to any browser logged into GTM.
  </p>

  <p>
    Open up an incog window next to our current portfolio site, and visit <b>localhost:8000</b>. Also, refresh our non-incog window and pull up the console for both. You should see our preview message in one and the old message in incog. Because we haven't published it, no one not logged into GTM will see our changes. This is great for debugging and testing out changes without effecting production. You can also share previews so people not logged in can get your GTM view, like if you wanted a 3rd party to test some changes.
  </p>

  <img src="/images/jamstack-jedi/gtm--preview.png" class="jamstack--img" alt="GTM First Tag Firing" />

  <p>
    Lets abandon our test tag, by going into it, and clicking the three dots at the top and choosing <b>delete</b>. <b>Save</b> the tag, then publish our changes.
  </p>

  <p>
    This was just the basic setup for GTM, and we'll cover more later. In the next chapter we'll setup <b>Google Analytics</b> via GTM.
  </p>

    <AniLink to="jamstack-jedi/seo-ga" className="nextBtn">Google Analytics &amp; Gatsby &raquo;</AniLink>
    <br />
  </Layout>
);

export default GTM;
