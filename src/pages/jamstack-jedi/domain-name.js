import React from 'react';

import AniLink from '../../components/Common/AniLink';
import Layout from '../../components/layout';

const DomainName = () => (
  <Layout
    title="Domain Name"
    author={{ gravatar: '//0.gravatar.com/avatar/b5eff8d62adf3ff34e67f153781d27c4' }}
    gitRepo={null}
    time={3}
    description="How and where to buy a domain name. What a domain name is."
      keywords="Buy a domain name, what is a domain name, where to get a domain name, how much is a domain name"
  >
    <strong>Say My [Domain] Name?</strong>
    <br />
    <br />
    <p className="tldr">
      A domain name is how people will reach your website, for example, this website's domain is <b>nerdswithcharisma.com</b>. We also have a security certificate installed to make it a secure connection, so our full website url is <b>https://nerdswithcharisma.com</b>. Some websites also include "www.", which we omit for a cleaner url.
    </p>

    <p>
      To get a domain name, you simply have to purchase one from a company that sells available domains such as <a href="http://godaddy.com/" target="_blank">goDaddy</a>, <a href="https://www.hostgator.com/" target="_blank">HostGator</a>, or <a href="https://domains.google/" target="_blank">Google</a>.
    </p>

    <p className="tldr">
      So what makes one domain registrar better then another? Mostly price &amp; freebies they include. For example, if you're trying to host and register in the same place for simplicity or billing, then you could go with goDaddy or HostGator. As they include a domain for the first year if you sign-up for hosting. My advice would be to go with who's the cheapest for your domain registration. You don't have to use the registrar and hosting in the same place, infact we will be using <b>Netlify</b> for our hosting in this tutorial. Who offer a very nice free plan as well as GitLab integration.
    </p>

    <p className="tldr">
      I've dealt with many domain companies and my favorite is HostGator, but they're all pretty much the same. You can also get cheaper domains if you don't need a <b>.com</b> domain. For example, briandausman.com is around $12/year but briandausman.me is only $5/year.
    </p>

    <p>
      Once you've picked out a domain name and purchased it, you'll need to fill out the <b>WHOIS</b> registration information. This is a legal requirement in the U.S. to identify who the domain will belong to. Later, once we've bought a hosting plan, we will point our domain to our hosting server.
    </p>

    <p className="tldr">
      Below is a walkthrough on purchasing a domain from HostGator. All registrar's are pretty similar, so the steps should be close for other providers. Most companies also have support that can help you with your purchase if you're stumped.
    </p>

    <p>
      Step 1 is to search for your domain:
    </p>

    <img src="/images/jamstack-jedi/domain--search.png" class="jamstack--img" alt="Search for a domain" />

    <p>
      Step 2 is to buy it. I've already purchased <b>nerdswithcharisma.com</b>, so it's not listed as available, but whatever domain you find, choose to checkout with it. If you would also like to do hosting with Hostgator (we're encouraging you to use Netlify for now), feel free to add a plan now as well. Usually the cheapest plan will be sufficient, and if you need to upgrade it is usually free of charge. Once you've paid for the domain, you'll be taken to the Hostgator portal, click on <b>Domains</b>.
    </p>

    <img src="/images/jamstack-jedi/domain--dashboard.png" class="jamstack--img" alt="Domain Dashboard" />

    <p className="tldr">
      If you purchase a domain and a hosting package together from HostGator, you don't have to do any setup. Your site will be ready shortly.
    </p>

    <p>
      If you did not purchase hosting, you'll have to point the domain to the place you wish to use. For now, lets leave our HostGator tab open while we decide where to host.
    </p>

    <hr />

    <p>
      If you want to keep up with us and use Netlify's hosting, continue to from the link below.
    </p>

    <AniLink to="jamstack-jedi/hosting-netlify" className="nextBtn">Setup Hosting (Netlify) &raquo;</AniLink>
    <br />
    <br />

    <p>
      If you went with HostGator for hosting, or any other traditional FTP hosting you can go to the chapter in the link below and meet back up with us in the "Javascript 101" chapter.
    </p>
    <AniLink to="jamstack-jedi/hosting-ftp" className="nextBtn">Setup Hosting (FTP) &raquo;</AniLink>
    <br />
    <br />
  </Layout>
);

export default DomainName;
