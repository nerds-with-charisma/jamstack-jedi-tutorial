import React from 'react';
import AniLink from '../../components/Common/AniLink';

import Layout from '../../components/layout';

import Markdown from '../../components/Common/markdown';
import YouTube from '../../components/Common/YouTube';

const HeroBuild = () => (
  <Layout
    title="Hero Continued"
    author={{ gravatar: '//0.gravatar.com/avatar/b5eff8d62adf3ff34e67f153781d27c4' }}
    gitRepo="https://gitlab.com/nerds-with-charisma/jamstack-jedi/tree/master/build-hero-2"
    time={30}
    description={null}
    keywords={null}
  >

    <strong>Break It Down</strong>
    <br />
    <br />

    <p>
      The last section had lots going on, and we've gotten Hero.js pretty bloated. If we kept going, it would just get worse, so lets break it up into some smaller components. If we look at what we have we should probably make a hero-heading file, a hero-quote file, a hero-contact file, and then a common file for our large N that we could reuse for the other files.
    </p>

    <p className="tldr">
      So lets do that now, kill your server if it's running and run the following commands:
    </p>

    <Markdown
      rel="Terminal"
      language="terminal"
      md={
        `  cd src/components/hero

  touch hero-heading.js hero-quote.js hero-contact.js

  mkdir ../common

  > ../common/large-letter.js

  cd ../../../`}
    />

    <p>
     We've now made empty files for all these components so lets move some content out of <b>Hero.js</b> and into each individual file. First lets move the hero heading, cut out the <b>h2</b> element from <b>Hero.js</b> and lets create the <b>HeroHeading</b> component:
    </p>

    <Markdown
      rel="hero-heading.js"
      language="javascript"
      md={
        `  import React from 'react';

  const HeroHeading = () => (
    <h2 className="font--125">
      {'websites.'}
      <br />
      {'apps.'}
      <br />
      {'seo.'}
      <br />
      {'better.'}
    </h2>
  );

  export default HeroHeading;`}
    />

    <p>
      Lets do the same for the <b>aside</b> tag that holds our quote, and create the <b>HeroQuote</b> component in <b>hero-quote.js</b>:
    </p>

    <Markdown
      rel="hero-quote.js"
      language="javascript"
      md={
        `  import React from 'react';

  const HeroQuote = () => (
    <aside className="font--40">
      {'Quote placeholder'}
    </aside>
  );

  export default HeroQuote;`}
    />

    <p>
      And our <b>HeroContact</b> component, by cutting out our button:
    </p>

    <Markdown
      rel="hero-quote.js"
      language="javascript"
      md={
        `  import React from 'react';

  const HeroContact = () => (
    <button
      type="button"
      className="btn bg--light font--dark font--24 padding-horiz-xl radius--lg border--none block no--wrap"
      onClick={() => alert('todo: scroll to footer')}
    >
      <strong>
        {'Get In Touch'}
      </strong>
    </button>
  );

  export default HeroContact;`}
    />

    <p>
      Next we have to import them all into <b>Hero.js</b>
    </p>

    <Markdown
      rel="hero.js"
      language="javascript"
      md={
        `  import React from 'react';
  import { StaticQuery, graphql } from 'gatsby';

  import HeroContact from './hero-contact';
  import HeroHeading from './hero-heading';
  import HeroQuote from './hero-quote';
  ...
    <span id="N">
      {'N'}
    </span>
    <div>
      <HeroHeading />
      <HeroQuote />
      <HeroContact />
    </div>
  ...`}
    />

    <hr />

    <p>
      Next we'll create the reusable component for our giant letters, open up <b>/src/components/common/large-letter.js</b>
    </p>

    <Markdown
      rel="large-letter.js"
      language="javascript"
      md={
        `  import React from 'react';

  const LargeLetter = ({ letter }) => (
    <span id={letter}>
      {letter}
    </span>
  );

  export default LargeLetter;`}
    />

    <p>
      We pass in a letter as a prop, so now lets import and use that in our <b>Hero.js</b> component:
    </p>

    <Markdown
      rel="Hero.js"
      language="javascript"
      md={
        `  ...
  import LargeLetter from '../common/large-letter';
  ...
    <LargeLetter letter="N" />
    <div>
      <HeroHeading />
      <HeroQuote />
      <HeroContact />
    </div>
  ...`}
    />

    <hr />

    <strong id="anchor--prop-types" className="tldr">What the PropTypes?</strong>

    <p className="tldr">
      I like using <b>PropTypes</b>, I know they're falling out of favor, but I think it makes the code more readable especially to new developers. PropTypes are basically definitions of what "type" a prop being passed to your component should be. Should it be a string, or a number, etc...
    </p>

    <p className="tldr">
      Using PropTypes consists of 3 main steps: import it, declare it's value, set a default if it's not always required. We've used props a few times in a couple of our components, which I'm going to quickly go back and add proptypes for. You can feel free to skip this section if you don't want to use them.
    </p>

    <Markdown
      rel="large-letter.js"
      language="javascript"
      md={`  import React from 'react';
  import { PropTypes } from 'prop-types';

  const LargeLetter = ({ letter }) => (
    <span id={letter}>
      {letter}
    </span>
  );

  LargeLetter.propTypes = {
    letter: PropTypes.string.isRequired,
  }

  export default LargeLetter;`}
    />

    <p className="tldr">
      Here you'll notice we import PropTypes from 'prop-types' which is a package the gatsby starter included for us. Then we add the name of the component right before we export it followed by ".propTypes" and make it an object. We are only passing in "letter" as a prop, so we set that as our only node. Which is a string and is required. If a dev forgot to pass this prop, a warning would print in the console during development. It would not show up in production so don't worry this is strictly to hold developers accountable during their dev stages.
    </p>

    <p>
      We do the same thing for the our Hero compnent:
    </p>

    <Markdown
      rel="Hero.js"
      language="javascript"
      md={`  import { PropTypes } from 'prop-types';
  ...
  Hero.propTypes = {
    supportsWebP: PropTypes.bool.isRequired,
  };

  export default Hero;
  ...`}
    />

    <p className="tldr">
      That should be all we need for now, we'll be adding these checks now, as we create a component, but we won't be mentioning them specifically each time. You can add them on your own if you'd like and they will be in our GitLab repos.
    </p>

    <hr />

    <p>
      The next thing we need to do is pull in a random quote for our hero-quote file. We can do that by making an array of words in our <b>Hero.js</b> file and passing it as a prop to our <b>hero-quote.js</b> file:
    </p>

    <Markdown
      rel="Hero.js"
      language="javascript"
      md={`  ...

  const randomQuote = [
    'Coding Since Before We Could Walk.',
    'The Best in the World at What We Do.',
    'Super-Awesome Apps for Super-Awesome Peeps.',
    'I Think We\\'re Gonna Need A Bigger Boat.',
    'Coding Ninjas.',
    'Punk Rock Pixels.',
    'The Best There Is, Was, and Ever Will Be.',
    'Do Or Do Not. There Is No Try.',
    'We Know "The Google".',
    'FOR SCIENCE!',
    'Reinforced by Space-Age Technology.',
  ];

  const Hero = ({ supportsWebP }) => (
    ...`}
    />

    <p>
      And then pass it to our <b>HeroQuote</b> component as the "randomQuote" prop, we're grabbing a random number between 0 and the length of the array from the randomQuote variable we created above:
    </p>

    <Markdown
      rel="Hero.js"
      language="javascript"
      md={`  ...
  <HeroQuote randomQuote={randomQuote[Math.floor(Math.random() * randomQuote.length)]} />
    ...`}
    />

    <p>
      Then we use it in our <b>hero-quote.js</b> file (and the PropType...seriously we'll stop calling them out soon):
    </p>

    <Markdown
      rel="hero-quote.js"
      language="javascript"
      md={`  import React from 'react';
  import { PropTypes } from 'prop-types';

  const HeroQuote = ({ randomQuote }) => (
    <aside className="font--40">
      {randomQuote}
    </aside>
  );

  HeroQuote.propTypes = {
    randomQuote: PropTypes.string.isRequired,
  };

  export default HeroQuote;`}
    />

  <img src="/images/jamstack-jedi/hero--random-quote.png" class="jamstack--img" alt="Hero Random Quote" />

  <hr />

  <p>
    One thing we're NOT doing so far after breaking the components up, is pulling in our data dynamically like we did earlier via <b>gatsby-config.js</b>. This is really a best practice, so we should always make our data come from there. So lets do some changes.
  </p>

  <p>
    We have 4 hard-coded points of copy right now: the quote array, our hero heading, and our contact button. Lets make those nodes in a new node in <b>gatsby-config.js</b> just below our <b>homepageData</b> node.
  </p>

  <Markdown
      rel="gatsby-config.js"
      language="javascript"
      md={`  heroData: {
  contactButtonText: 'Get In Touch',
  heroCopy: ['websites.', 'apps.', 'seo.', 'better.'],
  randomQuote: [
    'Coding Since Before We Could Walk.',
    'The Best in the World at What We Do.',
    'Super-Awesome Apps for Super-Awesome Peeps.',
    'I Think We\\'re Gonna Need A Bigger Boat.',
    'Coding Ninjas.',
    'Punk Rock Pixels.',
    'The Best There Is, Was, and Ever Will Be.',
    'Do Or Do Not. There Is No Try.',
    'We Know "The Google".',
    'FOR SCIENCE!',
    'Reinforced by Space-Age Technology.',
  ],
},`}
    />

    <p>
      Now we can go back to <b>Hero.js</b> and <u>remove the old randomQuote variable</u>, and pull the data in from GraphQl. We can simply add more to our query below the image query we created earlier:
    </p>

    <Markdown
      rel="Hero.js"
      language="javascript"
      md={`  query HeroQuery {
    fileName: file(relativePath: { eq: "bg--hero.jpg" }) {
      childImageSharp {
        sizes(maxWidth: 1900, maxHeight: 1518) {
          src
          srcWebp
        }
      }
    }

    site {
      siteMetadata{
        heroData {
          contactButtonText
          heroCopy
          randomQuote
        }
      }
    }
  }`}
    />

    <p>
      We can then modify the HeroQuote prop for randomQuote to use our GraphQl data source:
    </p>

    <Markdown
      rel="Hero.js"
      language="javascript"
      md={`  ...
      <HeroQuote
        randomQuote={data.site.siteMetadata.heroData.randomQuote[Math.floor(Math.random() * data.site.siteMetadata.heroData.randomQuote.length)]}
      />
  ...`}
    />

    <p>
      Next lets do our hero heading, we're already pulling in the data in our query, so lets pass it to our <b>HeroHeading</b> component:
    </p>

    <Markdown
      rel="Hero.js"
      language="javascript"
      md={`  ...
      <HeroHeading
        heroCopy={data.site.siteMetadata.heroData.heroCopy}
      />
  ...`}
    />

    <p>
      If you noticed we created an array, so we'll want to loop through that, so we need to restructure our code a little. Head on over to <b>hero-heading.js</b>.
    </p>

    <p>
      We pull it into <b>hero-heading.js</b> as a prop, then we'll map over the array, spitting out an h2 for each:
    </p>

    <Markdown
      rel="hero-heading.js"
      language="javascript"
      md={`  import React from 'react';
  import { PropTypes } from 'prop-types';

  const HeroHeading = ({ heroCopy }) => (
    <h2 className="font--125">
      { (heroCopy.map((copy) => (
        <React.Fragment key={copy}>
          {copy}
          <br />
        </React.Fragment>
      )))}
    </h2>
  );

  HeroHeading.propTypes = {
    heroCopy: PropTypes.array.isRequired,
  };

  export default HeroHeading;`}
    />

    <p className="tldr">
      We're using a React <b>Fragment</b> because we don't need to wrap our copy in any sort of tag, but we're also not allowed to have 2 children inside of a map without a wrapper. So we can't output the copy and the line break without putting something around it. Since we don't want to dirty up our markup, we don't want an actual tag, so we use a fragment. We also assign it a key, which is best practice when mapping through things so React can keep track of everything.
    </p>

    <p>
      The final bit of data we need to pull in dynamically is our contact button text, so head back to <b>Hero.js</b>, and pass it as a prop:
    </p>

    <Markdown
      rel="Hero.js"
      language="javascript"
      md={`  ...
      <HeroContact
        contactButtonText={data.site.siteMetadata.heroData.contactButtonText}
      />
  ...`}
    />

    <p>
      And modify our <b>hero-contact.js</b> component to use it:
    </p>

    <Markdown
      rel="hero-contact.js"
      language="javascript"
      md={`  import React from 'react';
  import { PropTypes } from 'prop-types';

  const HeroContact = ({ contactButtonText }) => (
    <button
      type="button"
      className="btn bg--light font--dark font--24 padding-horiz-xl radius--lg border--none block no--wrap"
      onClick={() => alert('todo: scroll to footer')}
    >
      <strong>
        {contactButtonText}
      </strong>
    </button>
  );

  HeroContact.propTypes = {
    contactButtonText: PropTypes.string.isRequired,
  };

  export default HeroContact;`}
    />

    <p className="tldr">
      And that's it for our dynamic data. That was a lot to cover...take a break and come on back so we can finish up our Hero component (I swear there's not much left).
    </p>

    <hr />

    <p className="tldr">
      The last bit we need to do are those social icons at the bottom. We'll do that by adding the links to our <b>gatsby-config.js</b> file, creating a new component for them, passing the data to the component, and outputing each. We'll then style it up and call our design build-out for our hero done-zo.
    </p>

    <p>
      Add our links to our Gatbsy config just below our homepageData (because we'll be using these icons elsewhere):
    </p>

    <Markdown
      rel="gatsby-config.js"
      language="javascript"
      md={`  socialLinks: [
    {
      link: 'https://gitlab.com/nerds-with-charisma',
      icon: 'fab fa-gitlab',
      title: 'GitLab',
    },
    {
      link: 'https://twitter.com/nerdswcharisma',
      icon: 'fab fa-twitter',
      title: 'Twitter',
    },
    {
      link: 'https://www.npmjs.com/~nerds-with-charisma',
      icon: 'fab fa-npm',
      title: 'NPM',
    },
    {
      link: 'https://medium.com/@nerdswithcharisma',
      icon: 'fab fa-medium-m',
      title: 'Medium',
    },
  ],`}
    />

    <p>
      Then lets add it to our query just below the heroData request:
    </p>

    <Markdown
      rel="Hero.js"
      language="javascript"
      md={`  ...
  heroData {
    contactButtonText
    heroCopy
    randomQuote
  }

  socialLinks {
    link
    icon
    title
  }
  ...`}
    />

    <p>
      Create our common component for our social links:
    </p>

    <Markdown
      rel="terminal"
      language="terminal"
      md={`  > src/components/common/social-links.js`}
    />

    <p>
      Restart our server, and lets add the <b>SocialLink</b> component to our <b>Hero.js</b> file:
    </p>

    <Markdown
      rel="Hero.js"
      language="javascript"
      md={`  ...
  import SocialLinks from '../common/social-links';
  ...
        <SocialLinks links={data.site.siteMetadata.socialLinks} />
      </section>
  ...`}
    />

    <p>
      Open up our <b>social-links.js</b> file, and lets build our component:
    </p>

    <Markdown
      rel="Hero.js"
      language="javascript"
      md={`  import React from 'react';
  import { PropTypes } from 'prop-types';

  const SocialLinks = ({ links }) => (
    <section className="social-links text-right">
      { links.map((link) => (
        <a key={link.title} className="font--grey" href={link.link} target="_blank">
          <i className={\`\${link.icon} font--24\`}>
          <div className="ir">
            {link.title}
          </div>
          </i>
        </a>
      ))}
    </section>
  );


  SocialLinks.defaultProps = {
    links: [],
  };

  SocialLinks.propTypes = {
    links: PropTypes.array,
  };

  export default SocialLinks;`}
    />

    <p className="tldr">
      Few things of importance here, we are using font-awesome, so the user can choose any icon that FA offers. We have a div inside of the icon with a class of "ir" or "image replacment" which will help with screen readers for the blind. We also have "defaultProps" which means that the user doesn't have to provide any links, and if they don't we'll default it to an empty array.
    </p>

    <p>
      So now we can style it up and call our desktop experience done for the hero, open up <b>_hero.scss</b> and add the following styles below where we put our button inside hero declaration:
    </p>

    <Markdown
      rel="_hero.scss"
      language="scss"
      md={`  .social-links {
    position: absolute; /* so that we can position at the bottom of our hero */
    bottom: 30px; /* 30 pixels from the bottom */
    right: 30px; /* 30 from the right */
    height: 30px; /* make sure it's only 30px high */
    z-index: 999; /* put it a layer above all the other hero elements */

    a {
      padding: 15px; /* give it some breathing room for tapping */
    }
  }`}
    />

    <p>
      One last thing we want to do is write the image replacement style, so open up <b>base.scss</b>, since this will be usable everywhere, and add the following to the bottom:
    </p>

    <Markdown
      rel="base.scss"
      language="scss"
      md={`  /* replace text off screen */
  .ir {
    text-indent: -9999px;
    font-size: 10px;
  }`}
    />

    <p className="tldr">
      This will move any text off the screen, but to a screen reader it will fall right in line where it is supposed to be.
    </p>

    <p>
      Start up your server and take a look. Our Hero component is done...for desktop! In the next chapter we'll finish up with a few mobile tweaks, and call the hero complete.
    </p>

    <img src="/images/jamstack-jedi/hero--desktop.png" class="jamstack--img" alt="Hero Desktop" />

    <AniLink to="jamstack-jedi/build-hero-3" className="nextBtn">The Hero Component Mobile &raquo;</AniLink>
    <br />
  </Layout>
);

export default HeroBuild;
