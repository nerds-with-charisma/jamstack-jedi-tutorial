import React from 'react';
import AniLink from '../../components/Common/AniLink';

import Layout from '../../components/layout';

const GSC = () => (
  <Layout
    title="Google Search Console"
    author={{ gravatar: '//0.gravatar.com/avatar/b5eff8d62adf3ff34e67f153781d27c4' }}
    gitRepo={null}
    time={8}
    description="Learn Google Search Console, how to setup Google Search Console. Google Search Console basics."
    keywords="Learn Google Search Console, Google Search Console Basics, Get better at Google Search Console, Google Search Console 101, Intro to Google Search Console"
  >

  <strong>
    Some Witty Title
  </strong>
  <br />
  <br />

  <p>
    Google Search Console is a tool provided by Google that will let us monitor our site's presence from a search engine standpoint. They'll alert us to any issue detected in regards to things like the sitemap, any crawl issues, and a lot more.
  </p>

  <p>
    GSC is completely free, so all we have to do is sign in to our existing Google account, head on over to <a id="anchor--gsc-signin" href="https://search.google.com/search-console/about" target="_blank">GSC's Start Page</a> and click <b>Start Now</b>.
  </p>

  <p>
    If you're not asked after signing in, then click the menu button and choose <b>Add Property</b>. In the <b>Domain</b> input, enter your website's address and click <b>Continue</b>.
  </p>

  <p>
    To verify our website's actually OUR website, we need to confirm we own it by adding an id to our DNS configuration. Which we can do in <b>Netlify's Backend</b>.
  </p>

  <p>
    Log back into Netlify and choose <b>Domain Settings</b> from our main landing page. Find our domain and click the 3 buttons ellipsis to open up the dropdown menu and choose <b>Go To DNS Panel</b>.
  </p>

  <img src="/images/jamstack-jedi/seo--netlify-dns.png" class="jamstack--img" alt="Netlify DNS Settings" />

  <p>
    From the DNS panel, click <b>Add new record</b>, and add the following; Record Type: TXT, Name: @, value: copy the code Google gave you in GSC, click <b>Save</b>.
  </p>

  <img src="/images/jamstack-jedi/seo--netlify-new-dns.png" class="jamstack--img" alt="Netlify New DNS Record" />

  <p>
    Flip back to GSC and click <b>Verify</b> (note: sometimes it takes up to a few hours for the DNS to update). Once verified, you'll get a message like this:
  </p>

  <img src="/images/jamstack-jedi/gsc--verified.png" class="jamstack--img" alt="Google Search Console Verified" />

  <p>
    You can click <b>Done</b> and now we're ready to poke around in GSC. First, from the <b>Nav Menu Icon</b> choose <b>Sitemaps</b>. Under <b>Add a new sitemap</b> lets enter the location that the Gatsby sitemap plugin generated our sitemap, it will be in your root, called "sitemap.xml".
  </p>

  <img src="/images/jamstack-jedi/gsc--submit-sitemap.png" class="jamstack--img" alt="Google Search Console Submit Sitemap" />

  <p>
    Hit <b>submit</b> and GSC will go out and read your sitemap. Next, at the top, enter your root domain in the "inspect any URL" input box and hit <b>Enter</b>. GSC will check if your site is indexed, which, it should not be yet. So lets tell it we want them to index us by clicking <b>Request Indexing</b>.
  </p>

  <p>
    Finally, we can test just how Google will see our site by clicking, under the <b>URL Inspection</b> section, <b>Test Live URL</b>. Once that finished we should see a new option called <b>View Tested Page</b>. Click this and a panel will slide out with the HTML GSC found on our site. Click the <b>Screenshot</b> tab to see what GSC visually saw. It should look very similar to our actual site on mobile. If not, there might have been an error reading our site, click the <b>More Info</b> tab and see if there's any errors here. If so, correct them, commit &amp; deploy, and run the test again to see if they were fixed.
  </p>

  <p>
    You can also use a Chrome Plugin called <a href="https://chrome.google.com/webstore/detail/user-agent-switcher/lkmofgnohbedopheiphabfhfjgkhfcgf" target="_blank">User Agent Switcher</a> to spoof a crawler, if for some reason you wanted to test your site as a bot or any other browser.
  </p>

  <p>
    That finishes up our GSC section, our site is now visible to Google and we have confirmed they won't have any issues seeing our content.
  </p>

  <p>
    Whenever you make changes to your site, you can manually tell GSC to re-crawl your site by going to the <b>URL Inspection</b> tab and selecting <b>Request Indexing</b>.
  </p>

  <p>
    That wraps up our GSC section, in the next section we'll be moving on to claiming our brands on social media. Even if you don't plan on using a certain service, it's a good idea to grab it anyway, incase you want to use it later and to prevent someone else from grabbing it.
  </p>


  <AniLink to="jamstack-jedi/social" className="nextBtn">Claim Brands on Social Media &raquo;</AniLink>
  <br />
  </Layout>
);

export default GSC;
