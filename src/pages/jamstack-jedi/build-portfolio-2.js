import React from 'react';
import AniLink from '../../components/Common/AniLink';

import Layout from '../../components/layout';

import Markdown from '../../components/Common/markdown';

const PortfolioBuild = () => (
  <Layout
    title="Portfolio Continued"
    author={{ gravatar: '//0.gravatar.com/avatar/b5eff8d62adf3ff34e67f153781d27c4' }}
    gitRepo={null}
    description={null}
    keywords={null}
  >

  <p>
    Lets style up our single portfolio item now that we have data wired up. For starters, we want the portfolio to slide in like a panel just like the menu we made ealier. We're toggling the classes "active" vs "inactive" to handle this, so lets style that first by adding some styles to the bottom of <b>_portfolio.scss</b>:
  </p>

  <Markdown
    rel="_portfolio.scss"
    language="scss"
    md={`  /* portfolio single */
  #portfolioSingle {
    position: fixed; /* fix our position so it's docked at the top */
    top: 110%; /* push it off the screen when nothing is loaded */
    left: 0; /* positioning */
    width: 100%;
    height: 100%;
    background: $light; /* make the background white */
    transition: all 0.3s linear; /* so that we can animate the single panel in and out */
    overflow: scroll; /* anything offscreen, we want to be able to still scroll to */
    z-index: 999999999; /* the topest of top elements */

    &.active {
      top: 0; /* when we have something loaded, put the panel at the top */
    }
  }`}
  />

  <p>
    Now that our panel is styled lets do a little styling to the inside elements:
  </p>

  <Markdown
    rel="_portfolio.scss"
    language="scss"
    md={`  ...
  #portfolioItem--hero {
    position: relative; /* relatively position parent so we can absolutely position children */
    overflow: hidden; /* hide anything that breaks through */

    img {
      max-width: 100%; /* fluid width image */
    }

    h2 {
      position: absolute; /* positioning to dock it at the bottom left */
      white-space: nowrap; /* don't let the text go to 2 lines */
      bottom: -45px;
      left: 0;
      width: 100%;
      color: $light; /* white text */
      font-size: 6em; /* make it big */
      text-transform: uppercase; /* make it all uppercase */
      font-weight: bold; /* make sure it's bold */
    }
  }
  ...`}
  />

  <img src="/images/jamstack-jedi/portfolio--hero.png" class="jamstack--img" alt="Portfolio Hero" />

  <p>
    The "type" section is pretty simple to style:
  </p>

  <Markdown
    rel="_portfolio.scss"
    language="scss"
    md={`  ...
  #portfolioItem--type {
    align-items: center; /* center the text vertically */
    text-align: right; /* right align the text horizontally */

    img {
      margin-left: 25px; /* add a little breathing room */
      border-radius: 5px; /* round the edges */
      box-shadow: 0 10px 40px rgba(0,0,0,0.2); /* add a nice big/subtle shadow */
    }
  }
  ...`}
  />

  <p>
    The "what" section is even simpler:
  </p>

  <Markdown
    rel="_portfolio.scss"
    language="scss"
    md={`  ...
  #portfolioItem--what {
    text-align: center;
    margin: 150px 0;
  }
  ...`}
  />


  <p>
    The "images" section mimics our browser styling:
  </p>

  <Markdown
    rel="_portfolio.scss"
    language="scss"
    md={`  ...
  #portfolioItem--images img {
    border-radius: 5px; /* round the edges */
    box-shadow: 0 10px 40px rgba(0,0,0,0.2); /* add a nice big/subtle shadow */
    margin-bottom: 35px;
  }
  ...`}
  />

  <p>
    The footer component is also very simple:
  </p>

  <Markdown
    rel="_portfolio.scss"
    language="scss"
    md={`  ...
  #portfolioItem--footer {
    margin: 75px 0;

    .btn {
      display: block;
      width: 400px;
      margin: auto;
    }
  }
  ...`}
  />

  <p>
    Finally, we just have to style the close button:
  </p>

  <Markdown
    rel="_portfolio.scss"
    language="scss"
    md={`  ...
  .close {
    position: fixed;
    right: 15px;
    top: 15px;
    border: none;
    color: #000;
    font-size: 4rem;
    z-index: 999;
  }
  ...`}
  />

  <img src="/images/jamstack-jedi/portfolio--final-style.png" class="jamstack--img" alt="Portfolio Final Style" />

  <hr />

  <p>
    Lookin' good, but we don't want the portfolio single open on page load, and we also want to be able to close it...so lets handle that now. First we need to restructure our portfolio components to be able to use state and handle logic for updating the portfolio items. Open up <b>Portfolio.js</b> and lets wrap our render function in curly braces:
  </p>

  <Markdown
    rel="Portfolio.js"
    language="javascript"
    md={`
  import React, { useState, useEffect } from 'react';
  ...
  const Portfolio = ({ portfolioData }) => {
    const [portItem, setPortItem] = useState(null);

    return (
      <section id="portfolio" className="position--relative overflow--container">
        ...
        <PortfolioItem key={\`\${item.alt}\${i}\`} item={item} setItem={setPortItem} />
        ...
        <PortfolioSingle item={portItem} setItem={setPortItem} />
      </section>
    )
  };
  ...`}
  />

  <p>
    We also pass down our setItem state function so that when we click the close button, we can set it to null, thus "closing" the single panel and set the selected item when we click it in portfolio item.
  </p>

  <p>
    Now we need to wire up those click events, lets modify our TODO in <strong>portfolio-item.js</strong> to pass the clicked "item" to <strong>setItem</strong>:
  </p>

  <Markdown
    rel="portfolio-item.js"
    language="javascript"
    md={`  ...
  const PortfolioItem = ({ item, setPortItem } ) => (
    ...
    <button
      type="button"
      onClick={() => setPortItem(item)}
    >
    ...
    PortfolioItem.propTypes = {
      item: PropTypes.object.isRequired,
      setPortItem: PropTypes.func.isRequired,
    };
  ...`}
  />

  <p>
    We set our "setItem" to a deconstructed variable, then we change our "todo" on click to use our <strong>setItem</strong> function and pass it the current item (and add the new proptype). Now, if you start your server and click on an item, it should load up and slide in to the single panel.
  </p>

  <p>
    We still can't close it, so lets fix that now, open up <strong>portfolio-single.js</strong> and lets add a couple of changes. First we'll add the click event to close the panel. And then we'll add a safety check to make sure we don't get any console errors when closing:
  </p>

  <Markdown
    rel="portfolio-single.js"
    language="javascript"
    md={`  ...
  const PortfolioSingle = ({ item, setItem }) => (
    ...
      { (item !== null)
        && (
          <>
            <button
              type="button"
              className="close"
              onClick={() => setItem(null)}
            >
              &times;
            </button>
            ...
          </>
        )
      }
  ...`}
  />

  <p>
    We wrap our internals in a null check for item. If we didn't do this, there would be a chance that something like "item.about" could try to populate when item is null. Which would cause at the very least an error to be thrown, and at worst, the page to crash because it flipped out not knowing what "item" is.
  </p>

  <p>
    That finishes up our click/loading events. Lets refresh what we still have to do:
  </p>

  <ul className="bulleted">
    <li>Update the url on click</li>
    <li>On refresh, load a portfolio item if in the url</li>
    <li>Get dynamic data from an external source and fallback to our hardcoded data</li>
  </ul>

  <hr />

  <p>
    The url updating is actually pretty easy, right after our <strong>useState</strong> call, we can "watch" for changes to our state with <strong>useEffect</strong>.
  </p>

  <p>
    Lets do a little POC to show how that works, inside <strong>Portfolio.js</strong> add the follwing below our <strong>useState</strong> declaration:
  </p>

  <Markdown
    rel="Portfolio.js"
    language="javascript"
    md={`  ...
  const [portItem, setItem] = useState(null);

  useEffect(() => {
    console.log('callback')
  }, [portItem]);
  ...`}
  />

  <p>
    Open your dev tools and we'll see that this "callback" console log appears in the console after we click a portfolio item...it appears again, then click close...appears again. So basically, any time <strong>item</strong> changes in our state, this will fire because we've specified to file on "[item]" change.
  </p>

  <p>
    Now, we can add some logic in here to update our url whenever we have a value in the <strong>item</strong> state var.
  </p>

  <p>
    If there is an item, we want to call the window's history push state to throw a custom url up, which is "portfolio" followed by whatever the item's alt text is, all in lowercse and with all spaces replaced with "-"s. Update our useEffect call inside of <b>Portfolio.js</b> to the following:
  </p>

  <Markdown
    rel="Portfolio.js"
    language="javascript"
    md={`  ...
  useEffect(() => {
    if (portItem) {
      window.history.pushState(null, null, \`/project/\${portItem.alt.toLowerCase().replace(/ /g, '-').toLowerCase()}\`);
    } else {
      window.history.pushState(null, null, '/');
    }
  }, [portItem]);
  ...`}
  />

  <p>
    Try it out, click on an item and watch the url update. Close the single panel, and watch it go back to our homepage:
  </p>

  <img src="/images/jamstack-jedi/portfolio--url-update.png" class="jamstack--img" alt="Portfolio Url Update" />

  <hr />

  <p>
    Try clicking on an item, then refresh the page...UH OH. 404 error, that's because we don't have routes setup for these <strong>/portfolio/</strong> "pages". What can we do?
  </p>

  <p>
    Well good news everyone! Gatsby has us covered, we can hook into our pages build and create some custom ones inside a file called <strong>gatsby-node.js</strong>
  </p>

  <p>
    There's a lot going on in here, but a lot of it is boilerplate, <a id="anchor--gatsby-node" href="https://www.gatsbyjs.org/docs/node-apis/" target="_blank">you can read about gatsby-node.js here</a>.
  </p>

  <Markdown
    rel="gatsby-node.js"
    language="javascript"
    md={`  const path = require(\`path\`); // you will need it later to point at your template component

  exports.createPages = async ({ graphql, boundActionCreators }) => {
    const { createPage } = boundActionCreators;

    // ## Create portfolio pages
    // we use a Promise to make sure the data are loaded
    // before attempting to create the pages with them
    const portfolioPromise = new Promise((resolve, reject) => {
      // fetch your data here, generally with graphQL.
      graphql(\`
      {
        site {
          siteMetadata {
            portfolioData {
              portfolioData {
                alt
                src
                type
                website
                about
                images
                hero
                tech
                launchDate
                sort
                browser
              }
            }
          }
        }
      }
      \`).then(result => {
        if (result.errors) {  // first check if there is no errors
          reject(result.errors);  // reject Promise if error
        }

        // if no errors, you can map into the data and create your static pages
        result.data.site.siteMetadata.portfolioData.portfolioData.forEach((node) => {
          // create page according to the fetched data
          createPage({
            path: \`/project/\${node.alt.toLowerCase().replace(/ /g, '-').toLowerCase()}\`, // your url -> /categories/animals
            component: path.resolve('./src/pages/index.js'), // your template component
            context: {
              currentProjectToOpen: node,
            },
          });
        });

        resolve();
      });
    });

    return {
      portfolioPromise,
    }
  };`}
  />

  <p>
    Lets break down what's going on here, first we call Gatsby's createPage hook. We create a promise where we specify the data we want to fetch inside, specifically our portfolio data.
  </p>

  <p>
    When that promise resolves, or it gets the data we're looking for, we check to make sure there's no errors and if successful we loop through our portfolioData to create a page for each one found.
  </p>

  <p>
    The <strong>createPage</strong> function takes an object where we specify the path, in this case it matches the path we're updating to in our portfolio on click. We then tell it which component to render. Everything is happening in our <strong>index.js</strong> file, so we tell it to use that.
  </p>

  <p>
    Finally, we provide the <strong>context</strong> as <strong>currentProjectToOpen</strong>, which is what we'll apply some logic to inside our <strong>index.js</strong> component. If we restart our server and navigate to one of our portfolio pages, then refresh we notice that the url doens't stay...but if we check our <strong>IndexPage</strong> component in <strong>React Devtools</strong> we notice that we have a new prop passed in called <b>pageContext</b>:
  </p>

  <img src="/images/jamstack-jedi/portfolio--create-page.png" class="jamstack--img" alt="Gatsby createPage" />

  <p>
    We now have a <strong>pathContext</strong> prop, and inside of it, we have our <strong>currentProjectToLoad</strong> object with the corresponding data inside it. Pretty neat. Now all we have to do is add our logic to update the single item and push to the url again.
  </p>

  <p>
    We can now add some logic to get our page context and pass it down as a prop, as well as update the url on page load from <b>index.js</b>:
  </p>

  <Markdown
    rel="index.js"
    language="javascript"
    md={`  ...
  const IndexPage = ({ pageContext }) => {
    ...
    const [supportsWebP, setSupportsWebP] = useState(true);
    const [currentProjectToOpen, setCurrentProjectToOpen] = useState(null);

    // will run once, when the component mounts
    useEffect(() => {
      // check if we can use webP images
      setSupportsWebP(/Chrome/.test(window.navigator.userAgent) && /Google Inc/.test(window.navigator.vendor));

      // after the page load, lets check if we should load a portfolio item right away
      if (pageContext.currentProjectToOpen) setCurrentProjectToOpen(pageContext.currentProjectToOpen);
    }, []);
    ...
    <Portfolio currentProjectToOpen={currentProjectToOpen} portfolioData={data.site.siteMetadata.portfolioData} />
    ...`}
  />

  <p>
    We've stored our currentProjectToOpen in a state var and we pass it down to <strong>Portfolio</strong>. This is where we already have logic to update the url and load the single item, so we can add some more logic to reuse that.
  </p>

  <p>
    The easiest way to do this, would be to create a separate <strong>useEffect</strong> block that only triggers when a current project is passed in, and set our item to the incoming <strong>currentProjectToOpen</strong>:
  </p>

  <Markdown
    rel="Portfolio.js"
    language="javascript"
    md={`  ...
  const Portfolio = ({ portfolioData, currentProjectToOpen }) => {
    ...
    useEffect(() => {
      if (currentProjectToOpen) setPortItem(currentProjectToOpen);
    }, [currentProjectToOpen]);


    useEffect(() => {
      if (portItem) {
        window.history.pushState(null, null, \`/project/\${portItem.alt.toLowerCase().replace(/ /g, '-').toLowerCase()}\`);
      } else {
        window.history.pushState(null, null, '/');
      }
    }, [portItem]);
    ...`}
  />

  <p>
    Give it a spin, and now when we have a url like <strong>/project/nerd-fit</strong>, we see the url update and the single item get pulled in. If we refresh with a single item open, it'll be shown after the page refreshes. If we close, it goes back to our root, click another and the url will update again!
  </p>

  <hr />

  <p>
    That's it for now! We've finished our portfolio, and it's all easy going from here, in my opinion. That was probably the hardest part of the whole tutorial.
  </p>

  <p>
    After the last few lessons, we're due for something easy, so lets work on our about section, which is pretty much just standard HTML.
  </p>

    <AniLink to="jamstack-jedi/build-about" className="nextBtn">Start Building The About Section &raquo;</AniLink>
    <br />
  </Layout>
);

export default PortfolioBuild;
