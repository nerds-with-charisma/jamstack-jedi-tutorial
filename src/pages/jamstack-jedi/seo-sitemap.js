import React from 'react';
import AniLink from '../../components/Common/AniLink';

import Layout from '../../components/layout';
import Markdown from '../../components/Common/markdown';

const Sitemap = () => (
  <Layout
    title="sitemap.xml"
    author={{ gravatar: '//0.gravatar.com/avatar/b5eff8d62adf3ff34e67f153781d27c4' }}
    gitRepo="https://gitlab.com/nerds-with-charisma/jamstack-jedi/tree/master/seo-sitemap"
    time={5}
    description="Learn how to create an xml sitemap in GatsbyJs"
    keywords="sitemap.xml, xml sitemap, setup sitemap.xml, create sitemap.xml, GatsbyJs sitemap.xml"
  >
  <strong>
    X Marks the Spot
  </strong>

  <br />
  <br />

  <p>
    Similar to how the <b>robots.txt</b> tells bots what directories and files they should crawl, the sitemap tells them exactly where they are. Taking any guesswork out of the crawlers job. You can think of it like a mall directory, the crawler sees <b>sitemap.xml</b> and reads it, saying this page is "HERE" that page is "THERE". Instead of having to go through our whole site searching link by link to find all our pages, the sitemap file tells the crawler where everything is at right off the bat.
  </p>

  <p>
    Also similar to the robots plugin, there's a <a id="anchor--gatsby-sitemap" href="https://www.gatsbyjs.org/docs/creating-a-sitemap/" target="_blank">Gatsby plugin</a> for automatically generating our sitemap (you should go through this docuemnt as well). So lets install that:
  </p>

  <Markdown
    rel="Terminal"
    language="terminal"
    md={`  npm install --save gatsby-plugin-sitemap`}
  />

  <p>
    Just like the robots config, we do all our sitemap config inside of <b>gatsby-config.js</b>. First we have to register the plugin:
  </p>

  <Markdown
    rel="gatsby-config.js"
    language="javascript"
    md={`  ...
  plugins: [
    \`gatsby-plugin-sitemap\`,
    ...`}
  />

  <p>
    Then we simply tell it our website's url and it will handle the rest whenever we do a build:
  </p>

  <Markdown
    rel="gatsby-config.js"
    language="javascript"
    md={`  module.exports = {
  siteMetadata: {
    siteUrl: \`https://nerdswithcharisma.com\`,
    homepageData: {
    ...`}
  />

  <p>
    Perform a build and we should now see <b>sitemap.xml</b> in our root.
  </p>

  <img src="/images/jamstack-jedi/seo--sitemap.png" class="jamstack--img" alt="Generated sitemap.xml file" />

  <p>
    You can see it also created paths for our portfolio items, so any new portfolio item we add will automatically be added to the sitemap on each build! That also goes for any new pages we create in the <b>/src/pages/</b> directory or programatically in <b>gatsby-node.js</b>
  </p>

  <p>
    Creating the sitemap is only part of the equation. We also need to tell Google and other search engines where it's at in their respective webmaster tools. We can't do that locally however, so we'll put that off until the end once we've deployed our site to production.
  </p>

  <p>
    In the next chapter, the last one for our SEO part 1 section, we'll add some extra meta data that will help search engines identify us as well as add schem markup.
  </p>

  <AniLink to="jamstack-jedi/seo-meta" className="nextBtn">Add Some Extra Meta Data &raquo;</AniLink>
  <br />
  </Layout>
);

export default Sitemap;
