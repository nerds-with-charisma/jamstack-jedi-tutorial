import React from 'react';
import AniLink from '../../components/Common/AniLink';

import Layout from '../../components/layout';

import Markdown from '../../components/Common/markdown';
import YouTube from '../../components/Common/YouTube';

const HeroBuild = () => (
  <Layout
    title="Hero Component"
    author={{ gravatar: '//0.gravatar.com/avatar/b5eff8d62adf3ff34e67f153781d27c4' }}
    gitRepo="https://gitlab.com/nerds-with-charisma/jamstack-jedi/tree/master/build-hero"
    time={20}
    description={null}
    keywords={null}
  >

    <strong>Lets Start Building!</strong>
    <br />
    <br />

    <p>
      This section will be pretty similar to our coming soon page, but with a few changes. For starters, we have our style library to help us while building they layout. So lets start by creating a hero folder in our components directory:
    </p>

    <Markdown
      rel="Terminal"
      language="terminal"
      md={
        `  mkdir src/components/hero`}
    />

    <p>
      And we can create our <b>Hero.js</b> component inside that folder:
    </p>

    <Markdown
      rel="Terminal"
      language="terminal"
      md={
        `  > src/components/hero/Hero.js`}
    />

    <p>
      And lets add some basic content so we can see that it's working:
    </p>

    <Markdown
      rel="src/components/hero/Hero.js"
      language="javascript"
      md={
        `  import React from 'react';

  const Hero = () => (
    <h1>Hero Component</h1>
  );

  export default Hero;`}
    />

    <p>
      And now lets import it into our <b>index.js</b> component:
    </p>

    <Markdown
      rel="src/pages/index.js"
      language="javascript"
      md={
        `  ...
  import Hero from '../components/hero/Hero';
  import SEO from '../components/seo';
  ...`}
    />

    <p>
      And now we can use it below our SEO component:
    </p>

    <Markdown
      rel="src/pages/index.js"
      language="javascript"
      md={
        `  ...
  <SEO
    title={data.site.siteMetadata.homepageData.title}
    lang={data.site.siteMetadata.homepageData.lang}
    meta={data.site.siteMetadata.homepageData.meta}
  />
  <Hero />
  ...`}
    />

    <p>
     Now, when we save it and restart our server, we should see our hero component rendering:
    </p>

    <img src="/images/jamstack-jedi/hero--init.png" class="jamstack--img" alt="Hero Init" />

    <hr />

    <p>
      There are 6 main todo's in our hero component: the background image, the large "N", our services &amp; the tagline, the contact button, and the social links.
    </p>

    <p>
      <b>First Todo</b> is to get our background image loaded. You can grab the bg image we'll be using at <a href="https://gitlab.com/nerds-with-charisma/jamstack-jedi/blob/master/build-hero/src/images/bg--hero.jpg" target="_blank">our GitLab repo</a> or supply your own. The suggested dimensions are 1900 x 1518.
    </p>

    <p>
      We've got a style library that we created previously, however there are certain cases where things are going to have to be custom, and this is one of them. So lets create a hero sass file:
    </p>

    <Markdown
      rel="terminal"
      language="Terminal"
      md={
        `  > src/styles/_hero.scss`}
    />

    <p className="tldr">
      Note the underscore in the name, <b>_hero.scss</b>, this indicates a "partial" or minor file to be imported into a major sass file. We could technically do this for our other files that aren't main.scss as well, however I've chosen to leave these without the _ so that we can quickly identify which files are for our style library and which are component files.
    </p>

    <p>
      Now, import it into <b>main.scss</b>:
    </p>

    <Markdown
      rel="main.scss"
      language="scss"
      md={
        `  @import 'hero.scss';`}
    />

    <p>
      Let start by laying out our hero HTML inside of <b>hero.js</b>:
    </p>

    <Markdown
      rel="src/components/hero/Hero.js"
      language="javascript"
      md={
        `  const Hero = () => (
    <section id="hero" className="font--light">
      <span id="N">
        {'N'}
      </span>
      <div>
        <h2 className="font--125">
          {'websites.'}
          <br />
          {'apps.'}
          <br />
          {'seo.'}
          <br />
          {'better.'}
        </h2>
        <aside className="font--40">
          {'Quote placeholder'}
        </aside>
        <button
          type="button"
          className="btn bg--light font--dark font--24 padding-horiz-xl radius--lg border--none block no--wrap"
          onClick={() => alert('todo: scroll to footer')}
        >
          <strong>
            {'Get In Touch'}
          </strong>
        </button>
      </div>
    </section>
  );`}
    />

    <p className="tldr">
      You'll notice the first thing we do is give the section an id of "hero" so that we can style it directly. We also give it a class of font--light from our style library to make the font white, since it will be on a dark background.
    </p>

    <p className="tldr">
      Next we have an id for "N" which will be our large N in the background.
    </p>

    <p className="tldr">
      Below that is our h2 of all our keywords, as well as a palceholder for our random quote, we also specify the font sizes for both.
    </p>

    <p className="tldr">
      Finally we have a button (with a todo on the on click event for later) where we specify the styling on the button.
    </p>

    <hr />

    <p>
      It doesn't look pretty yet, so lets change that by adding some styles, lets open up <b>_hero.scss</b> and add some base styles:
    </p>

    <Markdown
      rel="_hero.scss"
      language="scssP"
      md={
        `  #hero {
    background-color: $primary; /* set a default background color */
    background-size: cover !important; /* make the background image "cover" the available area */
    display: flex; /* set our flex display property */
    min-height: 300px; /* make sure it's at least 300px tall */
    padding: 20px 0; /* add some extra padding to the top and bottom */
    align-items: center; /* vertically align content */
    overflow: hidden; /* hide anything that breaks outside, specifically the large N */
    position: relative; /* give it relative positioning so that we can absolutely position our inner content */

    #N {
      position: absolute; /* set it so we can move it off screen a bit */
      z-index: 9; /* make the z-index or layering slightly higher than normal */
      font-size: 1200px; /* set the font size to something very big */
      font-weight: 800; /* make it bold */
      color: rgba(0, 0, 0, 0.4); /* set it to black with an opacity of 40% */
      left: -200px; /* nudge it offscreen to the left */
      top: -900px; /* nudge it offscreen on top */
    }

    h2 {
      line-height: 110px; /* shrink our line height so it looks closer together */
      font-weight: bold; /* bold the font */
    }

    div {
      position: relative; /* position it relative */
      z-index: 99; /* set the z-index slightly above our large N */
      padding: 120px 10%; /* give it some padding on top and bottom as well as 10% from the left (and right but you won't see it) */
    }

    aside {
      font-weight: 300; /* make our font thin */
      padding: 30px 0; /* add some padding to the top and bottom */
    }

    button {
      padding: 15px 70px; /* add some padding around the button */
    }
  }`}
    />

    <img src="/images/jamstack-jedi/hero--styles.png" class="jamstack--img" alt="Hero Styles" />

    <hr />

    <p>
      Comments are inline here, nothing should look much different than our coming soon page. We didn't set a background image yet, because we have one very specific thing with Gatsby that we can do here.
    </p>

    <p>
      For starters, lets just get our image appearing in the background, open up <b>hero.js</b> and lets add some inline styles:
    </p>

    <Markdown
      rel="src/components/hero/Hero.js"
      language="javascript"
      md={`  ...
    import HeroBg from '../../images/bg--hero.jpg';
    ...
    <section id="hero" className="font--light" style={{
      background: \`url(\${HeroBg}) no-repeat center center fixed\`
    }}>
  ...`}
    />

    <p className="tldr">
      Here we're setting the background to cover the entire available area and stay fixed as the user scrolls. We're importing the image as a "HeroBg" up at the top, and you'll see why in just a second.
    </p>

    <p>
      Gatsby lets us use some pretty neat image processing tools to determine the best available image to use at the time. In this case, we could determine to use a jpg by default, and if the browser supports it, a compressed and smaller <a id="anchor--webp" href="https://insanelab.com/blog/web-development/webp-web-design-vs-jpeg-gif-png/" target="_blank">webp image</a>.
    </p>

    <p>
      So how do we do that? I must admit that the <a id="anchor--gatsby-image" href="https://www.gatsbyjs.org/packages/gatsby-image/" target="_blank">Gatsby image docs</a> are pretty lacking here and it took me a while to figure it out, but this is how I got it working.
    </p>

    <p>
      First we need to kill our server and install a package (there are other packages that are required, but our starter theme already included them).
    </p>

    <Markdown
      rel="Terminal"
      language="terminal"
      md={
        `  npm install --save gatsby-image`}
    />

    <p>
      Next we need to import a few deps into Hero.js, also remove the <b>HeroBg import</b> as we will use gatsby-image instead:
    </p>

    <Markdown
      rel="Hero.js"
      language="javascript"
      md={
        `  import React from 'react';
  import { StaticQuery, graphql } from 'gatsby';
  ...`}
    />

    <p>
      Next we need to wrap our component in the StaticQuery tag like we did for index.js:
    </p>

    <Markdown
      rel="Hero.js"
      language="javascript"
      md={
        `  const Hero = () => (
    <StaticQuery
      query={}
      render={(data) => (
        <section id="hero" className="font--light" style={{
          ...
        </section>
      )}
    />
  );
  ...`}
    />

    <p>
      And then we need to form our query,
    </p>

    <Markdown
      rel="Hero.js"
      language="javascript"
      md={
        `  ...
  <StaticQuery
  query={ graphql\`
    query HeroQuery {
      fileName: file(relativePath: { eq: "bg--hero.jpg" }) {
        childImageSharp {
          sizes(maxWidth: 1900, maxHeight: 1518) {
            src
            srcWebp
          }
        }
      }
    }
  \`}
  ...`}
    />

    <p className="tldr">
      This is a little odd for a query, but we're basically asking the gatsby file system to look in our images directory (which is already setup in gatsby-config.js) for a file called <b>bg--hero.jpg</b>. We want it at our default size of 1900 x 1518. And we'd like the default src for the jpg file and the src for the webp file which gatsby creates for us.
    </p>

    <p>
      Which we can now use in our background image:
    </p>

    <Markdown
      rel="Hero.js"
      language="javascript"
      md={`  ...
  <section id="hero" className="font--light" style={{
    background: \`url(\${data.fileName.childImageSharp.sizes.src}) no-repeat center center fixed\`
  }}>
...`}
    />

    <p>
      In order to determine if we can use the webp format, we'll do a little check in our index component and pass it down via props. Open up <b>index.js</b> and modify the React import:
    </p>

    <Markdown
      rel="index.js"
      language="javascript"
      md={`  import React, { useState, useEffect } from 'react';`}
    />

    <p>
      We're going to use react hooks to test if the browser is able to support webp images. Then we'll set it to true/false and pass it down to our child components:
    </p>

    <Markdown
    rel="Hero.js"
    language="javascript"
    md={
      `  const IndexPage = () => {
    const [supportsWebP, setSupportsWebP] = useState(true);

    return (
      <StaticQuery
        ...
      />
    )
  };

  export default IndexPage;`}
  />

    <p>
      We restructure our return statement by adding a curly bracket and an explicit return statement so that we can add a state variable for <b>supportsWebP</b> and we default it to true.
    </p>

    <p>
      Next we want to actually test to make sure webp is usable:
    </p>

    <Markdown
      rel="index.js"
      language="javascript"
      md={`  ...
  const [supportsWebP, setSupportsWebP] = useState(true);

  // will run once, when the component mounts
  useEffect(() => {
    // check if we can use webP images
    setSupportsWebP(/Chrome/.test(window.navigator.userAgent) && /Google Inc/.test(window.navigator.vendor));
  }, []);
      ...`}
    />

    <p>
      We run use effect on the first time loading (the old way like component did mount) and do a regex to test if it's a browser type that supports webp. We use the <b>setSupportsWebP</b> hook function to then set the bool based on what the regex returns.
    </p>

    <p>
      Finally, we need to pass this down to our hero component:
    </p>

    <Markdown
      rel="index.js"
      language="javascript"
      md={`  ...
  <Hero supportsWebP={supportsWebP} />
      ...`}
    />

    <p>
      Then in our Hero component, we can add some logic to determine which file to use:
    </p>

    <Markdown
      rel="hero.js"
      language="javascript"
      md={`  ...
  const Hero = ({ supportsWebP }) => (
    ...
    <section id="hero" className="font--light" style={{
      background: \`url(\${(supportsWebP ? data.fileName.childImageSharp.sizes.srcWebp : data.fileName.childImageSharp.sizes.src)}) no-repeat center center fixed\`
    }}>
      ...`}
    />

    <p>
     If supportsWebP is true, we load the srcWebp file, if not we load the jpg image instead. You can test this by checking the src in Chrome vs Firefox (Firefox at the time of this tut does not support webP).
    </p>

    <p className="tldr">
      That was a lot of reconfiguring so please check the <a href="https://gitlab.com/nerds-with-charisma/jamstack-jedi/tree/master/build-hero" target="_blank">final files on our GitLab</a> if you get lost.
    </p>

    <hr />

    <p>
      The last few things we need to do is add our social icons and our random quote generator. But at this point, our hero file is getting pretty robust, and I think it makes sense to break it up into smaller components. Lets do that in the next chapter.
    </p>

    <AniLink to="jamstack-jedi/build-hero-2" className="nextBtn">The Hero Component Continued &raquo;</AniLink>
    <br />
  </Layout>
);

export default HeroBuild;
