import React from 'react';
import AniLink from '../../components/Common/AniLink';

import Layout from '../../components/layout';

import Markdown from '../../components/Common/markdown';

const GA = () => (
  <Layout
    title="Google Analytics"
    author={{ gravatar: '//0.gravatar.com/avatar/b5eff8d62adf3ff34e67f153781d27c4' }}
    gitRepo="https://gitlab.com/nerds-with-charisma/jamstack-jedi/tree/master/seo-ga"
    time={10}
    description="Learn Google Analytics, how to setup google Analytics. Google Analytics basics."
    keywords="Learn Google Analytics, Google Analytics Basics, Get better at Google Analytics, Google Analytics 101, Intro to Google Analytics"
  >

  <strong>The Who, What, Where</strong>
  <br />
  <br />

  <p>
    In the last chapter we installed <b>Google Tag Manager</b>. In a previous section, we added <b>Google Analytics Tracking</b> to our coming soon page. We want tracking on this page as well, so lets grab our GA ID from the coming soon section (or sign up). If you didn't go through the coming soon section, or lost your ID, it can be found by logging into <a href="https://analytics.google.com" target="_blank">Google Analytics</a>, and navigating to <b>Admin > Tracking Info > Tracking Code</b>.
  </p>

  <p>
    Lets copy that ID, something like "UA-141701162-1", and head on over to <b>GTM</b>. Leave preview mode if you're still there from the last chapter and click on <b>Tags</b>.
  </p>

  <p>
    Click <b>New</b>, and name it "Google Analytics", then click on the <b>Tag Configuration</b> panel and choose <b>Google Analytics: Universal Analytics</b>.
  </p>

  <p>
    Make sure "Track Type" is <b>Page View</b> and choose "New Variable" from <b>Google Analytics Settings</b> dropdown. Paste our GA ID into the <b>tracking id</b> input and hit <b>Save</b>. You can name it <b>Google Analytics Settings</b>.
  </p>

  <p>
    When you try to save the tag, it'll tell you that you need to add a "trigger". Click <b>Add Trigger</b> and choose <b>All Pages</b>. If you'd like to read more about the different default trigger types, you can check <a id="anchor--gtm-triggers" href="https://support.google.com/tagmanager/topic/7679108?hl=en&ref_topic=7679384" target="_blank">this article</a>. Now save the tag and click <b>Publish</b>.
  </p>

  <p>
    Now, go back to GA and check the <b>Realtime > Overview</b> section and you should see 1 active user on your page. You can also confirm in the network panel by filtering by your id:
  </p>

  <img src="/images/jamstack-jedi/ga--network.png" class="jamstack--img" alt="Install Google Analytics via Google Tag Manager" />

  <hr />

  <p className="tldr">
    We could be done  here, but we want to track whenever someone visits our portfolio pages too, right? We also would probably like to know how many people use our navigation to scroll to certain sections. We can add events to track these as well.
  </p>

  <p>
    To track portfolio clicks, it's actually really simple in GTM. We just need to add another trigger for <b>History Change</b>. Which means anytime the url updates, we fire our tracking code again.
  </p>

  <p>
    Head back to GTM and choose <b>Tags</b> and pick our <b>Google Analytics</b> tag again. Click the <b>Triggers Panel</b> and click the plus sign. In the new panel that opens up there will be another plus sign at the top right, click this and we can name our new trigger <b>History Change</b>.
  </p>

  <p>
    Click <b>Trigger Configuration</b> and click on the <b>History Change</b> event. <b>Save</b> it, <b>Save</b> our tag as well, and <b>Publish</b> everything.
  </p>

  <p>
    Refresh our page and open up the <b>console</b>, and type:
  </p>

  <Markdown
    rel="Chrome Console"
    language="terminal"
    md={`  window.dataLayer`}
  />

  <p>
    Hit enter and you should see a few items, this is everything we're sending to GTM. Scroll down to one of our portfolio items and click it. The url updates, and if we console log the data layer again, we should see a new node added on for a history change. If we twirl it down we see our page has been tracked in GA.
  </p>

  <img src="/images/jamstack-jedi/ga--history-change.png" class="jamstack--img" alt="Virtual page view, google tag manager" />

  <p>
    And if we look in GA, we see our virtual page view counted as a realtime page view:
  </p>

  <img src="/images/jamstack-jedi/ga--realtime-history.png" class="jamstack--img" alt="Virtual page view, google analytics" />

  <hr />

  <p>
    The last thing we want to accomplish is to setup a page view event when someone clicks on a nav link. Just to let us know how many people are navigating through our site by the navigation. To do this, we can apply some code to our
  </p>

  <p>
    Open up <b>Nav.js</b> and go inside the <b>scrollToSection</b> function. In here we'll update the url with a hash to signal we're scrolling down to an anchor:
  </p>

  <Markdown
    rel="Nav.js"
    language="javascript"
    md={`  const scrollToSection = (link) => {
    setIsOpen(false);

    window.dataLayer.push({
      event: 'navClick',
      navClickLabel: link,
    });

    document.getElementById(link).scrollIntoView({ behavior: 'smooth' });
  }`}
  />

  <p>
    When this click function fires, we will now be doing a push to the GTM dataLayer, with an event we've named "navClick" and a custom variable called "navClickLabel" which will populate our link value.
  </p>

  <p>
    Now, to get that tracking in GA we need to make a small change in GTM. Go to <b>Variables</b> and click <b>New</b> under "user defined variables". Name it <b>navClick</b>, choose <b>Data Layer Variable</b>
  </p>

  <p>
  Close this panel and click <b>New</b> under "user defined variables". And name it <b>URL from Fragment</b>. Choose <b>Custom JS Variable</b> and add the "variable name" will match what we pushed to the dataLayer in our code, so <b>navClickLabel</b> then <b>save</b>.
  </p>

  <p>
    Now head to triggers, and click <b>new</b>, name this <b>navClick</b> as well, choose <b>custom event</b> and the "event name" will also be <b>navClick</b>.
  </p>

  <p>
    Finally, we have to make a tag for this, choose <b>new</b> and name it also <b>navClick</b>. The "tag type" is <b>Google Analytics: Universal Analytics</b>, track type: "event", category: "Navigation", action: "Click", label: {`\{\{navClick}}`}, google analytics settings: {`\{{Google Analytics Settings}}`}".
  </p>

  <p>
    Then choose the trigger to be <b>navClick</b>. Save everything and publish.
  </p>

  <img src="/images/jamstack-jedi/ga--navClick.png" class="jamstack--img" alt="Custom event in GTM to GA" />

  <p>
    Try it out. Refresh our page and click on a nav item. If you head to GA and go to <b>Real time > Events</b>, you should see our events firing and tracking!
  </p>

  <img src="/images/jamstack-jedi/ga--event-realtime.png" class="jamstack--img" alt="Realtime event in GA" />

  <p>
    Now that we have GTM &amp; GA wired up we can move on to our next task, setting up a custom 404 page.
  </p>

  <AniLink to="jamstack-jedi/seo-404" className="nextBtn">Gatsby 404 Page &raquo;</AniLink>
  <br />
  </Layout>
);

export default GA;