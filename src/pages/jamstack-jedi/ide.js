import React from 'react';

import AniLink from '../../components/Common/AniLink';
import Markdown from '../../components/Common/markdown';
import Layout from '../../components/layout';
import YouTube from '../../components/Common/YouTube';

const IDE = () => (
  <Layout
    title="Editor"
    author={{ gravatar: '//0.gravatar.com/avatar/b5eff8d62adf3ff34e67f153781d27c4' }}
    gitRepo="https://gitlab.com/nerds-with-charisma/jamstack-jedi/tree/master/coming-soon-html-start"
    time={4}
    description="Choosing an IDE, or editor and how to setup VS Code"
    keywords="VS Code, Best front-end editor, VS Code setup"
  >
    <strong>Choose Wisely</strong>
    <br />
    <br />
    <p>
      I'm going to start by assuming you have zero knowledge on how to develop. To get started, you'll need a few things installed on your machine. The very first thing you need installed is an editor (or IDE - Integrated Development Environment). This is where you'll write the code that will create your website.
    </p>
    <p className="tldr">
      There are lots of options out there for editors and everyone has their own preference. Technically, you could start writing code with whatever text editor came with your computer. But there are better options.
    </p>

    <p id="anchor--vs-code" className="tldr">
      If you'd like to follow along with us, we'll be using <b>VS Code</b> (aka Visual Studio Code) developed by Microsoft.
    </p>
    <p className="tldr">
      Some alternatives that work great are: <a href="https://atom.io/" target="_blank">Atom by Github</a>, <a href="https://www.sublimetext.com/" target="_blank">Sublime Text</a>, and <a href="http://brackets.io/" target="_blank">Brackets by Adobe</a>. Feel free to play around with any of these and choose the one that best fits your preference.
    </p>
    <p>
      I enjoy VS Code because it's very stable, it has an integrated terminal for running commands, the layout is intuitive, and there are tons of plugins.
    </p>
    <br />
    <hr />
    <br />
    <strong>Installing VS Code</strong>
    <br />
    <br />
    <p>Installing VS Code is easy, simply go to <a href="https://code.visualstudio.com/" target="_blank">https://code.visualstudio.com/</a> and click the large download button.</p>

    <img src="/images/jamstack-jedi/editor--install.jpg" class="jamstack--img" alt="Install VS Code" />
    <p className="tldr">
      Once your download is complete, open up the zip file, double click the installer file and VS Code should open up with a blank project.
    </p>

    <div className="font--12">
      <b>Note:</b> If you get an warning saying that OSX cannot open files from unknown developers you can check <a href="https://stackoverflow.com/questions/32960282/visual-studio-code-cant-be-opened-because-the-identity-of-the-developer-cannot-b" target="_blank" className="font--primary">this thread from stack overflow</a> for more info about why you get this warning
    </div>

    <br />

    <img src="/images/jamstack-jedi/editor--vscode.jpg" class="jamstack--img" alt="Open VS Code" />

    <hr />
    <br />
    <p>
      Lets go ahead and make our first file in VS Code, we'll do this with the integrated terminal by clicking typing <code>ctrl + ~</code>
    </p>
    <p className="tldr">
      You should see the terminal docked to the bottom of your screen now. Navigate to where you want to start your coming soon project and make a new directory:
    </p>

    <img src="/images/jamstack-jedi/editor--terminal.png" class="jamstack--img tldr" alt="VS Code Open Terminal" />

    <Markdown
      rel="Terminal"
      language="terminal"
      md={
        `  mkdir jamstack-jedi`
      }
    />

    <p>
      You've now created the root folder for your project, but it's not loaded into VS Code per say. To do that, go to <b>File > Open</b> and choose the folder you just created. You may have to open the terminal again after loading the new folder with ctrl + ~
    </p>

    <p id="anchor--settings-json" className="tldr">
      Now lets make our first file, a settings file for VS Code to override some of the default settings for VS Code. These are all just preferential, but will put us all on the same setup.
    </p>

    <Markdown
      rel="Terminal"
      language="terminal"
      md={
        `  mkdir .vscode
  > .vscode/settings.json`
      }
    />

    <p className="tldr">
      You should now see a settings file in the Explorer panel in VS Code. Click on it to open it so we can edit this file.
    </p>

    <img src="/images/jamstack-jedi/editor--settings.png" class="jamstack--img" alt="VS Code Create settings.json" />

    <p>
      Lets add our first bit of code. We'll keep it really simple for now, we can expand later if need be. Add the following code to your newly created <u>settings.json</u> file and save it.
    </p>

    <Markdown
      rel="settings.json"
      language="javascript"
      md={
        `  {
    "editor.tabSize": 2,
    "workbench.colorCustomizations": {
      "titleBar.activeBackground": "#FF2C70",
      "titleBar.inactiveBackground": "#FF2C70CC",
    }
  }`
      }
    />

    <p className="tldr">
      "editor.tabSize" changes our spacing from 4 to 2 spaces. Even if you hit the tab button, it will still convert it automatically to space! I prefer 2 spaces to 4, but you can change this value to your personal preference.
    </p>

    <p className="tldr">
      The next few lines are a convention of VS Code that allow us to change the title bar's color. I like to do this so it's apparent which project I'm working on. You can change this for every project based on the main color you're using and it should be super evident which window you're in.
    </p>

    <img src="/images/jamstack-jedi/editor--settings-added.png" class="jamstack--img" alt="VS Code Create settings.json applied" />

    <p>
      One more thing we need to do before we move on, is create our project's entry point file. By default this is called <b>index.html</b>.
    </p>
    <div className="font--12">
      Note: Why "index.html"? Because in the early days of the internet it was thought we'd only be browsing directories. If you wanted to create a list of your own, you could overwrite the default directory listing with an index.html file. This isn't technically a standard, but most hosting providers look for this or a similar file to know what to run. <a href="https://www.lifewire.com/index-html-page-3466505" target="_blank">Lifewire explains it really well.</a>
    </div>

    <Markdown
      rel="Terminal"
      language="terminal"
      md={
        `  > index.html`
      }
    />

    <img src="/images/jamstack-jedi/editor--index.png" class="jamstack--img" alt="VS Code Create index.html" />

    <p>
      Lets finish up this chapter by adding our first bit of HTML code to this file.
    </p>

    <Markdown
      rel="index.html"
      language="html"
      md={
        `  <!DOCTYPE html>
  <html lang="en">
    <head>
      <title>Jamstack Jedi - Coming Soon</title>
    </head>
    <body>
      <h1>Hello Nerds!</h1>
    </body>
  </html>`
      }
    />

    <p className="tldr">
      We'll go into more detail about what's happening here in the HTML chapter, but for now, save your changes and that's it! You've made your first HTML file and are on your way to making your first website.
    </p>

    <p className="tldr">
      The goal of this whole series won't be to teach you everything about front-end development, it's to provide you with real world examples and experience. However, I want this to be an exhaustive <u>resource</u> where you can find more info on what we do so that you can understand what's going on. There's lots of content out there covering every minute detail, which is explained way better than I ever could. So I will provide exteranl links/videos/reading that I <b>HIGHLY</b> suggest you go through to dive deeper into what we're doing.
    </p>
    <p>
      Our first learning resource comes from <a id="anchor--vs-code-youtube" href="https://youtu.be/fnPhJHN0jTE" target="_blank">Traversy Media's VS Code Tutorial</a>. Again, I highly suggest you go through any content that  is linked out to, but it is not required.
    </p>

    <YouTube url="https://www.youtube.com/embed/fnPhJHN0jTE" />

    <br />
    <br />
    <AniLink to="jamstack-jedi/browser" className="nextBtn">Picking a Browser &raquo;</AniLink>
    <br />
  </Layout>
);

export default IDE;
