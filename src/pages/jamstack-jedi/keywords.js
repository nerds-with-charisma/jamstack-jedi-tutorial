import React from 'react';
import AniLink from '../../components/Common/AniLink';

import Layout from '../../components/layout';
import Markdown from '../../components/Common/markdown';

const Keywords = () => (
  <Layout
    title="Keywords SEO"
    author={{ gravatar: '//0.gravatar.com/avatar/b5eff8d62adf3ff34e67f153781d27c4' }}
    gitRepo="https://gitlab.com/nerds-with-charisma/jamstack-jedi/tree/master/keywords"
    time={20}
    description="Keyword analysis and keyword brainstorming to determine target or focus keywords"
    keywords="Keyword analysis, keyword brainstorming, focus keywords, target keywords"
  >
    <strong>
      Keywords Are King
    </strong>
    <br />
    <br />

    <p>
      Before even starting this chapter, I want to be completely clear. Keyword research is a long and indepth process. I wouldn't say hard, but it can be a full-time job itself. That being said, I'm going to give you a VERY basic concept for keyword brainstorming. This is just the tip of the iceberg. If you really get serious about it, I would suggest you check out <a href="https://neilpatel.com/" target="_blank">Neil Patel's</a> site and <a href="https://www.youtube.com/channel/UCl-Zrl0QhF66lu1aGXaTbfw" target="_blank">YouTube</a> channel. This dude is, in my opinion, the best at explaining things in simple and practical terms.
    </p>

    <p>
      Keyword research for SEO is an important aspect of ranking well on search engines. There are lots of competitors out there for generic keywords like "Web Development", but there's a lot fewer if you add your city to the keyword, like "Bartlett Web Design". At this point, we should probably already be ranking for our website name, especially if your company name is unique, you should be very close to the top and hopefully on the first page.
    </p>

    <p>
      And that's great, but we also want to rank for other works like your name, and some local services. The first thing we should do is <a id="anchor--keyword-brainstorm" href="https://mangools.com/blog/keyword-research/" target="_blank">brainstorm</a>, lets figure out 10 keywords that we'd like to rank with. We'll do this to boost our on-site optimization.
    </p>

    <p>
      For us, we for sure want to rank for our brand name, "Nerds with Charisma". We also want to rank for our names, "Brian Dausman" and "Andrew Bieganski". Next we want to rank for some services like, "Web Development", "Awesome Websites", "Full-stack For Hire", "Websites for non-profits", and finally, just for some fun to prove you can rank rather quickly for unique terms "Bitchin websites".
    </p>

    <p>
      We can analyze each of these keywords to see where we currently rank on Google by using the <a id="anchor--small-seo-tools" href="https://smallseotools.com/keyword-position/" target="_blank">Small SEO Tools Keyword Position Checker</a>. Simply enter our domain and enter in all these keywords to see if and where we rank.
    </p>

    <img src="/images/jamstack-jedi/local--rankings.png" class="jamstack--img" alt="Google Rankings" />

    <p>
      We can see we're already ranking pretty well for some of these. Our company name, we're number 2 (behind our npm repo). Our names are on pages 1 and 2. Now we can compare the terms not ranking to their difficulty by using Google's <a id="anchor--keyword-planner" href="https://ads.google.com/home/tools/keyword-planner/" target="_blank">Keyword Planner</a>. We can enter our keywords into tool and see the "cost" per keyword.
    </p>

    <p>
      Generally speaking, the more a word costs, the harder it would be to rank. If an item costs nothing, it's either VERY specific or not worth targeting. Web development, obviously, is very high in cost. Looking at the others on our list, we can decide that our names are still in, Awesome websites have some traction, and so does Websites for non-profits. The others, not so much...so lets rethink our strategy, terms like "React Developers" instead of Full-Stack for hire have a better chance of getting results. And we can try a more local approach by saying "Web Design Chicago" which is the closest large city to us.
    </p>

    <p>
      Now we have a sold set of keywords we want to target, this is just the beginning, we should always be researching and changing these, but for now we'll start building our strategy in the code. To recap, we'll go with the following keywords: nerds with charisma, Brian Dausman, Andrew Bieganski, Web Design Chicago, Awesome Websites, React Developers, Websites for non-profits, Bitchin websites
    </p>

    <p>
      The first thing we want to do is target our keywords in our page title (title's should be about 65 characters). Open up <strong>gatsby-config.js</strong> and lets modify our site's title to include our names:
    </p>

    <Markdown
      rel="gatsby-config.js"
      language="javascript"
      md={`  title:  'Nerds With Charisma - Awesome Websites : Brian Dausman & Andrew Bieganski',`}
    />

    <p>
      Next we want to make sure all our keywords are used in our description (160 characters max) as well as listed in our keywords meta:
    </p>

    <Markdown
      rel="gatsby-config.js"
      language="javascript"
      md={
      `  ...
  {
    name: 'description',
    content: 'Nerds With Charisma are React developers making awesome websites and websites for non-profits, handcrafted by Brian Dausman & Andrew Biegasnki. Web Design Chicago',
  },
  {
    name: 'keywords',
    content: 'Nerds with Charisma, Brian Dausman, Andrew Bieganski, Web Design Chicago, Awesome Websites, React Developers, Websites for Non-Profits, Bitchin Websites',
  },
  ...`}
    />

    <p>
      Next we want to make sure that our content has headings in it that contain our keywords, even though we might already have our keywords in our content, it doesn't help unless we put it in a heading.
    </p>

    <p>
      This involves modifying some of the ways we pull in data which we'll walk through each individually. For starters we should have an H1 tag with our company name in it, which we did in <strong>Logo.js</strong>, so that's great.
    </p>

    <p>
      Next we should have an h2 element for each of our keywords, which we can do without effecting our page layout/design by adding a special class to our <strong>helpers.scss</strong> file:
    </p>

    <Markdown
      rel="helpers.scss"
      language="scss"
      md={
      `  ...
  .seo--heading {
    font-size: inherit;
    display: inline;
    margin: 0;
    padding: 0;
    color: inherit
  }
  ...`}
    />

    <p>
      This should reset any styles being applied to the heading element, which will allow us to use it in paragraphs and similar. So now we want to modify our copy to have these headings sprinkled throughout. The first, most logical place to add some keywords is our services tagline in <strong>gatsby-config.js</strong>:
    </p>

    <Markdown
      rel="gatsby-config.js"
      language="javascript"
      md={
      `  ...
  tagline: 'Here are some of the ways <h2 class="seo--heading">Nerds With Charisma</h2> can help you build an <h2 class="seo--heading">awesome website</h2>',
  ...`}
    />

    <p>
      Then we have to make a minor tweak to <b>services-heading.js</b> and <b>portfolio-heading.js</b> to be able to display html:
    </p>

    <Markdown
      rel="services-heading.js"
      language="javascript"
      md={
      `  ...
  <strong className="font--48">
    {'SERVICES'}
  </strong>
  <br />
  <div dangerouslySetInnerHTML={{ __html: tagline }} />
  ...`}
    />

    <Markdown
      rel="portfolio-heading.js"
      language="javascript"
      md={
      `  ...
  <strong className="font--48">
    {'PORTFOLIO'}
  </strong>
  <br />
  <div dangerouslySetInnerHTML={{ __html: tagline }} />
  ...`}
    />

    <p>
      Cool, we'll do the same in a few more places in <strong>gatsby-config.js</strong> then modify in the appropriate files:
    </p>

    <Markdown
      rel="gatsby-config.js"
      language="javascript"
      md={
      `  ...
    portfolioData: {
      tagline: 'Checkout some of the <h2 class="seo--heading">awesome websites</h2> and apps as well as <h2 class="seo--heading">websites for non-profits</h2> we\'ve worked on',
    ...
    where: {
      image: '/portfolio/maeby.jpg',
      alt: 'That\'s our project manager and QA expert, Maeby, she\'s in charge of making sure we continue to make awesome websites.',
      body: 'We provide development and <h2 class="seo--heading">web design to Chicago</h2> and the surrounding suburbs. We do lots of local work, but are totally willing to do long distance remote work, so long as you\'re ok with it. We\'re also very comfortable working with people pretty much anywhere in the world. We\'re previously worked with people in England, India, South America, and Israel.<br /><br />We also really enjoy doing websites for nonprofits. We can work with you and your budget to get your projects rolling. Simply shoot us an email below, and we\'ll let you know what we can do.',
    },
    ...
    signOff: \`
      <strong>
       Designed &amp; Developed By
      <br />
      <a href="/docs/brian-dausman-resume-2019.pdf">
        <h2 class="font--16 font--primary inline">
          brian dausman
        </h2>
      </a>
       +
      <a href="http://www.andrewbieganski.com/images/andrewbieganski2018.pdf" target="_blank" rel="noopener noreferrer">
        <h2 class="font--16 font--primary inline">
          andrew bieganski
        </h2>
      </a>
    </strong>\`,
    ...
    footerTagline: 'NWC making <h2 class="seo--heading">bitchin websites</h2> since the 90s!',
  ...`}
    />

    <p>
      Then we need to modify our outputs to render HTML not static text:
    </p>

    <Markdown
      rel="Footer.js"
      language="javascript"
      md={
      `  ...
  <span className="font--text opacity8">
    <div dangerouslySetInnerHTML={{ __html: footerTagline }} />
  </span>
  ...`}
    />

    <hr />

    <p>
      Now that we've added headings to our copy we want to verify that our images have good enough alt texts:
    </p>

    <Markdown
      rel="gatsby-config.js"
      language="javascript"
      md={
      `  ...
      alt: 'That\\'s me, Brian Dausman, founder of Nerds with Charisma and React Developer',
      ...
      alt: 'That\\'s Andrew Bieganski, he\\'s been with with Nerds with Charisma for years helping build some truely awesome websites!',
      ...
      alt: 'That\\'s our project manager and QA expert, Maeby, she\\'s in charge of making sure we continue to make bitchin websites. She prefers when we do websites for non-profits',
      ...
  ...`}
    />

    <p>
      That is about all the on-page optimization we need to do for now, I really like running a <a id="anchor--woo-rank" href="https://www.woorank.com/en/www/nerdswithcharisma.com" target="_blank">WooRank audit</a> at this point and making sure we've got as much green as possible. If you've been following along this whole time, you should be in pretty good shape. If not, please fix any issues you see before moving on.
    </p>

    <p>
      The next chapter is kind of frifrivalous, but it's kind of neat. We'll cover how to add a "Buy me a coffee" button so people can send you money if they appreciate your content.
    </p>

    <AniLink to="jamstack-jedi/coffee" className="nextBtn">Buy Me a Coffee &raquo;</AniLink>
    <br />
  </Layout>
);

export default Keywords;
