import React from 'react';
import AniLink from '../../components/Common/AniLink';

import Layout from '../../components/layout';

import Markdown from '../../components/Common/markdown';

const BuildFooter = () => (
  <Layout
    title="Footer Component"
    author={{ gravatar: '//0.gravatar.com/avatar/b5eff8d62adf3ff34e67f153781d27c4' }}
    gitRepo="https://gitlab.com/nerds-with-charisma/jamstack-jedi/tree/master/build-footer"
    time={20}
    description={null}
    keywords={null}
  >
  <strong>
    We've Made It!
  </strong>

  <br />
  <br />

  <p>
    We're almost done with our <b>Build</b> section. We just have to put in our footer component, which should be pretty simple. We'll start by making our files
  </p>

  <Markdown
    rel="Terminal"
    language="terminal"
    md={`  mkdir src/components/footer

  > src/components/footer/Footer.js`}
  />

  <p>
    As usual, we'll add some data to <b>gatsby-config.js</b> below our title, we'll grab it in <b>index.js</b> and pass it down to our component:
  </p>

  <Markdown
    rel="gatsby-config.js"
    language="javascript"
    md={` ...
  title: \`Nerds With Charisma\`,
  signOff: \`
  <strong>
   Designed &amp; Developed By
  <br />
  <a href="/docs/brian-dausman-resume-2019.pdf">
    <h2 class="font--16 font--primary inline">
      <strong>brian dausman</strong>
    </h2>
  </a>
    +
  <a href="/docs/andrew-bieganski-resume-2019.pdf" target="_blank" rel="noopener noreferrer">
    <h2 class="font--16 font--primary inline">
      <strong>andrew bieganski</strong>
    </h2>
  </a>
</strong>\`,
  footerTagline: 'NWC making bitchin websites since the 90s!',
    ...`}
  />

  <p>
    Add our query to <b>index.js</b>:
  </p>

  <Markdown
    rel="index.js"
    language="javascript"
    md={` ...
    title
    signOff
    footerTagline
    ...`}
  />

  <p>
    Import our footer component, and pass down our two props:
  </p>

  <Markdown
    rel="index.js"
    language="javascript"
    md={` ...
  import Footer from '../components/footer/Footer';
  ...
  <Contact />
  <Footer title={data.site.siteMetadata.title} signOff={data.site.siteMetadata.signOff} footerTagline={data.site.siteMetadata.footerTagline} />
  ...`}
  />

  <hr />

  <p>
     And then we layout our <b>Footer</b> component:
  </p>

  <Markdown
    rel="Footer.js"
    language="javascript"
    md={`  import React from 'react';
  import { PropTypes } from 'prop-types';

  import SocialLinks from '../common/social-links';

  const Footer = ({ title, signOff, footerTagline }) => (
    <section id="footer" className="container text-center padding-lg lh-md font--16 position--relative">
      <strong>
        <div dangerouslySetInnerHTML={{ __html: signOff || '' }} />
      </strong>

      <br />
      <br />

      <SocialLinks />

      <br />
      <br />

      <small className="font--text">
        {\`copyright © \${new Date().getFullYear()} \${title.toLowerCase()}\`}
        <br />
        <span className="font--light">
          {footerTagline}
        </span>
      </small>
    </section>
  );

  Footer.propTypes = {
    title: PropTypes.string.isRequired,
    signOff: PropTypes.string.isRequired,
    footerTagline: PropTypes.string.isRequired,
  };

  export default Footer;`}
  />

  <p>
    You might notice that our social links aren't showing up. If you remember, we pulled our social links into our <b>hero</b> component and passed it down, but wouldn't it make more sense to do that in the <b>SocialLink</b> component itself now that we're reusing it? Of course it would, so lets do that, open up <b>SocialLinks.js</b> and lets modify it:
  </p>

  <Markdown
    rel="Footer.js"
    language="javascript"
    md={` import React from 'react';
  import { PropTypes } from 'prop-types';
  import { StaticQuery, graphql } from 'gatsby';

  const SocialLinks = () => (
    <StaticQuery
      query={ graphql\`
        query SocialQuery {
          site {
            siteMetadata{
              socialLinks {
                link
                icon
                title
              }
            }
          }
        }
      \`}

      render={(data) => (
        <section className="social-links">
          { data.site.siteMetadata.socialLinks.map((link) => (
            <a key={link.title} className="font--grey padding-md" href={link.link} target="_blank">
              <i className={\`\${link.icon} font--24\`}>
              <div className="ir">
                {link.title}
              </div>
            </i>
          </a>
        ))}
      </section>
    )}
    />
  );

  export default SocialLinks;`}
  />

  <p>
    Now we can remove that query and the props from <b>Hero.js</b> because we don't need it specifically there anymore:
  </p>

  <Markdown
    rel="Hero.js"
    language="javascript"
    md={` ...
  query={ graphql\`
    query HeroQuery {
      fileName: file(relativePath: { eq: "bg--hero.jpg" }) {
        childImageSharp {
          sizes(maxWidth: 1900, maxHeight: 1518) {
            src
            srcWebp
          }
        }
      }

      site {
        siteMetadata{
          heroData {
            contactButtonText
            heroCopy
            randomQuote
          }
        }
      }
    }
  \`}
  ...
  <SocialLinks />
  ...`}
  />

  <p>
    And now, our social links are showing up in both places! But they're in the bottom right for the footer, when we actually want them centered. We can fix that by specifying the hero styles specifically. Open up <b>_hero.scss</b>, but the social icons declaration and move it up just inside of the closing curly brace for our hero styles:
  </p>

  <Markdown
    rel="_hero.scss"
    language="javascript"
    md={` #hero {
    background-color: $primary; /* set a default background color */
    background-size: cover !important; /* make the background image "cover" the available area */
    display: flex; /* set our flex display property */
    min-height: 300px; /* make sure it's at least 300px tall */
    padding: 20px 0; /* add some extra padding to the top and bottom */
    align-items: center; /* vertically align content */
    overflow: hidden; /* hide anything that breaks outside, specifically the large N */
    position: relative; /* give it relative positioning so that we can absolutely position our inner content */

    h2 {
      line-height: 110px; /* shrink our line height so it looks closer together */
      font-weight: bold; /* bold the font */
    }

    div {
      position: relative; /* position it relative */
      z-index: 99; /* set the z-index slightly above our large N */
      padding: 120px 10%; /* give it some padding on top and bottom as well as 10% from the left (and right but you won't see it) */
    }

    aside {
      font-weight: 300; /* make our font thin */
      padding: 30px 0; /* add some padding to the top and bottom */
    }

    button {
      padding: 15px 70px; /* add some padding around the button */
    }

    .social-links {
      position: absolute; /* so that we can position at the bottom of our hero */
      bottom: 30px; /* 30 pixels from the bottom */
      right: 30px; /* 30 from the right */
      height: 30px; /* make sure it's only 30px high */
      z-index: 999; /* put it a layer above all the other hero elements */

      a {
        padding: 15px; /* give it some breathing room for tapping */
      }
    }
  }

  ...`}
  />

  <img src="/images/jamstack-jedi/footer--layout.png" class="jamstack--img" alt="Footer layout" />

  <hr />

  <p>
    The last thing we need to do, is tweak some styles on our links, and then to link our resumes. When creating our style library, we made a helper class for "block" but we never added one for inline, lets add that now:
  </p>

  <Markdown
    rel="_helpers.scss"
    language="javascript"
    md={` .inline {
    display: inline;
  }`}
  />

  <p>
    That will finish up our styles, don't forget to upload your resume to a place online and use an absolute url or upload it to a <b>/docs</b> folder like we have in the <b>static directory</b>.
  </p>

  <p>
    And that's it, that's our portfolio build. Now we can get into some optimizations, SEO, and other important parts of being a Front End developer!
  </p>

    <AniLink to="jamstack-jedi/seo-1" className="nextBtn">SEO Part 1 &raquo;</AniLink>
    <br />
  </Layout>
);

export default BuildFooter;
