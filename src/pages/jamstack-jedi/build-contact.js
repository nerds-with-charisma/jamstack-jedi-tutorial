import React from 'react';
import AniLink from '../../components/Common/AniLink';

import Layout from '../../components/layout';

import Markdown from '../../components/Common/markdown';

const Contact = () => (
  <Layout
    title="Contact Component"
    author={{ gravatar: '//0.gravatar.com/avatar/b5eff8d62adf3ff34e67f153781d27c4' }}
    gitRepo="https://gitlab.com/nerds-with-charisma/jamstack-jedi/tree/master/build-contact"
    time={18}
    description={null}
    keywords={null}
  >

  <strong>What's that Now?</strong>
  <br />
  <br />

  <p className="tldr">
    We're getting pretty close to finishing our build. We have two main sections left, the contact and the footer. In this section we'll build the contact portion in this chapter, however, we won't actually hook up any functionality to it. As we'll be using <b>MailChimp</b> to handle our signups, we will cover that in a later chapter.
  </p>

  <p className="tldr">
    That means this section will pretty much be a layout lession very similar to the last. We do want to revisit one of our "todos" from a previous chapter, and wire up our hero contact button to scroll us to this section when clicked.
  </p>

  <p>
    We'll start by creating our files, and skeleton components:
  </p>

  <Markdown
    rel="Terminal"
    language="terminal"
    md={`  mkdir src/components/contact

  > src/components/contact/Contact.js

  > src/styles/_contact.scss`}
  />

  <Markdown
    rel="main.scss"
    language="scss"
    md={`  ...
  @import 'contact.scss';`}
  />

  <Markdown
    rel="Contact.js"
    language="javascript"
    md={`  import React from 'react';

  const Contact = () => (
    <section id="contact" className="text-center bg--offLight padding-lg">
      <h1>Contact</h1>
    </section>
  );

  export default Contact;`}
  />

  <Markdown
    rel="index.js"
    language="javascript"
    md={`  ...
  import Contact from '../components/contact/Contact';
  ...
    <About aboutData={data.site.siteMetadata.aboutData} />
    <Contact />
  ...`}
  />

  <img src="/images/jamstack-jedi/contact--skeleton.png" class="jamstack--img" alt="Contact Skeleton layout" />

  <hr />

  <p>
    Now that we have everything wired up, lets build the component out.
  </p>

  <Markdown
    rel="Contact.js"
    language="javascript"
    md={`  import React, { useState } from 'react';

  const Contact = () => {
    const [email, setEmail] = useState(null);
    const [message, setMessage] = useState(null);

    const handleSubmit = (e) => {
      e.preventDefault();
      console.log(email, message);
    };

    return (
      <section id="contact" className="text-center bg--offLight padding-lg">
        <strong className="font--48">
          {'What can we help you with?'}
        </strong>
        <br />
        <br />
        <form id="contactForm" onSubmit={(e) => handleSubmit(e)}>
          <label htmlFor="email">
            <input
              type="email"
              id="email"
              name="email"
              placeholder="Your Email"
              onChange={(e) => setEmail(e.target.value)}
            />
          </label>
          <label htmlFor="message">
            <input
              type="message"
              id="message"
              name="message"
              placeholder="Your Message"
              onChange={(e) => setMessage(e.target.value)}
            />
          </label>

          <button type="submit" className="btn btn--gradient">
            {'Contact Us!'}
          </button>
        </form>
      </section>
    )
  };

  export default Contact;`}
  />

  <p className="tldr">
    We import <b>useState</b>, where we set our state variables for "email" and "message" inside our component declaration.
  </p>

  <p className="tldr">
    Next we have our section with a light grey background, and we build our form inside. There's an <b>onSubmit</b> event handler on the form so that it will submit whenever the user clicks the submit button or hits enter.
  </p>

  <p className="tldr">
    The <b>handleSubmit</b> function itself isn't doing much right now, simply console logging out our state variables. Which we're setting with the <b>onChange</b> even on each input.
  </p>

  <hr />

  <p>
    Now that we have a contact section, lets wire up our hero contact button and add the contact button from the mockups to the services section in <b>hero-contact.js</b>:
  </p>

  <Markdown
    rel="hero-ontact.js"
    language="javascript"
    md={`  ...
  <button
    type="button"
    className="btn bg--light font--dark font--24 padding-horiz-xl radius--lg border--none block no--wrap"
    onClick={() => document.getElementById('contact').scrollIntoView({ behavior: 'smooth' })}
  >
  ...`}
    />

    <p>
      We used similar logic earlier in our nav, now we're applying it to our hero button. We'll do similar to the <b>services-logos.js</b> file, right below our map:
    </p>

    <Markdown
    rel="services-logos.js"
    language="javascript"
    md={`  ...
    <button
      type="button"
      className="btn btn--gradient font--light font--21 radius--lg border--none block no--wrap col-12"
      onClick={() => document.getElementById('contact').scrollIntoView({ behavior: 'smooth' })}
    >
      <strong>
        {'Need More Info? Reach Out!'}
      </strong>
    </button>
  ...`}
    />

    <p>
      Now we just need to style that button, we can do that in <b>_theme.scss</b> since it will also be in our contact component:
    </p>

    <Markdown
    rel="Contact.js"
    language="javascript"
    md={`  ...
  .btn--gradient {
    background: linear-gradient(52deg, rgba(144,18,254,1) 0%, rgba(242,51,185,1) 100%);
    font-size: 16px;
    border-radius: 300px;
    padding: 12px 35px;
    color: #fff;
    font-weight: bold;
    border: none;
    box-shadow: 0 4px 15px rgba(0, 0, 0, 0.2);
    transition: all 0.3s linear;
    margin: 20px 0;
    max-width: 100%;
    display: inline-block;
    clear: both;

    &:hover {
      background: rgb(144,18,254);
      box-shadow: 0 8px 28px rgba(0, 0, 0, 0.3);
    }
  }
  ...`}
    />

    <img src="/images/jamstack-jedi/contact--button.png" class="jamstack--img" alt="Contact Button Gradient Styles" />

    <hr />

    <p>
      Now we need to style the forms in the contact section, open up <b>_contact.scss</b> and lets add some styles for the inputs:
    </p>

    <Markdown
    rel="Contact.js"
    language="javascript"
    md={`  #contact {
    input {
      margin-right: 20px;
      padding: 15px;
      border: none;
      border-radius: 7px;
      width: 300px;
      box-shadow: 0 4px 15px rgba(0, 0, 0, 0.2);
      font-size: 16px;
    }
  }`}
    />

    <img src="/images/jamstack-jedi/contact--design.png" class="jamstack--img" alt="Contact Form" />

    <p>
      The last thing we need to do in this section is handle some basic validation, make sure they put in a valid email, and that there's some sort of message. First we'll make a state var to keep track of the error message, then we'll add some logic in our <b>handleSubmit</b> function to set the message if we have an issue. Open up <b>Contact.js</b> and modify to the following:
    </p>

    <Markdown
    rel="Contact.js"
    language="javascript"
    md={`  ...
    const [messageError, setMessageError] = useState(null);

    ...
    const handleSubmit = (e) => {
      e.preventDefault();

      setMessageError(null);// remove the message each time they submit

      const regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      if (!regex.test(email)) {
        setMessageError('Please enter a valid e-mail.');
        return false;
      }

      if (!message || message.length < 2) {
        setMessageError('Please enter a valid message.');
        return false;
      }

      // todo: everything's good, we'll submit to mailchimp in a later chapter
      console.log(email, message);
    };
  ...`}
    />

    <p className="tldr">
      We are performing a regex to check that our email has some normal characters first, then an @ sign, then some more character, then a period and some more characters, or [characters]@[characters].[characters]. If not, we throw an error.
    </p>

    <p className="tldr">
      We do a simpler check to make sure that the message is filled out, and that it's at least 2 characters, so you have to at least say "Hi" in order to submit.
    </p>

    <p className="tldr">
      If everything is all good, we console log out our state vars, which we'll later send to mailchimp.
    </p>

    <img src="/images/jamstack-jedi/contact--error.png" class="jamstack--img" alt="Contact Error" />

    <hr />

    <p>
      Then we add the message to our form:
    </p>

    <Markdown
    rel="Contact.js"
    language="javascript"
    md={`  ...
  { (messageError)
    && (
      <span class="bg--cta font--light padding-sm">
        { messageError }
      </span>
    )
  }
  <form id="contactForm" onSubmit={(e) => handleSubmit(e)}>
  ...`}
    />

    <p>
      That's it for our contact section, for now, lets move on to our final main section, the Footer!
    </p>

    <AniLink to="jamstack-jedi/build-footer" className="nextBtn">The Final Component &raquo;</AniLink>
    <br />
  </Layout>
);

export default Contact;
