import React from 'react';
import AniLink from '../../components/Common/AniLink';

import Layout from '../../components/layout';

import Markdown from '../../components/Common/markdown';

const PortfolioAbout = () => (
  <Layout
    title="About Component"
    author={{ gravatar: '//0.gravatar.com/avatar/b5eff8d62adf3ff34e67f153781d27c4' }}
    gitRepo="https://gitlab.com/nerds-with-charisma/jamstack-jedi/tree/master/build-about"
    time={10}
    description={null}
    keywords={null}
  >

  <strong>What About Me?</strong>

  <br />
  <br />

  <p className="tldr">
    In the <b>About</b> section we really don't have to do anything new. Create our folder &amp; files, add some data to <b>gatsby-config.js</b>, pull that in, and layout our page. All stuff we've done before. So lets quickly run through all those steps.
  </p>

  <p>
    First create our "about" folder and files:
  </p>

  <Markdown
    rel="Terminal"
    language="terminal"
    md={`  mkdir src/components/about

  > src/components/about/About.js`}
  />

  <p>
    Next we can add our data to <b>gatsby-config.js</b> below our <b>portfolioData</b> node:
  </p>

  <Markdown
    rel="gatsby-config.js"
    language="javascript"
    md={`  ...
  aboutData : {
    letter: 'C',
    who: {
      image: '/portfolio/brian.jpg',
      alt: 'That\\'s me, Brian Dausman, founder of Nerds with Charisma',
      body: '<div>Nerds With Charisma are a small band of designers, developers, and creatives. Our unique process involves a hands on, back-and-forth approach to get your project off the ground and into the wild exactly as you envision it.<br /><br />Find out how we can bring fresh ideas and a new approach to your company. We don\\'t just build your website, we build your brand and cultivate your online identity. We collaborate closely with you to learn your business, discover new opportunities, and bring your ideas to life.</div>',
    },
    what: {
      image: '/portfolio/andy.jpg',
      alt: 'That\\'s Andrew Bieganski, he\\'s been with with Nerds with Charisma for years!',
      body: '<div>We utilize the latest technologies & trends to get your site setup and rocking. Our help doesn\\'t stop at just a website. Sure, we can help design & develop your site, but we will also discuss and explain your user\\'s experience, come up with a content strategy for continued success, get you going on social media, explain the new technologies, go over analytics, and get your SEO juice flowing.<br /><br /></div>',
    },
    where: {
      image: '/portfolio/maeby.jpg',
      alt: 'That\\'s our project manager and QA expert, Maeby, she\\'s in charge of making sure we continue to make awesome websites.',
      body: 'We\\'re located in the suburbs outside of Chicago, IL. We do lots of local work, but are totally willing to do long distance remote work, so long as you\\'re ok with it. We\\'re also very comfortable working with people pretty much anywhere in the world. We\\'re previously worked with people in England, India, South America, and Israel.<br /><br />We also really enjoy doing websites for nonprofits. We can work with you and your budget to get your projects rolling. Simply shoot us an email below, and we\\'ll let you know what we can do.',
    },
  },
  ...`}
  />

  <p className="tldr">
    Our data contains 3 sections, each with an image, an alt text for the image, and the body copy.
  </p>

  <p>
    To use it, lets pull it in to <b>index.js</b> in our GraphQL query below our "portfolioData" node:
  </p>

  <Markdown
    rel="index.js"
    language="javascript"
    md={`  ...
    aboutData {
      letter
      who {
        image
        alt
        body
      }
      what {
        image
        alt
        body
      }
      where {
        image
        alt
        body
      }
    }
  ...`}
  />

  <p>
    Also in <b>index.js</b>, we'll import our about component and pass down our props:
  </p>

  <Markdown
    rel="index.js"
    language="javascript"
    md={`  ...
  import Portfolio from '../components/portfolio/Portfolio';
  import About from '../components/about/About';
  ...
    <Portfolio currentProjectToOpen={currentProjectToOpen} portfolioData={data.site.siteMetadata.portfolioData} />
    <About aboutData={data.site.siteMetadata.aboutData} />
  ...`}
  />

  <p>
    And the skeleton for our <b>About.js</b> file:
  </p>

  <Markdown
    rel="About.js"
    language="javascript"
    md={`  import React from 'react';
  import { PropTypes } from 'prop-types';
  import LazyLoad from 'react-lazyload';

  import LargeLetter from '../common/large-letter';

  const About = ({ aboutData }) => (
    <section id="about" className="container position--relative overflow--container">
      <h1>About</h1>
    </section>
  );

  About.propTypes = {
    aboutData: PropTypes.object.isRequired,
  };

  export default About;`}
  />

  <img src="/images/jamstack-jedi/about--skeleton.png" class="jamstack--img" alt="Skeleton layout for about section" />

  <hr />

  <p>
    Now it's just a matter of laying out our component's actual content. We're also importing the <b>React LazyLoad</b> plugin here since we'll wrap our images with it and the LargeLetter component:
  </p>

  <Markdown
    rel="About.js"
    language="javascript"
    md={`  <section id="about" className="container position--relative overflow--container">
    <span className="opacity1">
      <LargeLetter letter={aboutData.letter} />
    </span>
    <div className="container position--relative">
      <div className="grid">
        <div className="col-12 col-lg-4 text-center">
          <br />
          <br />
          <div>
            <LazyLoad offset={200}>
              <img
                alt={aboutData.who.alt}
                className="radius--lg"
                src={aboutData.who.image}
              />
            </LazyLoad>
          </div>
        </div>
        <div className="col-12 col-lg-8 text-center lh-md">
          <strong>
            {'WHO'}
          </strong>
          <hr className="shorty" />
          <br />
          <div dangerouslySetInnerHTML={{ __html: aboutData.who.body }} />
        </div>

        <div className="col-12 col-lg-8 text-center lh-md">
          <br />
          <br />
          <br />
          <strong>
            {'WHAT'}
          </strong>
          <hr className="shorty" />
          <br />
          <div dangerouslySetInnerHTML={{ __html: aboutData.what.body }} />
        </div>
        <div className="col-12 col-lg-4 text-center lh-md">
          <br />
          <br />
          <br />
          <div>
            <LazyLoad offset={200}>
              <img
                alt={aboutData.what.alt}
                className="radius--lg"
                src={aboutData.what.image}
              />
            </LazyLoad>
          </div>
        </div>

        <div className="col-12 col-lg-4 text-center">
          <br />
          <br />
          <div>
            <LazyLoad offset={200}>
              <img
                alt={aboutData.where.alt}
                className="radius--lg"
                src={aboutData.where.image}
              />
            </LazyLoad>
          </div>
        </div>
        <div className="col-12 col-lg-8 text-center lh-md">
          <strong>
            {'WHERE'}
          </strong>
          <hr className="shorty" />
          <br />
          <div dangerouslySetInnerHTML={{ __html: aboutData.where.body }} />
        </div>
      </div>
    </div>
  </section>`}
  />

  <p className="tldr">
    You'll notice we're using the <b>dangerouslySetInnerHTML</b> node here, because our about copy could potentially have HTML in it.
  </p>

  <img src="/images/jamstack-jedi/about--layout.png" class="jamstack--img" alt="About layout" />

  <hr />

  <p>
    The last thing we need to do is tweak the styles a little bit, since if we adjust the browser size, our images don't shrink down properly. Lets create that file and import it:
  </p>

  <Markdown
    rel="Terminal"
    language="terminal"
    md={` > src/styles/_about.scss`}
  />

  <Markdown
    rel="main.scss"
    language="scss"
    md={` ...
  @import 'portfolio.scss';
  @import 'about.scss';
    ...`}
  />

  <p>
    We only need some minor styles here to make things look good:
  </p>

  <Markdown
    rel="_about.scss"
    language="scss"
    md={` #about {
    img {
      width: 50%;
      margin: 25px auto 0 auto;
    }
  }`}
  />

  <hr />

  <img src="/images/jamstack-jedi/about--layout-final.png" class="jamstack--img" alt="About layout final" />

  <p>
    That's it for our <b>About</b> component, now that we have several sections added, we can see there's some spacing issues compared to our mockup, specifically "services" and "about" should have a larger gap between them. So lets add that style in <b>_theme.scss</b>:
  </p>

  <Markdown
    rel="_about.scss"
    language="scss"
    md={` ...
  #services, #about {
    padding-top: 200px;
    padding-bottom: 200px;
  }`}
  />

  <p>
    Looking good. That's it for our <b>About</b> section. Lets move on to our next section, the <b>Contact</b> section.
  </p>

    <AniLink to="jamstack-jedi/build-contact" className="nextBtn">Contact Section is Next &raquo;</AniLink>
    <br />
  </Layout>
);

export default PortfolioAbout;
