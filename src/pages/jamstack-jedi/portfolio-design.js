import React from 'react';
import AniLink from '../../components/Common/AniLink';

import Layout from '../../components/layout';

import YouTube from '../../components/Common/YouTube';

const PortfolioDesign = () => (
  <Layout
    title="Design Overview"
    author={{ gravatar: '//0.gravatar.com/avatar/b5eff8d62adf3ff34e67f153781d27c4' }}
    gitRepo="https://gitlab.com/nerds-with-charisma/jamstack-jedi/blob/master/mockup--NWC-portfolio.xd"
    time={8}
    description="Overview of the Adobe XD portfolio design we've created."
      keywords="Adobe XD, Design a Portfolio in Adobe XD"
  >
    <strong>What We'll Be Building</strong>
    <br />
    <br />

    <p>
      Go ahead and download the design file from our GitLab  above, and take a look at it. I built it in <a id="anchor--xd-download" href="https://www.adobe.com/products/xd.html" target="_blank">Adobe XD</a>, which is a free design and prototyping program, so you don't need to pay for anything in order to go through it and see what's going on.
    </p>

    <p>
      If you're interested in learning Adobe XD there are a lot of great tutorials on YouTube, specifically I like <a id="anchor--xd-tutorial" href="https://www.youtube.com/watch?v=5tzP2CyCLvQ&list=PLuQZzG-z6zBJvcSTgDleHPDyqRUXYkA6c" target="_blank">this tutorial series</a> by Howard Pinsky as it shows off a lot of XD's powerful time saving tools.
    </p>

    <YouTube url="https://www.youtube.com/embed/5tzP2CyCLvQ" />

    <br />
    <p className="tldr">
      Lets go through our design before we start building it.
    </p>

    <p>
      First we have our header with a menu icon to bring up our navigation as well as our logo on the right side.
    </p>

    <img src="/images/jamstack-jedi/design--header.png" class="jamstack--img" alt="Design Header" />

    <p>
      When you click on the hamburger menu on the left, we want to open up a slide panel with anchors to our sections.
    </p>

    <img src="/images/jamstack-jedi/design--menu.png" class="jamstack--img" alt="Design Menu" />

    <hr />

    <p>
      Our header will be done with a fixed position, meaning that as we scroll down, it will travel with us. It will overlay each section of our portfolio.
    </p>

    <p>
      Next is the <strong>Hero</strong> component. It should look familiar to you if you went through the comming soon section. We've added a little bit more to it and we will be rotating through a tagline now instead of just the main text.
    </p>

    <p>
      We still have a contact button, but we will be linking this up with an email form later on instead of directly composing an email. Finally we've added our sharing icons to the bottom right for some contrast, balance, as well as usability since most people prob won't bother checking out our footer.
    </p>

    <img src="/images/jamstack-jedi/design--hero.png" class="jamstack--img" alt="Design Hero" />

    <p>
     One other thing you might notice is that we have our company initials ghosting in the background. This is a design embelishment that will use positioning with CSS, you can skip this if it doesn't make sense with your brand or initials.
    </p>

    <hr />

    <p>
      The next section is a list of our services that we offer as well as some logos representing the technologies and tools we're familiar with.
    </p>

    <img src="/images/jamstack-jedi/design--services.png" class="jamstack--img" alt="Design Services" />

    <p>
      Each of the services will give a little zoom effect when you hover over them, just to add a touch of flair. We also add another contact button that will link up with our email form. Since the main goal of our site's conversion would be to get someone in touch with us, we want to reaffirm this through the design. We also make this a fancy looking gradient button!
    </p>

    <hr />

    <p>
      The next section is our portfolio, where we will showcase previous projects we've worked on. We will display them in a masonry grid. We won't go too much into our portfolio here, as this will be one of the larger chapters we'll cover it later. Just note that we'll have a little animation effect when you hover over each portfolio item.
    </p>

    <img src="/images/jamstack-jedi/design--portfolio.png" class="jamstack--img" alt="Design Portfolio" />

    <hr />

    <p>
      After the portfolio, we have our about component. Which will be some info about us personally and as a company. Nothing too fancy here, just some light reading if the user wants to know more about us.
    </p>

    <img src="/images/jamstack-jedi/design--about.png" class="jamstack--img" alt="Design About" />

    <p>
     After the about section, we have our contact area, which will be a mailing form linked up to our <strong>MailChimp</strong> account. We'll talk more about that when too we get to it.
    </p>

    <img src="/images/jamstack-jedi/design--contact.png" class="jamstack--img" alt="Design Contact" />

    <hr />

    <p>
      Finally we have our footer, with all our personal info, which links to our resumes and social media. We also have similar copyright info here like we did in the coming soon section.
    </p>

    <img src="/images/jamstack-jedi/design--footer.png" class="jamstack--img" alt="Design Footer" />

    <hr />

    <p>
      So there you have it, our design broken down by section. Feel free to checkout the finished product on <a href="https://nerdswithcharisma.com" target="_blank">our website</a>. We'll also cover stacking things on mobile as we go.
    </p>

    <AniLink to="jamstack-jedi/build-hero" className="nextBtn">Build the Hero Component &raquo;</AniLink>
    <br />
  </Layout>
);

export default PortfolioDesign;
