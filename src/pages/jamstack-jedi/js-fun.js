import React from 'react';
import AniLink from '../../components/Common/AniLink';

import Layout from '../../components/layout';
import Markdown from '../../components/Common/markdown';

const Fun = () => (
  <Layout
    title="Javascript Fun"
    author={{ gravatar: '//0.gravatar.com/avatar/b5eff8d62adf3ff34e67f153781d27c4' }}
    gitRepo="https://gitlab.com/nerds-with-charisma/jamstack-jedi/tree/master/js-fun"
    time={45}
    description="Learn how to look up users location with reverse geolocation, detect user keyboard inputs, and user mouse location"
    keywords="Geolocation, reverse address lookup, google address lookup, javascript keyboard inputs, javascript mouse detection"
  >

    <strong>
      Lets Have Some Fun
    </strong>

    <br />
    <br />

    <p>
      This chapter will have a few fundimentals in them that you can choose to keep or skip/comment out. We'll show you how to get the users location and get a human readable address, detect keyboard interaction, and finally find out if they're about to close the browser.
    </p>

    <p>
      Some of these (alerts on close) can be considered annoyances and we're not condoning using them the way we show you, instead you can come up with a better use case based on the underlying principals we show you.
    </p>

    <strong id="anchor--geolocation">User Location</strong>
    <br />
    <br />
    <p>
      First up, geolocation, which is a fairly new API that allows us to ask for lat/long of the users location. This will be more accurate on mobile but still fairly accurate for desktop. In order to do this we first need to get permission from the user, then execute a callback function, finally we can handle the data.
    </p>

    <p>
      First up, what is geolocation? It's basically the browser/device allowing us to know where the user is located by way of lat/long. Which by itself might not be the best info, but combined with another service like Google maps, can give us a lot of great info like where the user is located by way of city/state/zip. Which IS handy info for targeting specific regions with specific code. Think targeting appliances in the United States in early winter...If you're in Chicago you might want to show users heaters or snowblowers, but Floridians probably don't need those things so you could show them yard tools or lawn mowers.
    </p>

    <p>
      Getting the users geolocation is a 3 part process, ask for permission, handle the callback, do something with the data. For our example, we'll do something pretty neat to customize the users experience. We'll use the geolocation API and a mapping API to get the city the user is from, we'll then display that in the "where" section saying we're willing to do work (remotely) near them.
    </p>

    <p>
      To start lets open up the <b>index.js</b> file and lets add the skeleton code for getting lat/long and console log it out:
    </p>

    <Markdown
      rel="index.js"
      language="javascript"
      md={
      `  ...
  // will run once, when the component mounts
  useEffect(() => {
    ...
    // get the users lat/long
    const geolocation = new Promise((resolve, reject) => {
      navigator.geolocation.getCurrentPosition(
        position => { resolve(position.coords) },
        error => { reject(error) }
      )
    }).catch(error => error);

    geolocation.then((data) => {
      console.log(data);
    });
  ...`}
    />

    <p>
      We add a check to see if geolocation is possible at the bottom of our useEffect hook. If the browser allows it, we run the showPosition function which will contain our coordinates data. Note that the user will be asked if they want to allow our website to know their location:
    </p>

    <img src="/images/jamstack-jedi/fun--geolocation.png" class="jamstack--img" alt="Allow Geolocation" />

    <img src="/images/jamstack-jedi/fun--geo-results.png" class="jamstack--img" alt="Geolocation Results" />

    <hr />

    <strong id="anchor--reverse-geocoding">Reverse Geocoding</strong>
    <br />
    <br />
    <p>
      Now that we have the user's location we want to be able to reverse geocode it to get the address details so that we can show the users city to them. There are lots of services that offer this, however the most well known is Google, but it's also one of the most complex to setup initially. We'll try our best to cover how to setup access to an API, if you have any issues please go through Google's docs on getting an api key.
    </p>

    <p>
      First you'll need to login to <a href="https://console.developers.google.com/apis/credentials?folder=&organizationId=&project=nerds-with-charisma" target="_blank">Google's Developer Console</a>. Go to <b>Credentials > Create credentials > API Key</b>. They will give you an API key, copy this and add it to our <b>gatsby-config.js</b>:
    </p>

    <Markdown
      rel="gatsby-config.js"
      language="javascript"
      md={
      `  ...
  coffeeUrl: 'https://www.buymeacoffee.com/yz6MAXbTE',
  googleMapsApiKey: 'AIzaSyCuke3pMotG7Ti70NQEwD6PiuCUzoOyIN4',
  ...`}
    />

    <p>
      We then need to load our API key in our source, we can do that in our <b>seo.js</b> component. First we pass the API key from <b>index.js</b> to our <b>SEO</b> component, then we add a new meta tag:
    </p>

    <Markdown
      rel="index.js"
      language="javascript"
      md={
      `  ...
  <SEO
    title={data.site.siteMetadata.homepageData.title}
    lang={data.site.siteMetadata.homepageData.lang}
    meta={data.site.siteMetadata.homepageData.meta}
    og={data.site.siteMetadata.homepageData.og}
    googleMapsApiKey={data.site.siteMetadata.googleMapsApiKey}
  />
  ...`}
    />

    <Markdown
      rel="seo.js"
      language="javascript"
      md={
      `  ...
  function SEO({ title, lang, meta, og, googleMapsApiKey }) {
    return (
      <Helmet
        htmlAttributes={{ lang }}
        title={title}
      >

        <script
          async
          defer
          src={\`https://maps.googleapis.com/maps/api/js?key=\${googleMapsApiKey}\`}
        />
  ...`}
    />

    <hr />

    <p>
      Now, this next part's a tad confusing, you need to first go to the <b>Overview</b> and enable billing (you need to add a credit card for Google, but they give you a free stipens each month). You then need to <b>Link</b> the billing account to your api's.
    </p>

    <p>
      Next you need to enable the geolocation, geocoding, and maps apis. Go to the <b>APIs</b> tab, you should see them in the list, click each and click <b>Enable</b> on each one.
    </p>

    <p>
      Now head back to <b>index.js</b> and lets add some code to reverse geocode, first pull in our apikey, then we'll write the function for getting the address:
    </p>

    <Markdown
      rel="index.js"
      language="javascript"
      md={
      `  ...
  siteMetadata {
    coffeeUrl
    googleMapsApiKey
    ...
    geolocation.then((data) => {
      setTimeout(() => {
        var geocoder = new window.google.maps.Geocoder();
        geocoder.geocode({'location': { lat: data.latitude, lng: data.longitude }}, function(results, status) {
          if (status === 'OK') {
            if (results[0]) {
              console.log(results);
            }
          }
        });
      }, 3000);
    });
  ...`}
    />

    <p>
      We put a timeout on here to give Google Maps some time to load (there are better ways to do this but this is a quick way for our tutorial purposes), we then create a new geocoder instance which is a function from Google.  After that is ready we use geocode with a payload of the lat & long that returns a promise with "results" we check to make sure the "status" is "OK" and if so we have a debugger there so we can see the results we're getting from Google. If there's an error we just won't display anything for the user.
    </p>

    <p>
      If you console log out results, you'll see a list of addresses that match our lat/long. We can use the first one to loop through and find the city.
    </p>

    <Markdown
      rel="index.js"
      language="javascript"
      md={
      `  ...
  if (results[0]) {
    const city = results[0].address_components.filter((obj) => {
      return obj.types.includes('locality');
    })[0].long_name;
  }
  ...`}
    />

    <p>
      Now we need to set a state var and pass it down to our <b>About</b> component:
    </p>

    <Markdown
      rel="index.js"
      language="javascript"
      md={
      `  ...
  (data) => {
    const [supportsWebP, setSupportsWebP] = useState(true);
    const [currentProjectToOpen, setCurrentProjectToOpen] = useState(null);
    const [city, setCity] = useState(null);
    ...
    if (results[0]) {
      const city = results[0].address_components.filter((obj) => {
        return obj.types.includes('locality');
      })[0].long_name;

      setCity(city);
    }
    ...
    <About aboutData={data.site.siteMetadata.aboutData} city={city} />
  ...`}
    />

    <p>
      Now head into <b>About.js</b> and lets add a message with the city. We'll wrap it in a null check, incase we can't get their location we won't show anything.
    </p>

    <Markdown
      rel="About.js"
      language="javascript"
      md={
      `  ...
  const About = ({ aboutData, city }) => (
    ...
    <div dangerouslySetInnerHTML={{ __html: \`
      \${aboutData.where.body}
      <br /><br />
      \${city ? \`We can totally help you dominate the \${city} SEO market!\` : \`\`}
      \`}}
    />
  ...`}
    />

  <p>
    If we have a city, we'll put in this message saying we can help the user out in their city. If we don't then we'll exclude this line and everything will be just fine and dandy and non-specific.
  </p>

  <p>
    The last thing you want to do is to lock down your API by restricting the urls that can use it. Head back to your Google Dev Console dashboard and you should see a message that says "You should secure your new API key", click the <b>SECURE CREDENTIALS</b> button in this message.
  </p>

  <p>
    Under <b>Application restrictions</b> choose "HTTP Referrers". A new section <b>Website restrictions</b> and add the domain for your website. Finally, under <b>Api restrictions</b> choose "Geocoding", "Geolocation", and "Maps Javascript" apis. Click <b>Save</b>.
  </p>

  <p>
    Now, if you refresh our local project, we should get an error saying this API key is not allowed from this referrer. You can add http://localhost:8000 to the allowed sites, but remove this before you push to prod as anyone could use your key and ring up your bill. You can also add alerts in the billing section with a specific amount, if you start to approach this amount Google can alert you at specific percentages as you get closer.
  </p>

  <hr />

  <strong id="anchor--keyboard-events">Keyboard Interaction</strong>
  <br />
  <br />

  <p>
    Another common use-case you'll come across is user's keyUp/keyDown interaction. We actually have a pretty good scenario where this would make sense, if we're in the portfolio and someone hits "esc" we should close the currently selected portfolio item.
  </p>

  <p>
    First we need to detect keyboard changes when the <b>Portfolio</b> component is open, we can do this with the "onkeydown" event. We'll use an empty useEffect to run this code when the component first mounts:
  </p>

  <Markdown
      rel="Portfolio.js"
      language="javascript"
      md={
      `  ...
  useEffect(() => {
    // detect escape key and close the project if hit
    document.onkeydown = (e) => {

    }
  }, []);
  ...`}
    />

    <p>
      We can simply test for the keycode of '27' which is the escape keys identifier, and then push the user back to the homepage if it's detected:
    </p>

    <Markdown
      rel="Portfolio.js"
      language="javascript"
      md={
      `  ...
  useEffect(() => {
    // detect escape key and close the project if hit
    document.onkeydown = (e) => {
      document.onkeydown = (e) => {
        console.log(e.keyCode === 27);
        if (e.keyCode === 27) {
          window.history.pushState(null, null, '/');
          setPortItem(null);
        }
      };
    }
  }, []);
  ...`}
    />

    <p>
      You can get a full list of keycodes from <a href="http://gcctech.org/csc/javascript/javascript_keycodes.htm" target="_blank">GCC Tech</a> in the event you wanted to detect another key event and perform a different set of functions.
    </p>

    <hr />
    <strong id="anchor--mouse-detection">Mouse Detection</strong>
    <br />
    <br />

    <p>
      The final thing we'll do is really just for demonstration purposes, we'll detect the users computer type and then watch their mous position. If they start to approach the close button we'll console log out something like "WAIT DON'T GO!". You can add your own functionality instead of a console log, just don't abuse this.
    </p>

    <p>
      Think really hard about throwing something at your users when they're leaving, one valid use case would be to offer them a discount to stick around if you're selling something. If they go to close the page you could trigger a modal that says "If you order now, take an extra 10% off" or something. You should also monitor that you've already shown this message to a user so that you're not constently harrassing them when they come back to your site.
    </p>

    <p>
      The first thing you need to do is setup some variables, we'll add these in <b>index.js</b> just below our geolocation code. We'll init our x and y coords, make a variable that checks for a cookie called showLeavingMessage. If this is anything other than false, we'll assume we want to show the message. finally we'll make a var to check if the user is on a mac, if not we'll assume windows. We do this because windows has the close button on the right where-as mac is on the left.
    </p>

    <Markdown
      rel="index.js"
      language="javascript"
      md={
      `  ...
      // watch for mouse going towards the close button
      let mouseX = 0;
      let mouseY = 0;
      let hasShown = document.cookie.match(new RegExp('(^| )showLeavingMessage=([^;]+)'));  // set a flag by checking for cookie named "showLeavingMessage"

      const isMac = (window.navigator.platform === 'MacIntel');  // check if it's a mac, if not assume windows

  ...`}
    />

    <p>
      Next we create an event listener for mouse move and set the mouse x and y vars to the current mouse position:
    </p>

    <Markdown
    rel="index.js"
    language="javascript"
    md={
    `  ...
    const isMac = (window.navigator.platform === 'MacIntel');  // check if it's a mac, if not assume windows

    document.addEventListener('mousemove', (e) => {
      mouseX = e.clientX;
      mouseY = e.clientY;
    });

...`}
  />

  <p>
    Next we'll have a check for if hasShown is false, if not we'll know to run these commands, then 2 similar commands for windows &amp; mac that checks the mouse position for getting close to close. If either of those trigger we'll set the cookie so that it doesn't happen on repeat visits and console log out a message:
  </p>

  <Markdown
    rel="index.js"
    language="javascript"
    md={
    `  ...
  document.addEventListener('mousemove', (e) => {
    mouseX = e.clientX;
    mouseY = e.clientY;

    if (!hasShown) { // if we haven't done our thing yet, go ahead
      if (isMac === true && mouseX < 30 && mouseY < 30) { // assume mac, close is upper left
        console.log('woah mrs osx, don\'t leave');  // do your functionality here
        document.cookie = "showLeavingMessage=false"; // trigger our flag so next time it won't bother the user
      }

      if (isMac !== true && (mouseX < window.innerWidth - 30) && mouseY < 30) { // windows close is upper right
        console.log('easy mr windows, don\'t leave'); // do your functionality here
        document.cookie = "showLeavingMessage=false"; // trigger our flag so next time it won't bother the user
      }
    }
  });
...`}
/>

<p>
  Try it out and see that our messages log when you approach the close button, fresh the page and you'll see that it won't happen again because we've set the cookie to false. This is just a basic skeleton for some functionality that you can implement, but you can expand on this by setting an expiration to the cookie so that it would happen after 2 weeks or so, etc...
</p>

<p>
  Again, these are really just for demonstration purposes, they really didn't add anything to our portfolio, but they showed you some important concepts like getting user locations, handling keyboard events, and working with the mouse.
</p>

<p>
  We're getting really close to the end of this series, in the next chapter we'll talk a bit about linting, and keeping our code clean.
</p>

    <AniLink to="jamstack-jedi/linting" className="nextBtn">Linting AirBnB Style &raquo;</AniLink>
    <br />
  </Layout>
);

export default Fun;
