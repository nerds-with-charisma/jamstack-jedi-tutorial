import React from 'react';
import AniLink from '../../components/Common/AniLink';

import Layout from '../../components/layout';
import YouTube from '../../components/Common/YouTube';

const CSS101 = () => (
  <Layout
    title="CSS 101"
     author={{ gravatar: '//0.gravatar.com/avatar/b5eff8d62adf3ff34e67f153781d27c4' }}
      gitRepo={null}
      time={3}
      description="Learn CSS, the first steps to becoming a front-end developer. CSS basics."
      keywords="Learn CSS, CSS Basics, Get better at CSS, CSS 101, Intro to CSS"
  >
    <strong>We Make This Look Good</strong>
    <br />
    <br />
    <p className="tldr">
      Similar to the HTML 101 chapter, we'll start with some further learning for you to go through first. HTML is pretty straight forward in my opinion, CSS is a little trickier. However CSS is not <em>HARD</em> I would say. It just takes some time to wrap your head around how it all works (the syntax is actually very easy). Specifically positioning and floating give people trouble.
    </p>

    <p>
      I would suggest you go through <a id="anchor--css-missing-manual" href="https://www.amazon.com/CSS-Missing-Manual-Manuals/dp/0596802447" target="_blank">CSS: The Missing Manual</a> by David McFarland. This is the best beginners guide to CSS I have found, and what got me started years ago to actually understanding how CSS works.
    </p>

    <p className="tldr">
      You should have already gone through the HTML &amp; CSS book in the HTML 101 chapter, this also covered some basic CSS. If you stopped at after the HTML go back through and read the CSS portion now.
    </p>

    <p id="anchor--css-full-course-youtube">
      If reading still isn't your thing (but I HIGHLY suggest reading these first few books), here's a great video from freeCodeCamp.org that covers a lot of the basics of CSS.
    </p>

    <YouTube url="https://www.youtube.com/embed/ieTHC78giGQ" />

    <hr />

    <p id="anchor--css-positioning-youtube">
      When teaching people CSS, positioning is what most people say is the hardest to wrap their head around. In all honesty, it's not that bad once you've walked some of the possible positioning combinations. One of the best explanations for positioning I've come across is <a href="https://www.youtube.com/watch?v=jx5jmI0UlXU" target="_blank">this video from Web Dev Simplified</a>. It doesn't go into great depth explaining all the things you can do with positioning, but it is a great foundation for helping you understand how the basics of positioning work.
    </p>

    <p className="tldr">
      You should understand the difference between the three main positionings, relative, absolute, and fixed. You should also understand how floats work as well as clearing. Once you get to that point you can move on to the next chapter where we start creating the visual layout of the coming soon page.
    </p>

    <YouTube url="https://www.youtube.com/embed/jx5jmI0UlXU" />

    <br />

    <p id="anchor--layout-exercises">
      Before you move on, I would challenge you to try to build a few common page layouts yourself to get comfortable. Reading and watching is all good, but you'll really start to understand by doing. <a href="https://www.w3schools.com/css/css_website_layout.asp" target="_blank">W3 Schools</a> has some exercises with common page layouts. I suggest you try to recreate these as they're very important layouts to get used to dealing with.
    </p>

    <AniLink to="jamstack-jedi/coming-soon-css" className="nextBtn">Start Designing Our Coming Soon's CSS &raquo;</AniLink>
    <br />

  </Layout>
);

export default CSS101;
