import React from 'react';
import AniLink from '../../components/Common/AniLink';

import Layout from '../../components/layout';
import Markdown from '../../components/Common/markdown';

const Gatsby404 = () => (
  <Layout
    title="404 Page"
    author={{ gravatar: '//0.gravatar.com/avatar/b5eff8d62adf3ff34e67f153781d27c4' }}
    gitRepo="https://gitlab.com/nerds-with-charisma/jamstack-jedi/tree/master/seo-404"
    time={6}
    description="Learn how to create a custom 404 page inside of GatsbyJs"
    keywords="404 page, Page not found, custom 404, custom page not found, 404 page GatsbyJs, Page not found GatsbyJs, How to 404 page"
  >

  <p>
    A <a id="anchor--what-is-404" href="https://www.woorank.com/en/edu/seo-guides/why-404-pages-are-important-for-seo" target="_blank">404 page</a> is a basic error page that tells the browser that the server couldn't find what they were looking for. Say you go to mywebsite.com/blog, but we don't have a blog...we would send the user to a 404 page saying that we couldn't find what they're looking for, then provide them something helpful like a link home or a search or our top categories.
  </p>

  <p>
    <a id="anchor--gatsby-404" href="https://www.gatsbyjs.org/docs/add-404-page/" target="_blank">Gatsby has a 404 page wired up by default</a> but it's not very pretty, but we're able to customize it and tell Gatsby to use that page if the url the user enters isn't found.
  </p>

  <p>
    To create our custom 404 page, we need to make a new file in the <b>pages</b> directory as well as some styles for it:
  </p>

  <Markdown
    rel="Terminal"
    language="terminal"
    md={`  > src/pages/404.js

  > src/styles/404.scss`}
  />

  <p>
    No other configuration is needed, however, because of the way Gatsby generates this file it won't work properly locally. Meaning if we type in an incorrect url locally, it will still show the default Gatsby 404 page. Once deployed to the server, it will work correctly.
  </p>

  <p>
    We can still test our 404 page locally though, by entering it directly into the url @ <b>http://localhost:8000/404</b>.
  </p>

  <p>
    We're going to create a very simple 404 page, feel free to expand on it and add whatever you feel would be helpful to your users:
  </p>

  <Markdown
    rel="404.js"
    language="javascript"
    md={`  import React from 'react';

  import '../styles/404.scss';

  export default () => (
    <div id="fourOhFour">
      <div>
        <aside>
          <img
            width="50"
            src="/404/logo--nwc-purple.svg"
            alt="Nerds With Charisma Logo"
          />
          <h1>
            {'404'}
          </h1>
          <img
            className="hero"
            src="/404/404.gif"
            alt="Bummer..."
          />
          <br />
          <br />
          <strong>
            {'We couldn\\'t find what you\\'re looking for.'}
          </strong>
          <br />
          <br />
          <a href="/">
            {'GO HOME'}
          </a>
        </aside>
      </div>
    </div>
  );`}
  />

  <p>
    You can grab the <a href="https://gitlab.com/nerds-with-charisma/jamstack-jedi/tree/master/seo-404/static" target="_blank">images from our GitLab</a>, or use your own. Place it in the <b>static</b> directory in it's own folder called <b>404</b>.
  </p>

  <p>
    Next lets create the styles, we won't go over too much here as it's just a simple layout:
  </p>

  <Markdown
    rel="404.js"
    language="javascript"
    md={`  html, body, div {
    height: 100%;
  }

  #fourOhFour {
    padding: 0;
    margin: 0;
    width: 100%;
    height: 100%;
    display: flex;
    text-align: center;
    font-family: Roboto;
    letter-spacing: 0;
    font-style: normal;
    text-rendering: optimizeLegibility;
    font-size: 20px;
    line-height: 1.4;
    color: #484848;

    div {
      flex: 1;
    }

    h1 {
      font-size: 200px;
      margin: 0;
    }

    .hero {
      max-width: 100%;
      border-radius: 10px;
    }

    a {
      background: linear-gradient(52deg, #9012fe 0%, #f233b9 100%);
      font-size: 16px;
      border-radius: 300px;
      padding: 12px 35px;
      color: #fff;
      font-weight: bold;
      border: none;
      box-shadow: 0 4px 15px rgba(0, 0, 0, 0.2);
      transition: all 0.3s linear;
      margin: 20px 0;
      width: 300px;
      max-width: 100%;
      display: inline-block;
      clear: both;
    }
  }`}
  />

  <p>
    Notice that we only import these styles into the 404 page, because we don't want them effecting anything on any other page.
  </p>

  <p>
    And that's it, you can preview your work on <a href="http://localhost:8000/404/">localhost:8000/404</a> to make sure everything looks right. We'll keep this chapter nice and short!
  </p>

  <img src="/images/jamstack-jedi/seo--404.png" class="jamstack--img" alt="404 page" />

  <p>
    Our next chapter should also be a short one, where we tackle the <b>Robots.txt</b> file.
  </p>

  <AniLink to="jamstack-jedi/seo-robots" className="nextBtn">Robots.txt Explained &raquo;</AniLink>
  <br />
  </Layout>
);

export default Gatsby404;
