import React, { useState, useEffect } from 'react';
import AniLink from '../../components/Common/AniLink';

const NavLink = ({ title, url }) => {
 return (
  <AniLink to={`jamstack-jedi/${url}`} className="font--light" activeClassName="active">
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    {title}
    <br />
  </AniLink>
 )
}

const JamstackNav = () => {
  const [toggles, setToggles] = useState({
    startHere: true,
    frontEnd101: true,
    spa101: true,
    design: false,
    gatsbyBuild: true,
    seo1: true,
    optimize: true,
    seo2: true,
    extra: true,
  });
  return (
  <div id="docs--nav" className="col-12 col-sm-6 col-md-4 col-lg-3 container font--16 lh-md font--light">
    <br />
    <br />
    <a class="bmc-button" target="_blank" href="https://www.buymeacoffee.com/yz6MAXbTE">
      <img src="https://bmc-cdn.nyc3.digitaloceanspaces.com/BMC-button-images/BMC-btn-logo.svg" alt="Buy me a coffee" />
      <strong className="font--16">
        &nbsp;&nbsp;&nbsp;&nbsp;Buy me a coffee
      </strong>
    </a>
    <br />
    <br />

    <AniLink to="jamstack-jedi/appendix" activeClassName="font--primary">
      <strong className="font--light">Appendix</strong>
    </AniLink>

    <br />

    <AniLink to="checklist" activeClassName="font--primary">
      <strong className="font--light">Checklist</strong>
    </AniLink>
    <br />
    <br />
    <strong className="cursor--pointer" onClick={() => setToggles({ ...toggles, startHere: !toggles.startHere }) }>
      {(toggles.startHere === true) ? (<span>[ - ]</span>) : (<span>[ + ]</span>)}
      {' Start Here'}
    </strong>
    {(toggles.startHere === true) &&
      <div>
        <NavLink title="Intro" url="intro" />
        <NavLink title="Overview" url="overview" />
        <NavLink title="IDE" url="ide" />
        <NavLink title="Browser" url="browser" />
      </div>
    }
    <br />
    <br />
    <strong className="cursor--pointer" onClick={() => setToggles({ ...toggles, design: !toggles.design }) }>
      {(toggles.design === true) ? (<span>[ - ]</span>) : (<span>[ + ]</span>)}
      {' Design & Discovery'}
    </strong>
    {(toggles.design === true) &&
      <div>
        Coming Soon
      </div>
    }
    <br />
    <br />
    <strong className="cursor--pointer" onClick={() => setToggles({ ...toggles, frontEnd101: !toggles.frontEnd101 }) }>
      {(toggles.frontEnd101 === true) ? (<span>[ - ]</span>) : (<span>[ + ]</span>)}
      {' Front-End 101'}
    </strong>
    {(toggles.frontEnd101 === true) &&
      <div>
        <NavLink title="HTML 101" url="html-101" />
        <NavLink title="Coming Soon HTML" url="coming-soon-html" />
        <NavLink title="CSS 101" url="css-101" />
        <NavLink title="Coming Soon CSS" url="coming-soon-css" />
        <NavLink title="Coming Soon Mobile" url="coming-soon-mobile" />
        <NavLink title="Git 101" url="git-101" />
        <NavLink title="GitLab" url="gitlab" />
        <NavLink title="Domain Name" url="domain-name" />
        <NavLink title="Hosting: FTP" url="hosting-ftp" />
        <NavLink title="Hosting: Netlify" url="hosting-netlify" />
        <NavLink title="Javascript 101" url="javascript-101" />
        <NavLink title="Wrapping Up 101" url="wrapping-up-101" />
      </div>
    }

    <br />
    <br />
    <strong className="cursor--pointer" onClick={() => setToggles({ ...toggles, spa101: !toggles.spa101 }) }>
      {(toggles.spa101 === true) ? (<span>[ - ]</span>) : (<span>[ + ]</span>)}
      {' GatsbyJs 101'}
    </strong>
    {(toggles.spa101 === true) &&
      <div>
        <NavLink title="Overview" url="spa-101" />
        <NavLink title="Gatsby Initial Setup" url="gatsby-init" />
        <NavLink title="Sass 101" url="sass-101" />
        <NavLink title="Style Library" url="style-library" />
        <NavLink title="GraphQl 101" url="graphql-101" />
      </div>
    }

    <br />
    <br />
    <strong className="cursor--pointer" onClick={() => setToggles({ ...toggles, gatsbyBuild: !toggles.gatsbyBuild }) }>
      {(toggles.gatsbyBuild === true) ? (<span>[ - ]</span>) : (<span>[ + ]</span>)}
      {' GatsbyJs Build-Out'}
    </strong>
    {(toggles.gatsbyBuild === true) &&
      <div>
        <NavLink title="Design Overview" url="portfolio-design" />
        <NavLink title="Hero Component" url="build-hero" />
        <NavLink title="Hero Continued" url="build-hero-2" />
        <NavLink title="Hero Mobile" url="build-hero-3" />
        <NavLink title="Header Component" url="build-header" />
        <NavLink title="Header Continued" url="build-header-2" />
        <NavLink title="Services Component" url="build-services" />
        <NavLink title="Portfolio Component" url="build-portfolio" />
        <NavLink title="Portfolio Continued" url="build-portfolio-2" />
        <NavLink title="About Component" url="build-about" />
        <NavLink title="Contact Component" url="build-contact" />
        <NavLink title="Footer Component" url="build-footer" />
      </div>
    }

    <br />
    <br />
    <strong className="cursor--pointer" onClick={() => setToggles({ ...toggles, seo1: !toggles.seo1 }) }>
      {(toggles.seo1 === true) ? (<span>[ - ]</span>) : (<span>[ + ]</span>)}
      {' SEO Part 1'}
    </strong>
    {(toggles.seo1 === true) &&
      <div>
        <NavLink title="SEO Overview" url="seo-1" />
        <NavLink title="Google Tag Manager" url="seo-gtm" />
        <NavLink title="Gatsby 404" url="seo-404" />
        <NavLink title="robots.txt" url="seo-robots" />
        <NavLink title="Sitemap.xml" url="seo-sitemap" />
        <NavLink title="Meta Data" url="seo-meta" />
      </div>
    }

    <br />
    <br />
    <strong className="cursor--pointer" onClick={() => setToggles({ ...toggles, optimize: !toggles.optimize }) }>
      {(toggles.optimize === true) ? (<span>[ - ]</span>) : (<span>[ + ]</span>)}
      {' Prod Prep'}
    </strong>
    {(toggles.optimize === true) &&
      <div>
        <NavLink title="Opti Overview" url="optimize-overview" />
        <NavLink title="Wave Accessibility" url="opti-wave" />
        <NavLink title="Page Speed" url="opti-speed" />
        <NavLink title="MailChimp Subscriptions" url="mailchimp" />
        <NavLink title="Prod Deployment" url="prod" />
      </div>
    }

    <br />
    <br />
    <strong className="cursor--pointer" onClick={() => setToggles({ ...toggles, seo2: !toggles.seo2 }) }>
      {(toggles.seo2 === true) ? (<span>[ - ]</span>) : (<span>[ + ]</span>)}
      {' SEO Part 2'}
    </strong>
    {(toggles.seo2 === true) &&
      <div>
        <NavLink title="SEO Part 2 Overview" url="seo-2" />
        <NavLink title="Google Search Console" url="gsc" />
        <NavLink title="Claim Social Brands" url="social" />
        <NavLink title="Google My Business Console" url="gmb" />
        <NavLink title="Other Search Engines" url="search-submit" />
        <NavLink title="Schema Markup" url="schema" />
      </div>
    }

    <br />
    <br />
    <strong className="cursor--pointer" onClick={() => setToggles({ ...toggles, extra: !toggles.extra }) }>
      {(toggles.extra === true) ? (<span>[ - ]</span>) : (<span>[ + ]</span>)}
      {' Extras'}
    </strong>
    {(toggles.extra === true) &&
      <div>
        <NavLink title="Extras Overview" url="extras" />
        <NavLink title="Slack Notifications" url="slack" />
        <NavLink title="Animated Menu" url="menu-revisted" />
        <NavLink title="Hotjar Heatmaps" url="hotjar" />
        <NavLink title="A/B Testing" url="ab" />
        <NavLink title="Keywords SEO" url="keywords" />
        <NavLink title="Buy Me a Coffee" url="coffee" />
        <NavLink title="Javascript Fun" url="js-fun" />
        <NavLink title="Linting" url="linting" />
        <NavLink title="Contentful" url="contentful" />
      </div>
    }

    <br />
    <br />
  </div>
)};

export default JamstackNav;
