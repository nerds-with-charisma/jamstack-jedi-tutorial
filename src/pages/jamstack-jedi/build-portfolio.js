import React from 'react';
import AniLink from '../../components/Common/AniLink';

import Layout from '../../components/layout';

import Markdown from '../../components/Common/markdown';

const PortfolioBuild = () => (
  <Layout
    title="Portfolio Components"
    author={{ gravatar: '//0.gravatar.com/avatar/b5eff8d62adf3ff34e67f153781d27c4' }}
    gitRepo={null}
    time={25}
    description={null}
    keywords={null}
  >
    <p>
      This chapter will probably be the most intensive and hardest to accomplish. We're going to build our portfolio in this section and it's going to require a bunch of steps
    </p>

    <p>
      Here's a list of everything we need to accomplish in our portfolio:
    </p>

    <ul class="bulletetd">
      <li>Create the components (Portfolio, port-heading, port-item, port-single [single will have some smaller components inside also but we'll visit that later])</li>
      <li>Add our base data to gatsby-config.js - this will be the default, but we'll allow content from another source later, in our last chapter)</li>
      <li>Create the layouts for each component</li>
      <li>Create our styles, including the masonry grid for displaying our portfolio</li>
      <li>Add a hover effect when a user is over a portfolio item</li>
      <li>Load the clicked item into the single component</li>
      <li>Add a click event to each portfolio item to set the single portfolio item to be displayed</li>
      <li>Update the url when clicked to that portfolio item</li>
      <li>When a user refreshes, pull up the selected portfolio item</li>
      <li>Create the layout and styles for the single portfolio item</li>
      <li>Create the close event to get out of the single portfolio item</li>
    </ul>

    <p>
      Wow, that's a lot, we've been doing a lot of stuff like creating components, importing them, passing props, etc...so we'll start to phase out calling these things out directly and simply add the code to make things more brief.
    </p>

    <hr />

    <p>
      Lets jump right in to creating our components, creating their skeletons and importing them into the parent component:
    </p>

    <Markdown
      rel="Terminal"
      language="terminal"
      md={`  mkdir src/components/portfolio

  cd src/components/portfolio/

  touch Portfolio.js portfolio-heading.js portfolio-item.js portfolio-single.js

  cd ../../../`}
    />

    <p>
      Step 1 done, step 2 is add some data (we're just going to c/p the same thing 6 times for now, feel free to populate with your own) to gatsby-config.js:
    </p>

    <Markdown
      rel="gatsby-config.js"
      language="javascript"
      md={`  ...
  portfolioData: {
    tagline: 'Checkout some of the awesome websites and apps we\\'ve worked on',
    "portfolioData": [
      {
        alt: 'Nerd Fit',
        src: '/portfolio/thumb--nerd-fit.jpg',
        type: 'Design, React Native Application',
        website: 'https://nerdswithcharisma.com',
        about: '<div>Nerd Fit is a lifestyle app meant to keep your life in order. It tracks your calories burned, steps, workouts, todos, grocery list, and more.<br /><br />This was an amazing project to build, and probably the most fun we\\'ve had on a project in a long time.<br /><br />NF will be in the appstore shortly.</div>',
        images: ['/portfolio/nerd-fit1.jpg', '/portfolio/nerd-fit3.jpg', '/portfolio/nerd-fit2.jpg'],
        hero: '//images.ctfassets.net/mdcl40cos6bh/2yHLzoHjK7xvfVjx40HGNn/213e974741f532f39d1efd73cc1e0092/hero--nerd-fit.jpg',
        tech: 'React Native, GraphQL',
        launchDate: '2019',
        sort: '1',
        browser: '//images.ctfassets.net/mdcl40cos6bh/38Ly2Ula6R16BK4qJ5szS8/e9697441edb790f74ed2e529b77f7241/nerd-fit3.jpg',
      },
      {
        alt: 'Nerd Fit',
        src: '/portfolio/thumb--nerd-fit.jpg',
        type: 'Design, React Native Application',
        website: 'https://nerdswithcharisma.com',
        about: '<div>Nerd Fit is a lifestyle app meant to keep your life in order. It tracks your calories burned, steps, workouts, todos, grocery list, and more.<br /><br />This was an amazing project to build, and probably the most fun we\\'ve had on a project in a long time.<br /><br />NF will be in the appstore shortly.</div>',
        images: ['/portfolio/nerd-fit1.jpg', '/portfolio/nerd-fit3.jpg', '/portfolio/nerd-fit2.jpg'],
        hero: '//images.ctfassets.net/mdcl40cos6bh/2yHLzoHjK7xvfVjx40HGNn/213e974741f532f39d1efd73cc1e0092/hero--nerd-fit.jpg',
        tech: 'React Native, GraphQL',
        launchDate: '2019',
        sort: '1',
        browser: '//images.ctfassets.net/mdcl40cos6bh/38Ly2Ula6R16BK4qJ5szS8/e9697441edb790f74ed2e529b77f7241/nerd-fit3.jpg',
      },
      {
        alt: 'Nerd Fit',
        src: '/portfolio/thumb--nerd-fit.jpg',
        type: 'Design, React Native Application',
        website: 'https://nerdswithcharisma.com',
        about: '<div>Nerd Fit is a lifestyle app meant to keep your life in order. It tracks your calories burned, steps, workouts, todos, grocery list, and more.<br /><br />This was an amazing project to build, and probably the most fun we\\'ve had on a project in a long time.<br /><br />NF will be in the appstore shortly.</div>',
        images: ['/portfolio/nerd-fit1.jpg', '/portfolio/nerd-fit3.jpg', '/portfolio/nerd-fit2.jpg'],
        hero: '//images.ctfassets.net/mdcl40cos6bh/2yHLzoHjK7xvfVjx40HGNn/213e974741f532f39d1efd73cc1e0092/hero--nerd-fit.jpg',
        tech: 'React Native, GraphQL',
        launchDate: '2019',
        sort: '1',
        browser: '//images.ctfassets.net/mdcl40cos6bh/38Ly2Ula6R16BK4qJ5szS8/e9697441edb790f74ed2e529b77f7241/nerd-fit3.jpg',
      },
      {
        alt: 'Nerd Fit',
        src: '/portfolio/thumb--nerd-fit.jpg',
        type: 'Design, React Native Application',
        website: 'https://nerdswithcharisma.com',
        about: '<div>Nerd Fit is a lifestyle app meant to keep your life in order. It tracks your calories burned, steps, workouts, todos, grocery list, and more.<br /><br />This was an amazing project to build, and probably the most fun we\\'ve had on a project in a long time.<br /><br />NF will be in the appstore shortly.</div>',
        images: ['/portfolio/nerd-fit1.jpg', '/portfolio/nerd-fit3.jpg', '/portfolio/nerd-fit2.jpg'],
        hero: '//images.ctfassets.net/mdcl40cos6bh/2yHLzoHjK7xvfVjx40HGNn/213e974741f532f39d1efd73cc1e0092/hero--nerd-fit.jpg',
        tech: 'React Native, GraphQL',
        launchDate: '2019',
        sort: '1',
        browser: '//images.ctfassets.net/mdcl40cos6bh/38Ly2Ula6R16BK4qJ5szS8/e9697441edb790f74ed2e529b77f7241/nerd-fit3.jpg',
      },
      {
        alt: 'Nerd Fit',
        src: '/portfolio/thumb--nerd-fit.jpg',
        type: 'Design, React Native Application',
        website: 'https://nerdswithcharisma.com',
        about: '<div>Nerd Fit is a lifestyle app meant to keep your life in order. It tracks your calories burned, steps, workouts, todos, grocery list, and more.<br /><br />This was an amazing project to build, and probably the most fun we\\'ve had on a project in a long time.<br /><br />NF will be in the appstore shortly.</div>',
        images: ['/portfolio/nerd-fit1.jpg', '/portfolio/nerd-fit3.jpg', '/portfolio/nerd-fit2.jpg'],
        hero: '//images.ctfassets.net/mdcl40cos6bh/2yHLzoHjK7xvfVjx40HGNn/213e974741f532f39d1efd73cc1e0092/hero--nerd-fit.jpg',
        tech: 'React Native, GraphQL',
        launchDate: '2019',
        sort: '1',
        browser: '//images.ctfassets.net/mdcl40cos6bh/38Ly2Ula6R16BK4qJ5szS8/e9697441edb790f74ed2e529b77f7241/nerd-fit3.jpg',
      },
      {
        alt: 'Nerd Fit',
        src: '/portfolio/thumb--nerd-fit.jpg',
        type: 'Design, React Native Application',
        website: 'https://nerdswithcharisma.com',
        about: '<div>Nerd Fit is a lifestyle app meant to keep your life in order. It tracks your calories burned, steps, workouts, todos, grocery list, and more.<br /><br />This was an amazing project to build, and probably the most fun we\\'ve had on a project in a long time.<br /><br />NF will be in the appstore shortly.</div>',
        images: ['/portfolio/nerd-fit1.jpg', '/portfolio/nerd-fit3.jpg', '/portfolio/nerd-fit2.jpg'],
        hero: '//images.ctfassets.net/mdcl40cos6bh/2yHLzoHjK7xvfVjx40HGNn/213e974741f532f39d1efd73cc1e0092/hero--nerd-fit.jpg',
        tech: 'React Native, GraphQL',
        launchDate: '2019',
        sort: '1',
        browser: '//images.ctfassets.net/mdcl40cos6bh/38Ly2Ula6R16BK4qJ5szS8/e9697441edb790f74ed2e529b77f7241/nerd-fit3.jpg',
      },
    ],
  },
  ...`}
    />

    <p>
      The base skeleton for each component:
    </p>

    <Markdown
      rel="Portfolio.js"
      language="javascript"
      md={`  import React from 'react';
  import { PropTypes } from 'prop-types';

  import PortfolioHeading from './portfolio-heading';
  import PortfolioItem from './portfolio-item';
  import PortfolioSingle from './portfolio-single';

  const Portfolio = ({ portfolioData }) => (
    <section id="portfolio" className="position--relative overflow--container">
      <PortfolioHeading tagline={portfolioData.tagline} />
      <PortfolioItem item={null} />
      <PortfolioSingle item={null} />
    </section>
  );

  Portfolio.propTypes = {
    portfolioData: PropTypes.object.isRequired,
  };

  export default Portfolio;`}
    />

    <Markdown
      rel="portfolio-heading.js"
      language="javascript"
      md={`  import React from 'react';
  import { PropTypes } from 'prop-types';

  const PortfolioHeading = ({ tagline } ) => (
    <div className="col-12">
      <strong className="font--48">
        {'PORTFOLIO'}
      </strong>
      <br />
      {tagline}
      <br />
      <br />
      <br />
    </div>
  );

  PortfolioHeading.propTypes = {
    tagline: PropTypes.string.isRequired,
  };

  export default PortfolioHeading;`}
    />

    <Markdown
      rel="portfolio-item.js"
      language="javascript"
      md={`  import React from 'react';
  import { PropTypes } from 'prop-types';

  const PortfolioItem = ({ item } ) => (
    <h1>Port Item</h1>
  );

  PortfolioItem.propTypes = {
    item: PropTypes.object.isRequired,
  };

  export default PortfolioItem;`}
    />

    <Markdown
      rel="portfolio-single.js"
      language="javascript"
      md={`  import React from 'react';
  import { PropTypes } from 'prop-types';

  const PortfolioSingle = ({ item } ) => (
    <h1>Port Single</h1>
  );

  PortfolioSingle.propTypes = {
    item: PropTypes.object.isRequired,
  };

  export default PortfolioSingle;`}
    />

    <p>
      And import it into our <b>index.js</b> component, while pulling in and passing down our data just below the servicesData node:
    </p>

    <Markdown
      rel="index.js"
      language="javascript"
      md={`  ...
  import Portfolio from '../components/portfolio/Portfolio';
  ...
  portfolioData {
    tagline
    portfolioData {
      alt
      src
      type
      website
      about
      images
      hero
      tech
      launchDate
      sort
      browser
    }
  }
  ...
    <Services servicesData={data.site.siteMetadata.servicesData} />
    <Portfolio portfolioData={data.site.siteMetadata.portfolioData} />
  ...`}
    />

    <p>
      That should get us up and running and complete step 2. Part 3, lets layout our components starting with <b>Portfolio.js</b>:
    </p>

    <Markdown
      rel="Portfolio.js"
      language="javascript"
      md={`  import React from 'react';
    import { PropTypes } from 'prop-types';

    import PortfolioHeading from './portfolio-heading';
    import PortfolioItem from './portfolio-item';
    import PortfolioSingle from './portfolio-single';

    const Portfolio = ({ portfolioData }) => (
      <section id="portfolio" className="position--relative overflow--container">
        <div className="col-12 text-center">
          <br />
          <br />
          <br />
          <PortfolioHeading tagline={portfolioData.tagline} />
        </div>

        <div className="masonry">
          { portfolioData.portfolioData.sort((a, b) => a.sort < b.sort).map((item, i) => (
              <PortfolioItem key={\`\${item.alt}\${i}\`} item={item} />
            ))
          }
        </div>

        <PortfolioSingle item={portfolioData.portfolioData[0]} />
      </section>
    );

    Portfolio.propTypes = {
      portfolioData: PropTypes.object.isRequired,
    };

    export default Portfolio;`}
    />

    <p>
      We'll pass the first portfolio item to our PortfolioSingle just for now, so that we can style it. But first, lets start with the portfolio layout.
    </p>

    <p>
      Remember to put your images in the <b>static folder</b> and if you want to use ours you can grab them from <a href="https://gitlab.com/nerds-with-charisma/jamstack-jedi/tree/master/build-portfolio/static/portfolio" target="_blank">our GitLab</a>. Put them in <b>/static/portfolio/</b>.
    </p>


    <p>
      Make an <b>_portfolio.scss</b> file and import it into into <b>main.scss</b>
    </p>

    <Markdown
      rel="Terminal"
      language="terminal"
      md={`  > src/styles/_portfolio.scss`}
    />

    <Markdown
      rel="main.scss"
      language="scss"
      md={`  @import 'portfolio.scss';`}
    />

    <p>
      Lets build our <b>portfolio-item.js</b> layout, and then we'll style it:
    </p>

    <Markdown
      rel="portfolio-item.js"
      language="javascript"
      md={`  import React from 'react';
  import { PropTypes } from 'prop-types';

  const PortfolioItem = ({ item } ) => (
    <figure className="brick">
      <button
        type="button"
        onClick={() => alert('todo')}
      >
        <img
          alt={item.alt}
          src={item.src}
        />
      </button>
    </figure>
  );

  PortfolioItem.propTypes = {
    item: PropTypes.object.isRequired,
  };

  export default PortfolioItem;`}
    />

    <p>
      Each portfolio item will consist of an image with a button wrapped around it and a figure tag with a class of "brick", we'll style these to sit horizontally across and stack one on top of the other.
    </p>

    <p>
      Open up <b>_portfolio.scss</b> and lets add some styles:
    </p>

    <Markdown
      rel="_portfolio.scss"
      language="scss"
      md={`  .masonry {
  figure {
    margin: 0;  /* remove any margins */
    width: 33.333%; /* on desktop we want 3 across */
    float: left; /* and float them all in a row */
  }

  figure:nth-child(3n) {  /* every 3rd item (3, 6, 9, etc) */
    button {
      margin-right: 5px; /* remove the right padding since it'll be against the browser */
    }
  }

  img {
    display: block; /* make the image block level to remove any whitespace */
    transition: all 0.2s linear; /* add for animation */
  }

  button {
    border: none; /* buttons have a border by default, remove it */
    padding: 0; /* remove default padding */
    float: left;  /* float them so they're inline */
    margin: 5px 10px 5px 0; /* adds a white board but lets us still hover effect the bg */
    background: $primary; /* set the default background color */
    cursor: pointer; /* make the cursor indicate we can click */

    &:hover {
      img {
        opacity: 0.3; /* fade out our image so that we can see the background color on hover */
      }
    }
  }
}

/* change the color of each one to our main colors */
.masonry figure:nth-child(2n) {
  button {
    background-color: $links;
  }
}

.masonry figure:nth-child(3n) {
  button {
    background-color: $info;
  }
}

.masonry figure:nth-child(4n) {
  button {
    background-color: $successAlt;
  }
}`}
    />

    <img src="/images/jamstack-jedi/portfolio--masonry.png" class="jamstack--img" alt="Initial masonry layout" />

    <p>
      You can see at the bottom we have a neat hover effect to change the background color when we mouse over.
    </p>

    <p>
      We should also change the grid based on device size, something like 2 on mobile, 3 on medium, and 4 on large:
    </p>

    <Markdown
      rel="_portfolio.scss"
      language="scss"
      md={`  /* change to 2 col grid for mobile */
  @media(min-width: 0px) and (max-width: 991px) {
    .masonry {
      figure {
        width: 50%; /* on desktop we want 3 across */
      }

      button {
        margin: 2% !important;
      }
    }
  }

  /* change to 4 col grid for big monitors */
  @media(min-width: 1500px) {
    .masonry {
      figure {
        width: 25%; /* on desktop we want 3 across */
      }

      button {
        margin: 2% !important;
      }
    }
  }`}
    />

    <img src="/images/jamstack-jedi/portfolio--mobile.png" class="jamstack--img" alt="Mobile masonry layout" />

    <hr />

    <p>
      That's 2 more steps down, next up is to style the single item display. We've already hardcoded the first item to be passed down, so it's just a matter of laying it out and styling it. Open up <b>portfolio-single.js</b> and lets do the layout:
    </p>

    <Markdown
      rel="portfolio-single.js"
      language="javascript"
      md={`  import React from 'react';
  import { PropTypes } from 'prop-types';

  const PortfolioSingle = ({ item } ) => {
    return (
      <aside
        id="portfolioSingle"
        className={(item !== null) ? 'active' : 'inactive'}
      >
        <button
          type="button"
          className="close"
          onClick={() => alert('todo')}
        >
          &times;
        </button>
      </aside>
    )
  };

  PortfolioSingle.propTypes = {
    item: PropTypes.object.isRequired,
  };

  export default PortfolioSingle;`}
    />

    <p>
      First we check if there is an item, if not, we'll give it a class of "inactive", which we'll push the panel off-screen in that scenario when we handle the styles.
    </p>

    <p>
      Next we add a close button, with a todo to handle the closing of the panel that we'll also tackle in a bit.
    </p>

    <p>
      To keep things organized lets create more components to hold all the sections, and pass down the necessary data only as props:
    </p>

    <Markdown
      rel="Terminal"
      language="terminal"
      md={`  cd src/components/portfolio/

  touch portfolio-hero.js portfolio-type.js portfolio-what.js portfolio-images.js portfolio-footer.js

  cd ../../../`}
    />

    <Markdown
      rel="portfolio-single.js"
      language="javascript"
      md={`  import React from 'react';
  import { PropTypes } from 'prop-types';

  import PortfolioHero from './portfolio-hero';
  import PortfolioType from './portfolio-type';
  import PortfolioWhat from './portfolio-what';
  import PortfolioImages from './portfolio-images';
  import PortfolioFooter from './portfolio-footer';

  const PortfolioSingle = ({ item } ) => (
    <aside
      id="portfolioSingle"
      className={(item !== null) ? 'active' : 'inactive'}
    >
      <button
        type="button"
        className="close"
        onClick={() => alert('todo')}
      >
        &times;
      </button>

      <PortfolioHero alt={item.alt} hero={item.hero} />
      <br />
      <br />
      <section className="container-md">
        <PortfolioType type={item.type} browser={item.browser} />
        <PortfolioWhat about={item.about} tech={item.tech} />
        <PortfolioImages images={item.images} />
        <PortfolioFooter website={item.website} launchDate={item.launchDate} />
      </section>
    </aside>
  );

  PortfolioFooter.defaultProps = {
    website: null,
    launchDate: null,
  };

  PortfolioFooter.propTypes = {
    website: PropTypes.string,
    launchDate: PropTypes.string,
  };

  export default PortfolioSingle;`}
    />

    <hr />

    <p>
      And now lets build each section out, in the <b>portfolio-hero.js</b> section we have an image and a heading that will overlay it:
    </p>

    <Markdown
      rel="portfolio-hero.js"
      language="javascript"
      md={`  import React from 'react';
  import { PropTypes } from 'prop-types';

  const PortfolioHero = ({ alt, hero } ) => (
    <section id="portfolioItem--hero">
      <img src={hero} alt={alt} />
      <h2>
        {alt}
      </h2>
    </section>
  );

  PortfolioHero.propTypes = {
    alt: PropTypes.string.isRequired,
    hero: PropTypes.string.isRequired,
  };

  export default PortfolioHero;`}
    />

    <p>
      Next is the <b>portfolio-type.js</b>, where we show a list of what type of project this was as well as an overall shot of the project we're calling "browser".
    </p>

    <Markdown
      rel="portfolio-type.js"
      language="javascript"
      md={`  import React from 'react';
  import { PropTypes } from 'prop-types';

  const PortfolioType = ({ type, browser } ) => (
    <section id="portfolioItem--type" className="grid">
      <div className="col-12 col-md-6 col-lg-6">
        <strong className="font--80 lh-sm">
          {type}
        </b>
      </div>
      <div className="col-12 col-md-6 col-lg-6">
        { browser && (
          <img
            alt={JSON.toString(type)}
            src={browser}
          />
        )}
      </div>
    </section>
  );

  PortfolioType.propTypes = {
    type: PropTypes.string.isRequired,
    browser: PropTypes.string.isRequired,
  };

  export default PortfolioType;`}
    />

    <p>
      <b>portfolio-what</b> is also very basic, we have a blurb about what we did using <b>dangerouslySetInnerHTML</b> because we're passing it markdown. It's ok to use this, so long as you know what you're passing down is simple HTML markup. We also pass down a string of tech used
    </p>

    <Markdown
      rel="portfolio-what.js"
      language="javascript"
      md={`  import React from 'react';
  import { PropTypes } from 'prop-types';

  const PortfolioWhat = ({ about, tech } ) => (
    <section id="portfolioItem--what">
      <strong className="font--42">
        {'What we did'}
      </b>
      <br />
      <br />
      <div
        className="font--16"
        dangerouslySetInnerHTML={{ __html: about || '' }}
      />
      <br />
      <br />
      <strong>
        {\`[ \${tech} ]\`}
      </strong>
    </section>
  );

  PortfolioWhat.propTypes = {
    about: PropTypes.string.isRequired,
    tech: PropTypes.string.isRequired,
  };

  export default PortfolioWhat;`}
  />

  <p>
    Next we simply want to display all the images in our portfolio for this specific project, so we map through each:
  </p>

  <Markdown
      rel="portfolio-images.js"
      language="javascript"
      md={`  import React from 'react';
  import { PropTypes } from 'prop-types';

  const PortfolioImages = ({ images } ) => (
    <section id="portfolioItem--images">
      { images.map((image) => (
        <img src={image} alt={image} key={image} />
      ))}
    </section>
  );

  PortfolioImages.propTypes = {
    images: PropTypes.array.isRequired,
  };

  export default PortfolioImages;`}
  />

  <p>
    Our final component is the <b>portfolio-footer.js</b> file, which will have the date it was launched and if there is a website for it, a button to go there:
  </p>

  <Markdown
      rel="portfolio-footer.js"
      language="javascript"
      md={`  import React from 'react';
  import { PropTypes } from 'prop-types';

  const PortfolioFooter = ({ website, launchDate } ) => (
    <section id="portfolioItem--footer">
      { (launchDate)
        && (
          <strong className="font--80">
            {\`Launched In \${launchDate}\`}
          </strong>
        )
      }
      <br />
      <br />
      { (website)
        && (
          <a
            href={website}
            target="_blank"
            rel="no-follow noopener noreferrer"
            className="btn radius--lg bordered bordered--xs bordered--primary font--28 bg--primary"
          >
            <strong className="font--light font--21">
              {'Visit Website'}
            </strong>
          </a>
        )
      }
    </section>
  );

  PortfolioFooter.propTypes = {
    images: PropTypes.array.isRequired,
  };

  export default PortfolioFooter;`}
  />

  <img src="/images/jamstack-jedi/portfolio--single.png" class="jamstack--img" alt="Single Portfolio Item" />

  <p>
    We've added a few null checks in here, if either launchDate or website don't exist, we won't print that section out.
  </p>

  <p>
    That will do it for the layout portion. Lets move to the next chapter to style it and add the necessary click events.
  </p>

  <AniLink to="jamstack-jedi/build-portfolio-2" className="nextBtn">Continue Portfolio Build &raquo;</AniLink>
    <br />
  </Layout>
);

export default PortfolioBuild;
