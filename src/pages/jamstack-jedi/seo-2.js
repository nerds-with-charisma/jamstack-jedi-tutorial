import React from 'react';
import AniLink from '../../components/Common/AniLink';

import Layout from '../../components/layout';

const SEO2 = () => (
  <Layout
    title="SEO Part 2"
    author={{ gravatar: '//0.gravatar.com/avatar/b5eff8d62adf3ff34e67f153781d27c4' }}
    gitRepo={null}
    time={3}
    description="Claim your social branding online, add schema markup, and perform an SEO audit"
    keywords="Learn social branding, learn schema markup, peroform seo audit"
  >

  <strong>
    On The [SEO] Road Again
  </strong>
  <br />
  <br />

  <p>
    Our site is now officially live and now we're ready dive deeper into SEO. In the coming chapters we'll go through how to setup <b>Google Search Console</b>, which will allow us to submit our sitemap and help identify any potential crawler issues that search engine bots might hit on our site.
  </p>

  <p>
    Next we'll claim our social brands, if you haven't already, and talk about how to link them to our site and some other best practices to really solidify our brand.
  </p>

  <p>
    In the same thought process we'll talk about how to brand our Twitter page, and add Twitter's meta tags to our site similar to how we did Facebook's Open Graph tags.
  </p>

  <p>
    Next we'll get our sites verified with major search engines, and cover the most common ways of accomplishing that.
  </p>

  <p>
    After that, we'll add schema data to our site to help out with our SERPs and let bots know what's going on at a glance with our site.
  </p>

  <p>
    Finally we'll go through an SEO audit and look for potential issues and competition.
  </p>

  <p>
    There won't be a lot of coding in this section, but more offsite optimizations and configurations. But anytime we do make code changes, remember that by simply committing code to the master branch, our production site will be updated as our Git repo is updated. So don't commit anything to master unless you're ready to push it live.
  </p>

  <AniLink to="jamstack-jedi/gsc" className="nextBtn">Google Search Console &raquo;</AniLink>
  <br />
  </Layout>
);

export default SEO2;
