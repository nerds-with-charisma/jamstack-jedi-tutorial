import React from 'react';
import AniLink from '../../components/Common/AniLink';

import Layout from '../../components/layout';

const Hotjar = () => (
  <Layout
    title="Hotjar"
    author={{ gravatar: '//0.gravatar.com/avatar/b5eff8d62adf3ff34e67f153781d27c4' }}
    gitRepo={null}
    time={5}
    description="Use Hotjar to get user generated heatmaps."
    keywords="Heatmaps, Heat maps, Hotjar, User mouse detection, Mouse hotspots, Javascript Heatmaps"
  >
    <strong>
      Some Like it Hot...Jar...
    </strong>
    <br />
    <br />

    <p>
      <b>Hotjar</b> is a service that offers heat maps, or visual representations of where users are spending time, on your site. They offer a premium service if you need to upgrade, but the free/basic plan should be good enough for us to get started.
    </p>

    <p>
      Along with heatmaps they also offer other features such as user feedback polls, user recordings, and analytics. Hotjar can be a really good tool for finding out where your users are clicking (or not clicking) and just how much of your page they're bothering to view.
    </p>

    <p>
      We can use our heatmaps along with A/B testing (covered later in this section) to test how visitors can better interact with our site.
    </p>


    <p>
      Head on over to <a href="https://www.hotjar.com/" target="_blank">https://www.hotjar.com/</a> and signup. Follow the wizard and fill out all fields, including your website url and click <b>Start Using Hotjar</b>.
    </p>

    <p>
      You should get prompted with a few steps once your signup is completed, on the <b>Create your first Heatmap report</b>, enter your domain and click <b>Create Heatmap</b>. Feel free to add or skip any of the other offerings.
    </p>

    <p>
      Once finished you should be presented with a code snippet, copy this to your clip board and head over to <b>Google Tag Manager</b>. Go to <b>Tags</b> and lets create a new tag.
    </p>

    <p>
      Name this tag <b>Hotjar</b> and click into the <b>Tag Configuration</b> panel. Choose <b>Custom HTML</b> and paste your code into the editor.
    </p>

    <p>
      Next click on <b>Triggers</b> and choose <b>All Pages</b>. Save and publish, once deployed you can open up devtools on your website, refresh, and filter by "Hotjar". You should see the HJ snippet loading correctly now.
    </p>

    <img src="/images/jamstack-jedi/hotjar--pageload.png" class="jamstack--img" alt="Hotjar snippet installed" />

    <p>
      Head back to Hotjar's website and it should auto-detect your snippet has been installed. If not, head to <b>Settings > Sites &amp; Organizations > Tracking Code</b> and choose <b>Verify</b>.
    </p>

    <p>
      At this point, you can hover over the <b>Business</b> plan badge, a new badge will appear, click the <b>Change Plan</b> link and switch to <b>Basic</b>. Click <b>Update Plans</b>. Head back to your dashboard.
    </p>

    <p>
      After about 15 min or so, your heatmaps should be generated, you can get to it by going to <b>Heatmaps</b> on the left nav and clicking <b>View Heatmap</b> next to your domain.
    </p>

    <img src="/images/jamstack-jedi/hotjar--heatmap.png" class="jamstack--img" alt="Hotjar Heatmap" />

    <p>
      The "hotter" the color the more interaction it has gotten. The cooler, less. If there is none, that means nothing in that area was interacted with. With this data we can now evaluate our site every so often and decide what we should change or move.
    </p>

    <p>
      Hotjar's a very cool tool, and when combined with A/B testing it can really help us improve our experience and hopefully increase conversions. Head on to the next chapter to get started with A/B testing.
    </p>


  <AniLink to="jamstack-jedi/ab" className="nextBtn">A/B Testing with Google Optimize &raquo;</AniLink>
  <br />
  </Layout>
);

export default Hotjar;
