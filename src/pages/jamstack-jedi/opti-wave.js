import React from 'react';
import AniLink from '../../components/Common/AniLink';

import Layout from '../../components/layout';
import Markdown from '../../components/Common/markdown';
import YouTube from '../../components/Common/YouTube';

const Wave = () => (
  <Layout
    title="Wave"
     author={{ gravatar: '//0.gravatar.com/avatar/b5eff8d62adf3ff34e67f153781d27c4' }}
    gitRepo="https://gitlab.com/nerds-with-charisma/jamstack-jedi/tree/master/opti-wave"
    time={20}
    description="What is accessibility and how do we do it?"
    keywords="Wave accessibility, debuggin accessibility, website accessibility, accessibility html, how to make a webiste accessible"
  >
  <strong>
    {'👋'}
  </strong>
  <br />
  <br />

  <p>
    We talked a little about <a href="https://www.w3.org/WAI/fundamentals/accessibility-intro/" target="_blank">what accessibility is</a> in the overview section. And incase you missed it, you'll want to grab the <a id="anchor--chrome-accessibility-plugin" href="https://chrome.google.com/webstore/detail/wave-evaluation-tool/jbbplnpkjmmeebjpijfedlgcdilocofh?hl=en-US" target="_blank">Chrome plugin</a> from Wave for our local testing.
  </p>

  <p>
    If you're interested in learning more about accessibility you can watch <a id="anchor--accessibilty-talk" href="https://www.youtube.com/watch?v=z8xUCzToff8" target="_blank">this talk by Rob Dodson</a>.
  </p>

  <YouTube url="https://www.youtube.com/embed/z8xUCzToff8" />

  <p>
    Once you have the Wave tool installed, right click the source and choose the <b>Wave</b> option.
  </p>

  <img src="/images/jamstack-jedi/audit--wave.png" class="jamstack--img" alt="Wave example" />

  <p>
    The panel will slide out and alert us to a summary of our issues:
  </p>

  <img src="/images/jamstack-jedi/audit--summary.png" class="jamstack--img" alt="Wave summary" />

  <p>
    The second tab, the flag, will show us all our errors one by one. Lets un-check all the top levels ones except <b>Errors</b> and fix these first since they're the biggest offenders.
  </p>

  <p>
    The first issue, we forgot an alt attribute on our logo in our <b>Drawer</b> component (this one is kind of hard to tell since it's hidden by default). Alt attributes let users who can't see know what the image is supposed to be. So we can use a descriptive alt tag to tell them what they would be looking at. We can give it an alt text of something like "Menu Logo", which should be descriptive enough:
  </p>

  <Markdown
    rel="Drawer.js"
    language="javascript"
    md={`  ...
  <img src={Logo} alt="Menu Logo" />
  ...`}
  />

  <p>
    Rerun the <b>Wave</b> plugin and our errors go down to 2!  The next ones are for inputs without a label. Which is required for screen readers, so lets add some in our <b>Contact.js</b> component:
  </p>

  <Markdown
    rel="Contact.js"
    language="javascript"
    md={`  ...
  <form id="contactForm" onSubmit={(e) => handleSubmit(e)}>
    <label htmlFor="email">
      <span>Email</span>
      <input
        type="email"
        id="email"
        name="email"
        placeholder="Your Email"
        onChange={(e) => setEmail(e.target.value)}
      />
    </label>
    <label htmlFor="message">
      <span>Message</span>
      <input
        type="message"
        id="message"
        name="message"
        placeholder="Your Message"
        onChange={(e) => setMessage(e.target.value)}
        />
    </label>

    <button type="submit" className="btn btn--gradient">
      {'Contact Us!'}
    </button>
  </form>
  ...`}
  />

  <p>
    Rerun the Wave tool again, and this fixes all our "errors" but now it doesn't look so great.
  </p>

  <img src="/images/jamstack-jedi/audit--errors-fixed.png" class="jamstack--img" alt="Wave fixed errors" />

  <p>
    Lets add some styles to make any span tags in a label to look nicer.
  </p>

  <Markdown
    rel="_contact.scss"
    language="scss"
    md={`  ...
  #contact {
    input {
      margin-right: 20px;
      padding: 15px;
      border: none;
      border-radius: 7px;
      width: 300px;
      box-shadow: 0 4px 15px rgba(0, 0, 0, 0.2);
      font-size: 16px;
    }

    label {
      position: relative;

      span {
        position: absolute;
        font-size: 14px;
        top: 5px;
        left: 10px;
        border-bottom: 2px solid #fff;
        padding: 0 10px;
        z-index: 999;
        opacity: 0;
        transition: all 0.2s linear;
      }

      &:focus-within {
        span {
          top: -28px;
          opacity: 1;
          border-color: $primary;
          font-weight: bold;
        }
      }
    }
  }
    ...`}
  />

  <p>
    Now, when the user clicks on the input, we'll animate it in so that it looks neat. But it's already there on page load so that screen readers can see it by default.
  </p>

  <hr />

  <p>
    Now that our errors are fixed lets move on to <b>Alerts</b>. The first one's a big one, we don't have an H1 tag anywhere...how could we have missed that?! Lets add one, but for design's sake we don't want it to actually show up, remember the "ir" class we created to position things off screen? We'll utilize that here. Open up <b>Logo.js</b> and lets add an H1:
  </p>

  <Markdown
    rel="Logo.js"
    language="javascript"
    md={`  ...
  const Logo = ({ title }) => (
    <a
      href="/"
      id="logo"
      className="font--light transition--all"
    >
      <img
        src={LogoSvg}
        alt={\`\${title} Logo\`}
      />
      <h1 className="ir">
        {title}
      </h1>
    </a>
  );
    ...`}
  />

  <hr />

  <p>
    The next issues are about linking to PDFs. We're not going to fix this one, as we want our resumes to be downloadable PDFs. If you wanted to fix this alert, you could create a new page and add an HTML resume, but we feel this is an ok alert to let go. The other issue is about a "noscript" tag, which only appears if a user has JS disabled. Since we don't care about those people (evil people) we'll ignore this one too.
  </p>

  <hr />

  <p>
    That finishes up our homepage Wave-ing, but we still have a portfolio and 404 page. So lets Wave those as well. We'll do the 404 page first, since it's really simple. Head on over to <b>http://localhost:8000/404/</b> and re-Wave the page.
  </p>

  <p>
    The first issue is...we don't have a page title or language specified, seems like a pretty big deal. Lets add this by adding React Helmet to our 404 page:
  </p>

  <Markdown
    rel="404.js"
    language="javascript"
    md={`  ...
  import Helmet from 'react-helmet';
  ...
  <div id="fourOhFour">
    <Helmet
      title="Page Not Found"
      htmlAttributes={{ lang: 'en' }}
    />
    ...`}
  />

  <p>
    Rerun Wave and that fixes all of our 404 issues (except the noscript issue we ignored before). So lets move on to our portfolio issues.
  </p>

  <hr />

  <p>
    Open up one of our portfolio items, refresh the page, then run Wave and we see...no errors, nice. But there are some alerts, so lets fix those.
  </p>

  <p>
    First it's telling us that our Hero's alt text is redundant. Which makes sense because it's also the title, we can appease it by making our alt text more descriptive, open up <b>portfolio-hero.js</b> and lets add some text to the alt attribute:
  </p>

  <Markdown
    rel="portfolio-hero.js"
    language="javascript"
    md={`  ...
    <img src={hero} alt={\`\${alt} hero banner\`} />
    ...`}
  />

  <p>
    The second issue is also related to alt text. When we loop through our images, we're printing out the image path, which doesn't help a screen reader. We should be more specific on what these images represent. Open up <b>portfolio-images.js</b> and lets add some descriptive text along with an index to differentiate each image:
  </p>

  <Markdown
    rel="portfolio-images.js"
    language="javascript"
    md={`  ...
  { images && images.map((image, i) => (
    <img src={image} alt={\`Portfolio shot \${i}\`} key={image} />
  ))}
    ...`}
  />

  <p>
    Finally, it alerts us to our PDFs and noscript issues again, which we'll ignore again. We're not done yet though.
  </p>

  <hr />

  <p>
    The last thing we need to fix are "contrast" issues. At the top of the Wave tool, there's 3 pill tabs, we've been working on <b>Styles</b> but we want to switch to <b>Contrast</b>
  </p>

  <p>
    A lot of these errors are our fault, we're using background styling that isn't supported by screen readers. So the fixes for most should be easy, for starters our hero, we specified "background-color" in our css, but then inline we style "background", which over-powers "background-color". So as far as the browser is concerned, we have white text on a white background if the image doesn't load. We can fix that by adding a color to our "background-color" css in <b>Hero.js</b>:
  </p>

  <Markdown
    rel="Hero.js"
    language="javascript"
    md={`  ...
  background: \`url(\${(supportsWebP ? data.fileName.childImageSharp.sizes.srcWebp : data.fileName.childImageSharp.sizes.src)}) 50% fixed no-repeat #9012FE\`
  ...`}
  />

  <p>
    Feel free to use any color you'd like, or black, just something dark.
  </p>

  <p>
    Similarly, our gradient buttons don't have a fallback solid color. Open up <b>_theme.scss</b> and lets change our gradient background to have 2 separate attributes, background-color and background-image (which will be our gradient):
  </p>

  <Markdown
    rel="_theme.scss"
    language="scss"
    md={`  ...
    .btn--gradient {
      background-color: $primary;
      background-image: linear-gradient(52deg, rgba(144,18,254,1) 0%, rgba(242,51,185,1) 100%);
  ...`}
  />

  <hr />

  <p>
    The next issue that's happening in several places is because of our .ir class. We move it offscreen but lots of times that makes it white on white again. So if we specify a contrast background/foreground we can remove a lot of those warnings. Open up <b>base.scss</b> and lets add those properties:
  </p>

  <Markdown
    rel="base.scss"
    language="scss"
    md={`  ...
  .ir {
    text-indent: -9999px;
    font-size: 10px;
    position: absolute !important;
    background: #000;
    color: #fff;
    left: -9999px;
  }
  ...`}
  />


  <p>
    The second to last contrast issue is the largeLetter in our hero. Since this is a design choice, we're going to leave it.
  </p>

  <p>
    Finally, our footer tagline, we made it white on white, we should have made it light gray. We still want it to be lighter so we'll use opacity to make it lighter. Open up <b>Footer.js</b> and add these classes:
  </p>

  <Markdown
    rel="Footer.js"
    language="javascript"
    md={`  ...
  <span className="font--text opacity6">
    {footerTagline}
  </span>
  ...`}
  />

  <p>
    And that's it for the Wave tool, one thing to note is that for something like our Hamburger menu, we could use an "Aria" label, but we chose the .ir class to put something like "Menu" under it, offscreen. This should suffice but if you want to add aria labels to your text-less elements, now would be the time to do it.
  </p>

  <p>
    Next up we'll tackle page speed!
  </p>

  <AniLink to="jamstack-jedi/opti-speed" className="nextBtn">Page Speed Optimization &raquo;</AniLink>
  <br />
  </Layout>
);

export default Wave;
