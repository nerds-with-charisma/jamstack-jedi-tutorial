import React from 'react';
import AniLink from '../../components/Common/AniLink';

import Layout from '../../components/layout';
import Markdown from '../../components/Common/markdown';

const Mailchimp = () => (
  <Layout
    title="Prod Deployment"
    author={{ gravatar: '//0.gravatar.com/avatar/b5eff8d62adf3ff34e67f153781d27c4' }}
    gitRepo={null}
    time={6}
    description="Deploy your production site to Netlify"
    keywords={null}
  >

  <strong>
    We're Almost There
  </strong>

  <br />
  <br />

  <p>
    Congrats on making it this far! At this point, we're ready to deploy to production and replace our coming soon page. If you're on the standard FTP hosting, then it's as simple as running "gatsby build" then coping the contents of the public directory to where you previously had your coming soon files (make a backup and delete those before you upload your Gatsby files).
  </p>

  <p>
    If you used the Netlify/GitLab deployment we did earlier then you have a few options. The first, would be to move our Gatsby files to the coming soon directory and simply deploy it by committing to GitLab. Auto-deploy will take care of the rest and then we just would have to configure the root directory to point to "public". The better solution is to change the repo we're publishing our site from.
  </p>

  <p>
    Before we go any farther, make sure that you've updated all the <b>gatsby-config.js</b> content to your company's info, uploaded your own images, etc...then perform a Gatsby build to make sure everything compiles with no errors:
  </p>

  <Markdown
    rel="Terminal"
    language="terminal"
    md={`  gatsby build`}
  />

  <p>
    Once you've updated all your content and the builds done, commit all our files to git and push them. Next we will link the Netlify deploy to this GitLab repo by logging into Netlify, going to <b>Settings</b> > <b>Build &amp; Deploy</b> > Build Settings > <b>Edit Settings</b> > <b>Link to a different repository</b> > Linking to this repo. Note that it will not work correctly just yet. That is because our Gatsby files are served from the <b>/public/</b> directory.
  </p>

  <p>
    Once the deployment is completed, we need to configure our public directory in Netlify. So login to our Netlify backend and choose our main project. Find the <b>Site Settings</b> section then choose <b>Build &amp; Deploy</b>.
  </p>

  <p>
    Under <b>build settings</b> choose <b>edit settings</b>. Find the <b>Build Command</b> input and make it "gatsby build", and make <b>publish directory</b> as "public/". Since this is where gatsby deploys our content on build we choose this as our main directory. We now need to re-trigger a build. Go to <b>Deploys</b> at the top and click <b>Trigger Deploy > Deploy Site</b>.
  </p>

  <p>
    If all is setup correctly, our Gatsby site should take the place of our coming soon page. Which means we're live in production!
  </p>

  <p>
    Now that we're live we can start the second portion of our SEO in the next chapter. If you'd like to check out our website at the current state, you can go to <a href="https://jedi1.nerdswithcharisma.com/" target="_blank">our staging site</a> to see what everything looks like.
  </p>

  <AniLink to="jamstack-jedi/seo-2" className="nextBtn">SEO the Next Generation &raquo;</AniLink>
  <br />
  </Layout>
);

export default Mailchimp;
