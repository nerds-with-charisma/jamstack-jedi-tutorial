import React from 'react';

import AniLink from '../../components/Common/AniLink';
import Layout from '../../components/layout';
import Markdown from '../../components/Common/markdown';

const ComingSoonHtml = () => (
  <Layout
    title="Coming Soon"
     author={{ gravatar: '//0.gravatar.com/avatar/b5eff8d62adf3ff34e67f153781d27c4' }}
      gitRepo="https://gitlab.com/nerds-with-charisma/jamstack-jedi/tree/master/coming-soon-html-final"
      time={11}
      description="Build an HTML coming soon page."
      keywords="HTML coming soon, learn html coming soon"
  >
    <strong>Roll up those sleeves and lest start building.</strong>
    <br />
    <br />

    <p>
      Incase you missed it or skipped the intro chapters, here's what we'll be building for our coming soon page. You can grab the starter files from our <a href="https://gitlab.com/nerds-with-charisma/jamstack-jedi/tree/master/coming-soon-html-start" target="_blank">GitLab Repo</a>. You can also check the <AniLink to="jamstack-jedi/html-101">previous chapter</AniLink> for a downloadable PSD of the coming soon design.
    </p>
    <img src="/images/jamstack-jedi/html--preview.png" class="jamstack--img" alt="Coming Soon Preview" />

    <p className="tldr">
      Lets open up our <u>index.html</u> page and start building! We'll start with some basic meta data that tells the browser a little bit about our website.
    </p>

    <p className="tldr">
      Previously we had a very simple page layout:
    </p>

    <span className="tldr">
    <Markdown
      rel="index.html"
      language="html"
      md={
        `  <!DOCTYPE html>
  <html lang="en">
    <head>
      <title>Jamstack Jedi - Coming Soon</title>
    </head>
    <body>
      <h1>Hello Nerds!</h1>
    </body>
  </html>`
      }
    />
    </span>

    <p>
      Lets take out everything in between the head tag, and add our own, personal info. Feel free to put in your personal info rather than mine.
    </p>

    <Markdown
      rel="index.html"
      language="html"
      md={
        `  ...
  <head>
    <title>Nerds With Charisma - Coming Soon</title>
    <link rel="shortcut icon" href="favicon.ico" />

    <meta charset="utf-8" />
    <meta name="description" content="Nerds With Charisma online portfolio coming soon!" />
    <meta name="keywords" content="Brian Dausman, Nerds With Charisma" />
  </head>
  ...`
      }
    />

    <p className="tldr">
      The first thing we do is set a title for the page, to be a little more specific about what the page is going to be. It's a coming soon page for our company.
    </p>

    <p className="tldr">
      Next we link to our favicon file, which is the little image that shows up when someone bookmarks our site and in the tab bar when they're browsing the site.
    </p>

    <p id="anchor--favicon">
      We linked to a favicon, but we don't actually have one created yet, so lets do that. The easiest way to create a favicon is to make a png image that is very large and put it through an online favicon generator such as <a href="https://favicon.io/favicon-converter/" target="_blank">Favicon.io</a>. Feel free to create your own image, save it at 512x512 pixels, and run it through a generator. You will get a file called favicon.ico which we will upload into our root directory. If you do not want to create your own favicon right now, you can <a href="https://gitlab.com/nerds-with-charisma/jamstack-jedi/blob/master/coming-soon-html-final/favicon.ico">use the one supplied in our GitLab</a> for this chapter. Remember to rename it to <b>favicon.ico</b>.
    </p>

    <p className="tldr">
      Now, if we save and refresh the page in our browser, we should see our favicon showing up in the active tab.
    </p>

    <img src="/images/jamstack-jedi/coming-soon--favicon.png" class="jamstack--img tldr" alt="Coming Soon Favicon" />

    <p className="tldr">
      Feel free to <a href="https://sympli.io/blog/2017/02/15/heres-everything-you-need-to-know-about-favicons-in-2017/" target="_blank">check out this article from Sympli</a> about the different types of icons.
    </p>

    <hr />

    <p className="tldr">
      The next three lines are meta tags that tell the browser what our <a id="anchor--character-encoding" href="https://www.w3.org/International/questions/qa-what-is-encoding" target="_blank">character encoding</a> will be, the description of our site that will show up in SERPS (or "search engine results pages"), and the keywords that will help define what our site is about to search engines.
    </p>

    <p className="tldr">
      That's all we need right now for our head, lets move on to some content.
    </p>

    <hr className="tldr" />

    <p>
      Lets layout our page's structure now. When writing HTML I find it helpful to think about how the page is logically laid out in major blocks like we showed in the <AniLink to="/amstack-jedi/html-101/">HTML 101</AniLink> chapter. So lets put in some placeholder skeleton code into our body tag that will represent our layout.
    </p>

    <Markdown
      rel="index.html"
      language="html"
      md={
        `  ...
  <body>
    <main id="main">
      <section id="copy-wrapper">
        Left
      </section>
      <section id="contact-wrapper">
        Right
      </section>
      <footer>
        Copyright
      </footer>
    </main>
  </body>
  ...`
      }
    />

    <p className="tldr">
      We have used an HTML5 specific tag called "main" to wrap all our body content in. We've also given it an ID of "main" just to be safe, incase we later use the main tag again, we'll be able to specifically target this specific tag.
    </p>

    <p className="tldr">
      Inside the main tag we have created two sections that will be our "left" and "right" columns. We don't ID them as left and right, because that's kind of arbitrary. It's better to use some sort of naming convention not related to layout, especially when you'll see how things stack when we start designing for mobile, there will be no "left" and "right".
    </p>

    <p className="tldr">
      Finally we have our "footer", which will hold the copyright information. This also uses an HTML5 specific tag.
    </p>

    <hr />

    <p>
      Now that we have our basic structure laid out, lets start by filling in our content.
    </p>

    <Markdown
      rel="index.html"
      language="html"
      md={
        `  ...
    <section id="copy-wrapper">
      <h2>
        Websites.
      </h2>
      <h2>
        Apps.
      </h2>
      <h2>
        SEO.
      </h2>
      <h2>
        Better.
      </h2>
      <h3>
        Our Portfolio Is Coming Soon
      </h3>
    </section>
  ...`
      }
    />

    <p className="tldr">
      Here, we add some H2 tags, we could have also wrapped all of these in a single H2, and added line breaks, but personally, I prefer this way. of doing it. As you'll find going through all these chapters, there are lots of ways to accomplish the same end goal.
    </p>

    <p className="tldr">
      The last heading is an H3, which if you look at the design, is less prominent than the H2's. Not only visually will it be smaller but SEO will treat H2's as higher importance than H3's.
    </p>

    <hr />

    <p>
      Next, we can fill in the contact wrapper:
    </p>

    <Markdown
      rel="index.html"
      language="html"
      md={
        `  ...
    <section id="contact-wrapper">
      <div id="social-icons">
        Twitter Dribbble Gitlab
      </div>
      <div id="contact-button-wrapper">
        <a href="">
          Shoot Us An Email
        </a>
      </div>
    </section>
  ...`
      }
    />

    <p className="tldr">
      We've added a div inside the "contact-wrapper" for our social icons, and for right now, we've added some placeholder text. Right now, we don't have the icons ready yet but we'll link them up shortly.
    </p>

    <p className="tldr">
      Under that, we add an anchor link, with an empty href (again, empty for now). Also, note that we're giving ID's to basically everything, which IMO, is a good practice. You never know when you'll need to directly select a tag later, so might as well ID it up now.
    </p>

    <hr />

    <p id="anchor--html-entities">
      The final bit of content we need to add is our copyright info. We'll use some code to show special characters in the HTML, specifically a the copyright symbol and a bullet. These are called <a href="https://www.w3schools.com/html/html_entities.asp" target="_blank">HTML Entities</a>, or reserved characters in HTML that will render a special symbol based on the combination of characters you type in. Check out <a href="https://www.freeformatter.com/html-entities.html" target="_blank">this list of characters</a> that are available to use.
    </p>

    <Markdown
      rel="index.html"
      language="html"
      md={
        `  ...
    <footer>
      &copy; Copyright Nerds With Charisma &bull; 2019
    </footer>
  ...`
      }
    />

    <p className="tldr">
      The {'&copy;'} and {'&bull;'} will be replaced with their respective symbols of &copy; and &bull; when our page is rendered in the browser.
    </p>

    <hr />

    <p>
      Now that we've got all of our coming soon content in, we have to go back and add in our icons. There's several ways we could do this (icon font, images, sprites, etc). The easiest for now, is to use a font library called <a href="https://fontawesome.com" target="_blank">Font Awesome</a>. Signup and get access to their CDN (content delivery network). The way the current version of Font Awesome works, is by adding a javascript tag to your website, this will deliver the font icons to your site and allow you to use anything listed in their free version. Follow the <a id="anchor--font-awesome" href="https://fontawesome.com/start" tareget="_blank">start</a> instructions for signing up and getting the CDN link.
    </p>

    <p className="tldr">
      Once you're received your FA url, lets add it to our index.html page, in our head, below our keywords:
    </p>

    <Markdown
      rel="index.html"
      language="html"
      md={
        `  ...
    <head>
      ...
      <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js" integrity="sha384-SlE991lGASHoBfWbelyBPLsUlwY1GwNDJo3jSJO04KZ33K2bwfV9YBauFfnzvynJ" crossorigin="anonymous"></script>
    </head>
  ...`
      }
    />

    <p className="tldr">
      Later we'll talk about why this isn't necessarily the best place to load something like this, but we'll cover stuff like that when we get to speed optimization.
    </p>

    <p>
      Now that we've added the script for FA, we can use it, and using it is very simple. Go to their website, click on <b>icons</b>, and search for the icon you want to use. We want to use a few social icons (twitter, dribbble, gitlab), so lets search for them and copy them to our site.
    </p>

    <img src="/images/jamstack-jedi/coming-soon-fa-search.png" class="jamstack--img" alt="Coming Soon Font Awesome Search" />

    <img src="/images/jamstack-jedi/coming-soon-fa-use.png" class="jamstack--img" alt="Coming Soon Font Awesome Use" />

    <p>
      We copy the icon's code from the FA website by clicking the script tag at the top, then we paste each into our HTML, replacing our previous placeholder text with the cooresponding FA codes:
    </p>

    <span className="tldr">
    <Markdown
      rel="index.html"
      language="html"
      md={
        `  ...
    <div id="social-icons">
      <a href="">
        <i class="fab fa-twitter"></i>
      </a>
      <a href="">
        <i class="fab fa-dribbble"></i>
      </a>
      <a href="">
        <i class="fab fa-gitlab"></i>
      </a>
    </div>
  ...`
      }
    />
    </span>

    <p className="tldr">
      Feel free to fill in the urls to your social links in each href. We'll put our own in there as a placeholder, but don't forget to switch them out.
    </p>

    <Markdown
      rel="index.html"
      language="html"
      md={
        `  ...
    <div id="social-icons">
      <a href="https://twitter.com/nerdswcharisma">
        <i class="fab fa-twitter"></i>
      </a>
      <a href="https://dribbble.com/nerdswithcharisma">
        <i class="fab fa-dribbble"></i>
      </a>
      <a href="https://gitlab.com/nerds-with-charisma">
        <i class="fab fa-gitlab"></i>
      </a>
    </div>
  ...`
      }
    />

    <hr />

    <p>
      That about does it for our content, but we still have one last thing to do. If you notice we have a button that says "Shoot Us an Email", right? Wouldn't it be nice if we could open up the user's mail app and pre-populate a subject?
    </p>

    <p className="tldr">
      To do that we use a special href identifier called "mailto":
    </p>

    <Markdown
      rel="index.html"
      language="html"
      md={
        `  ...
    <div id="contact-button-wrapper">
      <a href="mailto:briandausman@gmail.com?subject=Nerds%20With%20Charisma%20Inquiry">
        Shoot Us An Email
      </a>
    </div>
  ...`
      }
    />

    <p className="tldr">
      Lets break this down, first we have the "mailto:" attribute, which tells the browser to open up the default email client on the user's device. Following the colon we have the email address we want to send it to, in my case it's my gmail account.
    </p>

    <p className="tldr">
      The final part contains a parameter, indicated by the question mark, this parameter is called "subject" and lets the email client know that we want to populate the subject field with the value after the equal sign. In this case we want to send "Nerds With Charisma Inquiry". It might look a little odd with these "%20" things...These will assure that our spaces are kept when the email client is opened and the subject is populated. Most modern email clients will do this if we don't include it, but to be safe we'll include them here.
    </p>

    <p>
      If you save the index.html file, refresh and then click on our "Shoot Us An Email" link, you should be presented with a new email in your default email client with the "to" and "subject" fields pre-populated. Pretty groovy! Learn more about composing emails from anchor links <a href="https://knowledgebase.constantcontact.com/articles/KnowledgeBase/6445-add-a-mailto-link-in-the-next-generation-editor?lang=en_US" target="_blank" id="anchor--mailto">here</a>. There's more parameters you can pass to the href attribute to populate other elements of the email that we won't be covering here.
    </p>

    <img src="/images/jamstack-jedi/coming-soon--draft-email.png" class="jamstack--img" alt="Coming Soon Draft Email" />

    <p>
      We've now done everything we need for the layout of our HTML. It's not pretty, because if you remember the house analogy, this is just our foundation and our studs. In the next chapter we'll cover some CSS basics, then we'll jump into styling the page so that looks a whole lot better.
    </p>

    <p>
      Remember, you can use the GitLab icon at the top to download the finished version of this chapter to compare your code with ours if something doesn't work right.
    </p>

    <AniLink to="jamstack-jedi/css-101" className="nextBtn">Lets Get Stylish &raquo;</AniLink>
    <br />
  </Layout>
);

export default ComingSoonHtml;
