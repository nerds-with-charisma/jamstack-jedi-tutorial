import React from 'react';

import AniLink from '../../components/Common/AniLink';
import Layout from '../../components/layout';
import YouTube from '../../components/Common/YouTube';

const Browser = () => {
  return (
    <Layout
      title="Browser"
      author={{ gravatar: '//0.gravatar.com/avatar/b5eff8d62adf3ff34e67f153781d27c4' }}
      gitRepo={null}
      time={2}
      description="Choosing a browser for developing, debugging with chrome, and simulating mobile devices"
      keywords="Chrome, Chrome Debugging, Chrome Simulator, Chrome Emulator"
    >
      <strong id="anchor--browser">
        {'What\'s In a Browser'}
      </strong>
      <br />
      <p className="tldr">
        Last chapter we picked out and setup our IDE, we even made a simple HTML file. Now we want to view it. That's where the browser comes in.
      </p>
      <p className="tldr">
        I'm sure you're familiar with what a browser is, there's a few big ones out there (Chrome, Firefox, IE/Edge, Safari), and LOTS of smaller ones. When you're developing, you'll want to pick a browser that adheres to best practices and has the best development tools.
      </p>
      <p className="tldr">
        That's why, I recommend using <a href="https://www.google.com/chrome/" target="_blank">Google's Chrome browser</a>. You might prefer Firefox, and that's totally fine. But we'll be using Chrome here, so if you're following along, just note that some things may render differently or options might be in a different spot.
      </p>
      <p>
        Install your browser if you haven't already and open it up. Once open, we're going to pull up the <u>index.html</u> file we made in the previous chapter.
      </p>
      <p>
        From Chrome, go to <b>File > Open File > [Root Directory of Your Project] > index.html</b> and BOOM! You should see our "Hello Nerds" html.
      </p>
      <img src="/images/jamstack-jedi/browser--index.png" class="jamstack--img" alt="Hello Nerds" />
      <p className="tldr">
        Now, anytime we make a change to index.html, we can refresh this page in our browser and it will reflect our changes.
      </p>
      <p id="anchor--dev-tools">
        One of the many reasons we chose Chrome is because of the powerful dev tools it possess. If you right click anywhere in the page and choose <b>Inspect...</b>, it will open the Chrome dev tools at the bottom.
      </p>
      <img src="/images/jamstack-jedi/browser--inspect.png" class="jamstack--img" alt="Chrome Inspect" />
      <p className="tldr">
        We'll dive a bit into dev tools as we go along, but before starting out, you should make yourself familiar with the basics behind them. The following videos might not make total sense to you yet, but they will as you keep going along with these lessons.
      </p>
      <p>
        To finish up this chapter, here are a few suggestions to learn about dev tools.
      </p>
      <p id="anchor--dev-tools-crash-course-youtube">
        These 2 videos by our old friend <a href="https://youtu.be/x4q86IjJFag" target="_blank">Traversy Media</a> and our new friend <a href="https://youtu.be/xkzDaKwinA8" target="_blank">Wes Bos</a> are great primers.
      </p>
      <YouTube url="https://www.youtube.com/embed/x4q86IjJFag" />

      <br />
      <br />

      <YouTube url="https://www.youtube.com/embed/xkzDaKwinA8" />
      <br />
      <br />
      <p id="anchor--dev-tools-youtube">
        This playlist from Google has some good information as well: <a href="https://www.youtube.com/playlist?list=PLNYkxOF6rcIC74v_mCLUXbjj7Ng7oTAPE" target="_blank">View it on Youtube</a>.
      </p>
      <p className="tldr">
        That's about all we need to get started. In the next chapter we'll actually start to build out our coming soon page!
      </p>

      <br />

      <AniLink to="jamstack-jedi/html-101" className="nextBtn">Lets Dabble with Some HTML and CSS &raquo;</AniLink>
      <br />
    </Layout>
  );
};

export default Browser;
