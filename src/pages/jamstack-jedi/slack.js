import React from 'react';
import AniLink from '../../components/Common/AniLink';

import Layout from '../../components/layout';

const Slack = () => (
  <Layout
    title="Slack Notifications"
    author={{ gravatar: '//0.gravatar.com/avatar/b5eff8d62adf3ff34e67f153781d27c4' }}
    gitRepo={null}
    time={5}
    description="How to connect Netlify deployments to Slack to send Slack notifications or send email notifications"
    keywords="Slack notifications, Netlify Slack Integration, Netlify Slack Notifications, Netlify email Notifications"
  >

  <strong>You're a Slacker</strong>
  <br />
  <br />

  <p>
    The idea for this chapter is that we'd like to set up our site to send us a Slack notification (or email) whenever a new build is triggered and finishes. The alert will contain a link to our Netlify backend with deployment details so that we can see if anything went wrong.
  </p>

  <p>
    If you don't have Slack, which is a great chat application for workplaces, then the first step is to head on over to <a href="https://slack.com/" target="_blank">slack.com</a> and sign up for an account. If you already use Slack, then you can sign in. If you don't want to use slack, you can still go through this chapter, simply skip the Slack steps and follow the email notifications step.
  </p>

  <p>
    Once you're signed in, head on over to <a href="https://api.slack.com/apps" target="_blank">Slack's app page</a> and click <b>Create New App</b>.
  </p>

  <p>
    Name your app, we'll call ours "NWC", and choose your workspace from the dropdown. Click <b>Create App</b>.
  </p>

  <p>
    Once successful, you should be brought to an admin section for your new app with several options. The first thing we need to do is enable <b>Web Hooks</b>, so click on <b>Incoming Webhooks</b>. This will allow us an api to post chats from external sources outside of Slack.
  </p>

  <p>
    Toggle the switch to <b>On</b>, scroll to the bottom and choose <b>Add New Webhook to Workspace</b>. In the <b>Post To</b> dropdown, choose the channel you want to  be able to post to, in our caes we'll choose to message ourselves, but you could choose "general" if you want everyone in your Slack group to get it, or a specific channel if you created one. Click <b>Allow</b>.
  </p>

  <p>
    You will now be brought back to the previous page, scroll back down and you'll see a new <b>Webhook URL</b>. Copy this value and lets head over to our <b>Netlify back-end</b>.
  </p>

  <img src="/images/jamstack-jedi/slack--webhook.png" class="jamstack--img" alt="Slack Webhook" />

  <hr />

  <p>
    Back in <b>Netlify</b> head over to <b>Settings > Build &amp; Deploy > Deployment Notifications</b>. From the <b>Add notification</b> dropdown, choose <b>Slack Integration</b>.
  </p>

  <p>
    From the <b>Event</b> dropdown, choose <b>Deploy Started</b>, paste your webhook url into the <b>Slack incoming webhook url</b> input, and put your channel name into the <b>Channel</b> input. Hit <b>Save</b>.
  </p>

  <p>
    Repeat those steps for <b>Deploy Succeeded</b> and <b>Deploy Failed</b>.  Now we're ready to take it for a test run.
  </p>

  <p>
    Make a small change to your site and commit/push it. Shortly after you push, you should get a Slack notification that a deployment has started and another when it is successful (or if it failed).
  </p>

  <img src="/images/jamstack-jedi/slack--deploy.png" class="jamstack--img" alt="Slack Deploy" />

  <p>
    If you click on the link, it will take you to your newly deployed site. If you click on <b>Build log</b> it will take you to the Netlify backend with detailed logs of what happend in the deployment. These should look simiar to what you see in the Gatsby output in terminal while you're building.
  </p>

  <img src="/images/jamstack-jedi/slack--logs.png" class="jamstack--img" alt="Slack Deployment logs" />

  <hr />

  <b id="anchor--email-notification">Email Notifications</b>
  <br />
  <p>
    Don't have Slack or don't want to have notifications sent there? That's ok, you can ues Netlify to send notifications to your email instead. Simply choose <b>Email Notification</b> from the <b>Add Notification</b> dropdown and enter your email into the input and hit save (do it for deploy, success, and fail just like the Slack notifications).
  </p>

  <p>
    Now when a build starts/succeeds/fails, you should get an email with the same details as the Slack notification.
  </p>

  <img src="/images/jamstack-jedi/slack--email-alert.png" class="jamstack--img" alt="Netlify Deployment Email" />

  <hr />

  <p>
    In my opinion, this is one of the coolest features of Netlify. Combine the GitLab integration, notifications, easy domain setup and management, and all the other features they offer for free makes them my favorite hosting site as of late. I have moved almost all my projects from other hosting providers like Heroku over to Netlify and have no regrets!
  </p>

  <p>
    In the next chapter we'll enhance our menu icon a bit with a nice SVG animation as well as color changing on scroll so that it's visible even on light backgrounds.
  </p>

  <AniLink to="jamstack-jedi/menu-revisited" className="nextBtn">Menu Button Revisited &raquo;</AniLink>
  <br />
  </Layout>
);

export default Slack;
