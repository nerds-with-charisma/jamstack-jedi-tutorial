import React from 'react';
import AniLink from '../../components/Common/AniLink';

import Layout from '../../components/layout';

import Markdown from '../../components/Common/markdown';

const ServicesBuild = () => (
  <Layout
    title="Services Component"
    author={{ gravatar: '//0.gravatar.com/avatar/b5eff8d62adf3ff34e67f153781d27c4' }}
    gitRepo="https://gitlab.com/nerds-with-charisma/jamstack-jedi/tree/master/build-services"
    time={25}
    description={null}
    keywords={null}
  >
    <p>
      Last chapter was pretty intense, this chapter will be less so, thankfully. Lets start by making our component's directory and files:
    </p>

    <Markdown
      rel="Terminal"
      language="terminal"
      md={`  mkdir src/components/services

  cd src/components/services/

  touch Services.js services-heading.js services-skills.js services-logos.js

  cd ../../../`}
    />

    <p>
      Open up <b>Services.js</b> and lets put in our standard boilerplate at this point, and import/use it in <b>index.js</b>:
    </p>

    <Markdown
      rel="Services.js"
      language="javascript"
      md={`  import React from 'react';

  const Services = () => (
    <section id="services">
      <h1>Services</h1>
    </section>
  );

  export default Services;`}
    />

    <Markdown
      rel="index.js"
      language="javascript"
      md={`  ...
  import Services from '../components/services/Services';

  <Hero supportsWebP={supportsWebP} />
  <Services />`}
    />

    <p className="tldr">
      Restart your server and our services section should be visible, so lets discuss what we need to accomplish in this section.
    </p>

    <p className="tldr">
      First we'll setup all our data inside of <b>gatsby-config.js</b> to start, since we should be pretty familiar with how this flow works now. We'll do it all up front. Then we'll pull that data in and pass it down to our <b>Services.js</b> component via props.
    </p>

    <p className="tldr">
      Then we'll build out our three components from that data. We'll also add our Large "W" character like we added the "N" in the hero. Finally we want to lazy load the images to not impact page load.
    </p>

    <p>
      We're going to be using some logos here, if you want to put your own in the <b>/src/images</b> folder, feel free and replace them with what we have here or <a href="https://gitlab.com/nerds-with-charisma/jamstack-jedi/tree/master/build-services/static/logos" target="_blank">grab the ones we used</a> from our GitLab.
    </p>

    <p>
      So lets do the first part by adding in all our content into <b>gatsby-config.js</b>, below our heroData node:
    </p>

    <Markdown
      rel="gatsby-config.js"
      language="javascript"
      md={`  servicesData: {
  letter: 'W',
  tagline: 'Here\\'s just some of the stuff Nerds With Charisma can help you with',
  services: [
    {
      title: 'DEVELOPMENT',
      items: [
        'Full-Stack - Node / React / GraphQL',
        'JamStack',
        'Mobile App / React Native',
        'Wordpress Websites',
        'SEO & Marketing',
        'Analytics, Tagging, Tracking Pixels',
        'Backups & Monitoring',
        'eCommerce',
      ],
    },
    {
      title: 'DESIGN & SEO',
      items: [
        'Business Cards',
        'Logos & Branding',
        'Stationary',
        'E-Blasts',
        'Social & Strategy',
        'Keyword Research',
        'Analytics',
        'And More...',
      ],
    },
  ],
  logos: [
    {
      url: '/images/logos/nodejs.svg',
      gridSize: '4',
      title: 'Node JS',
    },
    {
      url: '/images/logos/react.svg',
      gridSize: '4',
      title: 'ReactJs & React Native',
    },
    {
      url: '/images/logos/graphql.svg',
      gridSize: '4',
      title: 'GraphQl & Prisma',
    },
    {
      url: '/images/logos/wordpress.svg',
      gridSize: '3',
      title: 'Wordpress Development',
    },
    {
      url: '/images/logos/google.svg',
      gridSize: '3',
      title: 'Google Analytics',
    },
    {
      url: '/images/logos/magento.svg',
      gridSize: '3',
      title: 'Magento',
    },
    {
      url: '/images/logos/photoshop.svg',
      gridSize: '3',
      title: 'Design / Adobe Creative Suite',
    },
    {
      url: '/images/logos/gitlab.svg',
      gridSize: '4',
      title: 'Consulting',
    },
    {
      url: '/images/logos/html5.svg',
      gridSize: '4',
      title: 'HTML5, Web Development',
    },
    {
      url: '/images/logos/css3.svg',
      gridSize: '4',
      title: 'CSS3, Design and UX',
    },
  ],
},`}
    />

    <p>
      This reminds me that we didn't pull our "N" in dynamically yet, so lets add that to our hero data node:
    </p>

    <Markdown
      rel="gatsby-config.js"
      language="javascript"
      md={`  heroData: {
    letter: 'N',
  ...`}
    />

    <hr />

    <p>
      Next lets pull that data in below the <b>homepageData</b> node, don't forget to restart your server, into <b>index.js</b> and pass it down as props:
    </p>

    <Markdown
      rel="gatsby-config.js"
      language="javascript"
      md={`  ...
  servicesData {
    letter
    tagline
    services {
      title
      items
    }
    logos {
      url
      gridSize
      title
    }
  }
  ...
  <Services servicesData={data.site.siteMetadata.servicesData} />`}
    />

    <p>
      We also want to pull in our <b>letter</b> node in <b>Hero.js</b> and replace our hardcoded "N":
    </p>

    <Markdown
      rel="Hero.js"
      language="javascript"
      md={`  ...
  siteMetadata{
    heroData {
      letter
  ...
  <LargeLetter letter={data.site.siteMetadata.heroData.letter} />`}
    />

    <p>
      Now that we have our data, lets build our services component:
    </p>

    <Markdown
      rel="Services.js"
      language="javascript"
      md={`  import React from 'react';

  import { PropTypes } from 'prop-types';

  import LargeLetter from '../common/large-letter';
  import ServicesHeading from './services-heading';
  import ServicesSkills from './services-skills';
  import ServicesLogos from './services-logos';

  const Services = ({ servicesData }) => (
    <section id="services" className="container">
      <div className="grid text-center">
        <LargeLetter letter={servicesData.letter} />
        <ServicesHeading tagline={servicesData.tagline} />
        <ServicesSkills services={servicesData.services} />
        <ServicesLogos logos={servicesData.logos} />
      </div>
    </section>
  );

  Services.propTypes = {
    servicesData: PropTypes.object.isRequired,
  };

  export default Services;`}
    />

    <p>
      All this should look familiar, importing our lesser components, passing down our data...old hat at this point. Next lets start building each lesser component, we'll start with <b>services-heading.js</b>:
    </p>

    <Markdown
      rel="services-heading.js"
      language="javascript"
      md={`  import React from 'react';
  import { PropTypes } from 'prop-types';

  const ServicesHeading = ({ tagline } ) => (
    <div className="col-12">
      <strong className="font--48">
        {'SERVICES'}
      </strong>
      <br />
      {tagline}
      <br />
      <br />
      <br />
    </div>
  );

  ServicesHeading.propTypes = {
    tagline: PropTypes.string.isRequired,
  };

  export default ServicesHeading;`}
    />

    <p>
      We have a 12 col grid, meaning 100% width, and then our services heading followed by our dynamic headline. We have some line breaks to add some breathing room below it.
    </p>

    <hr />

    <p>
      Next lets build the <b>skills</b> component:
    </p>

    <Markdown
    rel="services-skills.js"
    language="javascript"
    md={`  import React from 'react';
  import { PropTypes } from 'prop-types';

  const ServicesSkills = ({ services }) => (
    <>
      { services.map((service, i) => (
        <div key={\`\${service}\${i}\`} className="col-12 col-md-6 col-lg-4 lh-md">
          <strong>
            {service.title}
          </strong>

          <hr className="shorty opacity5" />

          { service.items.map((item, i) => (
            <React.Fragment key={\`\${item}\${i}\`}>
              {item}
              <br />
            </React.Fragment>
          ))}
        </div>
      ))}
    </>
  );

  ServicesSkills.propTypes = {
    services: PropTypes.array.isRequired,
  };

  export default ServicesSkills;`}
  />

  <p className="tldr">
    We map over our services and print out the title, then we map over the items inside each service. This is our first nested map function and it's totally legal to do that.
  </p>

  <p>
    Now our services section is looking pretty good, lets finish up the <b>services-skills.js</b> section by adding our logos:
  </p>

  <Markdown
    rel="services-skills.js"
    language="javascript"
    md={`  import React from 'react';
  import { PropTypes } from 'prop-types';

  const ServicesLogos = ({ logos }) => {

    return (
    <div className="col-12 col-lg-4">
      <br />
      <div className="row grid text-center">
        { logos.map((logo) => {
          return (
            <div className={\`col-\${logo.gridSize}\`} key={logo.title}>
              <img
                className="serviceLogo"
                width="50"
                src={logo.url}
                alt={logo.title}
              />
            </div>
          )
        })}
      </div>
    </div>
  )};

  ServicesLogos.defaultProps = {
    logos: PropTypes.array.isRequired,
  };

  export default ServicesLogos;`}
/>

<p>
  Problem...check out our services section and what do we see? Broken Images!?
</p>

<img src="/images/jamstack-jedi/services--broken-images.png" class="jamstack--img" alt="Broken Service Images" />

<p className="tldr">
  Well why is that? Remember earlier we either pulled in images via GraphQl or we imported them first? We're not doing that here, we're trying to directly use them (<a id="anchor--gatsby-static-folder" href="https://www.gatsbyjs.org/docs/static-folder/" target="_blank">Read more about it here</a>). And it SEEMS like that should work right? But that's not how Gatsby handles images, if you check out our logo src, it's not logo.svg, it's a compiled svg. Gatsby handles generating and pulling in the correct source at build time. But it can't do that here.
</p>

<p>
  So what do we do? Gatsby has logic to allow scenarios like this by adding a <b>static</b> directory to our project and serving files from there. So lets make that and move our files there:
</p>

<Markdown
  rel="Terminal"
  language="terminal"
  md={`  mkdir static

  sudo mv src/images/logos static/`}
  />

  <p className="tldr">
    What we're saying here is make a static directory at the root of our project, then move the src/images/logos directory to the static directory we just created.
  </p>

  <p className="tldr">
    Restart the server and......still broken...why? Well because Gatsby already knows they're images, so we don't need to specify that in our directory string anymore. So lets modify our <b>gatsby-config.js</b> to remove that:
  </p>

  <p>
    Remove the "/images" from all our URLs, save it and <b>NOW</b> all our images should be showing up! Remember to remove it for all images.
  </p>

  <Markdown
  rel="gatsby-config.js"
  language="javascript"
  md={`  ...
  logos: [
    {
      url: '/logos/nodejs.svg',
      gridSize: '4',
      title: 'Node JS',
    },
    {
      url: '/logos/react.svg',
      gridSize: '4',
      title: 'ReactJs & React Native',
    },
    {
      url: '/logos/graphql.svg',
      gridSize: '4',
      title: 'GraphQl & Prisma',
    },
    {
      url: '/logos/wordpress.svg',
      gridSize: '3',
      title: 'Wordpress Development',
    },
    {
      url: '/logos/google.svg',
      gridSize: '3',
      title: 'Google Analytics',
    },
    {
      url: '/logos/magento.svg',
      gridSize: '3',
      title: 'Magento',
    },
    {
      url: '/logos/photoshop.svg',
      gridSize: '3',
      title: 'Design / Adobe Creative Suite',
    },
    {
      url: '/logos/gitlab.svg',
      gridSize: '4',
      title: 'Consulting',
    },
    {
      url: '/logos/html5.svg',
      gridSize: '4',
      title: 'HTML5, Web Development',
    },
    {
      url: '/logos/css3.svg',
      gridSize: '4',
      title: 'CSS3, Design and UX',
    },
  ],
    ...`}
  />

  <p>
    Lets add a sass file specifically for services and add some minor CSS to it:
  </p>

  <Markdown
    rel="Terminal"
    language="terminal"
    md={`  > src/styles/_services.scss`}
  />

  <Markdown
    rel="_services.scss"
    language="scss"
    md={`  .serviceLogo {
    margin: 20px 0; /* add some breathing room */
    transition: transform 0.3s; /* add a nice little animation when hovered */

    &:hover {
      transform: scale(2);  /* animate it on a little zoom */
    }
  }`}
  />

  <p>
    And then import it into <b>main.scss</b>
  </p>

  <Markdown
    rel="_services.scss"
    language="scss"
    md={`  @import 'services.scss';`}
  />

  <hr />

  <p className="tldr">
    We have our large "N" in the hero section, our large "W" will be the same thing right? So lets cut that out of <b>_hero.scss</b> and put it in a common file.
  </p>

  <Markdown
    rel="Terminal"
    language="terminal"
    md={`  > src/styles/_theme.scss`}
  />

  <p>
    Paste our "#N" code into here, and lets think about reusability for a second. We could add "#W" as well, but that doesn't make much sense, what if you're company's letters aren't N and W and C? So lets change it up to be based off a class, we'll call it "largeLetter".
  </p>

  <Markdown
    rel="_theme.scss"
    language="scss"
    md={`  .large-letter {
  position: absolute; /* set it so we can move it off screen a bit */
  z-index: 9; /* make the z-index or layering slightly higher than normal */
  font-size: 1200px; /* set the font size to something very big */
  font-weight: 800; /* make it bold */
  color: rgba(0, 0, 0, 0.4); /* set it to black with an opacity of 40% */
  left: -200px; /* nudge it offscreen to the left */
  top: -900px; /* nudge it offscreen on top */
}`}
  />

  <p>
    Import it into <b>main.scss</b>:
  </p>

  <Markdown
    rel="main.scss"
    language="scss"
    md={`  ...
  @import 'theme.scss';
  ...`}
  />

  <p>
    And now, lets add the class in <b>large-letter.js</b>, which won't get us all the way there yet:
  </p>

  <Markdown
    rel="large-letter.js"
    language="javascript"
    md={`  ...
  <span id={letter} className="large-letter">
    {letter}
  </span>
  ...`}
  />

  <p>
    Since our sections are not positioned relatively everything stacks at the top, lets go into our <b>Hero.js</b> and <b>Services.js</b> components ad add one of our helper classes to position them remotely:
  </p>

  <Markdown
    rel="Hero.js"
    language="javascript"
    md={`  ...
    <section id="hero" className="font--light position--relative overflow--container" style={{
  ...`}
  />

  <Markdown
    rel="Services.js"
    language="javascript"
    md={`  ...
    <section id="services" className="container position--relative overflow--container">
  ...`}
  />

  <p>
    This positions them correctly but they bleed over into neighboring sections. So we'll create that "overflow--container" class to hide any excess. Open up <b>_theme.scss</b> again and lets add that class:
  </p>

  <Markdown
    rel="_theme.scss"
    language="scss"
    md={` ...
  .overflow--container {
    overflow: hidden;
  }`}
  />

  <p>
    We also want to the opacity a little lower on the white background, so lets add one class to that as well in <b>Services.js</b>:
  </p>

  <Markdown
    rel="Services.js"
    language="javascript"
    md={` ...
  <span className="opacity1">
    <LargeLetter letter={servicesData.letter} />
  </span>
  ...`}
  />

  <p>
    And now we're looking good. We could call it a day here but there's one more thing I would like to talk about. Page speed optimization.
  </p>

  <img src="/images/jamstack-jedi/services--logos.png" class="jamstack--img" alt="Service Logo Images" />

  <hr />

  <p>
    These logos LOOK nice, and they give us some neat little animations, but from a page loading perspective they're not needed. They aren't even on the screen when the page loads, so why load them now? Why not load them when the user starts to scroll down? They're small so they should load quickly, but every byte counts.
  </p>

  <p>
    We should <u>always</u> be aware about performance issues while coding, infact we'll later go through a checklist on just what needs to be done to optimize your page. But why wait until then to fix all our issues when we can prevent them all together?
  </p>

  <p>
    To solve this, we'll employ a tactic known as <b>Lasy Loading</b> and we'll use a plugin that's already been written with React components in mind. The package we'll use is called <a id="anchor--react-lazyload" href="https://www.npmjs.com/package/react-lazyload" target="_blank">React LazyLoad</a>, which you can take a quick peak at their docs to get a feel for how it works.
  </p>

  <p>
    Lets start by installing the dep:
  </p>

  <Markdown
    rel="Terminal"
    language="terminal"
    md={`  npm i react-lazyload --save`}
  />

  <p>
    Then we can open up our <b>Services.js</b> file and import it:
  </p>

  <Markdown
    rel="Services.js"
    language="javascript"
    md={`  ...
  import LazyLoad from 'react-lazyload';
  ...
  <LazyLoad offset={400}>
    <ServicesLogos logos={servicesData.logos} />
  </LazyLoad>`}
  />

  <p className="tldr">
    We use it by wrapping whatever we want lazy loaded in a "LazyLoad" component tag and then give it an offset. We'll go with 400, since that would be a good amount to have the user scroll before they become visible.
  </p>

  <p>
    Now, if you open up the <b>network</b> tab and manually refresh your page, and are at the top of the document, you won't see any of our logos loaded:
  </p>

  <img src="/images/jamstack-jedi/services--lazy-load.png" class="jamstack--img" alt="Lazy Load 1" />

  <p>
    Now, start scrolling down, once those images are within 400 pixels of the visible browser, you'll see them load in!
  </p>

  <img src="/images/jamstack-jedi/services--lazy-load-2.png" class="jamstack--img" alt="Lazy Load 2" />

  <p>
    If you compare the number of requests at the bottom right, we made about 30 requests on page load, then another 10 once they started scrolling down. Each kb saved and each http request deferred on page load helps our page speed out.
  </p>

  <p>
   We could lazy load the entire services component if we want, but that wouldn't make sense because we want these services listed for SEO value.
  </p>

  <p>
    That's it for our services section. We covered a pretty good amount here. Lets keep plugging along and tackle our portfolio next!
  </p>

  <AniLink to="jamstack-jedi/build-portfolio" className="nextBtn">Build the Portfolio &raquo;</AniLink>
    <br />
  </Layout>
);

export default ServicesBuild;
