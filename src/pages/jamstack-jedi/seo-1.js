import React from 'react';
import AniLink from '../../components/Common/AniLink';

import Layout from '../../components/layout';

const SEO1 = () => (
  <Layout
    title="SEO Overview"
    author={{ gravatar: '//0.gravatar.com/avatar/b5eff8d62adf3ff34e67f153781d27c4' }}
    gitRepo={null}
    time={3}
    description="Learn how to lay the foundation for your search engine optimization"
    keywords="Learn Google Analytics, Learn Google Tag Manager, Custom 404 Page, robots.txt, sitemap.xml Setup Google Search Console, Learn Schema Markup"
  >

  <strong>SEO Overview</strong>
  <br />
  <br />

  <p>
    At this point, we have a fully functional website, but if we want to get discovered by search engines we need to optimize our site for crawlers &amp; bots. In this first SEO section we'll cover a lot of the basics of SEO to get our site tagged and ready for crawlers.
  </p>

  <p>
    First we'll implement <b>Google Tag Manager</b> which will allow us to add marketing tags to our site without having to actually build and deploy our site. We'll sign-up and install GTM, then we'll add the snippet to our portfolio, and from then on we can add scripts like <b>Google Analytics</b> without touching our actual site's code.
  </p>

  <p>
    GTM allows us to keep all our marketing related snippets/scripts/pixels in one place, rather than polluting our code in our project with them.
  </p>

  <p>
    Next we'll setup a custom 404 page in Gatsby, which for a one page website isn't a big deal, but it's nice to have, especially if you plan on making more pages later.
  </p>

  <p>
    After that we'll setup our <b>robots.txt</b> file and <b>sitemap.xml</b> files, so that google knows where to look and can easily crawl our site.
  </p>

  <p>
    We'll then setup our <b>Google Search Console</b> account and link our sitemap to it.
  </p>

  <p>
    Finally we'll update our meta data to include some important tags we didn't add earlier as well as add some <b>schema markup</b> to help search engines better understand our site.
  </p>

  <p>
    As with previous chapters, before our build out, I will provide extra learning resources in-case you want to dive deeper into the how/why of each.
  </p>

    <AniLink to="jamstack-jedi/seo-gtm" className="nextBtn">Google Tag Manager &amp; Gatsby &raquo;</AniLink>
    <br />
  </Layout>
);

export default SEO1;
