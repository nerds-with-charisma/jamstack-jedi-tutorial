import React from 'react';
import AniLink from '../../components/Common/AniLink';

import Layout from '../../components/layout';
import Markdown from '../../components/Common/markdown';
import YouTube from '../../components/Common/YouTube';

const SPA101 = () => (
  <Layout
    title="SPA Overview"
    author={{ gravatar: '//0.gravatar.com/avatar/b5eff8d62adf3ff34e67f153781d27c4' }}
    gitRepo="https://gitlab.com/nerds-with-charisma/jamstack-jedi/tree/master/spa-101"
    time={15}
    description="What are single page applications? How do React and Gatsby make a single page application?"
    keywords="Single Page Applications, Single Page React Applications, Single Page Gatsby Applications"
  >
    <strong>SPA...Spa...Spaghetti?</strong>
    <br />
    <br />

    <p>
      What is an "SPA"? SPA stands for "single page application", which means that you load everything once, then use some magic to replace the body content of your app with the correct content based on the page. Think a standard header and footer at the top and bottom, then a main tag in the middle where you load your homepage content. When you click to another page, say the about page, all we do is load the about copy via some means, like ajax, and replace the homepage content with the about content. We don't need to reload our header and footer, we don't need to load our scripts or our styles, since our page uses all the same stuff, except the content in the main section in the middle.
    </p>

    <p className="tldr">
      Traditional, non-SPAs, use entire page refreshes to get all the content on a per page basis. This is more costly, usually slower, and pretty wasteful. Our content is maybe 10% of our page, so why reload the other 90% if we've already got it? That's where the SPA architecture really shines. We only load the 10% we need on a page (or route) change, making things much quicker and using less bandwidth.
    </p>

    <p className="tldr">
      Single page applications started coming into prominance a few years ago and have exploded recently. There are several SPA frameworks like AngularJs, Backbone, Vue, etc...The framework we'll be focusing on is <b>ReactJs</b>, created by Facebook.
    </p>

    <hr />

    <b>What is React</b>

    <p>
      Lets start by talking a bit about React was released in 2013. As of 2018, React surpassed AngularJS (made by Google) as the most used JS framework. The user base is huge, the amount of help on services like Stack Overflow is immense, and the amount of tutorials (for better or worse) are plentiful. In other words, there are lots of resources out there.
    </p>

    <p className="tldr">
      Additionally, I'd say React is actually really easy to learn. I know a lot of people disagree, but with helpers such as "create-react-app" or Gatsby, setting up a React app is actually really easy now. The "difficulty" comes when you need to customize and extend your setup. Which I wouldn't classify as "difficult" but rather "extendable". Which, sure, can be hard, but you can do pretty much anything you dream of...you just have to figure out HOW.
    </p>

    <p className="tldr">
      Another downside I've heard is that a lot of tuts are outdated, and that is very true. But that is because development is moving so quickly recently that one is in a constant state of learning the latest and greatest. I don't think that's really a React problem but a Javascript problem in general.
    </p>

    <p className="tldr">
      Finally there are a lot of new React features and lots of moving parts, that can be confusing. Do you use a state management plugin like Redux? Do you write "stateless" components? Should you use the new React "hooks" yet? These are all questions that can scare away new developers, and rightfully so. There's lots of buzz-words, and add-ons that can quickly change your setup.
    </p>

    <p className="tldr">
      All that being said, I still feel React is the best choice out there, and that you'll really enjoy it once you're comfortable with it. Hopefully none of the above scared anyone away.
    </p>

    <hr />

    <p className="tldr">
      I converted a very large eCommerce application from jQuery to Angular then finally to React. I know that I'm now happier to maintain it now that it's in React. It's also much faster to maintain, we've benchmarked that it's about 4x faster for dev's to make changes now than it was when it was in jQuery and Angular.
    </p>

    <p className="tldr">
      You can check out <a href="https://gist.github.com/tkrotoff/b1caa4c3a185629299ec234d2314e190" target="_blank">this article comparing React's rise</a> to the other major frameworks out there. There's also no shortage of <a href="https://www.indeed.com/q-React-Js-jobs.html" target="_blank">React jobs</a> found online (including lots of remote jobs) with high salaries attached.
    </p>

    <hr />

    <p className="tldr">
      The first thing you should do is become comfortable with working in React from a basics perspective. Understand how to make a component, and reuse a component. Know what props &amp; state are and how to pass them between components. How to map through a list of items, etc...
    </p>

    <p className="tldr">
      There are a lot of resources out there, a lot are outdate, a lot are not good. Here's a list of videos I suggest going through that will get you up and running.
    </p>

    <p>
      As for the standard 101 learning, I highly suggest you check out <a id="anchor--wes-bos-react-for-beginners" href="https://reactforbeginners.com/" target="_blank">Wes Bos' <b>React for Beginners</b></a>. This is a paid course, but I feel it is extremely worth it.
    </p>

    <p>
      If you're not in a position to pay for a tutorial or don't want to commit to such a long series, I would suggest this nice and quick tutorial called <a id="anchor--react-for-beginners-dev-ed" href="https://www.youtube.com/watch?v=dGcsHMXbSOA" target="_blank">React Tutorial For Beginners</a> by Dev Ed.
    </p>

    <YouTube url="https://www.youtube.com/embed/dGcsHMXbSOA" />
    <br />
    <p className="tldr">
      This tutorial uses <b>Create React App</b> to bootstrap their example, we'll be using Gatsby, but the actual React info is pretty universal.
    </p>

    <p>
      We'll also be using <b>React Hooks</b> so <a id="anchor--react-hooks" href="https://www.youtube.com/watch?v=-MlNBTSg_Ww" target="_blank">this tutorial by Acemind</a> is a really great primer on them.
    </p>

    <YouTube url="https://www.youtube.com/embed/-MlNBTSg_Ww" />
    <br />
    <hr />

    <strong>What is Gatsby</strong>

    <p className="tldr">
      So we've mentioned this term "Gatsby" a few times. What is it?
    </p>

    <p>
      Gatsby is a "static site generator", or in lay terms they take all of our html, css, javascript, bundle them up and optimize them, spit them out so that we can host them anywhere. It also grabs our dynamic data and compiles it into the page so that we don't need to communicate via ajax (you totally still can if you want).
    </p>

    <p>
      It also comes with a bunch of neat under the hood stuff that you can <a id="anchor--gatsby-get-started" href="https://www.gatsbyjs.org/tutorial/" target="_blank">check out and go through their tutorials</a> on the Gatsby website.
    </p>

    <p className="tldr">
      Some alternatives to Gatsby, with different caviats and functionality are Next.js, create-react-app, and rolling your own Node/React/Webpack setup.
    </p>

    <p className="tldr">
     We're building a portfolio though, which Gatsby is PERFECT for. If you're not feeling Gatsby, or you know you'll need to extend beyond what Gatsby is capable of, or you'll have lots of dynamic data, feel free to use another solution. Most of what we cover will be able to transfer over, just the Gatsby specific stuff you'll need to work out on your own.
    </p>

    <p>
      Andrew Mead is awesome enough to offer his <a id="anchor--gatsby-andrew-mead" href="https://www.youtube.com/watch?v=8t0vNu2fCCM" target="_blank">full length Gatbsy course</a> for free on YouTube, which you should totally check out.
    </p>

    <YouTube url="https://www.youtube.com/embed/8t0vNu2fCCM" />
    <br />
    <p className="tldr">
      Side note: I know it seems like there's lots of videos and reading and lots of tangents, but trust me they are worth it. This guide isn't meant to re-invent the wheel. These tutorials are from some of the best of the best in the development world. I wouldn't even be able to remotely compare in quality to what they're teaching from a basics stand point.
    </p>

    <p className="tldr">
      These 101 chapters are really to make sure you have a good, solid understanding of the underlying technologies. If you already know something, skip it! Also, please like, subscribe, or donate to these folks. They have spent a lot of time and energy to teach us, most of them for free. I wouldn't be where I am or able to keep up with the latest technologies without them and other people like them.
    </p>

    <p className="tldr">
      Also, I've read or watched everything I'm putting in these guides. My goal is to help you cut through the fat, and find the best resources to learn. I've wasted many hours reading books or following tuts that stalled or were just plain awful. Hopefully this vetting process I'm doing fast tracks your learning. You can follow along and just repeat what we show you, but if you really want to understand the underlying code and concepts you should consume as much as possible.
    </p>

    <blockquote className="text-center tldr">
      I am the wisest man alive, for I know one thing, and that is that I know nothing.
      <br />
      <b>- Socrates</b>
    </blockquote>
    <hr />

    <p>
      Now that we've gotten all that learning out of the way, before we actually start coding, we need to make sure you have the following completed (if you followed any of those tutorials you should be all set but lets confirm):
    </p>

    <hr />
    <b>Steps to setup</b>
    <p>
      Before we start to code our actual portfolio we need to install some dependencies, mainly NodeJs &amp; NPM, React DevTools, and GatsbyJs.
    </p>

    <p>
      Start by heading over to <a id="anchor--install-nodejs" href="https://nodejs.org/en/" target="_blank">NodeJs' website</a> and click the download button for the stable version. Once the download is completed, install node restart VS Code, and open up the terminal and type:
    </p>

    <Markdown
      rel="Terminal"
      language="terminal"
      md={
        `  node -v

   $ v10.16.3`}
    />

    <p id="anchor--update-nodejs">
        <b>Note: If you just need to upgrade your current version of node you can run the flowing commands "sudo npm cache clean -f", then "sudo npm install -g n", and finally "sudo n stable"</b>
    </p>

    <hr />

    <p>
      Next lets install the <a href="https://chrome.google.com/webstore/detail/react-developer-tools/fmkadmapgofadopljbjfkapdkoienihi?hl=en" target="_blank">React Dev Tools</a> Chrome extension. Once the plugin is installed, you can head to any website that uses ReactJs, like <a href="https://nerdswithcharisma.com" target="_blank">our website</a>, and enable the devtools (Right Click > Inspect).
    </p>

    <p>
      If you installed the plugin and enabled it, you should see a new option at the end of devtools called "React" if you're on the old version, or "Components" if you're on version 4+.
    </p>

    <img id="anchor--react-devtools" src="/images/jamstack-jedi/spa--react-devtools.png" class="jamstack--img" alt="React DevTools Plugin" />

    <p>
      React's Devtools are really awesome, you can step through all your components while developing and it really helps with debugging.
    </p>

    <hr />

    <p>
      The last thing we need to install is Gatsby, <a id="anchor--gatsby-install" href="https://www.gatsbyjs.org/docs/quick-start/" target="_blank">head on over to the Gatsby website</a> and follow the instructions for installing Gatsby, create a new site (replacing "&lt;portfolio name&gt;" with your company name), and start the dev server:
    </p>

    <Markdown
      rel="Terminal"
      language="terminal"
      md={
        `  npm install -g gatsby-cli

  gatsby new <portfolio name>

  cd <portfolio name>

  gatsby develop
  `}
    />

    <p>
      Once Gatsby is running you'll see a message saying it's available on localhost port 8000. So open up chrome, enter <b>http://localhost:8000</b> and hit enter. You should now see the default Gatsby page!
    </p>

    <img src="/images/jamstack-jedi/spa--gatsby-init.png" class="jamstack--img" alt="Created a new Gatsby project" />

    <p>
      Now that we've got a Gatsby project up and running, we can start building our portfolio!
    </p>

    <p className="tldr">
      This section will cover the basics of pulling in some data from our gatsby meta data. Later we will setup our meta data to come in from one file (gatsby-config.js), this will allow us to quickly change things without having to dig through our code, instead it will keep it all in one spot for quick changes. For now, we'll pass in our data as a prop.
    </p>

    <p className="tldr">
      Next, we'll setup Gatsby to use SASS, a CSS pre-processor that will let us write more efficient styles that are reusable as well as set up our basic variables and helpers. We will not be using Styled components, but rather a philosophy similar to SMACCS, or reusable styles. Feel free to use whatever styling philosophy you prefer.
    </p>

    <AniLink to="jamstack-jedi/gatsby-init" className="nextBtn">Gatsby Initial Setup &raquo;</AniLink>
    <br />
  </Layout>
);

export default SPA101;
