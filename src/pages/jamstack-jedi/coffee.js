import React from 'react';
import AniLink from '../../components/Common/AniLink';

import Layout from '../../components/layout';
import Markdown from '../../components/Common/markdown';

const Coffee = () => (
  <Layout
    title="Buy Me a Coffee"
    author={{ gravatar: '//0.gravatar.com/avatar/b5eff8d62adf3ff34e67f153781d27c4' }}
    gitRepo="https://gitlab.com/nerds-with-charisma/jamstack-jedi/tree/master/coffee"
    time={10}
    description="Setup a Buy Me a Coffee button in React."
    keywords="Buy me a coffee, accept donations, Buy me a coffee React"
  >
    <strong>
      I Honestly Don't Even Drink Coffee
    </strong>

    <br />
    <br />

    <p>
      There's lots of scenarios where you might want to accept some donations for your hard work (like writing a tutorial series!!!) but you're not exactly sure the best way to do it. There's a few services out there that allow users to send you money, you could use something direct like Google Pay or Stripe, or you could use something a little more dedicated to donations like <a href="https://www.buymeacoffee.com/" target="_blank">https://www.buymeacoffee.com/</a>
    </p>

    <p>
      Their service lets you setup a landing page, create a button, and then accept donations that you can put on your site. <a href="https://www.buymeacoffee.com/faq" target="_blank">Check out their FAQs</a> if you'd like to learn more about their service. They keep 5% of your donation, so if that sounds like too much or you're not interested in adding a button you can skip this entire section.
    </p>

    <p>
      If that DOES sound cool, then lets get started. First sign-up for an account and they will ask you how you want to connect to recieve funds, we chose PayPal, which is very simple. Simply click the PayPal button and click <b>Verify</b>. You will get a PayPal login modal popup where you're asked to sign in to your PayPal and allow Buy Me a Coffee.
    </p>

    <p>
      If all went well, you should be brought to the BMAC dashboard. Go ahead and fill in all the details on your <b>Edit Page</b> section as well as the <b>Coffee Options</b>
    </p>

    <p>
      Finally, lets create our button. Head to the <b>Overview</b> tab and click <b>Create your button</b>. Customize it by changing the <b>custom text</b>, <b>font</b>, and <b>color</b>. Click <b>Copy Image Code</b> (feel free to use the HTML code if you want to modify it). We can't use the code as is, because we're using react so we need to make a few changes, but we can put the code in our footer (or wherever you'd like) and we'll modify it from there.
    </p>

    <p>
      Open up <b>Footer.js</b> and at the bottom lets add our image code:
    </p>

    <Markdown
      rel="Footer.js"
      language="javascript"
      md={
      `  <small className="font--text">
    {\`copyright © ${new Date().getFullYear()} \${title.toLowerCase()}\`}
    <br />
    <span className="font--text opacity8">
      <div dangerouslySetInnerHTML={{ __html: footerTagline }} />
    </span>
  </small>

  <br />

  <a href="https://www.buymeacoffee.com/yz6MAXbTE" target="_blank">
    <img
      src="https://bmc-cdn.nyc3.digitaloceanspaces.com/BMC-button-images/custom_images/white_img.png"
      alt="Buy Me A Coffee"
    />
  </a>`}
    />


    <p>
      If you want to use the HTML code your code would look something like this:
    </p>

    <Markdown
      rel="Footer.js"
      language="javascript"
      md={
      `  <small className="font--text">
    {\`copyright © ${new Date().getFullYear()} \${title.toLowerCase()}\`}
    <br />
    <span className="font--text opacity8">
      <div dangerouslySetInnerHTML={{ __html: footerTagline }} />
    </span>
  </small>

  <br />

  <a className="bmc-button" target="_blank" href="https://www.buymeacoffee.com/yz6MAXbTE">
    <img
      src="https://bmc-cdn.nyc3.digitaloceanspaces.com/BMC-button-images/BMC-btn-logo.svg"
      alt="Buy me a coffee"
    />
    <span>
      {' Buy me a coffee'}
    </span>
  </a>`}
    />

    <p>
      Then we  need to add some styles to make it look right so create a new file and import it into <b>main.scss</b>:
    </p>

    <Markdown
      rel="Terminal"
      language="terminal"
      md={
      `  > src/styles/_coffee.scss`}
    />

    <Markdown
      rel="main.scss"
      language="scss"
      md={
      `  ...
  @import 'contact.scss';
  @import 'coffee.scss';
  ...`}
    />

    <p>
      Restart your server and open up the <b>_coffee.scss</b> file. Add the following code, which is just a port from the styles generated when you created the button:
    </p>

    <Markdown
      rel="_coffee.scss"
      language="scss"
      md={
      `  .bmc-button img{
        width: 27px !important;
        margin-bottom: 1px !important;
        box-shadow: none !important;
        border: none !important;
        vertical-align: middle !important;
      }

      .bmc-button{
        line-height: 36px !important;
        height:37px !important;
        text-decoration: none !important;
        display:inline-flex !important;
        color:#000000 !important;
        background-color:#FFFFFF !important;
        border-radius: 3px !important;
        border: 1px solid transparent !important;
        padding: 1px 9px !important;
        font-size: 22px !important;
        letter-spacing:0.6px !important;
        box-shadow: 0px 1px 2px rgba(190, 190, 190, 0.5) !important;
        -webkit-box-shadow: 0px 1px 2px 2px rgba(190, 190, 190, 0.5) !important;
        margin: 0 auto !important;
        font-family:'Cookie', cursive !important;
        -webkit-box-sizing: border-box !important;
        box-sizing: border-box !important;
        -o-transition: 0.3s all linear !important;
        -webkit-transition: 0.3s all linear !important;
        -moz-transition: 0.3s all linear !important;
        -ms-transition: 0.3s all linear !important;
        transition: 0.3s all linear !important;
      }

      .bmc-button:hover, .bmc-button:active, .bmc-button:focus {
        -webkit-box-shadow: 0px 1px 2px 2px rgba(190, 190, 190, 0.5) !important;
        text-decoration: none !important;
        box-shadow: 0px 1px 2px 2px rgba(190, 190, 190, 0.5) !important;
        opacity: 0.85 !important;
        color:#000000 !important;
      }
      `}
    />

    <p>
      The final thing to do is to load the Google webfont (if you want to, or you can save some loading time and just use a default font):
    </p>

    <Markdown
      rel="_coffee.scss"
      language="scss"
      md={
      `  @import url('https://fonts.googleapis.com/css?family=Cookie&display=swap');

  .bmc-button img{
  ...`}
    />

    <p>
     That's it, super easy right? You can test it by clicking the button and seeing it go to your BMAC page.
    </p>

    <p>
      Before we go, we should probably move the url to our <b>gatsby-config.js</b> file so that we can quickly change it later. Here's the code for that if you want to move it:
    </p>

    <Markdown
      rel="gatsby-config.js"
      language="javascript"
      md={
      `  ...
  siteMetadata: {
    siteUrl: \`https://nerdswithcharisma.com\`,
    coffeeUrl: 'https://www.buymeacoffee.com/yz6MAXbTE',
  ...`}
    />

    <Markdown
      rel="index.js"
      language="javascript"
      md={
      `  ...
  siteMetadata {
    coffeeUrl
    title
  ...
  <Footer
    title={data.site.siteMetadata.title}
    signOff={data.site.siteMetadata.signOff}
    footerTagline={data.site.siteMetadata.footerTagline}
    coffeeUrl={data.site.siteMetadata.coffeeUrl}
  />
  ...`}
    />

    <Markdown
      rel="Footer.js"
      language="javascript"
      md={
      `  ...
  const Footer = ({ title, signOff, footerTagline, coffeeUrl }) => (
  ...
  { coffeeUrl && (
    <a className="bmc-button" target="_blank" href={coffeeUrl}>
    <img
      src="https://bmc-cdn.nyc3.digitaloceanspaces.com/BMC-button-images/BMC-btn-logo.svg"
      alt="Buy me a coffee"
    />
    <span>
      {' Buy me a coffee'}
    </span>
  </a>
  )}
  ...`}
    />

    <p>
      That wraps up this chapter, in the next chapter we'll continue our frivolous things by adding some javascript that we don't really need. But you might want to do something with it later so we'll add it, show you how to do stuff with it, and comment it out if you don't think it's necessary.
    </p>

    <AniLink to="jamstack-jedi/js-fun" className="nextBtn">Javascript Fun &raquo;</AniLink>
    <br />
  </Layout>
);

export default Coffee;
