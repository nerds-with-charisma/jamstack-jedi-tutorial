import React from 'react';

import AniLink from '../../components/Common/AniLink';
import Layout from '../../components/layout';
import Markdown from '../../components/Common/markdown';

const Git101 = () => (
  <Layout
    title="GitLab"
    author={{ gravatar: '//0.gravatar.com/avatar/b5eff8d62adf3ff34e67f153781d27c4' }}
    gitRepo={null}
    time={12}
    description="Get setup with GitLab and get your code version controlled"
      keywords="Learn GitLab, GitLab Basics, Get better at GitLab, GitLab 101, Intro to GitLab"
  >
    <strong>GitLab Setup</strong>
    <br />
    <br />

    <p>
      Head on over to <a href="https://about.gitlab.com/" target="_blank">GitLab</a> and click on <b>register</b> if you don't already have an account. If you do, go ahead and login now. Once signed up or logged in you should be presented with your projects dashboard. If you just signed up, then this should be empty.
    </p>

    <p>
      Lets setup a new project for our coming soon page, click on the "New Project" button at the top right and lets fill out the fields for our new repo.
    </p>

    <p>
      Make sure you choose <b>Blank project</b> from the tab at the top. Then lets fill out our new repo's data. For <b>Project Name</b> lets use "coming-soon". <b>Project URL</b> and <b>Project slug</b> you shouldn't have to change. Finally, change the <b>visibility</b> radio to <b>Public</b> (or choose private if you don't want your project to be visible by anyone not logged in). And do not check the <b>Initialize repository with a README</b> checkbox. Click <b>Create project</b>.
    </p>

    <img src="/images/jamstack-jedi/gitlab--new-repo.png" class="jamstack--img" alt="Create a new GitLab repo" />

    <p>
      Now we're ready to use our new repo to store our coming soon page's code. Return to VS Code and open the built-in terminal (ctl + ~). And type in the following:
    </p>

    <Markdown
      rel="Terminal"
      language="terminal"
      md={
        `  git init`
      }
    />

    <p className="tldr">
      You should see something like, "Initialized empty Git repository in /path/on/your/computer/.git/"
    </p>

    <p className="tldr">
      This initializes a git repo in our project, but it's only local. So let's link it to our GitLab repo and push our code up there.
    </p>

    <p>
      Return to your GitLab repo and you should see lots of initial commands you can run.
    </p>

    <img src="/images/jamstack-jedi/gitlab--repo-dash.png" class="jamstack--img" alt="New Repo Landing Page" />

    <p>
      We want to push our existing folder up to GitLab, so we'll follow the <b>Push an existing folder</b> instructions. We've already initialized our git repo, so we'll now link it to our GitLab repo.
    </p>

    <Markdown
      rel="Terminal"
      language="terminal"
      md={
        `  git remote add origin {your git repo goes here}

  example:
  git remote add origin git@gitlab.com:briandausman/coming-soon.git`
      }
    />

    <p>
      Then we want to add all our files to git with the short-hand command for add all:
    </p>

    <Markdown
      rel="Terminal"
      language="terminal"
      md={
        `  git add .`
      }
    />


    <p>
      Commit our changes, with a message:
    </p>

    <Markdown
      rel="Terminal"
      language="terminal"
      md={
        `  git commit -m "Initial commit"`
      }
    />

    <p>
      Finally, we want to push all our files to the git repo on GitLab's server:
    </p>

    <Markdown
      rel="Terminal"
      language="terminal"
      md={
        `  git push -u origin master`
      }
    />

    <p className="tldr">
      We should see a success message in the terminal, and if we return to our GitLab repo, we should see the files in the repo if we refresh.
    </p>

    <img src="/images/jamstack-jedi/gitlab--initial-commit.png" class="jamstack--img" alt="First Git Commit" />

    <p>
      This is our Git flow in a nutshell. We'll be doing this over and over again. Add, commit with a message, push. You should also get used to pulling before you commit your code.
    </p>

    <p>
      Even though we're the only ones working on this project, lets pull just to make sure we have the most up to date code.
    </p>

    <Markdown
      rel="Terminal"
      language="terminal"
      md={
        `  git pull`
      }
    />

    <p>
      You should see a message like, "Already up-to-date." which makes sense. We're the only ones working and we haven't made any changes. But lets simulate working with someone else on this project. Return to GitLab and click the <b>+ Dropdown</b> and select <b>New File</b>.
    </p>

    <img src="/images/jamstack-jedi/gitlab--new-file.png" class="jamstack--img" alt="GitLab new file" />

    <p>
      Name this new file readme.md (.md is a markdown file), Put something in the body, add a commit message, leave branch set to "master" and click <b>Commit Changes</b>.
    </p>

    <img src="/images/jamstack-jedi/gitlab--new-readme.png" class="jamstack--img" alt="Create a Readme in GitLab" />

    <p>
      Return to your repo's homepage and you should now see the readme file in the root directory. But check your local workspace, there's no readme file there.
    </p>

    <p>
      To get that file, simply do "git pull" again.
    </p>

    <Markdown
      rel="Terminal"
      language="terminal"
      md={
        `  git pull`
      }
    />

    <img src="/images/jamstack-jedi/gitlab--pull.png" class="jamstack--img" alt="Pull from GitLab" />

    <p>
      You should now see the readme file locally in your working code! This is how your average Git flow will go. So lets recap:
    </p>


    <p id="anchor--git-flow">
      <li>First you should pull to get the latest files from the server with <b>git pull</b></li>
      <li>Next, make your changes, when you're done stage them with <b>git add .</b> or <b>git add &lt;filename&gt;</b> if you wish to only stage certain files</li>
      <li>Commit your files with a message, <b>git commit -m "Some Message!"</b></li>
      <li>Pull again, since you don't know if there's been changes since you started your code, <b>git pull</b></li>
      <li>Push your code to the remote repo on GitLab, <b>git push -u origin master</b> or simply <b>git push</b></li>
      <li>That's it! This will be the majority of what you do in Git.</li>
    </p>

    <br />

    <p>
      Not digging the command line? That's ok, there are lots of Git clients out there. If you'd prefer to use a graphical application, I would suggest <a id="anchor--fork" href="https://git-fork.com/" target="_blank">Fork</a>, which is an awesome Git client.
    </p>

    <p>
      Now that we have our coming soon page on a GitLab repo, go ahead and make any changes you'd like before we push it to a server. This is your time to customize it with your info, images, contact information, etc...
    </p>

    <p>
      Once you're done making any customizations, push it to the GitLab repo (git pull / git add . / git commit -m "message" / git pull / git push) and you'll be ready to upload it somewhere. In the coming chapters we'll cover purchasing a domain name, and two kinds of deployment methods.
    </p>


    <AniLink to="jamstack-jedi/domain-name" className="nextBtn">Buying a Domain Name &raquo;</AniLink>
    <br />
  </Layout>
);

export default Git101;
