import React from 'react';
import AniLink from '../../components/Common/AniLink';

import Layout from '../../components/layout';
import Markdown from '../../components/Common/markdown';

const Mailchimp = () => (
  <Layout
    title="Mailchimp"
    author={{ gravatar: '//0.gravatar.com/avatar/b5eff8d62adf3ff34e67f153781d27c4' }}
    itRepo="https://gitlab.com/nerds-with-charisma/jamstack-jedi/tree/master/mailchimp"
    time={20}
    description="Connect to the Mailchimp api to signup users from a custom form"
    keywords="Mailchimp API, Mailchimp custom form, Mailchimp signups"
  >

  <strong>Stupid Monkey</strong>
  <br />
  <br />

  <p>
    The last thing we need to do before we can push to prod is wire up our contact form. We can achieve this with the Mailchimp api. First you need to signup for a <a href="https://login.mailchimp.com/signup/" target="_blank">Mailchimp account</a>.
  </p>

  <p className="tldr">
    Mailchimp is an email delivery service that allows us to collect subscribers, generate forms, send out emails, etc...I've used them for a long time and they are simply amazing. They offer a very generous free tier that you can grow and expand if your website really starts to take off and you're collecting thousand of subscribers.
  </p>

  <p>
    Once you're signed up with MC, we need to create a new <b>Form</b> to let MC know what inputs to expect. Head on over to the <b>Audience</b> section and click on <b>Signup Forms</b>.
  </p>

  <p>
    Select the <b>Form Builder</b> option and from the list on the right, under "Add a field", choose <b>Text</b>. You should see it added to the bottom of our form, click it to select it and name it <b>Message</b>, give it a field name of "MESSAGE", and make it required. Click <b>Save Field</b> and lets head back to the audience tab.
  </p>

  <img src="/images/jamstack-jedi/mailchimp--form-builder.png" class="jamstack--img" alt="Mailchimp form builder" />

  <p>
    Now that we've built our form, we need to grab the url for it. Head back to <b>Signup Forms</b> and choose <b>Embedded Forms</b>. Scroll into the copy/paste section and find the form "action". Copy the attribute value for this, it'll be something like <b>https://gmail.us20.list-manage.com/subscribe/post?u=xxxxx&amp;id=yyyyy</b>.
  </p>

  <hr />

 <p>
    We're going to use the <a id="anchor--gatsby-mailchimp" href="https://www.gatsbyjs.org/packages/gatsby-plugin-mailchimp/" target="_blank">Gatsby Mailchimp Plugin</a> to handle the form submission. So lets install that:
 </p>

 <Markdown
    rel="Terminal"
    language="terminal"
    md={`  npm install gatsby-plugin-mailchimp`}
  />

  <p>
    Next we need to configure it in <b>gatsby-config.js</b> just like our other plugins:
  </p>

  <Markdown
    rel="gatsby-config.js"
    language="javascript"
    md={`  ...
  {
    resolve: 'gatsby-plugin-mailchimp',
    options: {
        endpoint: 'https://gmail.us20.list-manage.com/subscribe/post?u=8914e75fae490805be15a9e74&amp;id=3ca0742e30',
    },
  },
    ...`}
  />

  <p>
    Place <b><u>YOUR</u></b> endpoint you got from the MC form's action in the endpoint node. Now head into our <b>Contact.js</b> component and we need to import the MC plugin:
  </p>

  <Markdown
    rel="Contact.js"
    language="javascript"
    md={`  ...
  import React, { useState } from 'react';
  import addToMailchimp from 'gatsby-plugin-mailchimp';
  ...`}
  />

  <p>
   Then locate our "todo" from earlier in the <b>handleSubmit</b> function, and replace it with the following:
  </p>

  <Markdown
    rel="Contact.js"
    language="javascript"
    md={`  ...
  // everything's good, submit to mailchimp
  addToMailchimp(email, {
    MESSAGE: message,
  }).then(data => {
    if (data.result === 'error') {
      setMessageError(data.msg);
    } else {
      setMessageSuccess('Thanks, we\\'ll get back to you shortly!');
    }
  });
  ...`}
  />

  <p className="tldr">
    We call the <b>addToMailchimp</b> function from the plugin and pass it the email state object, we also pass it the "merge field" for "message" that we setup in our form builder earlier.
  </p>

  <p className="tldr">
    We get a "data" response back, and check if the data.result node is equal to "error", if it is, we print whatever message they sent back. This will be a message like, "This email is already subscribed" or something similar, if an error occurred while trying to signup.
  </p>

  <p>
    If there's no error we set a success message, which we haven't created yet, so lets do that now.
  </p>

  <Markdown
    rel="Contact.js"
    language="javascript"
    md={`  ...
  const [messageError, setMessageError] = useState(null);
  const [messageSuccess, setMessageSuccess] = useState(null);
  ...
  { (messageError)
    && (
      <span className="bg--cta font--light padding-sm" dangerouslySetInnerHTML={{ __html: messageError }} />
    )
  }

  { (messageSuccess) ? (
    <span className="bg--success font--light padding-sm">
      { messageSuccess }
    </span>
  ) : (
    <form id="contactForm" onSubmit={(e) => handleSubmit(e)}>
      <label htmlFor="email">
        <span>Email</span>
        <input
          type="email"
          id="email"
          name="email"
          placeholder="Your Email"
          onChange={(e) => setEmail(e.target.value)}
        />
      </label>
      <label htmlFor="message">
        <span>Message</span>
        <input
          type="message"
          id="message"
          name="message"
          placeholder="Your Message"
          onChange={(e) => setMessage(e.target.value)}
          />
      </label>

      <button type="submit" className="btn btn--gradient">
        {'Contact Us!'}
      </button>
    </form>
  )}
  ...`}
  />

  <p className="tldr">
    First we create our state var for the success message. Then we change the messageError code to accept some HTML, since it's possible the MC api could return HTML instead of a simple string.
  </p>

  <p className="tldr">
    Then we wrap our form in a conditional check for the success message, if there is a success message, show the success message and hide the form, if not, show the form and not the message.
  </p>

  <p>
    And we need to make one more style change since MC can return html, place this just before the closing brace for our #contact styles:
  </p>

  <Markdown
    rel="_contact.scss"
    language="javascript"
    md={`  ...
    .bg--cta {
      box-shadow: 0 4px 15px rgba(0, 0, 0, 0.2);

      a {
        color: #fff;
        text-decoration: underline;
      }
    }
  ...`}
  />

  <p>
    Try submitting the form, do an error test as well as a successful test to make sure both scenarios work and look right.
  </p>

  <img src="/images/jamstack-jedi/mailchimp--success.png" class="jamstack--img" alt="Mailchimp signup success" />

  <br />

  <img src="/images/jamstack-jedi/mailchimp--failure.png" class="jamstack--img" alt="Mailchimp signup failure" />

  <p>
    Head over to mailchimp, and check for the test you just made, you should see it with the message field populated:
  </p>

  <img src="/images/jamstack-jedi/mailchimp--subscriber.png" class="jamstack--img" alt="Mailchimp subscriber" />

  <p>
    That's it, we're now ready to deploy to prod and replace our coming soon page! In the next section, we'll configure that, but that doesn't mean we're done with this tutorial series. There's still lots of SEO and optimizations to do once our site is live.
  </p>

  <AniLink to="jamstack-jedi/prod" className="nextBtn">Deploy to Prod &raquo;</AniLink>
  <br />
  </Layout>
);

export default Mailchimp;
