import React from 'react';

import AniLink from '../../components/Common/AniLink';
import Layout from '../../components/layout';
import YouTube from '../../components/Common/YouTube';

const Git101 = () => (
  <Layout
    title="Git 101"
    author={{ gravatar: '//0.gravatar.com/avatar/b5eff8d62adf3ff34e67f153781d27c4' }}
    gitRepo={null}
    time={8}
    description="Learn Git, the first steps to becoming a front-end developer. Git basics."
    keywords="Learn Git, Git Basics, Get better at Git, Git 101, Intro to CSGitS"
  >
    <strong>Git Got!</strong>
    <br />
    <br />

    <p>
      What is Git? What does it do? Do I have to use the command line?
    </p>

    <p className="tldr">
      Git's a tricky subject, my goal is to provide you with the most layman's explanation of what Git is so that you can understand the basic workflow.
    </p>

    <p>
      So what is Git? In the simplest terms Git will keep track of the changes you make to files. This allows you to look back at what you've previously done, compare a file's history, and in a worst case scenario, roll back your changes if you broke something. Git, like SVN, is a "version control system".
    </p>

    <p className="tldr">
      Git is especially great for teams, think about how you would work on the same project with multiple people. If you didn't have Git, you'd have to communicate which files you're working on or manually copy/paste your code into files so that you wouldn't step on each other's toes or overwrite other people's changes.
    </p>

    <p className="tldr">
      With Git, you checkout a remote repository, or bundle of code, locally to your computer. You then work on a specific "branch". Think of branches just like a tree, where the base is the main "master" code bundle, and each branch is a specific task someone is working on. Eventually you merge your branch into the master, other developers do the same, and all the code ends up in one central place.
    </p>

    <p className="tldr">
      With Git, if there's a conflict where your code and someone elses code effected the same lines, Git will try to automatically add your changes along with the other devs changes, if the changes are too large for Git to understand, it will give you the opportunity to "merge" the two files manually. Git will allow you to pick which lines of code from your and the other dev to keep.
    </p>

    <p className="tldr">
      Also, being able to see your history is great. Say you created our coming soon page, everything works great, but then someone else tries to update the contact email button. They commit their code and you find out the next day that the email button no longer works. Oh no! Fear not, two ways git can help you is it gives you the ability to see what code that developer changed in their specific commit. Second, it allows you to see what the code was <em>BEFORE</em> they made their change. Now you have a few options, you can revert their code, setting it back to the way it was before their changes or you can see their change and fix what they broke.
    </p>

    <hr />

    <p>
      That's a super high level overview of Git, so lets see how it actually works in practice. First you need to <a id="anchor--git-windows" href="https://git-for-windows.github.io/" target="_blank">install Git</a> if you're on Windows. If you're on OSX, Git should come pre-installed. You can confirm this by typing "git" into your command line.
    </p>

    <p>
      Now that we have Git installed, we need to decide where we're going to host our repo. You don't <em>HAVE</em> to use an external repository, you could simply use Git on your machine, but if you want to work with other developers or on different machines, you'll need a global repository somewhere.
    </p>

    <p>
      The main players in the cloud-git-repo-game are <a href="https://github.com" target="_blank">Github</a>, <a href="https://gitlab.com" target="_blank">GitLab</a>, and <a href="https://bitbucket.org" target="_blank">Bitbucket</a>. There are others, but I would suggest sticking with the big dogs. My preference is Gitlab.
    </p>

    <p className="tldr">
      Why GitLab? There's a couple of reasons, it's simple, I believe their setup is more helpful, I like the interface better, and the final (but probably most important) reason is they allow you private repositories without paying. Github (who is the largest git service) charges you if you want to use non-public repos. Lost of times, public is fine, but there will be some instances where you just don't want other people to see your code. This makes GitLab the best hosting provider in my opinion.
    </p>
    <hr />

    <p>
      Before we get going, I would suggest you go through <a id="anchor--gitlab-training" href="https://docs.gitlab.com/ee/university/training/end-user/" target="_blank">GitLab's training docs</a>.
    </p>

    <p>
      Next, to understand how the Git flow works and get comfortable with the common actions you'll be performing, I suggest going through <a id="anchor--rys-git-tutorial" href="https://johnmathews.eu/rys-git-tutorial.html" target="_blank">Ry's Git Tutorial</a>, which is super well explained.
    </p>
    <p>
      Another book that is great (but deep) is <a id="anchor--pro-git" href="https://git-scm.com/book/en/v2" target="_blank">Pro Git</a>, which I wouldn't say is required reading, but I'd suggest it if you want to go all the way down the Git rabbit hole.
    </p>

    <p>
      As always, if you're not the reading type, here's another video you can watch a <a id="anchor--git-crash-course" href="https://www.youtube.com/watch?v=SWYqp7iY_Tc" target="_blank">by Traversy Media</a>.
    </p>

    <YouTube url="https://www.youtube.com/embed/SWYqp7iY_Tc" />

    <p className="tldr">
      Now that you've gone through some learning materials on Git, you should have a decent understanding of what it is and does. But you might not totally understand where it fits in with our daily workflow. In the next section, we'll go through some practical examples and get our coming soon page onto a remote repo.
    </p>


    <AniLink to="jamstack-jedi/gitlab" className="nextBtn">Start Using GitLab &raquo;</AniLink>
    <br />
  </Layout>
);

export default Git101;
