import React from 'react';
import AniLink from '../../components/Common/AniLink';

import Layout from '../../components/layout';
import Markdown from '../../components/Common/markdown';

const Speed = () => (
  <Layout
    title="Speed"
    author={{ gravatar: '//0.gravatar.com/avatar/b5eff8d62adf3ff34e67f153781d27c4' }}
    gitRepo="https://gitlab.com/nerds-with-charisma/jamstack-jedi/tree/master/opti-speed"
    time={15}
    description="Learn how to make your website faster by using page speed audits to determine how to speed up your website."
    keywords="Lighthouse, page speed audits, website optimization"
  >
    <strong>
       Must Go Faster...Must Go Faster...
    </strong>
    <br />
    <br />

    <p className="tldr">
      Perhaps the most important thing we can do to optimize our website is to make it faster. We have to find a happy medium between speed and usability, but we can do a lot to make things snappy while still maintaining all the data and info.
    </p>

    <p>
      We're going to use 2 devtools to audit our site, the first is the <b>Lighthouse Audit</b> tab in Chrome. The other is the network tab. Lets open up the <b>Lighthouse Audit</b> by opening up devtools and selecting the "Audits" tab.
    </p>

    <img src="/images/jamstack-jedi/speed--settings.png" class="jamstack--img" alt="Lighthouse Desktop settings" />

    <p>
      We'll perform 2 tests, one for desktop and one for mobile. Lets start with desktop, use the following settings and hit <b>Run audits</b>: Device: Desktop, Audits: All but Progressive web app, Throttling: Simulated Slow, Clear Storage: checked/true.
    </p>

    <img src="/images/jamstack-jedi/speed--results.png" class="jamstack--img" alt="Lighthouse Desktop results" />

    <p>
      We're running locally, so our scripts and what not have not been optimized, so we'll ignore <b>Performance</b> for right now. All the others look pretty good, we can fix them and get pretty close to 100 though. Scroll down to <b>Accessibility</b> and you'll see it's yelling at us for something we fixed in Wave, it's saying our contrast for the footer tagline still isn't good enough. Fair enough, lets fix that by adjusting the opacity class a litte bit:
    </p>

    <Markdown
    rel="Footer.js"
    language="javascript"
    md={`  ...
    <span className="font--text opacity8">
  ...`}
  />

  <p>
    Changing the opacity from 60% to 80% might not seem like a lot, but if it appeases the accessibility gods, then we might as well do it. Rerun the audit and we're at 100% for accessibility!
  </p>

  <p>
    Next it yells at us for "Does not use HTTP/2 for all it's resources". This isn't something we need to worry about locally, so lets skip that.
  </p>

  <p>
    Finally it tells us that we have "Links to cross-origin destinations" which is unsafe (<a id="anchor--link-cross-origin" href="https://developers.google.com/web/tools/lighthouse/audits/noopener?utm_source=lighthouse&utm_medium=devtools" target="_blank">read more here</a>). This means that we're linking to a website outside of our domain, and that search engines see this as a problem. It could negatively effect our site, so they suggest adding a "rel" tag telling the browser to not allow the page we're opening to access our site's info. This is a very easy fix, we simply need to add rel="noopener noreferrer" to any place we're using target="_blank". We only did this in a one place, so it's a quick fix. Open up <b>social-links.js</b> and edit our links to the following:
  </p>

  <Markdown
    rel="social-links.js"
    language="javascript"
    md={`  ...
  <a
    key={link.title}
    className="font--grey padding-md"
    href={link.link} target="_blank"
    rel="noopener noreferrer"
  >
  ...`}
  />

  <p>
    Next, under <b>SEO</b> it's yelling at us for not having a meta description. But we added that in <b>SEO.js</b> a long time ago, right? Something seems to have broken this, remember when we changed our Helmet tag from self-closing to have content in-between...yup that broke it. So lets fix this. Open up <b>Seo.js</b> and lets modify how we print out our meta items similar to how we did the Open Graph items:
  </p>

  <Markdown
    rel="social-links.js"
    language="javascript"
    md={`  ...
  <Helmet
    htmlAttributes={{ lang }}
    title={title}
  >
    { meta.map((item) => (
      <meta key={item.name} name={item.name} content={item.content} />
    ))}
  ...`}
  />

  <p>
    The last item says that we have an invalid <b>canonical tag</b>. Which is sort of true, we used our domain name, but we're working on localhost. Once we push to prod, this will fix itself. So nothing left to do here.
  </p>

  <hr />

  <p>
    Now for performance, the only way we can test this without deploying to our server is to do a <b>gatsby build</b> then serve it via the internal server Gatsby offers. So lets do that and run the audit again.
  </p>

  <Markdown
    rel="Termianl"
    language="terminal"
    md={`  gatsby build

  gatsby serve`}
  />

  <p>
    Now open up <b>http://localhost:9000/</b> and run the same audit (note: it's best to run this in an incog window with plugins disabled). Our score is much better now, 97/100!
  </p>

  <p>
    There are 3 minor issues, 2 related to Font-Awesome. The first is removing unused CSS, since we're using Font-Awesome we don't have access to the source to only load what we're using, so we have to ignore this one. The second related to FA, is "Ensure text remains visible during webfont load", which I have tried to find a work around to...but so far I have not. So we will ignore this for now. A possible work around would be to make your own SVG icons for social links and the menu button and remove FA completely.
  </p>

  <p>
    The other issue says that we should serve our portfolio items in "next-gen" image formats. We could serve them as webp images but we could also just lazy load these items, since they're not really needed on page load.
  </p>

  <p>
    Open up <b>Portfolio.js</b> and import our Lazy plugin, then wrap our <b>PortfolioItem</b> component in it, we'll give it a threshold of 400 pixels:
  </p>

  <Markdown
    rel="Portfolio.js"
    language="javascript"
    md={`  ...
  import LazyLoad from 'react-lazyload';
  ...
  <LazyLoad offset={400}>
    <div className="masonry">
      { portfolioData.portfolioData.sort((a, b) => a.sort < b.sort).map((item, i) => (
        <PortfolioItem key={\`\${item.alt}\${i}\`} item={item} setPortItem={setPortItem} />
      ))}
    </div>
  </LazyLoad>`}
  />

  <p>
    Save our changes, build and server again, and rerun the audit and now we're up to 98! This is as good as we'll get in terms of desktop. Lets try rerunning the audit on mobile.
  </p>

  <img src="/images/jamstack-jedi/audit--desktop.png" class="jamstack--img" alt="Lighthouse Desktop Results" />

  <p>
    Mobile looks pretty good too! Lets do one final check-in manually by opening up our network panel and refreshing the page. If we sort by size, we can find if there are any outliers like large images that would slow our site down.
  </p>

  <img src="/images/jamstack-jedi/speed--waterfall.png" class="jamstack--img" alt="Waterfall analysis" />

  <p className="tldr">
    We've done a good job of keeping everything lean as we went. If there was anything that jumped out as being large here, we'd want to address it. You can see at the bottom, that our page took 428ms to load, which is really really good. Expect it to increase a little when we put it onto a server as there is some waiting time for the server to communicate with the browser. But once we push live, things should be looking really good!
  </p>

  <hr />

  <p>
    Before we close out this chapter, I highly suggest you go through the <a id="anchor--lighthouse-docs" href="https://developers.google.com/web/tools/lighthouse/v3/scoring" target="_blank">Scoring Docs</a> and understand what all the criteria are for Lighthouse. Since we were aware of trying to keep things nice and speedy we had a lot of "Passes" when we started, as those items are collapsed by default, we didn't see anything about them. But you should familiarize yourself with each item and how to fix them in the event you encounter them later.
  </p>

  <p className="tldr">
    We'll wrap up this chapter here. The next section will be a short section, where we finish up everything we need to do before we deploy to production. Then we'll take that as an opportunity to finish up our SEO and then do some extras that could come in handy.
  </p>

  <p>
    Lets wire up our MailChimp API in the next chapter to handle email subscriptions.
  </p>

  <p>
    <b>Note:</b> We plan on expanding on this chapter in the future, so check back for more optimization tips soon.
  </p>

  <AniLink to="jamstack-jedi/mailchimp" className="nextBtn">E-Mail Signups with MailChimp &raquo;</AniLink>
  <br />
  </Layout>
);

export default Speed;
