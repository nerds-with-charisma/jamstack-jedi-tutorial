import React from 'react';
import AniLink from '../../components/Common/AniLink';

import Layout from '../../components/layout';
import Markdown from '../../components/Common/markdown';

const Robots = () => (
  <Layout
    title="robots.txt"
    author={{ gravatar: '//0.gravatar.com/avatar/b5eff8d62adf3ff34e67f153781d27c4' }}
    gitRepo="https://gitlab.com/nerds-with-charisma/jamstack-jedi/tree/master/seo-robots"
    time={5}
    description="What is a robots.txt file and how to create a robots.txt file"
    keywords="robots.txt file, what is a robots.txt file, how to create a robots.txt file, robots.txt with GatsbyJs"
  >

   <strong>I, Robots.txt</strong>
   <br />
   <br />

   <p>
    In short, a <b>robots.txt</b> file sits at the root of your website and tells search engines which pages to index. Or it can tell them which ones not to index. For example, if you had a login only section to your site you wouldn't want it indexed.
   </p>

   <p>
     Moz has a <a id="anchor--what-is-robots" href="https://moz.com/learn/seo/robotstxt" target="_blank">great write-up</a> on everything you'll need to know about this file, so please read through it.
   </p>

   <p>
    Gatsby allows us to setup our robots.txt file <a id="anchor--gatsby-robots" href="https://www.gatsbyjs.org/packages/gatsby-plugin-robots-txt/" target="_blank">with a plugin</a>, similar to how we configured GTM. Lets install that plugin:
   </p>

   <Markdown
    rel="Terminal"
    language="terminal"
    md={`  npm install --save gatsby-plugin-robots-txt`}
  />

  <p>
    Now we need to configure the file that Gatsby will generate. Open up <b>gatsby-config.js</b> and lets add the plugin to our plugins array just before our sass plugin declaration:
  </p>

  <Markdown
    rel="gatsby-config.js"
    language="javascript"
    md={`  ...
  plugins: [
    {
      resolve: 'gatsby-plugin-robots-txt',
    },
    \`gatsby-plugin-sass\`,
  ...`}
  />

  <p>
    This is enough to generate the file, but we want to specify some attributes, which we can do with the <b>options</b> node. Modify our object to the following:
  </p>

  <Markdown
    rel="gatsby-config.js"
    language="javascript"
    md={`  ...
  {
    resolve: 'gatsby-plugin-robots-txt',
    options: {
      host: 'https://nerdswithcharisma.com',
      sitemap: 'https://nerdswithcharisma.com/sitemap.xml',
      policy: [{ userAgent: '*', allow: '/' }],
    },
  },
  ...`}
  />

  <p>
    We tell it our host, or root domain. Specify a sitemap (which we'll create in the next chapter) and then put our "policy". We only have this page, so we're saying that all bots can crawl all files and directories.
  </p>

  <p>
    If we were to start up our server, we wouldn't really notice a difference because we don't need a robots.txt file for local. However, if we do our first <b>Build</b> we will find our robots.txt file is created. To do this, kill our server and run the following command:
  </p>

  <Markdown
    rel="Terminal"
    language="terminal"
    md={`  gatsby build`}
  />

  <p>
    Let Gatsby do it's thing, and when done, you'll notice our <b>public</b> directory has a LOT going on in it now. Scroll down until you see <b>robots.txt</b> and open it up.
  </p>

  <img src="/images/jamstack-jedi/seo--robots.png" class="jamstack--img" alt="Generated robots.txt file" />

  <p>
    You can see all our settings from <b>gatsby-config.js</b> are now in this newly generated <b>robots.txt</b> file. Pretty nice, huh?
  </p>

  <p>
    If we make any changes to our configuration, we simply do a new build and deploy and those changes will be automatically built into our robots file.
  </p>

  <p>
    In the next chapter we'll build that sitemap.xml file we referenced above.
  </p>

  <AniLink to="jamstack-jedi/seo-sitemap" className="nextBtn">sitemap.xml Explained &raquo;</AniLink>
  <br />
  </Layout>
);

export default Robots;
