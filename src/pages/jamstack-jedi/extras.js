import React from 'react';
import AniLink from '../../components/Common/AniLink';

import Layout from '../../components/layout';

const Extras = () => (
  <Layout
    title="Extras"
    author={{ gravatar: '//0.gravatar.com/avatar/b5eff8d62adf3ff34e67f153781d27c4' }}
    gitRepo={null}
    time={2}
    description={null}
    keywords={null}
  >

  <strong>Extra Extra</strong>
  <br />
  <br />

  <p>
    This section will be last section of this tutorial series. At this point we've gone through everything needed to get a portfolio online, but there's still some improvements we can make and some neat little tricks that you might want to incorporate in your site that will commonly come up in your day-to-day coding.
  </p>

  <p>
    We'll tackle the following things in this section: Deployment notifications with <b>Slack</b>, using <b>animated SVGs</b>, setting up <b>esLint</b>, creating a <b>MailChimp</b> campaign and email, running code via <b>Google Tag Manager</b>, <b>A/B testing</b> via Google Optimize, <b>Local SEO</b>.
  </p>

  <p>
    We'll also cover getting data from a headless cms via <b>Contentful</b>, analyzing our site's SEO juice with online tools like <b>WooRank</b>, setup a <b>Buy me a coffee</b> button, <b>geolocation</b> and <b>notifications</b> APIs, detect <b>keyboard key presses</b>, and working with <b>Google Maps</b>.
  </p>

  <p>
    If all that sounds good, and you're ready to finish off your portfolio, lets get started!
  </p>

  <AniLink to="jamstack-jedi/slack" className="nextBtn">Slack Notifications &raquo;</AniLink>
  <br />
  </Layout>
);

export default Extras;
