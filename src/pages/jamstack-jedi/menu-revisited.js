import React from 'react';
import AniLink from '../../components/Common/AniLink';

import Layout from '../../components/layout';
import Markdown from '../../components/Common/markdown';

const Menu2 = () => (
  <Layout
    title="Menu Pt. 2"
    author={{ gravatar: '//0.gravatar.com/avatar/b5eff8d62adf3ff34e67f153781d27c4' }}
    gitRepo="https://gitlab.com/nerds-with-charisma/jamstack-jedi/tree/master/menu-revisited"
    time={20}
    description="Create an animated svg menu and close button"
    keywords="Animated SVG, Animaged Menu Button, Animated Hamburger Menu"
  >
  <strong>Menu, Revisited</strong>
  <br />
  <br />

  <p className="tldr">
    In a previous chapter we built our our menu using icons from FontAwesome, but they're not very slick. They're thick and bulky, they don't transition seemlessly, they're...kinda boring (totally awesome and time-saving though, don't get me wrong).
  </p>

  <p className="tldr">
    If you've ever stumbled upon something like the <a href="https://jonsuh.com/hamburgers/" target="_blank">Tasty CSS-animated hamburgers</a> project, then you've seen some cool animated svgs for navigation buttons and might want to add something like that to our site. We could use this library, but it's actually not that hard to make our own, so we'll attempt that here.
  </p>

  <p>
    Hop back into your local Gatsby environment and lets open up <b>Nav.js</b>. Which is where our old icon currently resides.
  </p>

  <p>
    First we want to remove our old span with the FA classes and replace it with a div with an empty span inside, this span will be used to create the bars with CSS pseudo selectors.
  </p>

  <p>
    We're also going to change our classes to have "nav--svg" along with "open" when the nav is opened. We will use this open class to animate the svg on click.
  </p>

  <Markdown
    rel="Nav.js"
    language="javascript"
    md={`  ...
  <button
    className={(isDark) ? 'isDark' : 'font--light'}
    type="button"
    onClick={() => setIsOpen(!isOpen)}
  >
    <div className={(isOpen) ? 'nav--svg open' : 'nav--svg'}>
      <span />
      <strong className="ir">
        Menu
      </strong>
    </div>
  </button>
  ...`}
  />

  <p>
    Now lets style it, open up <b>_header.scss</b> and just before the closing header styles lets add our nav--svg styles:
  </p>

  <Markdown
    rel="_header.scss"
    language="scss"
    md={`  ...
  .nav--svg {
    width: 40px;  /* define a set width */

    &:after,
    &:before,
    & span {
      background-color: #fff; /* make the background white */
      opacity: 0.7; /* by default change the opacity to 70% */
      content: ''; /* put in an empty content attribute, without this we wouldn't populate anything */
      display: block; /* make it block level */
      height: 3px; /* each bar's height */
      margin: 8px 0; /* the gap between each bar */
      transition: all .2s ease-in-out; /* for animating */
    }
  }
  ...`}
  />

  <p>
    Fire up our server and lets take a look:
  </p>

  <img src="/images/jamstack-jedi/menu--css.png" class="jamstack--img" alt="CSS for hamburger icon" />

  <p className="tldr">
    It's already looking cleaner in my opinion. Next we'll work on the animating, which is just a matter of changing some CSS when isOpen is active, which will add our "open" class to the nav--svg class.
  </p>

  <p>
    Add the following open styles below the after:before:span styles above, still inside the nav--style group:
  </p>

  <Markdown
    rel="_header.scss"
    language="scss"
    md={`  ...
  &.open {
    &:before {
      transform: translateY(12px) rotate(135deg); /* rotate the top bar */
      margin: 10px 0;  /* nudge it down a little so they're even */
      opacity: 1;
    }

    &:after {
      transform: translateY(-12px) rotate(-135deg); /* rotate the bottom bar */
      opacity: 1;
    }

    & span {
      opacity: 0; /* fade out our center bar */
    }
  }
  ...`}
  />

  <p>
    When we click our nav and it opens up, the top and bottom bars rotate, the middle fades out, and creates an X. Click it again to close and it animates back to a hamburger icon!
  </p>

  <img src="/images/jamstack-jedi/menu--close.png" class="jamstack--img" alt="CSS for hamburger closed icon" />

  <hr />

  <p>
    That's not all we should do to our menu, take a look at what happens if you scroll down the page and come over a white section.
  </p>

  <img src="/images/jamstack-jedi/menu--on-white.png" class="jamstack--img" alt="Menu on white background" />

  <p>
    Since the nav icon is white, and the background is white, you can't see it to click it. There's a few ways we could fix this...a drop shadow, changing the color of the nav, etc. But we can do a pretty nifty check on scroll, if the current div in the viewport is X, Y, or Z (which would be our light background div's) then change the color of our menu. Else keep it white.
  </p>

  <p className="tldr">
    We'll break that down into 3 parts, the styles for the dark nav, detecting scroll, and then the logic for if we should add a class or not.
  </p>

  <p>
    For our styles, we just need to specify a background color for our bars, put this below the "nav--svg" declaration, not inside of it:
  </p>

  <Markdown
    rel="_header.scss"
    language="scss"
    md={`  ...
  .isDark {
    .nav--svg {
      &:after,
      &:before,
      & span {
        background-color: #2c2c2c;
      }
    }
  }
  ...`}
  />

  <p className="tldr">
    Next we detect our scroll event, when the user scrolls, we'll add a class of "isDark" to our button inside of <b>Nav.js</b>.
  </p>

  <p>
    First we need to import <b>useEffect</b>, then we need to create a new hook for <b>isDark</b> and then we want to create an event listener inside of use state that calls a function called <b>handleScroll</b> when the user scrolls. Finally we want to toggle the classes on the button when scrolling.
  </p>

  <Markdown
    rel="Nav.js"
    language="javascript"
    md={`  import React, { useState, useEffect } from 'react';
  ...
  const Nav = ({ navigation }) => {
  const [isOpen, setIsOpen] = useState(false);
  const [isDark, setIsDark] = useState(false);

  useEffect(() => {
    window.addEventListener('scroll', handleScroll);
  }, []);

  const handleScroll = () => {
    setIsDark(true);
  }
  ...`}
  />

  <p>
    If we test it now, as soon as we start scrolling the icon should change to dark. The last thing we need to do is detect if we're in a light background div, and then switch isDark only when it is. Revert it to light if it's not.
  </p>

  <p className="tldr">
    We can do that by getting our elements with a light background and saving them to a variable inside our handleScroll function, then then we can test to see if it's visible on the screen by comparing the offset position to the elements height, if it falls between there we know it's on the screen. We can set our state var, which toggles our class, based on that.
  </p>

  <Markdown
    rel="Nav.js"
    language="javascript"
    md={`  ...
  const handleScroll = () => {
    const services = document.getElementById('services');
    const about = document.getElementById('about');

    const inLightDiv = (services.getBoundingClientRect().top <= 0
    && (-1 * services.getBoundingClientRect().top) <= services.offsetHeight)
    || (about.getBoundingClientRect().top <= 0
    && (-1 * about.getBoundingClientRect().top) <= about.offsetHeight)

    if (inLightDiv) {
      setIsDark(true);
    } else {
      setIsDark(false);
    }
  };
    ...`}
  />

  <p>
    We already have an animation in our CSS so the transition should be seamless.
  </p>

  <img src="/images/jamstack-jedi/menu--dark.png" class="jamstack--img" alt="Menu, dark, on white background" />

  <hr />

  <p>
    One issue, if you scroll to a light area, the icon turns dark, then click on it to open the nav...since the icon is dark we can't see it on our menu's dark background. So we need to add some css to make sure that no matter what, if the nav is open, the menu turns white. Which is pretty easy to do, we just have to use some specificity to get more specific that when the "open" class is there, all our bars background colors should be white:
  </p>

  <Markdown
    rel="_header.scss"
    language="scss"
    md={`  ...
  .isDark {
    .nav--svg {
      &:after,
      &:before,
      & span {
        background-color: #2c2c2c;
      }

      &.open {
        &:after,
        &:before,
        & span {
          background-color: #fff;
        }
      }
    }
  }
    ...`}
  />

  <p>
    And that's it, we've made our menu icon just a tad cooler. In the next chapter we'll talk about implementing Heatmaps via GTM with a service called <b>HotJar</b>.
  </p>

  <AniLink to="jamstack-jedi/hotjar" className="nextBtn">Hotjar Heatmaps &raquo;</AniLink>
  <br />
  </Layout>
);

export default Menu2;
