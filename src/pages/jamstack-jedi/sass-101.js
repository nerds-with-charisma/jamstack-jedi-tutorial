import React from 'react';
import AniLink from '../../components/Common/AniLink';

import Layout from '../../components/layout';

import Markdown from '../../components/Common/markdown';
import YouTube from '../../components/Common/YouTube';

const Sass101 = () => (
  <Layout
    title="Sass 101"
    author={{ gravatar: '//0.gravatar.com/avatar/b5eff8d62adf3ff34e67f153781d27c4' }}
    gitRepo="https://gitlab.com/nerds-with-charisma/jamstack-jedi/tree/master/sass-101"
    time={5}
    description="Learn Sass, and how it differs from standard CSS. Sass basics."
    keywords="Learn Sass, Sass Basics, Get better at Sass, Sass 101, Intro to Sass, SCSS, SASS"
  >
    <strong>Lets Get Sassy</strong>
    <br />
    <br />

    <p>
      We already know how to write CSS, right? So why do we need to learn Sass? We don't technically, but Sass is so awesome that I think you'll really wonder how you ever wrote CSS without it.
    </p>

    <p className="tldr">
      How does Sass differ from straight up CSS? Well, truth be told, it doesn't...you could write .scss files as straight up CSS and it would still work. But Sass lets you do so much more than CSS alone, like variables (although CSS variables are starting to gain some traction), logical operations like loops, and nesting.
    </p>

    <p>
      I'd recommend <a id="anchor--sass-book" href="https://abookapart.com/products/sass-for-web-designers" target="_blank">this book</a> from abookapart.com to learn all about Sass. If you're just looking for a crash course in Sass, then <a id="anchor--sass-video" href="https://www.youtube.com/watch?v=Zz6eOVaaelI" target="_blank">this video from Dev Ed</a> is a great primer. Note: We won't need to use the Sass VS Code compiler in our application.
    </p>

    <YouTube url="https://www.youtube.com/embed/Zz6eOVaaelI" />

    <br />

    <p>
      Also, I'd recommend you read through the <a id="anchor--sass-docs" href="https://sass-lang.com/documentation" target="_blank">Sass Docs</a> as they will cover absolutely everything you'll encounter with Sass.
    </p>

    <hr />

    <p>
      Lets quickly setup Sass in our Gatsby project so we can use it later. For starters, lets make a new folder in our src directory as well as a file called <b>main.scss</b>:
    </p>

    <Markdown
      rel="Terminal"
      language="terminal"
      md={
        `  mkdir src/styles
  > src/styles/main.scss`}
    />

    <p>
      Open up our new main.scss file and lets add a test style:
    </p>

    <Markdown
      rel="main.scss"
      language="sass"
      md={
        `  html {
    background: red;
  }`}
    />

    Then we need to import our sass file to use it in our app, so lets open up <b>layout.js</b> and add this line above our layout component declaration:

    <Markdown
      rel="layout.js"
      language="javascript"
      md={
        `  ...
  import '../styles/main.scss';

  const Layout = ({ children }) => {
    ...
  }`}
    />

    <p>
      If we restart our server, we should notice an error that says something like, "Module parse failed: Unexpected token (1:5) You may need an appropriate loader to handle this file type."
    </p>

    <p className="tldr">
      We get this error because by default Gatsby doesn't support Sass out of the box, but telling it to allow us to use Sass is very easy. Gatsby allows the use of "plugins" to use 3rd parties or accomplish common things, so we'll follow the steps for enabling Sass. Check out <a href="https://www.gatsbyjs.org/packages/gatsby-plugin-sass/" target="_blank">the docs</a> for enabling Sass here, or follow along below.
    </p>

    <p id="anchor--gatsby-sass">
      First we need to install the deps for using Sass, so stop your server (ctrl + c) if it's running and run the following commands from the root:
    </p>

    <Markdown
      rel="Terminal"
      language="terminal"
      md={
        `  npm install --save node-sass gatsby-plugin-sass`}
    />

    <p>
      Next we need to tell Gatsby we're going to use Sass, so open up <b>gatsby-config.js</b> and add the following inside the plugin's array above the react-helmet declaration:
    </p>

    <Markdown
      rel="gatsby-config.js"
      language="javascript"
      md={
        `  ...
  plugins: [
    \`gatsby-plugin-sass\`,
    \`gatsby-plugin-react-helmet\`,
  ...`}
    />

    <p className="tldr">
      That's it, now we can start using Sass. Fire up your server (gatsby develop) and it should start without issues now. We should see the whole background is red now, because we've got our Sass file wired up and we've made that rule for our html selector.
    </p>

    <p>
      We don't actually need the background to be red, so you can remove that style from <b>main.scss</b>. In the next chapter we'll start building our portfolio and our style library.
    </p>

    <AniLink to="jamstack-jedi/style-library" className="nextBtn">Creating a Style Library &raquo;</AniLink>
    <br />
  </Layout>
);

export default Sass101;
