import React from 'react';
import { PropTypes } from 'prop-types';
import { Helmet } from 'react-helmet';

import '../../styles/main.scss';

import Footer from '../../components/Footer/Footer';
import JamstackNav from './JamstackNav';

const Layout = ({
  children,
  description,
  keywords,
  title,
}) => (
  <div id="jamstack">
    <Helmet>
      <html lang="en" />
      <title>{title}</title>

      <link rel="shortcut icon" href="favicon.ico" />

      <meta charSet="utf-8" />
      <meta
        name="viewport"
        content="width=device-width, initial-scale=1, shrink-to-fit=no"
      />
      <meta name="theme-color" content="#9012fe" />
      <meta name="google-site-verification" content="" />
      <meta name="apple-mobile-web-app-capable" content="yes" />
      <meta name="robots" content="index, follow" />

      <meta name="description" content={description} />
      <meta name="keywords" content={keywords} />
      <link rel="canonical" href="https://nerdswithcharisma.com/" />

      <meta property="og:title" content="Nerds With Charisma" />
      <meta property="og:site_name" content="Nerds With Charisma" />
      <meta property="og:url" content="https://nerdswithcharisma.com" />
      <meta property="og:type" content="website" />
      <meta
        property="og:description"
        content="Nerds With Charisma is a cutting edge digital media agency specializing in awesome websites."
      />
      <meta
        property="og:image"
        content="https://nerdswithcharisma.com/images/nwc-tile.png"
      />

      <meta name="twitter:card" content="summary_large_image" />
      <meta name="twitter:site" content="https://nerdswithcharisma.com" />
      <meta name="twitter:creator" content="Brian Dausman" />
      <meta name="twitter:title" content="Nerds With Charisma" />
      <meta
        name="twitter:description"
        content="Nerds With Charisma is a cutting edge digital media agency specializing in awesome websites."
      />
      <meta name="twitter:image:src" content="/images/nwc-tile.png" />

      <meta name="google-site-verification" content="VNLuFr6uYVgjRt5AipwVIwD4ZP6wx6toZPoupfUoyvA" />
    </Helmet>

    <Helmet>
      <link
        rel="alternate stylesheet"
        href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900"
        onload="this.rel='stylesheet'"
      />
      <link
        rel="alternate stylesheet"
        href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
        integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr"
        crossOrigin="anonymous"
        onload="this.rel='stylesheet'"
      />
    </Helmet>
    <main>
      <section id="hero" className="font--light text-center">
        <h3
          className="font--150"
          style={{
            lineHeight: '130px',
            display: 'block',
            width: '100%',
          }}
        >
          <strong>
            {title}
          </strong>
          <br />
        </h3>
      </section>
      <section id="docs--container" className="container">
        <section id="docs--grid" className="grid">
          <JamstackNav />
          <div id="jamstack--main" className="col-12 col-sm-6 col-md-8 col-lg-9">
            <br />
            {children}
          </div>
        </section>
      </section>
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <Footer />
    </main>
  </div>
);

Layout.defaultProps = {
  showSchema: false,
};

Layout.propTypes = {
  children: PropTypes.element.isRequired,
  description: PropTypes.string.isRequired,
  isHomepage: PropTypes.bool.isRequired,
  keywords: PropTypes.string.isRequired,
  showSchema: PropTypes.bool,
  title: PropTypes.string.isRequired,
};

export default Layout;
