import React from 'react';
import AniLink from '../../components/Common/AniLink';

import Layout from '../../components/layout';
import Markdown from '../../components/Common/markdown';

const AB = () => (
  <Layout
    title="A/B Testing"
    author={{ gravatar: '//0.gravatar.com/avatar/b5eff8d62adf3ff34e67f153781d27c4' }}
    gitRepo={null}
    time={7}
    description="Learn Google Optimize A/B Testing, how to setup Google Optimize A/B Testing. Google Optimize A/B Testing basics."
    keywords="Learn Google Optimize A/B Testing, Google Optimize A/B Testing Basics, Get better at Google Optimize A/B Testing, Google Optimize A/B Testing 101, Intro to Google Optimize A/B Testing"
  >

  <strong>Testing...Testing</strong>
  <br />
  <br />

  <p>
    A/B testing is the act of testing something against something else. That could be almost anything your imagination can come up with, some examples are: Button color, font size, content changes like taglines, changing the order of items on the page, etc...
  </p>

  <p>
    We can use a free service from Google called <b>Optimize</b> to do simple A/B testing on our site which will include result reporting on goals of your choosing. It will also natively tie into your Google Analytics account.
  </p>

  <p>
    The free version has a few limitations, but they should be good enough for a small to medium sized company. So lets sign-up for it and get the snippet installed, which we'll do with GTM like we did Hotjar.
  </p>

  <p>
    Head to <a href="https://optimize.google.com" target="_blank">https://optimize.google.com</a> and sign-in. On the <b>Accounts</b> page, click <b>Create Account</b> and name it, select the checkboxes you wish, and click <b>Next</b>.
  </p>

  <p>
    Next add the <b>domain</b> for your site and click <b>Create</b>. It will link with your <b>GTM Account</b> and ask you to create your first experience. Click <b>Lets go</b> to get started. We'll keep our test simple, we'll make 2 variants, the first will change the text on our CTA (call to action) button in the hero to "Click Here to Contact Us". In varient 2 we'll leave the text the same, but change the design of the button.
  </p>

  <p>
    Optimize should ask you to <b>name</b> your experiment, we'll call it "Hero Contact Button Change" and then enter your <b>Url</b> and select <b>A/B Test</b>. Click <b>Create</b>.
  </p>

  <img src="/images/jamstack-jedi/optimize--first-exp.png" class="jamstack--img" alt="First A/B Test" />

  <hr />

  <p>
    Click <b>Add Variant</b>, and name it "Change Hero Button Text", click <b>Done</b>.
  </p>

  <p>
    A new varient will now show up under our original control. Click the <b>Edit</b> button next to this new variant and the visual editor will pull up our website where we can now modify it to our liking. We want to change the Hero contact button's text to "Click Here to Contact Us", so click the button's text and make sure that our strong tag is selected:
  </p>

  <img src="/images/jamstack-jedi/optimize--variant-1.png" class="jamstack--img" alt="Variant 1 changes" />

  <p>
    Click <b>Edit element</b> from the inspector on the right and choose <b>Edit Text</b>, and change it to "Click Here to Contact Us". Click <b>Done</b> then <b>Save</b>.
  </p>

  <p>
    Back at the dashboard, click <b>+ Add Variant</b> to create our second test. Name it "Hero Button Design Change" and click <b>Done</b> then <b>Edit</b> on the new line item that we created.
  </p>

  <p>
    Select the button this time and choose <b>Edit element</b> then <b>Edit HTML</b>. An editor with our source will now come up. We'll change the design to have the same gradient background our other buttons below have, and since we have a reusable class for this, we can simply add it here and the styles will be applied.
  </p>

  <p>
    Edit the html to include our "btn--gradient" class like so:
  </p>

  <Markdown
      rel="Experiement Editor"
      language="html"
      md={
        `  <button type="button" class="btn bg--light font--dark font--24 padding-horiz-xl radius--lg border--none block no--wrap btn--gradient"><strong>Get In Touch</strong></button>`
      }
    />

    <p>
      Click <b>Apply</b> and we should see the styles instantly kick in.
    </p>

    <img src="/images/jamstack-jedi/optimize--variant-2.png" class="jamstack--img" alt="Variant 2 changes" />

    <p>
      Click <b>Save</b> then <b>Done</b> to return to the dashboard. We can see that Optimize has already evenly distributed our variants to each have 33% split. So 33% of users will see our original/unedited site. 33% will see the new button text, and 33% will see our button color change.
    </p>

    <p>
      Scroll down and you'll see <b>Targeting</b>, we don't have to edit this since we're a one page website, but if you wanted to modify the page that this experiment would run on, you would do it here. You can also run it on multiple pages that have the similar urls like "portfolio" by changing the "url matches" dropdown.
    </p>

    <p>
      Continue scrolling down to <b>Audience targeting</b>, which again, we don't need to modify since we will let this run for everyone. If you wanted to only run for a specific sub-set of users like "mobile" only, you would do that here.
    </p>

    <p>
      Scroll down some more and add a <b>Description</b>, not required but why not, and keep scrolling down to <b>Measurement and Objectives</b>, click the <b>Link to Analytics</b> button. Choose your <b>Property</b> on the panel that slides out and click <b>Link</b>.
    </p>

    <p>
      This will give us advanced data that we can trudge through in Google Analytics when the experiment is over.
    </p>

    <p>
      You should now get a pop-up saying you need to add the Optimize snippet to your website. We'll do this via GTM so copy the snippet and head over to GTM. Go to <b>Tags</b> and click <b>New</b>. Name it <b>Google Optimize</b> and when you choose a tag type, there should be an option for <b>Google Optimize</b> specifically. Choose this and instead of pasting the entire snippet they provided us, we can enter just the ID, it will start with "GTM-". Paste this into the input, then in <b>Google Analytics Settings</b> choose "Google Analytics Settings".
    </p>

    <p>
      Do not select anything under <b>Triggers</b>. Click <b>Save</b>. Go to the <b>Google Analytics</b> tag and choose <b>advanced</b>, find <b>tag sequencing</b> and twirl it down, check <b>Fire a tag before Google Analytics fires</b> and choose the Google Optimize tag we just created. <b>Save &amp; Publish</b> the tag, head back to Optimize and click <b>Done</b>.
    </p>

    <p>
      Head back to Optimize, and we need to choose an experiment objective. Click <b>Choose from a list</b> and select <b>Session duration</b>, this will see which variant leads users to hang out on our site longer. You can click <b>Add additional objectives</b> and select "bounces" (people who come to our site and bouce away quickly) and "Pageviews" (how many users viewed more pages). Pageviews aren't as important for us, but it'll be nice to know if people click on other things later in their journey.
    </p>

    <p>
      If there's an error right now, that's ok, it's most likely because we haven't setup an experiment to run yet. So skip down to <b>Email Notifications</b> and enable them if you'd like. Scroll back up to the top and click <b>Start</b> (you can also schedule when the exp. will start if you don't want to do it right away).
    </p>

    <p>
      Once enough traffic hits your site, you can view the <b>Reporting</b> tab and after a few weeks you should see a "winner". Whichever variant wins, you can then decide to implement it into your production code, or abandon and keep the original.
    </p>

    <p>
      You can also force your browser into an experiment to see how it would look to a user by going into the experiment lading page and choosing the variant you want to test and selecting <b>Preview</b> followed by the device you want to test with.
    </p>

    <img src="/images/jamstack-jedi/optimize--preview.png" class="jamstack--img" alt="Optimize Preview" />

    <p>
      If you're looking for some ideas on A/B tests to run, you should check out this article from <a id="anchor--optimizely" href="https://blog.optimizely.com/2013/04/30/71-things-to-ab-test/" target="_blank">Optimizely</a> (a competitor to Optimize, but a great tool and a great article).
    </p>

    <p>
      That's all we need to cover for A/B testing. As mentioned previously, combined with our heatmaps, we now have a very powerful optimization tool.
    </p>

    <p>
      In the next section we will cover how to improve our SEO and make our website come up higher on search results.
    </p>

    <AniLink to="jamstack-jedi/keywords" className="nextBtn">Keywords SEO &raquo;</AniLink>
    <br />
  </Layout>
);

export default AB;
