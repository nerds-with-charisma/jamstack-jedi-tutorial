import React from 'react';
import AniLink from '../../components/Common/AniLink';

import Layout from '../../components/layout';
import Markdown from '../../components/Common/markdown';

const Social = () => (
  <Layout
    title="Social Branding"
     author={{ gravatar: '//0.gravatar.com/avatar/b5eff8d62adf3ff34e67f153781d27c4' }}
    gitRepo="https://gitlab.com/nerds-with-charisma/jamstack-jedi/tree/master/social"
    time={12}
    description="Claim your brands on social media. Establish your online precense on social media."
    keywords="Social signup, business signup, brands signup, social brands"
  >

  <strong>
    Social Awkward
  </strong>

  <br />
  <br />

  <p>
    As mentioned at the end of the last chapter, you should claim your brand on all social media sites even if you don't plan on using them. You can do as many as you'd like, but you absolutely need to sign up for Twitter, Facebook, YouTube, and Pinterest. These are currently the most popular social networks and at the very least you should put a presence on them.
  </p>

  <p>
    You should also consider signing up for Linked In, Reddit, and any others that might be important to your field. For example, as a dev or designer you would consider Dribbble and Medium.
  </p>

  <p>
    We'll quickly go through how to signup for each site, mention any important bits like branding it, and how best to link to your site if applicable.
  </p>

  <hr />

  <strong id="anchor--twitter-signup">Twitter</strong>
  <br />
  <p>
    First, go to <a href="https://twitter.com/" target="_blank">twitter.com</a> and click the <b>Sign Up</b> button. Follow the steps in the editor, make sure to name your account what your business name is.
  </p>

  <p>
    Once you've completed the signup, you'll get an activation email. Once activated you'll be presented with your Twitter homepage. Now we need to customize it for our brand.
  </p>

  <p>
    From the nav click <b>Profile</b> then <b>Edit Profile</b> from the middle section.
  </p>

  <p>
    Fill out the fields, they can match your meta data if you'd like, for <b>bio, location, and website.</b>
  </p>

  <p>
    At the top, you'll see 2 image placeholders, click the camera and choose an image on your hard-drive to represent your header image and your avatar.
  </p>

  <p>
    You can use our images as a template for sizing, the <a href="https://pbs.twimg.com/profile_banners/627857803/1560187570/1500x500" target="_blank">header image</a> should be <b>1500x500</b>. We'll be using a similar background to our website's hero background.
  </p>

  <p>
    The <a href="https://pbs.twimg.com/profile_images/1138135274125438976/WaFHZKu6_400x400.jpg" target="_blank">avatar image</a> should be around <b>400x400</b>, and represent your brand, as this will be what is shown on every Tweet you publish. We have chosen a colorful version of our logo.
  </p>

  <p>
    Now that our profile is setup, you should follow 10 people in your field. This is just a starting number, feel free to follow as many as you'd like, but this will get the ball rolling and people will also start following you. The more you post, the more people will discover you and you're numbers will gradually increase.
  </p>

  <p>
    Finally, similar to the <b>Open Graph</b> meta tags we added earlier, we can add some Twitter meta tags. You can <a id="anchor--twitter-cards" href="https://developer.twitter.com/en/docs/tweets/optimize-with-cards/overview/abouts-cards" target="_blank">read more about cards here</a> to see all available cards or find out more about what they do and how they work.
  </p>

  <p>
    Flip back over to our portfolio and open up <b>gatsby-config.js</b>. Inside our <b>meta</b> node, we want to add a few more for the Twitter cards.
  </p>

  <p>
    You can use <a href="https://nerdswithcharisma.com/nwc-tile.jpg" target="_blank">our tile image</a> as a template for yours:
  </p>

  <Markdown
    rel="gatsby-config.js"
    language="javascript"
    md={`  meta: [
  {
    name: 'description',
    content: 'Nerds With Charisma online portfolio coming soon!',
  },
  {
    name: 'keywords',
    content: 'Brian Dausman, Nerds With Charisma',
  },
  {
    name: 'twitter:card',
    content: 'summary_large_image',
  },
  {
    name: 'twitter:site',
    content: 'https://nerdswithcharisma.com',
  },
  {
    name: 'twitter:creator',
    content: 'Brian Dausman',
  },
  {
    name: 'twitter:title',
    content: 'Nerds With Charisma',
  },
  {
    name: 'twitter:description',
    content: 'Nerds With Charisma is a cutting edge digital media agency specializing in awesome websites.',
  },
  {
    name: 'twitter:image:src',
    content: 'https://nerdswithcharisma.com/images/nwc-tile.png',
  },
],`}
  />

  <p>
    Once you add your tags, you can commit your work and the deploy will auto publish:
  </p>

  <Markdown
    rel="Terminal"
    language="terminal"
    md={`  git pull

  git add .

  git commit -m "Add Twitter meta

  git pull

  git push`}
  />

  <p>
    Once your site is deployed, you can validate the card is created correctly by going to <a id="anchor--twitter-card-validator" href="https://cards-dev.twitter.com/validator" target="_blank">Twitter's validator</a> and entering your website url then hitting <b>Preview Card</b>. Everthing should be successful, if not, you most likely have a syntax error in your tags.
  </p>

  <hr />

  <b>Facebook</b>
  <br />
  <p>
    Next up is Facebook, which requires you to have a personal account to create a business page. So you can either use your own account to admin, or sign up with an email specifically for your company.
  </p>

  <p>
    Once your standard account is created, head over to the <a anchor="anchor--facebook-page" href="https://www.facebook.com/pages/creation/" target="_blank">Page Creation</a> section and click <b>Get Started</b> under "company or organization". It will ask you to create a page name and category. For <b>Page name</b> choose your company name, and <b>category</b> start typing and it will offer suggested categories for you to choose from.
  </p>

  <p>
    Some more profile fields will come up, fill them out and finalize your page. Create a profile image, <b>180x180</b> pixels, you can resize the tile image you used on Twitter if you'd like. You can also reuse the twitter background image for the <b>Cover Photo</b> which has a dimension of <b>315x828</b> pixels.
  </p>

  <p>
    You can also go to <b>settings >  page info</b> to add some additional business info. I would suggest somewhere in your bio, linking to your domain name.
  </p>

  <hr />

  <b>
    YouTube
  </b>

  <p>
    Even if you don't plan on making videos, you should still sign up for a YouTube channel. First sign in at <a id="anchor--youtube-signup" href="https://youtube.com" target="_blank">youtube.com</a> and click the <b>Profile Icon</b> at the top.
  </p>

  <p>
     From the dropdown choose <b>YouTube Studio</b>, then click on the profile icon again and choose <b>Your Channel</b>. Choose <b>Customize Channel</b>, from here you can upload a cover image, <b>2560x1440</b>, after picking an image, go to the <b>About</b> tab and fill out your <b>Channel Description, email, location, and links</b>.
  </p>

  <p>
    Again, it would be a good idea to add your domain link to your description. That goes for pretty much every one of these social media accounts.
  </p>

  <hr />

  <b>Pinterest</b>

  <p>
    Pinterest also requires an existing account to create a business account. So signup and then visit their <a id="anchor--pinterest-signup" href="https://www.pinterest.com/business/create/" target="_blank">Business Page</a>. Enter the info for your business in the wizard (note that Pinterest is not a fan of Ad-Blockers, if you have one you'll have to temporarily disable it).
  </p>

  <p>
    Once you're brought to the <b>ads dashboard</b> you can click the ellipsis at the top right and choose <b>Settings</b> to add a photo, profile, location, and more.
  </p>

  <p>
    Next choose <b>Claim</b> from the profile nav and claim your website. Pinterest will ask how you'd like to do this, via a file or a meta tag. Lets choose meta tag, and then flip back to <b>gatsby-config.js</b>
  </p>

  <p>
    Just below our Twitter meta, add the following node, replacing your values from the Pinterest dash (replace the content with your ID):
  </p>

  <Markdown
    rel="gatsby-config.js"
    language="javascript"
    md={`  {
    name: 'p:domain_verify',
    content: '6b1bf384b5f509e1ffd1e8cf6307730c',
  },`}
  />

  <p>
    Commit &amp; deploy, once done, return to Pinterest and click <b>Next</b>. It will tell you that they will get back to you within the next 24 hours to confirm, however it notified us almost instantly.
  </p>

  <hr />

  <p>
    Go ahead and finish signing up for all the other social networks you think are important to your company. Once you're done, we'll work on setting up our local Google pages.
  </p>

  <AniLink to="jamstack-jedi/gmb" className="nextBtn">Google My Business &raquo;</AniLink>
  <br />
  </Layout>
);

export default Social;
