import React from 'react';
import AniLink from '../../components/Common/AniLink';

import Layout from '../../components/layout';

const GMB = () => (
  <Layout
    title="Google My Business"
    author={{ gravatar: '//0.gravatar.com/avatar/b5eff8d62adf3ff34e67f153781d27c4' }}
    gitRepo={null}
    time={5}
    description="Learn Google My Business, how to setup Google My Business. Google My Business basics."
    keywords="Learn Google My Business, Google My Business Basics, Get better at Google My Business, Google My Business 101, Intro to Google My Business"
  >

  <strong>Mind Your Business</strong>
  <br />
  <br />

  <p>
    Google My Business is a free tool to control how your company shows up in search listings on Google, specifically your business address, services, map listing, hours, etc...
  </p>

  <p>
    It's very helpful in letting users know what you offer, when you're open, and if you have a physical location, and how to get there. To start using GMB you need to sign in to your Google account, then visit <a id="anchor--gmb" href="https://www.google.com/business/" target="_blank">https://www.google.com/business/</a>.
  </p>

  <p>
    Click <b>Sign in</b> and once you're logged in you should be brought to your GMB dashboard. Go to the <b>Info</b> tab and lets start filling out the data including your company name, address, service area, hours, phone numbers, website, services, description, and add a few photos.
  </p>

  <p>
    Once all is filled out, you will need to verify your business location, you should see a <b>Verify Now</b> option, that will take you to a page that has information for a physical mailer that Google will send to you.
  </p>

  <p>
    Make sure the address is correct and click <b>send postcard</b>. The postcard will be mailed out and should get to you in about <b>2 weeks</b>.
  </p>

  <p>
    Once you get the postcard, return to your GMB dashboard open the location you previously created and click the <b>Verify Location</b> or <b>Verify Now</b> button again.
  </p>

  <p>
    Enter the code from the postcard Google sent you and then click <b>Submit</b>. Congrats, you're now verified on google!
  </p>

  <p>
    Now when someone searches for your business by name, they will be presented with a special layout with all your company info:
  </p>

  <img src="/images/jamstack-jedi/gmb--result.png" class="jamstack--img" alt="Google My Business listing" />

  <p>
    If you had any questions or issues signing up with GMB, <a id="anchor--gmb-hubspot" href="https://blog.hubspot.com/marketing/google-my-business" target="_blank">HubSpot</a> has a very nice article about it.
  </p>

  <p>
    In a previous chapter we setup Google Search Console. These two tools are very helpful in managing your online presence with Google. But there's still Bing and Yahoo and DuckDuckGo that we need to tell we have a website.
  </p>

  <p>
    We'll do that in the next chapter.
  </p>

  <AniLink to="jamstack-jedi/search-submit" className="nextBtn">Submit Site to Other Search Engines &raquo;</AniLink>
  <br />
  </Layout>
);

export default GMB;
