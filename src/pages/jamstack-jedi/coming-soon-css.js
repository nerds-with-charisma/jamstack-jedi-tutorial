import React from 'react';

import AniLink from '../../components/Common/AniLink';
import Layout from '../../components/layout';
import Markdown from '../../components/Common/markdown';

const ComingSoonCss = () => (
  <Layout
    title="Coming Soon"
     author={{ gravatar: '//0.gravatar.com/avatar/b5eff8d62adf3ff34e67f153781d27c4' }}
    gitRepo="https://gitlab.com/nerds-with-charisma/jamstack-jedi/tree/master/coming-soon-css-final"
    time={15}
    description="Style a coming soon page with CSS."
    keywords="CSS coming soon, learn CSS coming soon"
  >
    <strong>I'm a Fancy Boy!</strong>
    <br />
    <br />

    <p>
      Lets jump right into styling our coming soon page. The first thing we need to do is create the CSS file. Head over to your terminal (make sure you're in the root) and create a new file called "styles.css".
    </p>

    <Markdown
      rel="Terminal"
      language="terminal"
      md={
        `  > styles.css`
      }
    />

    <p>
      This creates the file, but it doesn't link it to our index.html file. To do that, open up the index file and lets link it in the head of our document.
    </p>

    <Markdown
      rel="index.html"
      language="html"
      md={
        `  <head>
    ...
    <link rel="stylesheet" type="text/css" href="styles.css" />
    ...
  </head>`
      }
    />

    <p>
      Jump back to the css file, and lets make sure we linked it properly. Add the following code to the css file and save. Refresh your browser and see if the background has changed.
    </p>

    <Markdown
      rel="styles.css"
      language="css"
      md={
        `  html {
    background: #9012FE;
  }`
      }
    />

    <img src="/images/jamstack-jedi/coming-soon-css-link.png" class="jamstack--img" alt="Coming Soon First Css" />

    <p className="tldr">
      It might not be pretty, but it means our file is linked correctly. If your page did not change background color, it probably means you linked the CSS file incorrectly. Make sure it's spelled right, it's got the right extension, and you typed the link tag exactly. Check in devtools, the network panel to see if it found your css file. If it did not, it will have an error code of 404 or "not found". This means it's linked incorrectly.
    </p>

    <hr />

    <p className="tldr">
      Ok, lets work on the layout similar to how we worked on the HTML, by styling part by part.
    </p>
    <p>
      We'll start with our content wrapper, open up styles.css and add the following CSS:
    </p>

    <Markdown
      rel="styles.css"
      language="css"
      md={
        `  #copy-wrapper h2 {
    font-size: 100px;
    color: #fff;
    margin: 0;
    line-height: 100px;
  }

  #copy-wrapper h3 {
    font-size: 36px;
    color: #fff;
    margin: 0;
    font-weight: normal;
  }`
      }
    />

    <img src="/images/jamstack-jedi/coming-soon-css-content.png" class="jamstack--img" alt="Coming Soon Content Styles" />

    <p className="tldr">
      Here we've set the font-size attribute for both the h2 and h3 tags to 100 and 36 pixels respectively. We change the font color to white. Remove the margins. For the h2 we adjust the line-height so that the text is a little closer together than normal. And for the h3 we make it so that the text is not bold, which a heading is by default.
    </p>

    <p id="anchor--font-serif" className="tldr">
      You might notice some minor issues here however. There's some blank space around the edges of the page, and all the fonts are serif fonts, meaning they have little "feet" on them. <a href="https://www.canva.com/learn/serif-vs-sans-serif-fonts/" target="_blank">Learn more about fonts</a> from Canva.
    </p>

    <p className="tldr">
      So how do we fix these minor issues? Most developers add what is called a "reset" to their pages to make all browsers behave the same and to basically start with a clean slate.
    </p>

    <p>
      Lets add a VERY basic reset to our code. Head back into our css file and lets update modify it a bit:
    </p>

    <Markdown
      rel="styles.css"
      language="css"
      md={
        `  html, body, h1, h2, h3, h4, h5, h6 {
    margin: 0;
    padding: 0;
    font-family: -apple-system, "Helvetica Neue", sans-serif;
  }

  #copy-wrapper h2 {
    font-size: 100px;
    color: #fff;
    line-height: 100px;
  }

  #copy-wrapper h3 {
    font-size: 36px;
    color: #fff;
    font-weight: normal;
  }`
      }
    />

    <p className="tldr">
      The first declaration we have is now targeting multiple tags, which is a great way to reuse styles for multiple elements. If you notice we have set the margin for the html tag, body, and all headings to 0. Previously we declared the margins on each the h2 and h3, which is redundant, so we have removed those and added it to this combination.
    </p>

    <p className="tldr">
      Next we also make the padding 0, and then we declare a "font-family", or the "font-stack" that the page will use. This is a very basic one, basically saying use the system font, or Helvetica Neue, or whatever the deafult sans-serif font on that particular machine is.
    </p>

    <p className="tldr">
      The h2 and h3 tags are the same, minus the removed margins.
    </p>

    <img src="/images/jamstack-jedi/coming-soon-css-reset.png" class="jamstack--img" alt="Coming Soon Reset" />

    <hr />

    <p>
      Now lets work on the right side with the contact info. Directly below our previous styles, lets add some new styles for the contact portion.
    </p>

    <Markdown
      rel="styles.css"
      language="css"
      md={
        `  #contact-wrapper {
    text-align: center;
  }

  #social-icons a {
    display: inline-block;
    font-size: 36px;
    padding: 20px 45px;
    color: #fff;
    text-decoration: none;
  }

  #contact-button-wrapper a {
    display: inline-block;
    background: #fff;
    border-radius: 300px;
    min-width: 400px;
    max-width: 480px;
    text-align: center;
    padding: 15px 0;
    font-weight: bold;
    font-size: 18px;
    color: #292929;
    text-decoration: none;
    transition: 0.2s all linear;
  }

  #contact-button-wrapper a:hover {
    background: #02e2ca;
    color: #fff;
  }`
      }
    />

    <p className="tldr">
      Lets talk about what's going on again. First we give our content wrapper a text-align of center so that our text is centered horizontally.
    </p>

    <p className="tldr">
      Next we style the social icons inside the social-icons section, specifically any anchor tags inside of it. We change the display property to "inline-block" so that we can give it some padding. Without doing this, we could only change the horizontal padding. Now we can effect all 4 directions. We also change the font size and text color.
    </p>

    <p className="tldr">
      The final declaration is "text-decoration: none", which, without this, most browsers put an underline under hyper links. Which, depending on your design you might want, but not in our case. So we remove it.
    </p>

    <p className="tldr">
      Next we style the main contact button, which is the only <b>a</b> tag inside of the contact-button-wrapper container. We set it to inline-block again, so we can better control it's padding. We want the background to be white and the button to be rounded. For that we use the new-ish "button-radius" attribute. Which we set to an arbitrarily high value to make it totally rounded.
    </p>

    <p className="tldr">
      Next we set a min and max width, so that the buttons is big...but not too big...we also align the text inside to center, add some padding, make it bold, change the font size and color, remove the text-decoration...all standard for us at this point.
    </p>

    <p className="tldr">
      That last line is a little weird though, right? The "transition" property allows us to animate our styles when their state changes, we're telling it to animate over 0.2 seconds, "all" means any attribute we change the browser can animate, and finally we say "linear" which is the easing we apply to the animate.
    </p>

    <p className="tldr">
      We'll get more into transitions later, but if you'd like to get a jump start on it check out <a id="anchor--css-transitions" href="https://zellwk.com/blog/css-transitions/" target="_blank">this article</a> by Zell Liew
    </p>

    <p className="tldr">
      The final css rule we create will be what's animated. We target the same element, "#contact-button-wrapper a", but we add the <strong>:hover</strong> <a id="anchor--pseudo-selectors" href="https://css-tricks.com/pseudo-class-selectors/" target="_blank">pseudo selector</a> to it. Which means to only apply this css when the mouse is hovered over it. The CSS we're applying changed the background color and the font color. Combining the transition property along with the hover property will make a nice, smooth, transition that looks pretty neat for minimal effort.
    </p>

    <p>
      Save your files and refresh the page.
    </p>

    <img src="/images/jamstack-jedi/coming-soon-css-contact.png" class="jamstack--img" alt="Coming Soon Contact Wrapper" />

    <hr />

    <p className="tldr">
      It's looking better, but now we want to position the content so that it's centered on the page. And also in two columns
    </p>

    <p>
      To handle our layouts, we'll be using <a id="anchor--css-tricks-flexbox" href="https://css-tricks.com/snippets/css/a-guide-to-flexbox/" target="_blank">Flexbox</a>
    </p>

    <Markdown
      rel="styles.css"
      language="css"
      md={
        `  #main {
    width: 75%;
    margin: auto;
    display: flex;
    flex-direction: row;
    flex-wrap: wrap;
    align-items: center;
  }

  #copy-wrapper, #contact-wrapper {
    width: 50%;
  }`
      }
    />

    <p className="tldr">
      We start by making a little container of our main element, it will only stretch 75% of the width of the page, but we set the margin to auto, so it will horizontally center itself.
    </p>

    <p className="tldr">
      Next we set the flex properties for display, set the direction to row (or horizontal) set the wrap to allow it to break down to the next line if it has to, and align items center, which will handle vertical centering.
    </p>

    <p>
      To visually finish our layout  we set the two wrappers, copy-wrapper and contact-wrapper, to a width of 50%, this will make the two wrappers display next to each other horizontally.
    </p>

    <img src="/images/jamstack-jedi/coming-soon-css-flex.png" class="jamstack--img" alt="Coming Soon Flex Layout" />

    <p className="tldr">
      It's looking great, but we're not done with the flex grid yet. You'll notice that it's still docked at the top when the window gets bigger. We have to tweak our overall page size a little to fix this. Head back into styles.css and lets modify a few selectors.
    </p>

    <p>
      Right below our html declaration, lets add another multiple tag selector:
    </p>

    <Markdown
      rel="styles.css"
      language="css"
      md={
        `  html, body, #main {
    height: 100%;
  }`
    } />

    <p className="tldr">
      Pretty simple, we tell the html tag, body tag and the element with an id of "main" to be 100% width. Now if you save and refresh the page, it should be looking pretty tight.
    </p>

    <img src="/images/jamstack-jedi/coming-soon-css-flex2.png" class="jamstack--img" alt="Coming Soon Flex Layout pt. 2" />

    <hr />

    <p>
      The final two things we need to do to finish our layout is to style the footer and add our background image. Which are both super simple. First the footer:
    </p>

    <Markdown
      rel="styles.css"
      language="css"
      md={
        `  footer {
    color: #fff;
    width: 100%;
    text-align: center;
  }`
    } />

    <p className="tldr">
      This should all look familiar, change the text color to white, make the width 100% of the way across the parent container (which is #main if you remember), align the text in the center.
    </p>

    <p>
      Now lets tackle the background image, this might look a little odd to start, but stick with me. I've included a <a href="https://gitlab.com/nerds-with-charisma/jamstack-jedi/blob/master/coming-soon-css-final/background.jpg" target="_blank">background image</a> you can use, or feel free to create your own.
    </p>

    <p>
      We'll save it in the same root directory as our styles.css file and reference it from there. Now lets modify our html selector:
    </p>

    <Markdown
      rel="styles.css"
      language="css"
      md={
        `  html {
    background: url(background.jpg) no-repeat center center fixed;
    background-size: cover;
    background-color: #9012FE;
  }`
    } />

    <p>
      First notice that the new code we've added is ABOVE the old background attribute and we've changed that to "background-color", this is a fallback incase the browser can't render the background image. Otherwise we'd have white background with white text...not too readable.
    </p>

    <p className="tldr">
      We've put a new background attribute that references our <u>background.jpg</u> file, we have to tell it specifically we'll be using a url so we wrap it with url(...). Next we have the repeat value, in our case we do not want it to repeat. The next are the positioning attributes, we want it centered vertically and horizontally, as well as the position set to "fixed". This will make sure that no matter where we scroll or shrink the page, it will stay in the same spot.
    </p>

    <p className="tldr">
      The final new attribute is "background-size" set to "cover", where the magic happens. This makes the image cover the entire visible browser window. CSS Tricks has a <a id="anchor--full-cover-css" href="https://css-tricks.com/perfect-full-page-background-image/" target="_blank">great article</a> explaining how all this works
    </p>

    <p>
      Save it up, and refresh it, and man...that looks pretty close to our design doesn't it?!
    </p>

    <img src="/images/jamstack-jedi/coming-soon-css-final.png" class="jamstack--img" alt="Coming Soon Final" />
    <br />

    <p>
      You might have noticed that my screenshots are on a fairly tiny browser, and the tagline is wrapping to the next line. If you keep shrinking the screen, things just get more jumbled. In the next chapter, we'll cover some basic <u>media queries</u> to make it look good on smaller devices like phones and tablets.
    </p>

    <AniLink to="jamstack-jedi/coming-soon-mobile" className="nextBtn">Coming Soon on Mobile &raquo;</AniLink>
    <br />
  </Layout>
);

export default ComingSoonCss;
