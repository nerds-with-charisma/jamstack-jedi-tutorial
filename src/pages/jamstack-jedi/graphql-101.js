import React from 'react';
import AniLink from '../../components/Common/AniLink';

import Layout from '../../components/layout';

import Markdown from '../../components/Common/markdown';
import YouTube from '../../components/Common/YouTube';

const GraphQL101 = () => (
  <Layout
    title="GraphQL 101"
    author={{ gravatar: '//0.gravatar.com/avatar/b5eff8d62adf3ff34e67f153781d27c4' }}
    gitRepo="https://gitlab.com/nerds-with-charisma/jamstack-jedi/tree/master/graphql-101"
    time={15}
    description="Learn GraphQL, and how to make queries in GatsbyJS. GraphQL basics."
      keywords="Learn GraphQL, GraphQL Basics, Get better at GraphQL, GraphQL 101, Intro to GraphQL"
  >
    <strong>Getting Some Data</strong>
    <br />
    <br />

    <p>
      In a previous chapter, we passed in our meta data by hardcoding it and passig it down as a prop in the <b>index.js</b> file. This is fine, but what if we could put all of our content in one file and pull it in? Or if we wanted to pull it from a database somewhere else on the web?
    </p>

    <p>
      With Gatsby, that's totally doable with a query language called <b>GraphQL</b>. With Gatsby, you don't really have to worry about the architecture of GraphQL, you only have to worry about writting your queries to pull in the data.
    </p>

    <p className="tldr">
      Before we get into the code, it would be a good idea to familiarize yourself with just what GraphQL is. In short, it's a query language, meaning you ask a server for data and it sends it to you.
    </p>

    <p>
      GraphQL is structured very literally, meaning whatever we ask for we get back, nothing more and nothing less. The two main components of communicating with a GraphQL server are <b>Queries</b> (a request to read data) and <b>Mutations</b> (a request to update/create/delete data).
    </p>

    <p>
      To learn the basics for this chapter, I would suggest reading the <a id="anchor--graphQL-intro-docs" href="https://graphQL.org/learn/" target="_blank">GraphQL Intro Docs</a>, it's Star Wars themed so you know it's good. As well as the <a id="anchor--graphQL-gatsby-docs" href="https://www.gatsbyjs.org/docs/graphQL/" target="_blank">Gatsby GraphQL Docs</a>. Those two resources would be enough to get you through what we'll accomplish with this tutorial since we won't be building our own server or anything.
    </p>

    <p>
      If you're really into GraphQL and want to dig deeper I would suggest going through the tutorials on <a id="anchor--how-to-graphQL-tutorial" href="https://www.howtographQL.com/" target="_blank">How To GraphQL</a>. They're a little confusingly laid out, but they're very good resources. Second I would suggest <a id="anchor--ace-mind-graphQL" href="https://www.youtube.com/watch?v=7giZGFDGnkc&list=PL55RiY5tL51rG1x02Yyj93iypUuHYXcB_" target="_blank">Ace Mind's GraphQL series</a>. It's long, but it's great.
    </p>

    <p>
      Finally, if you're REALLY into GraphQL, I'd suggest buying <a id="anchor--udemy-graphQL-tutorial" href="https://www.udemy.com/graphQL-bootcamp/" target="_blank">Andrew Mead's GraphQL tutorial on Udemy</a>. You can get it on-sale for about $10. This one is <i>SUPER</i> long, but it's exhaustive. It will cover everything you need to know about GraphQL. My only crit. is there is a lot of early content that requires re-writting later once you setup your server. Also, deployment is covered very quickly in a later chapter, and I ran into errors and had to spend hours Googling. But the content is incredible.
    </p>

    <hr />

    <b>Setting up GraphQL Playground in Gatsby</b>

    <p>
      There's two built-in GraphQL playground's in Gatsby, one is GraphiQL and the other is GraphQL Playground. Personally I prefer Playground, so lets enable that one by default.
    </p>

    <p id="anchor--graphQL-playground">
     Open up package.json and modify the <b>develop</b> command to the following:
    </p>

    <Markdown
      rel="package.json"
      language="javascript"
      md={
        `  "develop": "GATSBY_GRAPHQL_IDE=playground gatsby develop",`}
    />

    <p>
      Now, instead of calling <b>gatsby develop</b> we call <b>npm run develop</b>, so kill your current server (cmd + c) and restart it with:
    </p>

    <Markdown
      rel="package.json"
      language="terminal"
      md={
        `  npm run develop`}
    />

    <p>
      Now point your browser to <b>http://localhost:8000/__graphql</b>
    </p>

    <img src="/images/jamstack-jedi/graphQL--playground.png" class="jamstack--img" alt="Enable GraphQL Playground" />

    <hr />

    <strong id="anchor--gatsby-static-query">Getting data via GraphQL in Gatsby</strong>

    <p>
      It's common practice to put all of your static data inside of the <b>gatsby-config.js</b> file. This makes it easier to update everything in one place. So lets move our homepage meta data to the gatsby-config file.
    </p>

    <Markdown
      rel="gatsby-config.js"
      language="javascript"
      md={
        `  module.exports = {
  siteMetadata: {
    homepageData: {
      title: 'Nerds With Charisma',
      lang: 'en',
      meta: [
        {
          name: 'description',
          content: 'Nerds With Charisma online portfolio coming soon!',
        },
        {
          name: 'keywords',
          content: 'Brian Dausman, Nerds With Charisma',
        },
      ],
    },
  ...`}
    />

    <p>
      Lets head on over to GraphQL Playground and build our query. You need to restart your server each time you add a new node to <strong>gatsby-config.js</strong>, just an FYI.
    </p>

    <Markdown
      rel="localhost:8000/__graphQL"
      language="graphQL"
      md={
        `  query IndexQuery {
    site {
      siteMetadata {
        homepageData {
          title
          lang
          meta {
            name
            content
          }
        }
      }
    }
  }`}
    />

    <p className="tldr">
      We start with the "query" keyword, this lets GraphQL know we are going to be expecting to receive data. We name it "IndexQuery" since this is our index component. This could be called anything you want.
    </p>

    <p className="tldr">
      Next we tell it we want to look in the "site" node, which is just a Gatsby convention for looking at the gatsby-config.js file. Inside of that we look for the "siteMetadata" node, which is one of our root nodes created inside of gatsby-config.js.
    </p>

    <p className="tldr">
      We created the homepageData node and we tell GraphQL we want the title, lang, and meta nodes. Inside the meta nodes we want name and content.
    </p>

    <p>
      Hit the play button and you should see our data pulled in in the exact structure we requested it. That means we can use it in our homepage component by using the StaticQuery function in Gatsby.
    </p>

    <hr />

    <p>
      Import static query and graphQL in order to use GraphQL in Gatsby, open up <b>index.js</b>:
    </p>

    <Markdown
      rel="index.js"
      language="javascript"
      md={
        `  import React from "react";
  import { StaticQuery, graphql } from 'gatsby';

  ...`}
    />

    <p>
      Next we wrap our existing component in the <b>StaticQuery</b> component, and replace all the hardcoded values with our dynamic content. We extract it as "data" from our props, so we can use <b>data.site.siteMetadata.homepageData</b> to print out our values.:
    </p>

    <Markdown
      rel="index.js"
      language="javascript"
      md={
        `  ...
  const IndexPage = () => (
    <StaticQuery
      query={ graphql\`
        query IndexQuery {
          site {
            siteMetadata {
              homepageData {
                title
                lang
                meta {
                  name
                  content
                }
              }
            }
          }
        }
      \`}
      render={(data) => (
        <Layout>
        <SEO
          title={data.site.siteMetadata.homepageData.title}
          lang={data.site.siteMetadata.homepageData.lang}
          meta={data.site.siteMetadata.homepageData.meta}
        />
        <h1>Hello World</h1>
      </Layout>
      )}
    />
  );

  ...`}
    />

    <p>
      Restart your server and take a look at the source, you should still see your meta data populated. Go ahead and make a change in gatsby-config and you should see it update instantly.
    </p>

    <hr />

    <img src="/images/jamstack-jedi/graphQL--meta.png" class="jamstack--img" alt="GraphQL pull in meta data" />

    <p className="tldr">
     Awesome, we now have our data coming in from a central source! This may seem a tad trivial right now, but later when we add more data and dynamic info, this way of organizing will be really great.
    </p>

    <p className="tldr">
      We've pretty much finished up our initial Gatsby setup. Now we're free to start building out our sections and individual components!
    </p>

    <p>
      Lets go over our design, then jump right in to building out our first section, the hero component.
    </p>

    <AniLink to="jamstack-jedi/portfolio-design" className="nextBtn">Review the Design &raquo;</AniLink>
    <br />
  </Layout>
);

export default GraphQL101;
