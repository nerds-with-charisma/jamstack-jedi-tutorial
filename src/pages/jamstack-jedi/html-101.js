import React from 'react';

import AniLink from '../../components/Common/AniLink';
import Layout from '../../components/layout';
import YouTube from '../../components/Common/YouTube';

const HTML101 = () => (
  <Layout
    title="HTML 101"
     author={{ gravatar: '//0.gravatar.com/avatar/b5eff8d62adf3ff34e67f153781d27c4' }}
      gitRepo={null}
      time={4}
      description="Learn HTML, the first step to becoming a front-end developer. HTML basics."
      keywords="Learn HTML, HTML Basics, Get better at HTML, HTML 101, Intro to HTML"
  >
    <strong>Are You Ready!</strong>
    <br />
    <br />
    <p>
      Unlike the last chapters where I provided farther learning at the <em>end</em>, this time I highly suggest you go through these at the beginning.
    </p>
    <p className="tldr">
      The first book I recommend and is absolutely essential if you've never coded any HTML before (and even if you have but you don't really understand it all) is:
    </p>
    <p>
      <a id="anchor--jon-duckett" href="https://www.amazon.com/HTML-CSS-Design-Build-Websites/dp/1118008189" target="_blank">HTML &amp; CSS - Design and Build Websites</a> by Jon Duckett.
      {/* https://wtf.tw/ref/duckett.pdf */}
    </p>

    <p className="tldr">
      It covers absolutely everything you'll need to know about the HTML specification. Read this, read it twice, learn it, get comfortable with it. We won't be covering anything basic like what a tag is, what tags do what, etc...this is your chance to learn it. A good foundation...builds...a strong...something...idk just read it!
    </p>

    <p>
      The second book gives a quick primer on the new HTML5 spec called:
    </p>

    <p>
      <a id="anchor--jeremy-keith" href="https://html5forwebdesigners.com/" target="_blank">HTML5 for Web Designers</a> by Jeremy Keith.
      {/* https://www.dmassociates.com/images/general/HTML5_for_Web_Designers.pdf */}
    </p>

    <p className="tldr">
      Remember that used books are an awesome resource, as well as your local library.
    </p>

    <p id="anchor--html-basics-youtube">
      If reading isn't your style, this video by Envato is good at introducing just the basics of HTML.
    </p>

    <YouTube url="https://www.youtube.com/embed/ONin3xInlGw" />

    <br />

    <p className="tldr">
      After going through these resources, you should have a good understanding of HTML tags and how we'll be laying everything out. If you're not totally comfortable, please go back through everything until you really understand it. We'll be using HTML in pretty much every chapter.
    </p>

    <p>
      In the next chapter, we'll start to build our coming soon page. Before we get to the actual code, we'll take a look at the finished product that we'll be building.
    </p>

    <img src="/images/jamstack-jedi/html--preview.png" class="jamstack--img" alt="Coming Soon Preview" />

    <p className="tldr">
      As you can see from the mockup, we'll be building a nice looking landing page that will cover a variety of topics such as, using a background image, headings, linking, opening an email client, and using special characters as seen in the copyright symbol.
    </p>

    <p>
      Feel free to <a id="anchor--coming-soon-psd" href="/images/coming-soon-mockup.psd">download the PSD</a> file for this design and see how it's built. It is very simple so if you're totally unfamiliar with PhotoShop, you should be able to deconstruct this to see how it's made.
    </p>

    <p className="tldr">
      If you do not have Photoshop, fear not, you can use an online tool called <a id="anchor--photopea" href="https://www.photopea.com/" target="_blank">Photopea</a> to open, edit, and export PSD files for free.
    </p>

    <p>
      We'll be breaking up our mockup logically like so:
    </p>

    <img src="/images/jamstack-jedi/design--main.png" class="jamstack--img" alt="Coming Soon Design, Main" />

    <p>
      We'll have our main wrapper that will hold our content (above). Next, we have 2 sections inside main for our copy-wrapper and contact-wrapper (below).
    </p>

    <img src="/images/jamstack-jedi/design--inner.png" class="jamstack--img" alt="Coming Soon Design, Inner" />

    <p>
      Our final major section is the footer:
    </p>

    <img src="/images/jamstack-jedi/design--footer2.png" class="jamstack--img" alt="Coming Soon Design, Footer" />

    <p className="tldr">
      We're keeping it simple for this coming soon page, but this should still give you a real world challenge of a custom layout. There are many ways we could code this, so feel free to try it on your own.
    </p>

    <p className="tldr">
      Once you're comfortable with the HTML basics, and you've looked over how the design was built, head on over to the next chapter to start building!
    </p>
    <AniLink to="jamstack-jedi/coming-soon-html" className="nextBtn">Start Building Our Coming Soon's HTML &raquo;</AniLink>
    <br />
  </Layout>
);

export default HTML101;
