import React from 'react';
import AniLink from '../../components/Common/AniLink';

import Layout from '../../components/layout';

import Markdown from '../../components/Common/markdown';
import YouTube from '../../components/Common/YouTube';

const HeroBuild3 = () => (
  <Layout
    title="Hero Continued"
    author={{ gravatar: '//0.gravatar.com/avatar/b5eff8d62adf3ff34e67f153781d27c4' }}
    gitRepo="https://gitlab.com/nerds-with-charisma/jamstack-jedi/tree/master/build-hero-3"
    time={10}
    description={null}
    keywords={null}
  >

    <strong>Finish It Up</strong>
    <br />
    <br />

    <p>
      We're just about done with our hero component, we just need to make it look good on mobile. So start up your devtools (right click > inspect) and choose the mobile emulation icon. I'm going to choose <b>iPhone X</b> as the emulator of choice.
    </p>

    <p>
      Lets gameplan a little. What's wrong with our current rendering? The fonts are too big, the tagline is too long, and the contact button is going off the screen. So lets start fixing some things.
    </p>

    <img src="/images/jamstack-jedi/hero--iphone.png" class="jamstack--img" alt="Hero iPhone" />

    <p>
      Open up <b>_hero.scss</b> and scroll down to the bottom. Lets add a media query for smaller screens:
    </p>

    <Markdown
      rel="_hero.scss"
      language="scss"
      md={` /* media query for anything under 900px */
  @media only screen and (max-width: 900px) {
    #hero {
      /* move everything up a little on mobile */
      div {
        padding-top: 75px;
      }

      /* make the font size for the heading much smaller */
      h2 {
        font-size: 60px;
        line-height: 55px;
      }

      /* tagline font smaller too */
      aside {
        font-size: 24px;
      }

      /* make the button text smaller */
      button {
        font-size: 18px;
      }
    }
  }`}
    />

    <p>
      We're really just doing some resizing here, I've added comments to everything inline and nothing should be too new. We add a little padding to the top, and changed the font sizes so they fit better on mobile.
    </p>

    <p>
      Now we can refresh our server and take a look, and....yeah...looking good!
    </p>

    <img src="/images/jamstack-jedi/hero--mobile.png" class="jamstack--img" alt="Hero mobile" />

    <p>
      Go ahead and change the device type in the simulator and refresh the page. Make sure the layout looks good on all major devices. If not, go ahead and add any custom fixes you need.
    </p>

    <p>
      Honestly...that's it for our hero, this was a nice and quick chapter. next up we'll tackle the header and navigation. If you want to grab the final files for the hero section, you can grab them on our GitLab.
    </p>

    <AniLink to="jamstack-jedi/build-header" className="nextBtn">The Header Component &raquo;</AniLink>
    <br />
  </Layout>
);

export default HeroBuild3;
