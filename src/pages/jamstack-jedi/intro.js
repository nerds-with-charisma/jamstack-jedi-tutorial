import React from 'react';

import AniLink from '../../components/Common/AniLink';
import Layout from '../../components/layout';
import Tooltip from '../../components/Common/Tooltip';

const JamstackJedi = () => {
  return (
    <Layout
      title="Intro"
      author={{ gravatar: '//0.gravatar.com/avatar/b5eff8d62adf3ff34e67f153781d27c4' }}
      gitRepo={null}
      time={4}
      description="Learn the JAMstack portfolio using GatsbyJs and learn to become a front-end developer "
      keywords="JAMstack, Gatsby, GatsbyJs, Developer Portfolio, Become a Front-End Developer"
    >
      <div>
        <strong>
          {'The TL;DR'}
        </strong>
        <br />
        <br />
        <p>
          Hi Everybody!
        </p>

        <p>
          How's it going? Are you ready to learn the in's and out's of front-end development? Then you've come to the right place. In the following pages you'll find a (hopefully) great tutorial on becoming a top-notch FED.
        </p>

        <p>
          So what's this guide all about? You can think of this as a road-map to becoming a <b>Front-End Developer</b> or more specifically a <b>Jamstack Jedi</b>. What's a Jamstack, you ask? <a id="anchor--jamstack" href="https://jamstack.org/" target="_blank">Jamstack</a> is a term for a stack (or combination of code/tools) using Javascript, API integration, and markup. It's not a true Full-Stack developer but it's close. In the coming chapters we'll go through how to build a portfolio with ReactJs (Javascript), GatsbyJs (React Framework & static site generator), and GraphQL (the API), as well as cover a lot of other common FED tasks and skills. By the end of this whole ordeal you should be comfortable building out pages and populating them with dynamic data that includes layouts, styles, interaction, and more.
        </p>

        <p>
          These lessons will be structured in a logical &amp; progressive way so that you'll build, from scratch, a complete portfolio that can be hosted virtually anywhere. We won't go into a lot of details on the basics, but you will be provided with resources to learn as much as you need to get comfortable with a section before you start on it. For example, we won't cover what all the HTML tags are, but you will be provided resources such as books and videos to get you up to speed before we build out the HTML for our site.
        </p>

        <p>
          We'll start by building a coming soon page to put on our server as a placeholder while we build our full portfolio. We'll cover basic HTML, CSS, and Javascript. From there we'll move on to building a portfolio with GatsbyJs &amp; ReactJs. We'll cover a lot of topics along the way like, HTML, CSS, Sass, Native Javascript, GatsbyJs, ReactJs, Google Analytics, Google Tag Manager, Accessibility, SEO &amp; Marketing, Design, Speed Optimization, A/B Testing, GraphQL, eBlasts, Git &amp; GitLab, and more!
        </p>

        <p>
          If all that sounds good, and you're interested in a deep dive into the Jamstack, then click the link at the bottom to get started.
        </p>

        <p>
          BTW, if you're looking for something specific or can't remember which chapter something was covered, check out our <AniLink to="appendix">Appendix</AniLink> to quickly find a topic.
        </p>

        <hr />
      </div>

      <div className="tldr">
        <strong>
          {'The Slightly Longer TL;DR'}
        </strong>
        <br />
        <br />
        <p>
          Below is a "fluffier" explanation of what this tutorial is all about, but before we get there, I want to draw your eyes to the header of each chapter. Notice the red Star Wars "Galactic Republic" <i className="fab fa-galactic-republic" /> icon on the right side? What's that all about you ask? This button, when activated, will hide all the fluff. If you're not interested in the deep dive, nitty-gritty of the tutorial, you can click this button and it will hide everything non-essential. Click the green Jedi icon <i className="fas fa-jedi" /> to bring everything back. Give it a shot...clicking the red icon will hide this section and only show the "important" bits above. Click it again to bring this section back.
        </p>

        <p>
          A bit about why I wrote this...Back in the day, we used to learn how to build websites by digging around in the code of other websites we liked. We would reverse engineer how they did it and apply what we learned to our own sites. Now, with all these fancy build systems, that's much more difficult. Minified files, generated variables, complex code splitting and the like make it almost impossible for even an experienced dev to see what's going on under the hood.
        </p>
        <p>
          Growing up, I spent countless hours reading books, watching videos, and taking courses to get to where I'm at now. But there's a few problems I've found consistently throughout my career; Some of these materials were poorly written, but you wouldn't come to that conclusion until you've wasted a few hours trying. Others didn't go into depth or went into way too much depth either glossing over important parts or boring you by explaining them too much. To this day I haven't found a tutorial (even a simple roadmap) that covers everything you need to actually be a front-end developer.
        </p>
        <p>
          How will this be any different? First, I plan to keep things light while providing links that go more in-depth on specific topics. For example, we'll build a coming soon page, but we won't talk about what an H1 tag is. I will however give you a link to some books and videos to learn the basics of HTML so that you can get that knowledge on your own. I'm also planning on teaching some fairly arbitrary things that might seem pointless when building out our project, but they are things that you'll probably run in to in your day-to-day development. So you can take that knowledge and apply it later in a more practical way.
        </p>
        <p>
          Finally, we'll cover everything I think you'll deal with on a semi-constant basis if you're the lead FED at your job. We're talking marketing, analytics, A/B testing, etc...This will be an exhaustive list of everything I think you'll need to not just land a job, but land a high-paying job that you can feel confident leading a team.
        </p>
        <p>
          One thing to remember about becoming a senior developer or lead is it's not always the amount you know, but you're ability to understand, find the solution, and most importantly <u><i>EXPLAIN</i></u> to others what's going on and why in simple and relatable terms.
        </p>
        <p>
          Will this guide be perfect? No. Will it help anyone? Man, I hope so. I would have killed for something like this in school.
        </p>
        <p>
          Who exactly is this for, you ask? Well...anyone trying to break into the front-end development scene I suppose. I'm asked by lots of people to teach them how to get into coding or how they can make more money at their current jobs. Hopefully these following pages can help someone get to where they want in their career or schooling.
        </p>
        <p>
          Enough jib-jabbing! Lets get started!
        </p>
      </div>

      <AniLink to="jamstack-jedi/overview" className="nextBtn">An Overview of What We're Building &raquo;</AniLink>
    </Layout>
  );
};

export default JamstackJedi;
