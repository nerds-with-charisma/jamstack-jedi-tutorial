import React from 'react';
import AniLink from '../../components/Common/AniLink';

import Layout from '../../components/layout';

import Markdown from '../../components/Common/markdown';

const HeaderBuild = () => (
  <Layout
    title="Header Continued"
    author={{ gravatar: '//0.gravatar.com/avatar/b5eff8d62adf3ff34e67f153781d27c4' }}
    gitRepo="https://gitlab.com/nerds-with-charisma/jamstack-jedi/blob/master/build-header-2"
    time={21}
    description={null}
    keywords={null}
  >

    <p>
      This chapter will focus totally on making our main navigation, that includes the click event to hide and show it, the styling of it, and pulling in our data dynamically. So lets hop right in and build out the nav component in <b>Nav.js</b>:
    </p>

    <Markdown
      rel="Nav.js"
      language="javascript"
      md={`  import React from 'react';

  import Drawer from './Drawer';

  const Nav = () => (
    <section id="headerNav">
      <button
        className="font--light"
        type="button"
      >
        <span>
          <i className="fas fa-bars">
            <span className="ir">
              Menu
            </span>
          </i>
        </span>
      </button>
      <Drawer />
    </section>
  );

  export default Nav;`}
    />

    <p>
      You'll notice we import a <b>Drawer</b> component, which we'll make next, and then place it below the button to trigger our menu.
    </p>

    <p className="tldr">
      Lets make that <b>Drawer</b> component next:
    </p>

    <Markdown
      rel="Terminal"
      language="terminal"
      md={`  > src/components/header/Drawer.js`}
    />

    <p>
      And put in our skeleton code to get it rendering:
    </p>

    <Markdown
      rel="Drawer.js"
      language="javascript"
      md={`  import React from 'react';

  const Drawer = () => (
    <h1>Drawer</h1>
  );

  export default Drawer;`}
    />

    <p>
      We should now see it rendering at the top of the screen, so lets start building it and styling it. Grab the <a href="https://gitlab.com/nerds-with-charisma/jamstack-jedi/blob/master/build-header-2/src/images/logo--teal.svg" target="_blank">Logo file Here</a> or use your own.
    </p>

    <img src="/images/jamstack-jedi/header--drawer.png" class="jamstack--img" alt="Drawer init" />

    <hr />

    <Markdown
      rel="Drawer.js"
      language="javascript"
      md={`  import React from 'react';

  import Logo from '../../images/logo--teal.svg';

  const Drawer = () => (
    <nav id="nav">
      <br />
      <img src={Logo} alt="TODO" />
      <br />
      <br />
      <button
        type="button"
        className="nav--item font--light border--none"
      >
        <strong>HOME</strong>
      </button>
      <br />
    </nav>
  );

  export default Drawer;`}
    />

    <p className="tldr">
      We import the SVG logo here, a different color one than our header, as well as make a static button, just so we can see what it will look like while we style.
    </p>

    <hr />

    <p>
      For styling, lets make a <b>_nav.scss</b> file, import it into <b>main.scss</b> and open that up:
    </p>

    <Markdown
      rel="Terminal"
      language="terminal"
      md={`  > src/styles/_nav.scss`}
    />

    <Markdown
      rel="main.scss"
      language="scss"
      md={`  ...
  @import 'nav.scss';`}
    />

    <p>
      In the <b>nav.scss</b> file we'll apply the following styles:
    </p>

    <Markdown
      rel="_nav.scss"
      language="scss"
      md={`  #nav {
    /* this will be used for sliding our nav in and out */
    &.out {
      margin-top: -450px;
    }

    transition: all 0.2s linear; /* animate the nav in and out */
    position: fixed; /* dock it to the top of our screen */
    top: 0;
    left: 0;
    width: 100%;  /* make it 100% across the screen */
    padding: 15px 0 75px 0; /* add some padding above and a decent bit below */
    text-align: center; /* center our text */
    background: #2c2c2c; /* the background color of the nav */
    z-index: 99999; /* put it above all other layers */
    color: #fff; /* make the text white */

    img {
      margin-left: 21px; /* small margin to center our logo */
    }

    .nav--item {
      position: relative; /* set the position of each nav item for our hover effect */
      font-weight: bolder; /* make it bold */
      font-size: 28px;
    }

    .nav--item:hover {
      opacity: 0.7;  /* on hover, drop the opacity */
    }

    /* fancy little bit to make a hover border under it, you don't have to understand everything going on here */
    .nav--item:before, #logo:before {
      content: "";
      position: absolute;
      width: 70%;
      height: 2px;
      bottom: -4px;
      left: 15%;
      background-color: $linksHover;
      opacity: 0.1;
      visibility: hidden;
      transform: scaleX(0);
      transition: all 0.3s ease-in-out 0s;
    }

    /* the magic to show the border below */
    .nav--item:hover:before, #logo:hover:before {
      visibility: visible;
      opacity: 1;
      transform: scaleX(1);
    }
  }`}
    />

    <hr />

    <p>
      Now our focus should be hiding it and toggling the nav on click, we'll also want a close button. We've already setup the necessary foundation for this with the <b>out</b> class we created, so head on over to <b>Drawer.js</b> and lets add that class to our nav as well as a state variable to keep track of if we should show the nav or not.
    </p>

    <p className="tldr">
      We'll just need to pass a prop to our Drawer component to tell it to be open or closed:
    </p>

    <Markdown
      rel="Drawer.js"
      language="javascript"
      md={`  import React from 'react';
  import { PropTypes } from 'prop-types';

  import Logo from '../../images/logo--teal.svg';

  const Drawer = ({ isOpen }) => (
    <nav id="nav" className={(isOpen) ? 'in' : 'out'}>
      ...
    </nav>
  );

  Drawer.propTypes = {
    isOpen: PropTypes.bool.isRequired,
  };

  export default Drawer;`}
    />

    <p>
      We'll handle the toggling in the <b>Nav.js</b> component. First, we'll import useState and useEffect so we can keep track:
    </p>

    <Markdown
      rel="Drawer.js"
      language="javascript"
      md={`  import React, { useState, useEffect } from 'react';`}
    />

    <p>
      Next, in <b>Nav.js</b>, we need to change our component to wrap it in curly braces and add a return statement, since we need to do some logic before we render it:
    </p>

    <Markdown
      rel="Nav.js"
      language="javascript"
      md={`  const Nav = () => {
    return (
      <section id="headerNav">
        ...
      </section>
    )
  };`}
    />

    <p>
      Now we can default an <b>isOpen</b> variable to keep track of our nav now just before the return statement and pass it to the Drawer component:
    </p>

    <Markdown
      rel="Nav.js"
      language="javascript"
      md={`  ...
  const Nav = () => {
    const [isOpen, setIsOpen] = useState(false);

    return (
      ...
      <Drawer isOpen={isOpen} />
  ...`}
    />

    <p>
      Now, all that's left to do is create a click event, that will toggle this variable:
    </p>

    <Markdown
      rel="Nav.js"
      language="javascript"
      md={`  ...
    <button
      className="font--light"
      type="button"
      onClick={() => setIsOpen(!isOpen)}
    >
  ...`}
    />

    <p>
      Try clicking our menu button and we should see our menu slide in real smooth. And you'll notice our "in" class has been applied...BUT...one small problem, how do we close it?
    </p>

    <p>
      The problem is, our menu button is behind our actual menu drawer, but that's a simple fix we can add with some z-index styles. Open up <b>_header.scss</b> and lets add that now:
    </p>

    <Markdown
      rel="_header.scss"
      language="scss"
      md={`  ...
  #headerNav {
    button {
      z-index: 999999; /* make our button the highest level */
  ...`}
    />

    <p>
      Now our button is above our drawer, but it's always just a menu, we want it to be an "x" for when it needs to close. We can do that by swapping out the class for a font-awesome close button on click as well inside of <b>Nav.js</b>:
    </p>

    <Markdown
      rel="Nav.js"
      language="javascript"
      md={`  ...
      <i className={(isOpen) ? 'fas fa-times' : 'fas fa-bars'}>
  ...`}
    />

    <p>
      Now, as we click, the button will switch to the close icon when the drawer is open and the hamburger menu when closed.
    </p>

    <img src="/images/jamstack-jedi/header--nav-open.png" class="jamstack--img" alt="Drawer Open" />

    <hr />

    <p>
      The last thing we need to do, is pull in our navigation dynamically from <b>gatsby-config.js</b>. I know we'll have the following sections, Home, Services, Portfolio, About, and Contact. So we'll add those now and then we'll add logic to scroll dynamically to those anchors on our page.
    </p>

    <Markdown
      rel="gatsby-config.js"
      language="javascript"
      md={`  ...
  title: \`Nerds With Charisma\`,
  navigation: [
    'home',
    'services',
    'portfolio',
    'about',
    'contact',
  ],
  ...`}
    />

    <p>
      We need to restart our server after adding a new node to <b>gatsby-config.js</b> and then we can pull it into <b>index.js</b> and pass it as props down to our drawer component:
    </p>

    <Markdown
      rel="index.js"
      language="javascript"
      md={`  ...
  siteMetadata {
    title
    navigation
  ...
  <Header title={data.site.siteMetadata.title} navigation={data.site.siteMetadata.navigation} />
  ...
  `}
    />

    <p>
      And pass down again to our <b>Nav.js</b> component from <b>Header.js</b>:
    </p>

    <Markdown
      rel="Header.js"
      language="javascript"
      md={`  ...
  const Header = ({ title, navigation }) => (
    <header>
      <Nav navigation={navigation} />
  ...
  `}
    />

    <p>
      And again to our <b>Drawer.js</b> component:
    </p>

    <Markdown
      rel="Nav.js"
      language="javascript"
      md={`  import React, { useState, useEffect } from 'react';
  import { PropTypes } from 'prop-types';

  import Drawer from './Drawer';

  const Nav = ({ navigation }) => {
    const [isOpen, setIsOpen] = useState(false);

    return (
      <section id="headerNav">
        <button
          className="font--light"
          type="button"
          onClick={() => setIsOpen(!isOpen)}
        >
          <span>
            <i class={(isOpen) ? 'fas fa-times' : 'fas fa-bars'}>
              <span className="ir">
                Menu
              </span>
            </i>
          </span>
        </button>
        <Drawer isOpen={isOpen} navigation={navigation} />
      </section>
    )
  };

  Nav.propTypes = {
    navigation: PropTypes.array.isRequired,
  };

  export default Nav;`}
    />

    <p>
      And now, instead of hardcoding our links, lets loop through them dynamically in <b>Drawer.js</b>:
    </p>

    <Markdown
      rel="Drawer.js"
      language="javascript"
      md={`  import React from 'react';
  import { PropTypes } from 'prop-types';

  import Logo from '../../images/logo--teal.svg';

  const Drawer = ({ isOpen, navigation }) => (
    <nav id="nav" className={(isOpen) ? 'in' : 'out'}>
      <br />
      <img src={Logo} />
      <br />
      <br />
      { navigation.map((link => (
        <React.Fragment key={link}>
          <button
            type="button"
            className="nav--item font--light border--none font--18"
          >
            <strong>
              {link.toUpperCase()}
            </strong>
          </button>
          <br />
        </React.Fragment>
      )))}
      <br />
    </nav>
  );

  Drawer.propTypes = {
    isOpen: PropTypes.bool.isRequired,
    navigation: PropTypes.array.isRequired,
  };

  export default Drawer;`}
    />

    <p className="tldr">
      We loop each into a fragment so that we can put each one on it's own line. Then we capitalize the string, as a design choice, but we don't have any sort of ability to go to those links when clicked. We can add that function now, and we can test it out in the next chapter when we build out our <b>Services</b> section.
    </p>

    <hr />

    <p>
      To scroll to these anchors, we want to use a function from <b>Nav.js</b> so that we can also close our drawer on click, so lets build that now and then pass it down as a prop:
    </p>

    <Markdown
      rel="Nav.js"
      language="javascript"
      md={`  ...
  const Nav = ({ navigation }) => {
    const [isOpen, setIsOpen] = useState(false);

    const scrollToSection = (link) => {
      console.log(link);
    }
  ...

  <Drawer isOpen={isOpen} navigation={navigation} scrollToSection={scrollToSection} />
  ...`}
    />

    <p>
      Right now, we're just going to console log out the section name, lets open up <b>Drawer.js</b> and use this function on click:
    </p>

    <Markdown
      rel="Drawer.js"
      language="javascript"
      md={`  ...
  const Drawer = ({ isOpen, navigation, scrollToSection }) => (
    ...
    <button
      type="button"
      className="nav--item font--light border--none font--18"
      onClick={() => scrollToSection(link)}
    >
  ...`}
    />

    <p>
      Now, when we click, we should see our link name printed out in console, the idea is that we will have an ID of the same name and scroll to it when clicked. So lets add that functionality back in <b>Nav.js</b>:
    </p>

    <Markdown
      rel="Nav.js"
      language="javascript"
      md={`  ...
  const scrollToSection = (link) => {
    setIsOpen(false);
    document.getElementById(link).scrollIntoView({ behavior: 'smooth' });
  }
  ...`}
    />

    <p className="tldr">
      The first line uses our state function to close nav, then we get the element by id (or the link string we passed from the drawer click), and then call "scrollIntoView". We add "behavior" of "smooth" to make it scroll down really nicely. You'll be able to test this later, or you could put a div with an id of "portfolio" way down the page in index.js and click the portfolio link to test it.
    </p>

    <hr />

    <p>
      And that's pretty much it for our nav, that wasn't so bad! Nothing is really needed extra for mobile, so lets jump right into the services section!
    </p>

    <AniLink to="jamstack-jedi/build-services" className="nextBtn">Build the Services Section &raquo;</AniLink>
    <br />
  </Layout>
);

export default HeaderBuild;
