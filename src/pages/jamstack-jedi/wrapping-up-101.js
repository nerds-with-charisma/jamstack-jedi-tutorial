import React from 'react';
import AniLink from '../../components/Common/AniLink';

import Layout from '../../components/layout';
import Markdown from '../../components/Common/markdown';

const WrappingUp101 = () => (
  <Layout
    title="Wrapping Up"
    author={{ gravatar: '//0.gravatar.com/avatar/b5eff8d62adf3ff34e67f153781d27c4' }}
    gitRepo={null}
    time={7}
    description="Add Google Analytics to an HTML website"
    keywords="Signup for Google Analytics, Google Analytics How To, Installing Google Analytics, Simple Google Analytics"
  >
    <strong>Lets Wrap It Up</strong>
    <br />
    <br />

    <p className="tldr">
      Going forward, we will assume you're using Netlify to deploy, if you are not, and you're using FTP, simply upload your new files to your server via FTP like you did in the FTP chapter. You should still follow the Git workflow (pull, add, commit, pull, push...we'll keep hammering that home) no matter which hosting you're using.
    </p>

    <p className="tldr">
      We've covered a lot of ground in these intro chapters, but we tried to keep things simple. These chapters have really been about exploring and taking in as much info as possible on your end to learn the basics of html, css, and javascript.
    </p>

    <p className="tldr">
      From here we'll start getting into more code and configurations so make sure you're comfortable with the basics of HTML, CSS, and JS. The best way to learn is by doing, so now would be a good time to take a break, make a few layouts or pages on your own, and really get comfortable. We've provided some simple examples, to get our coming soon page up and running, but we'll start digging in deeper in the coming chapters.
    </p>

    <p>
      One last thing you'll want to do before we call our Front-end 101 section done is install Google Analytics so that you can track the number of visitors to your website.
    </p>

    <p>
      To start, you'll need a GA account, so head on over to <a id="anchor--google-analytics" href="http://analytics.google.com" target="_blank">http://analytics.google.com</a>, sign-in, and click the <b>Admin</b> link at the bottom.
    </p>

    <p>
      Choose <b>Create Account</b>, give it a name, choose <b>Web</b>, and click <b>next</b>.
    </p>

    <p>
      Next, name your site, choose https if you installed the SSL cert, and enter your domain name. Choose an industry from the dropdown, and set your time zone. Click <b>Create</b>.
    </p>

    <img src="/images/jamstack-jedi/wrapping--google-analytics-new.png" class="jamstack--img" alt="Create a new Google Analytics Account" />

    <p>
      You'll be provided with a tracking code, copy this to your clipboard and lets head back to our index.html page:
    </p>

    <img src="/images/jamstack-jedi/wrapping--ga-tracking-code.png" class="jamstack--img" alt="Google Analytics Tracking" />

    <p>
      Below our scripts.js file, lets add the GA tracking code. Don't forget to <u>use your tracking id</u> instead of ours:
    </p>

    <Markdown
      rel="index.html"
      language="html"
      md={
        `  ...
    <script src="scripts.js"></script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-146407659-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-146407659-1');
    </script>
  </body>
  ...`
      }
    />

    Lets commit this to our GitLab so that Netlify will build it to our prod server (or upload these files if you're using FTP).

    <Markdown
      rel="Terminal"
      language="terminal"
      md={
        `  git pull
  git add .
  git commit -m "Add google analytics
  git pull
  git push`
      }
    />

    <p>
      Once the build is done, lets refresh our page and open up the network panel. Filter by typing "analytics":
    </p>

    <img src="/images/jamstack-jedi/wrapping--network-ga.png" class="jamstack--img" alt="Google Analytics network panel" />

    <p>
      If everything looks right, we can go back to our GA dashboard, and refresh the <b>Real time</b> overview section, and we should see 1 live visitor:
    </p>

    <img src="/images/jamstack-jedi/wrapping--ga-realtime.png" class="jamstack--img" alt="Google Analytics real time" />

    <p className="tldr">
      Feel free to poke around in GA, take a look around and see how they track almost everything you could ever want to know about your site and it's visitors.
    </p>

    <hr />

    <p>
      Congrats! That's it for the Front-end 101 chapters. You've now got a fully functional coming soon page.
    </p>

    <p className="tldr">
      We've learned how to make a basic HTML file, structure it, style it, and add a bit of interactivity to it via Javascript! We also learned how to publish it to the internet for everyone to see. Finally we added tracking code to be able to see how many people are using the site.
    </p>

    <p>
      In the next section we'll take a look at ReactJs and GatsbyJs, which is a framework for building front-ends with Javascript and a React Framework for creating static websites with React, respectively.
    </p>

    <AniLink to="jamstack-jedi/spa-101" className="nextBtn">Lets Learn About Single Page Applications &raquo;</AniLink>
    <br />
  </Layout>
);

export default WrappingUp101;
