import React from 'react';
import AniLink from '../../components/Common/AniLink';

import Layout from '../../components/layout';
import Markdown from '../../components/Common/markdown';

const GatsbyInit = () => (
  <Layout
    title="Gatsby Init"
    author={{ gravatar: '//0.gravatar.com/avatar/b5eff8d62adf3ff34e67f153781d27c4' }}
    gitRepo="https://gitlab.com/nerds-with-charisma/jamstack-jedi/tree/master/gatsby-init"
    time={15}
    description="Initial setup for a GatsbyJs website. How to install GatsbyJs and make your first website."
    keywords="GatsbyJS, My First GatsbyJs Site, Setup GatsbyJs, What is GatsbyJs"
  >
    <strong>Gatsby Init</strong>
    <br />
    <br />

    <p>
      If you haven't already, create your initial Gatsby project by following the steps in the last chapter.
    </p>

    <p className="tldr">
      Gatsby's default starter comes wired up with some useful stuff, but we don't need a lot of it to start with. So, lets do some cleanup before we get started.
    </p>

    <p>
      Almost everything we'll be modifying resides in the <b>src</b> directory. From that directory delete the following files: <b>/src/components/layout.css, /src/components/header.js, /src/pages/page-2.js</b>, and <b>/src/components/image.js</b>. You can also delete the images called: <b>gatsby-astronaut.png</b> and <b>gatsby-icon.png</b>.
    </p>

    <p>
      This will break our project, but don't worry, we'll fix it now to get it rendering again. Open up <b>/src/components/layout.js</b> and remove the following lines:
    </p>

    <Markdown
      rel="layout.js"
      language="javascript"
      md={
        `  ...
    import Header from "./header"
    import "./layout.css"
  ...`}
    />

    <p>
      And lets modify our layout render to be the following:
    </p>

    <Markdown
      rel="layout.js"
      language="javascript"
      md={
        `  ...
    return (
      <main>{children}</main>
    )
  ...`}
    />

    <p>
      We also need to comment out one line in our <b>gatsby-config.js</b> file since we deleted some icons. Find the following line and comment it out:
    </p>

    <Markdown
      rel="gatsby-config.js"
      language="javascript"
      md={
        `  // icon: \`src/images/gatsby-icon.png\`, // This path is relative to the root of the site.`
      }
    />

    <p>
      This should be enough to get us up and running again, but we can clean up some more. Open up <b>/src/pages/index.js</b> and modify to the following:
    </p>

    <Markdown
      rel="layout.js"
      language="javascript"
      md={
        `  import React from "react";

  import Layout from "../components/layout";

  const IndexPage = () => (
    <Layout>
      <h1>Hello World</h1>
    </Layout>
  );

  export default IndexPage;`}
    />

    <p className="tldr">
      This will get us starting with a clean slate and we can start developing our portfolio from a clean base. Here's some info about the directories and files that are still left:
    </p>

    <ul className="bulleted tldr">
      <li>
        <b>node_modules</b> - this is where all our dependencies will be stored, basically our third party plugins.
      </li>

      <li>
        <b>public directory</b> - where our output files will be built and minified to. You will not edit anything in here.
      </li>

      <li>
        <b>src directory</b> - our working files, we'll spend most of our time in here.
      </li>

      <li>
        <b>.gitignore</b> - tells git which files to not check in, for example we don't want to check-in our node_modules folder.
      </li>

      <li>
        <b>gatsby-[browser, config, node, and ssr]</b> - these are Gatsby config files that we'll cover a bit later.
      </li>

      <li>
        <b>package.json</b> - a list of our dependencies as well as some basic info about our app.
      </li>

      <li>
        <b>Readme.md</b> - a readme file tells users a bit about the project if they were to visit the git repo.
      </li>
    </ul>

    <hr />

    <b id="anchor--initial-metadata">Initial Metadata</b>

    <p>
      Lets start off by adding some meta data, if you remember our coming soon page, our meta data in the head of our page looked like this:
    </p>

    <Markdown
      rel="index.html"
      language="html"
      md={
        `  <head>
    <title>Nerds With Charisma - Coming Soon</title>
    <link rel="shortcut icon" href="favicon.ico" />

    <link rel="stylesheet" type="text/css" href="styles.css" />

    <meta charset="utf-8" />
    <meta name="description" content="Nerds With Charisma online portfolio coming soon!" />
    <meta name="keywords" content="Brian Dausman, Nerds With Charisma" />

    <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js" integrity="sha384-SlE991lGASHoBfWbelyBPLsUlwY1GwNDJo3jSJO04KZ33K2bwfV9YBauFfnzvynJ" crossorigin="anonymous"></script>
  </head>`}
    />

    <p>
      We'll do something similar using the <b>seo.js</b> component that came with the Gatsby starter. Crack open that file and lets talk about what's happening a little bit.
    </p>

    <p>
      First lets take a look at the current seo.js file, there's a lot of good stuff here...but we're not ready for it. So, lets modify this file to be like so:
    </p>

    <Markdown
      rel="seo.js"
      language="javascript"
      md={
        `  import React from "react"
        import Helmet from "react-helmet"

        function SEO({ title, lang, meta }) {
          return (
            <Helmet
              htmlAttributes={{ lang }}
              title={title}
              meta={meta}
            />
          )
        }

        export default SEO`}
    />

    <p>
      Lets start from the top, we import React, like we do in basically every file.
    </p>

    <p>
      Then we import <a id="anchor--react-helmet" href="https://github.com/nfl/react-helmet" target="_blank">React Helmet</a>, which is the standard way of setting meta data in a React app.
    </p>

    <p>
      Next we create our <b>SEO</b> component (and export it at the bottom). Inside we'll be retuning a Helmet component that gets our title, lang, and meta props. Which we'll pass in from our homepage.
    </p>

    <p>
      Lets do that now, save <b>seo.js</b> and head over to <b>index.js</b>:
    </p>

    <Markdown
      rel="seo.js"
      language="javascript"
      md={
        `  ...
  import SEO from '../components/seo';
  ...
  <Layout>
    <SEO
      title="Nerds With Charisma"
      lang="en"
      meta={[
        {
          name: 'description',
          content: 'Nerds With Charisma online portfolio coming soon!',
        },
        {
          name: 'keywords',
          content: 'Brian Dausman, Nerds With Charisma',
        },
      ]}
    />
    <h1>Hello World</h1>
  ...`}
    />

    <p className="tldr">
      What we did here is import our SEO component so we can use it.
    </p>

    <p className="tldr">
      Then, inside our layout we use the SEO component and setup the 3 props, title &amp; lang are simple strings, meta is an array of object. In this case we're making our description and keywords meta tags. Helmet lets us pass an array of objects to easily create meta tags, which is nice.
    </p>

    <p>
      Save all our files, head over to our browser and check out the page (which should have refreshed itself). Turn on dev tools and inspect the page, find the <b>head tag</b> and expand it and you'll find our new meta tags towards the bottom.
    </p>

    <img src="/images/jamstack-jedi/gatsby--meta.png" class="jamstack--img" alt="Gatsby Meta Data" />

    <p>
      Alright, our Gatsby meta's initial setup is coming along nicely, next lets setup our styles using Sass.
    </p>

    <AniLink to="jamstack-jedi/sass-101" className="nextBtn">Sass 101 &raquo;</AniLink>
    <br />
  </Layout>
);

export default GatsbyInit;
