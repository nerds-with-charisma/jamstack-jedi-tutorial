import React from 'react';
import AniLink from '../../components/Common/AniLink';

import Layout from '../../components/layout';
import Markdown from '../../components/Common/markdown';

const Schema = () => (
  <Layout
    title="Schema Markup"
    author={{ gravatar: '//0.gravatar.com/avatar/b5eff8d62adf3ff34e67f153781d27c4' }}
    gitRepo="https://gitlab.com/nerds-with-charisma/jamstack-jedi/tree/master/schema-markup"
    time={15}
    description="Learn Schema markup, how to setup Schema markup. Schema markup basics."
    keywords="Learn Schema markup, Schema markup Basics, Get better at Schema markup, Schema markup 101, Intro to Schema markup"
  >

  <strong>
    Look At the Size of that Schema
  </strong>

  <br />
  <br />

  <p>
    Schema markup are sets of tags that will improve the way search engines read your site and how they display it on the SERP page. There's 2 ways of doing schema markup, you can do inline (boo) or you can do a specific micro-data tag with key/value pairs to represent your content (yay).
  </p>

  <p>
    Schema markup can be a tad confusing in that there's a specific standard, but there's a lot of interpretation of that standard. We're writing a portfolio, so we probably want to represent a specific person, but what if it's a design company? Do we want our location data in here? There's dozens of possible combinations of tags we can include. So how do we know what to do?
  </p>

  <p>
    I would say a combination of trial and error, add markup now, let it run, then modify or add to it and see if you improve in search results. Also, spy on your competition...what are they doing? You might be surprirsed but most people won't have schema markup. Which means that by adding it, you'll have that much better a chance to rank higher than them.
  </p>

  <p>
    When adding my schema markup, I searched for "Web development companies near me". I then looked at some of the top results, I opened up dev-tools, and searched the elements panel for "application/ld+json" (which is how Schema markup will be attributed if they're using the micro-data scripts). I then compared all the results and found a combination that worked for me.
  </p>

  <p>
    Is that combination perfect? No. Is it performing well? Sure. So the moral of the story is, always be adapting and changing your content to find what performs better.
  </p>

  <hr />

  <p>
    So what should we add? Well, in our case we want a <b>Person</b>. We also have a <b>Corporation</b> or business we want to represent. This is also a <b>Website</b>, I'm also comfortable entering a <b>Location</b>, and we can add a <b>Breadcrumb</b> as well. Even though we're a one page site technically, it can't hurt I think. The moral of this story is that we'll show you how to add schema markup, and you can fill in the blanks on your own to suit your needs.
  </p>

  <p>
    You can read about (I find their "docs" absolutely awful) each here: <a href="https://schema.org/Person" target="_blank">Person</a>, <a href="https://schema.org/Corporation" target="_blank">Corporation</a>, <a href="https://schema.org/WebSite" target="_blank">Website</a>, <a href="https://schema.org/location" target="_blank">Location</a>, <a href="https://schema.org/BreadcrumbList" target="_blank">Breadcrumb</a>.
  </p>

  <p>
    We could also probably use <a href="https://schema.org/CreativeWork" target="_blank">CreativeWork</a> for our portfolio items as well but we won't cover that in this tutorial.
  </p>

  <p>
    We could use <b>gatsby-config.js</b> to populate all our tags, then print them out in <b>SEO.js</b> wrapped in null checks, incase we ever want to remove them. But our <b>gatsby-config.js</b> file is getting huge. So I'm going to break this out into a separate file. Feel free to do the same or place it directly below our other nodes.
  </p>

  <hr />

  <p>
    Lets start by making a directory of our configs that we'll be breaking out and the file to hold our schema data:
  </p>

  <Markdown
      rel="Terminal"
      language="terminal"
      md={`  mkdir src/config

  > src/config/schema-markup.js`}
    />

    <p>
      Open up that file and lets export some data:
    </p>

    <Markdown
      rel="schema-markup.js"
      language="javascript"
      md={`  exports.data = [
    {
      "@context": "http://schema.org",
      "@type": "Person",
      "address": {
        "@type": "PostalAddress",
        "addressLocality": "Bartlett",
        "addressRegion": "IL",
        "postalCode": "60103",
        "streetAddress": "436 Smoketree Ln."
      },
      "colleague": ["http://andrewbieganski.com/"],
      "email": "briandausman@gmail.com",
      "image": "/portfolio/brian.jpg",
      "jobTitle": "Developer",
      "name": "Brian Dausman",
      "alumniOf": "NortherN Illinois University",
      "gender": "male",
      "url": "https://nerdswithcharisma.com",
    },
  ];`}
    />

    <p>
      Obviously, fill out these fields for your information, then lets head over to <b>gatsby-config.js</b> and import this data:
    </p>

    <Markdown
      rel="gatsby-config.js"
      language="javascript"
      md={`  const Schema = require('./src/config/schema-markup');

  module.exports = {
    siteMetadata: {
      schema: Schema,
  ...`}
    />

    <p>
      We're creating a "schema" node in our siteMetadata which we'll need to import in <b>index.js</b>:
    </p>

    <Markdown
      rel="index.js"
      language="javascript"
      md={`  ...
  site {
    siteMetadata {
      schema {
        data {
          _context
          _type
          address {
            _type
            addressLocality
            addressRegion
            postalCode
            streetAddress
          }
          colleague
          email
          image
          jobTitle
          name
          alumniOf
          gender
          url
        }
      }
  ...

  <SEO
    title={data.site.siteMetadata.homepageData.title}
    lang={data.site.siteMetadata.homepageData.lang}
    meta={data.site.siteMetadata.homepageData.meta}
    og={data.site.siteMetadata.homepageData.og}
    schema={data.site.siteMetadata.schema}
  />
  ...`}
    />

    <p>
      You'll notice we replaced the "@" with "_", this is because graphQl can't handle variables with special characters. By adding an underscore, when we print out our keys in React, we have something we can hook onto and replace.
    </p>

    <p>
      Now lets print them out in <b>seo.js</b>:
    </p>

    <Markdown
      rel="seo.js"
      language="javascript"
      md={`  ...
  function SEO({ title, lang, meta, og, schema }) {
    ...
    { schema.data.map(item => (
      <script type="application/ld+json">
        {JSON.stringify(item).replace(/_/g, '@')}
      </script>
    ))}
  ...`}
    />

    <p>
      You can put this anywhere in the component, we chose after the meta map we did earlier. We're mapping through all items in the schema.data object, putting them inside a script tag that has a type of "application/ld+json" which signifies this will be schema markup and then simply spitting out or object as a string. So whatever we put in gatsby-config.js will print directly in the src. At the end, we have a replace that uses a regular expression to find all underscores and replace them with the proper @ symbol.
    </p>

    <hr />

    <p>
       With schema markup, there's a lot of fields that are the same for different tags. Like context, address, etc...instead of typing all these out every time, and making changes to every one every time we need to modify it, we can create variables at the top of the file and then use the spread operator to print them where we need them in each tag.
    </p>

    <p>
      At the top of <b>schema-markup.js</b> add some variables with our common fields that we'll use later and we can re-write our Person object like so:
    </p>

    <Markdown
    rel="schema-markup.js"
    language="javascript"
    md={`  const context = "http://schema.org";
  const email = "briandausman@gmail.com";
  const name = "Brian Dausman";
  const url = "https://nerdswithcharisma.com";

  const address = {
    "@type": "PostalAddress",
    "addressLocality": "Bartlett",
    "addressRegion": "IL",
    "postalCode": "60103",
    "streetAddress": "436 Smoketree Ln."
  };

  exports.data = [
    {
      "@type": "Person",
      "colleague": ["http://andrewbieganski.com/"],
      "image": "https://nerdswithcharisma.com/images/portfolio/brian.jpg",
      "jobTitle": "Developer",
      "alumniOf": "Northern Illinois University",
      "gender": "male",
      "@context": context,
      "email": email,
      "name": name,
      "url": homepageUrl,
      "address": {
        ...address
      },
    },
  ];`}
  />

  <p>
    Now, when we add our other schema fields we can reuse these variables, we'll create more as well for other items that will be reused across multiple tags. Below is the entire code to populate everything rahter than going through it one by one we'll dump it all in once and you can go through it on your own:
  </p>

  <Markdown
    rel="seo.js"
    language="javascript"
    md={`  const context = "http://schema.org";
  const email = "briandausman@gmail.com";
  const name = "Brian Dausman";
  const url = "https://nerdswithcharisma.com";
  const telephone = "815 403 4803";
  const description = "Nerds With Charisma is a cutting edge digital media agency specializing in awesome websites.";

  const address = {
    "@type": "PostalAddress",
    "addressLocality": "Bartlett",
    "addressRegion": "IL",
    "postalCode": "60103",
    "streetAddress": "436 Smoketree Ln."
  };

  const sameAs = [
    "https://twitter.com/nerdswcharisma",
    "https://www.facebook.com/Nerds-With-Charisma-728481650525727/",
    "https://gitlab.com/nerds-with-charisma",
    "https://www.instagram.com/ernie_hudsons_paycheck/"
  ];

  const image = {
    "@type": "ImageObject",
    "url": "https://nerdswithcharisma.com/images/nwc-tile.png",
    "height": "1200",
    "width": "1200"
  };

  const logo = {
    "@type": "ImageObject",
    "url": "https://nerdswithcharisma.com/images/logo--nwc-purple.svg",
    "height": "1200",
    "width": "1200"
  };

  exports.data = [
    {
      "@type": "Person",
      "colleague": ["http://andrewbieganski.com/"],
      "image": "https://nerdswithcharisma.com/images/portfolio/brian.jpg",
      "jobTitle": "Developer",
      "alumniOf": "Northern Illinois University",
      "gender": "male",
      "@context": context,
      "email": email,
      "name": name,
      "url": url,
      "address": {
        ...address
      },
    },
    {
      "@context": context,
      "@type": "Corporation",
      "name": name,
      "url": url,
      "sameAs": sameAs,
      "image": image,
      "telephone": telephone,
      "email": email,
      "address": {
        ...address,
      },
      "logo": logo,
      "location": {
        "@type": "Place",
        "name": name,
        "telephone": telephone,
        "image": image,
        "logo": logo,
        "url": "https://nerdswithcharisma.com/",
        "sameAs": sameAs,
        "address": {
          ...address
        }
      }
    },
    {
      "@context": context,
      "@type": "WebSite",
      "name": name,
      "description": description,
      "url": url,
      "image": "https://nerdswithcharisma.com/images/logo--nwc-purple.svg",
      "sameAs": sameAs,
      "copyrightHolder": {
        "@type": "Corporation",
        "name": name,
        "url": url,
        "sameAs": sameAs,
        "image": image,
        "telephone": telephone,
        "email": email,
        "address": {
          ...address
        },
        "logo": logo,
        "location": {
          "@type": "Place",
          "name": name,
          "telephone": telephone,
          "image": image,
          "logo": logo,
          "url": url,
          "sameAs": sameAs,
          "address": {
            ...address
          }
        }
      },
      "author": {
        "@type": "Corporation",
        "name": name,
        "url": url,
        "sameAs": sameAs,
        "image": image,
        "telephone": telephone,
        "email": email,
        "address": {
          ...address
        },
        "logo": logo,
        "location": {
          "@type": "Place",
          "name": name,
          "telephone": telephone,
          "image": image,
          "logo": logo,
          "url": url,
          "sameAs": sameAs,
          "address": {
            ...address
          }
        }
      },
      "creator": {
        "@type": "Organization"
      }
    },
    {
      "@context": context,
      "@type": "Place",
      "name": name,
      "telephone": telephone,
      "image": image,
      "logo": logo,
      "url": url,
      "sameAs": sameAs,
      "address": {
        ...address,
      }
    },
    {
      "@context": context,
      "@type": "BreadcrumbList",
      "itemListElement": [
        {
          "@type": "ListItem",
          "position": "1",
          "item": {
            "@id": url,
            "name": "Home"
          }
        }
      ]
    }
  ];`}
  />

  <p>
    Our next step would be to import everything into <b>index.js</b> but there's a problem you might have noticed. In order to import something into GraphQl it needs to be explicitly defined and common. But all our schema nodes are slightly different, we would be getting lots of "nulls" and our query would get REALLY long REALLY quick. So how do we handle something this large?
  </p>

  <p>
    Well, we can import it directly into our SEO component, and remove the previous wiring we did to get it via GraphQl. Which is a totaly valid way of doing it but one word of caution is that we previously included all of our dynamic content into our <b>gatsby-config.js</b> file. This will break from that tradition, so long as we remember our config directory has content as well (perhaps add that to our readme or build docs) we should be fine.
  </p>

  <p>
    So for starters, lets open up <b>gatsby-config.js</b> and remove our import and remove the node from our object:
  </p>

  <Markdown
      rel="REMOVED FROM: gatsby-config.js"
      language="javascript"
      md={`  const Schema = require('./src/config/schema-markup');
  ...
  schema: Schema,
  ...`}
    />

    <p>
      Then we can remove our query from <b>index.js</b>:
    </p>

    <Markdown
      rel="REMOVED FROM: index.js"
      language="javascript"
      md={`  ...
  schema {
    data {
      _context
      _type
      address {
        _type
        addressLocality
        addressRegion
        postalCode
        streetAddress
      }
      colleague
      email
      jobTitle
      name
      alumniOf
      gender
      url
    }
  }
  ...
  schema={data.site.siteMetadata.schema}
  ...`}
    />

    <p>
      And now, we can import our <b>schema-meta.js</b> file into <b>seo.js</b> and use it almost identically. Don't forget to remove the prop declaration passed to the SEO component as well:
    </p>

    <Markdown
      rel="seo.js"
      language="javascript"
      md={`  ...
  import schema from '../config/schema-markup';

  function SEO({ title, lang, meta, og }) {
  ...`}
    />

    <p>
      Everything else can stay the same, and if we restart our server we should see all our schema markup printed into the src:
    </p>

    <img src="/images/jamstack-jedi/schema--printed.png" class="jamstack--img" alt="All shcema markup in page" />

    <p>
      To test if everything is correct, we can deploy it to prod and use Google's markup validator to verify all is well:
    </p>

    <Markdown
      rel="Terminal"
      language="terminal"
      md={`  git pull

  git add .

  git commit -m "Add schema markup"

  git pull

  git push`}
    />

    <p>
      Once the deploy is done and you've confirmed all content is in our SRC, you can head over to <a id="anchor--schema-validator" href="https://search.google.com/structured-data/testing-tool/" target="_blank">the Structured Data Testing Tool</a> and enter your url by clicking <b>New Test</b> and entering it into the modal and hitting <b>Run Test</b>.
    </p>

    <p>
      This will tell us if there are any errors in our markup as well as which schema tags were found on the page.
    </p>

    <img src="/images/jamstack-jedi/schema--validated.png" class="jamstack--img" alt="Validated schema" />

    <p>
      If it found any errors, go ahead and fix those and retest them. Also, feel free to drill into each to make sure all the data look right.
    </p>

    <p>
      That was a lot of copy and pasting, but it'll be worth it in the long run. That about covers it for the Schema chapter and our SEO part 2 chapter in general. In the next chapter, we'll cover some <b>Extras</b> to get your portfolio polished up before we wrap up this whole tutorial.
    </p>

  <AniLink to="jamstack-jedi/extras" className="nextBtn">Extra's Overview &raquo;</AniLink>
  <br />
  </Layout>
);

export default Schema;
