import React from 'react';

import AniLink from '../../components/Common/AniLink';
import Layout from '../../components/layout';
import Markdown from '../../components/Common/markdown';
import YouTube from '../../components/Common/YouTube';

const ComingSoonMobile = () => (
  <Layout
    title="Coming Soon Mobile"
     author={{ gravatar: '//0.gravatar.com/avatar/b5eff8d62adf3ff34e67f153781d27c4' }}
      gitRepo="https://gitlab.com/nerds-with-charisma/jamstack-jedi/tree/master/coming-soon-mobile-final"
      time={10}
      description="Use media queries to make a simple page look good on mobile"
    keywords="Media queries, responsive website, learn responsive, learn media queries, css media queries"
  >
    <strong>Can I Use Your Phone?</strong>
    <br />
    <br />

    <p className="tldr">
      In just a few short years, mobile traffic has exploded. Now, mobile visitors will most likely make up more than 50% of the traffic to your site. Fortunately, CSS3 introduced media queries that allow us to target specific devices sizes and apply styles to only those specific device sizes.
    </p>

    <p className="tldr">
      Media queries are basically "if statements" that allow you to pass in a condition, if the condition is met, the query will take over, if not, it will skip it.
    </p>

    <p>
      Here's a nice, <a id="anchor--media-query-intro" href="https://www.youtube.com/watch?v=2KL-z9A56SQ" target="_blank">quick video</a> from Kevin PowellYout to give you an introduction to media queries (aka MQ's).
    </p>

    <YouTube url="https://www.youtube.com/embed/2KL-z9A56SQ" />

    <br />

    <p>
      Lets get started making our page look good one smaller devices. Open up <b>styles.css</b> and lets add some media queries down at the bottom. First we need to enable the devtools simulator to mimic a mobile experience.
    </p>

    <p className="tldr">
      To do this, open up devtools if you don't already have it open (f12 or right click > inspect element). You'll see two icons at the top left of devtools, one is a square with an arrow, the other looks like a cellphone and a tablet. Click the cellphone and tablet icon.
    </p>

    <p className="tldr">
      To dock the panel on the right hand side, click the three dots stacked vertically on the right side and select the right side fill.
    </p>

    <img src="/images/jamstack-jedi/coming-soon-mobile-devtools.png" class="jamstack--img" alt="Coming Soon Devtools setup" />

    <p className="tldr">
      Our site doesn't look awful, but it's def not great on mobile. Set the emulator to iphone X at the top, and lets start fixing some issues.
    </p>

    <p>
      Open up <b>styles.css</b> again and lets add some placeholders to the bottom of our file:
    </p>

    <span className="tldr">
    <Markdown
      rel="styles.css"
      language="css"
      md={
        `  /* Mobile Styles */
  @media(min-width: 0px) and (max-width: 991px) {
    html {
      background: red;
    }
  }

  /* Tablet Styles */
  @media(min-width: 992px) and (max-width: 1199px) {
    html {
      background: green;
    }
  }

  /* Desktop Styles */
  @media(min-width: 1200px) {
    html {
      background: blue;
    }
  }`
      }
    />
    </span>

    <p className="tldr">
      If you refresh the page now, and change the emulator size, you should see the background color changing based on the size of the viewport. We're not going to keep these background colors, they're just to prove our media queries are working, so you can delete them now.
    </p>

    <img src="/images/jamstack-jedi/coming-soon-mobile-mq-placeholder.png" class="jamstack--img tldr" alt="Coming Soon Media Query Placeholder" />

    <hr />

    <p>
      Lets tackle our first media query, the 0-991 pixel range, or cellphone to small tablet. The first thing we want to do is stack our copy and contact tags so that they're bigger on mobile. Then we want to adjust the sizing of everything so that it's more legible on small devices.
    </p>

    <p>
      Going forward, I'm going to start commenting inline what each line does. If it's something we've already covered, I will skip over it. You do not need to add these comments when you're writing your code.
    </p>

    <Markdown
    rel="styles.css"
    language="css"
    md={
      `  /* Mobile Styles */
  @media(min-width: 0px) and (max-width: 991px) {
    /* make our wrappers 100% width */
    #copy-wrapper, #contact-wrapper {
      width: 100%;
    }

    /* adjust the size of our h2 tags */
    #copy-wrapper h2 {
      font-size: 150px;
      line-height: 140px;
    }

    /* adjust the size of our h3 tag and give it some breathing room */
    #copy-wrapper h3 {
      font-size: 50px;
      margin-top: 75px;
    }

    /* make the social icons a lot bigger and add some breathing room */
    #social-icons a {
      font-size: 100px;
      margin-bottom: 75px;
    }

    /* adjust the width of the button so that it spans the width of it's parent */
    #contact-button-wrapper a {
      font-size: 45px;
      min-width: 100%;
      max-width: 100%;
      padding: 35px 25px;
    }

    /* make the footer text bigger */
    footer {
      font-size: 34px;
    }
  }`
    }
  />

  <p className="tldr">
    You know the drill, save it up and refresh. Yeah...that looks a lot better. Also, make sure you test it on other small devices, if anything looks off, try to fix the issues.
  </p>

  <img src="/images/jamstack-jedi/coming-soon-mobile-xs.png" class="jamstack--img" alt="Coming Soon XS styles" />

  <hr />

  <p>
    Switch your simulator to ipad and change it to horizontal. Now we'll style the tablet layout.
  </p>

  <Markdown
    rel="styles.css"
    language="css"
    md={
      `  /* Tablet Styles */
  @media(min-width: 992px) and (max-width: 1199px) {
    #copy-wrapper h2 {
      font-size: 72px;
      line-height: 75px;
    }

    #copy-wrapper h3 {
      font-size: 28px;
      margin-top: 25px;
    }

    #social-icons a {
      font-size: 42px;
      padding: 20px;
      margin-bottom: 20px;
    }

    #contact-button-wrapper a {
      font-size: 32px;
      width: 100%;
      padding: 15px 0;
    }

    footer {
      font-size: 21px;
    }
  }`
    }
  />

  <p className="tldr">
    Nothing really different here, just changing up some of the sizing and margins. Nothing here should be a surprise at this point.
  </p>

  <img src="/images/jamstack-jedi/coming-soon-mobile-md.png" class="jamstack--img" alt="Coming Soon MD styles" />

  <p>
    For desktop, we don't really need to do anything fancy right now...so lets just leave an empty media query incase we want to add something later.
  </p>

  <Markdown
  rel="styles.css"
  language="css"
  md={
    `  /* Desktop Styles */
  @media(min-width: 1200px) {}`
  }
/>

  <p>
    And that's it! We've made our coming soon page, styled it, and made it look good on mobile. We're now ready to toss it onto a server. Before we do that though, I want to introduce you to Git. It's a little scary, but stick with it. Once you get the basics down, it's not very intimidating. Plus there's some great Apps that handle most anything Git will through at you.
  </p>

    <AniLink to="jamstack-jedi/git-101" className="nextBtn">Get Git &raquo;</AniLink>
    <br />
  </Layout>
);

export default ComingSoonMobile;
