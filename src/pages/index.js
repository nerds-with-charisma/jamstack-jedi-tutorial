import React from 'react';

import Layout from '../components/layout';
import Footer from '../components/Common/Footer';

const App = () => (
  <Layout>
    <div id="container" className="container" style={{ display: 'flex', flexDirection: 'column' }}>
        <h1 className="font--58 lh-sm">
          <strong>
            {'JAMSTACK'}
            <br />
            {'JEDI'}
          </strong>
        </h1>
        <p className="font--18">
          {'A roadmap & tutorial on how to become a front-end developer, that’ll help you learn the stack that the cool kids are currently working with!'}
        </p>
      </div>
      <Footer />
  </Layout>
);

export default App;
