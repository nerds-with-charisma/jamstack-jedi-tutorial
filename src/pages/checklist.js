import React, { useState, useEffect } from 'react';

import Layout from '../components/layout';
import Footer from '../components/Common/Footer';

const checklist = [
  {
    name: 'Design',
    desc: 'Get the design for your project finalized first, otherwise scope creep will come back to haunt you. Go one dribble to find a style, make a mockup, create a full design.',
    link: null,
    checked: false,
  },
  {
    name: 'Git Setup',
    desc: 'Signup/Login to Gitlab',
    link: 'jamstack-jedi/gitlab',
    checked: false,
  },
  {
    name: 'Buy Domain',
    desc: 'Purchase a Domain Name',
    link: 'jamstack-jedi/domain-name',
    checked: false,
  },
  {
    name: 'Setup Hosting',
    desc: 'Choose a Hosting Provider and Setup Your Environment',
    link: 'jamstack-jedi/hosting-netlify',
    checked: false,
  },
  {
    name: 'Connect Domain to Hosting',
    desc: 'Point your domain to your hosting provider',
    link: 'jamstack-jedi/hosting-netlify#anchor--domain-hosting-connection',
    checked: false,
  },
  {
    name: 'Enable HTTPS',
    desc: 'Enable a security certificate on your domain',
    link: 'jamstack-jedi/hosting-netlify#anchor--https',
    checked: false,
  },
  {
    name: 'First Deploy',
    desc: 'Deploy your initial commit or coming soon page to your hosting provider',
    link: null,
    checked: false,
  },
  {
    name: 'Signup For Google Analytics',
    desc: 'Signup For GA and get your UA ID for use later',
    link: 'jamstack-jedi/wrapping-up-101',
    checked: false,
  },
  {
    name: 'Setup a Gatsby Project',
    desc: 'Install Gatsby and create a new project',
    link: 'jamstack-jedi/spa-101#anchor--gatsby-install',
    checked: false,
  },
  {
    name: 'Add Base Meta Data',
    desc: 'Add your favicon, charset, lang, description, keywords, etc',
    link: 'jamstack-jedi/gatsby-init#anchor--initial-metadata',
    checked: false,
  },
  {
    name: 'Setup Sass',
    desc: 'Make sure Sass is working and setup initial scss file',
    link: 'jamstack-jedi/sass-101',
    checked: false,
  },
  {
    name: 'Optional: Use a Style Libraby',
    desc: 'Make or use a style library such as ours or Bootstrap',
    link: null,
    checked: false,
  },
  {
    name: 'Get Meta Data from gatsby-config',
    desc: 'Instead of statically pulling in content, get it from Gatsby Config',
    link: 'jamstack-jedi/graphql-101',
    checked: false,
  },
  {
    name: 'Build Your Components',
    desc: 'Build out all your components and finish the styling of the site',
    link: null,
    checked: false,
  },
  {
    name: 'Verify Everything On Mobile',
    desc: 'Make sure it all looks good on mobile',
    link: null,
    checked: false,
  },
  {
    name: 'Build Contact Form',
    desc: 'Make a contact form as a way for users to contact you',
    link: 'jamstack-jedi/build-contact',
    checked: false,
  },
  {
    name: 'Connect Form to Mailchimp',
    desc: 'Connect your form to MC to collect emails and get messages',
    link: 'jamstack-jedi/mailchimp',
    checked: false,
  },
  {
    name: 'Setup Google Tag Manager',
    desc: 'Signup and install GTM to be able to run external scripts',
    link: 'jamstack-jedi/seo-gtm',
    checked: false,
  },
  {
    name: 'Install Google Analytics via GTM',
    desc: 'Connect your GA account to GTM and setup triggers',
    link: 'jamstack-jedi/seo-ga',
    checked: false,
  },
  {
    name: '404 Page',
    desc: 'Create a custom 404 page',
    link: 'jamstack-jedi/seo-404',
    checked: false,
  },
  {
    name: 'robots.txt',
    desc: 'Create a robots.txt file to tell bots what to index',
    link: 'jamstack-jedi/seo-robots',
    checked: false,
  },
  {
    name: 'sitemap.xml',
    desc: 'Setup sitemap generation',
    link: 'jamstack-jedi/seo-sitemap',
    checked: false,
  },
  {
    name: 'Add Additional Meta Data',
    desc: 'Add address bar color, canonical url, open graph,  meta tags',
    link: 'jamstack-jedi/seo-meta',
    checked: false,
  },
  {
    name: 'WAVE Accessibility Audit',
    desc: 'Make sure all pages pass accessibility audits for people with disabilities',
    link: 'jamstack-jedi/opti-wave',
    checked: false,
  },
  {
    name: 'Optimize for Speed',
    desc: 'Run speed audits and fix any issues',
    link: 'jamstack-jedi/seo-speed',
    checked: false,
  },
  {
    name: 'Setup Google Search Console',
    desc: 'Verify property in GSC and submit your sitemap',
    link: 'jamstack-jedi/gsc',
    checked: false,
  },
  {
    name: 'Claim Brands on Social Media',
    desc: 'Signup for all social networks you need',
    link: 'jamstack-jedi/social',
    checked: false,
  },
  {
    name: 'Setup Google My Business',
    desc: 'Signup for GMB and fill out all fields applicable',
    link: 'jamstack-jedi/seo-meta',
    checked: false,
  },
  {
    name: 'Submit to Bing',
    desc: 'Submit your site to Bing Webmaster Tools',
    link: 'jamstack-jedi/search-submit#anchor--bing',
    checked: false,
  },
  {
    name: 'Add Schema Markup',
    desc: 'Setup Schema meta data and pull into SEO component',
    link: 'jamstack-jedi/schema-markup',
    checked: false,
  },
  {
    name: 'Create Slack Notifications',
    desc: 'Setup Slack notifications for build alerts',
    link: 'jamstack-jedi/slack',
    checked: false,
  },
  {
    name: 'Setup HotJar Heatmaps',
    desc: 'Signup for and install heatmaps for user tracking form Hotjar',
    link: 'jamstack-jedi/hotjar',
    checked: false,
  },
  {
    name: 'Signup for and Install Google Optimize',
    desc: 'Setup A/B testing from Google Optimize',
    link: 'jamstack-jedi/ab',
    checked: false,
  },
  {
    name: 'Perform Keyword Research',
    desc: 'Analyze your and your competitor keywords and decide which will work best for you',
    link: 'jamstack-jedi/keywords',
    checked: false,
  },
  {
    name: 'Cleanup and Lint Code',
    desc: 'Setup esLint and AirB&B standard, fix any issues',
    link: 'jamstack-jedi/linting',
    checked: false,
  },
  {
    name: 'Get Content from Contentful',
    desc: 'Pull in data from a Headless CMS',
    link: 'jamstack-jedi/contentful',
    checked: false,
  },
  {
    name: 'Auto-Build when Updating Contentful',
    desc: 'Setup a webhook to auto build/deploy when changes are published in Contentful',
    link: 'jamstack-jedi/contentful#anchor--contentful-webhooks',
    checked: false,
  },
];

const Checklist = () => {
  let [checklistItems, setChecklistItems] = useState([]);

  const setLocalStorageChecklist = () => {
    localStorage.setItem('checklist', JSON.stringify(checklistItems));
  }

  useEffect(() => {
    const lsChecklist = localStorage.getItem('checklist');

    if (lsChecklist) {
      setChecklistItems(JSON.parse(lsChecklist));
    } else {
      setChecklistItems(checklist);
      setLocalStorageChecklist();
    }
  }, []);

  const setChecklist = (index) => {
    let newList = checklistItems;
    newList[index].checked = !newList[index].checked;
    setChecklistItems([...newList]);
    setLocalStorageChecklist();
  }

  const resetChecklist = () => {
    const confirmed = confirm('You sure?');

    console.log(confirmed);
    if (confirmed === true) {
      setChecklistItems(checklist);
      setLocalStorageChecklist();
    }
  }

  return (
  <Layout>
    <div id="container" className="container" style={{ display: 'flex', flexDirection: 'column' }}>
        <strong className="font--28 lh-sm">
          <strong>
            {'Checklist'}
          </strong>
        </strong>
        <p className="font--18">
          {'Use this checklist as a quick way to track your progress on a future project. Use the links for each to quickly jump to details on any item.'}
        </p>

        <div className="position--relative">
          <br />
          <ul>
            { checklistItems.map((item, i) => (
              <li key={item.link}>
                <button
                  type="button"
                  className="btn--checkbox float-left bordered--none font--24"
                  style={{ margin: '6px 10px' }}
                  onClick={() => setChecklist(i)}
                >
                  { (item.checked === true) ? (
                    <i className="far fa-check-circle" />
                  ) : (
                    <i className="far fa-circle" />
                  )}
                </button>

                <div className="lh-sm">
                  <b className="font--14">
                    { item.link ? (
                      <a
                        href={item.link}
                        className="font--light"
                      >
                        {item.name}
                      </a>
                    ) : (
                      <span>
                        {item.name}
                      </span>
                    )}
                  </b>
                  <br />
                  <small className="opacity6">
                    {item.desc}
                  </small>
                </div>
                <br />
              </li>
            ))}
          </ul>
        </div>
        <br />
        <br />
        <button
          type="button"
          className="font--18 radius--lg"
          onClick={() => resetChecklist()}
        >
         <strong>
           {'Reset Checklist'}
         </strong>
        </button>
        <br />
        <br />
      </div>
  </Layout>
)};

export default Checklist;
