import React from 'react';

import AniLink from '../components/Common/AniLink';
import Layout from '../components/layout';


const Appendix = () => {
  return (
    <Layout>
      <section id="appendix">
        <h1 className="font--30 container font--light">
          {'Appendix'}
        </h1>
        <section id="appendix--wrapper" className="bg--light">
          <div className="container">
            <br />
            <h3>#</h3>
            <AniLink to="seo-404#anchor--what-is-404">404 Page Explained</AniLink>

            <h3>A</h3>
            <AniLink to="/jamstack-jeddi/optimize-overview#anchor--accessibility">Accessibility</AniLink>
            <AniLink to="overview">https://www.academia.edu/</AniLink>
            <AniLink to="portfolio-design#anchor--xd-download">Adobe XD</AniLink>
            <AniLink to="linting#anchor--airbnb-standard">AirBnB Linting Standard</AniLink>
            <AniLink to="menu-revisited">Animated Hamburger Icon</AniLink>

            <h3>B</h3>
            <AniLink to="searc-submit#anchor--bing-webmaster">Bing Webmaster Tools</AniLink>
            <AniLink to="html-101#anchor--css-missing-manual">Book: CSS: The missing Manual</AniLink>
            <AniLink to="html-101#anchor--jon-duckett">Book: HTML & CSS - Design and Build Websites</AniLink>
            <AniLink to="html-101#anchor--jeremy-keith">Book: HTML5 for Web Designers</AniLink>
            <AniLink to="javascript-101#anchor--javascript-and-jquery">Book: JavaScript and JQuery: Interactive Front-End Web Development</AniLink>
            <AniLink to="git-101#anchor--pro-git">Book: Pro Git</AniLink>
            <AniLink to="sass-101#anchor--sass-book">Book: Sass For Web Designers - A Book Apart</AniLink>
            <AniLink to="browser#anchor--browser">Browser</AniLink>
            <AniLink to="coffee">Buy Me a Coffee</AniLink>

            <h3>C</h3>
            <AniLink to="seo-meta#anchor--canonical">Canonical Tags</AniLink>
            <AniLink to="coming-soon-html#anchor--character-encoding">Character Encoding</AniLink>
            <AniLink to="browser#anchor--browser">Chrome</AniLink>
            <AniLink to="opti-speed#anchor--AniLink-cross-origin">Cross-Origin AniLinks</AniLink>
            <AniLink to="css-101">CSS 101</AniLink>
            <AniLink to="style-library#anchor--css-normalize">CSS Normalize</AniLink>
            <AniLink to="coming-soon-css#anchor--css-transitions">CSS Transitions</AniLink>
            <AniLink to="coming-soon-css">Coming Soon CSS</AniLink>
            <AniLink to="coming-soon-mobile">Coming Soon Mobile</AniLink>
            <AniLink to="css-101#anchor--layout-exercises">Common CSS Layouts</AniLink>
            <AniLink to="hosting-ftp#anchor--cyberduck">CyberDuck FTP Client</AniLink>

            <h3>D</h3>
            <AniLink to="browser#anchor--dev-tools">Dev Tools</AniLink>
            <AniLink to="domain-name">Domain Names</AniLink>

            <h3>E</h3>
            <AniLink to="coming-soon-html#anchor--mailto">Email from href</AniLink>
            <AniLink to="ide">Editor</AniLink>

            <h3>F</h3>
            <AniLink to="social#anchor--facebook-page">Facebook Business Page Creation</AniLink>
            <AniLink to="coming-soon-html#anchor--favicon">Favicon</AniLink>
            <AniLink to="ide">First File</AniLink>
            <AniLink to="coming-soon-html#anchor--font-awesome">Font Awesome</AniLink>
            <AniLink to="coming-soon-html#anchor--font-serif">Fonts Serif vs Sans</AniLink>
            <AniLink to="gitlab#anchor--fork">Fork Git Client</AniLink>
            <AniLink to="hosting-ftp">FTP</AniLink>
            <AniLink to="coming-soon-css#anchor--full-cover-css">Full Cover Images in CSS</AniLink>

            <h3>G</h3>
            <AniLink to="seo-404#anchor--gatsby-404">Gatsby: 404 Page</AniLink>
            <AniLink to="spa-101#anchor--gatsby-canonical">Gatsby: Canonical Tag Plugin</AniLink>
            <AniLink to="linting#anchor--eslint-plugin">Gatsby: esLint Plugin</AniLink>
            <AniLink to="spa-101#anchor--gatsby-get-started">Gatsby: Getting Started</AniLink>
            <AniLink to="spa-101#anchor--gatsby-install">Gatsby: Install</AniLink>
            <AniLink to="build-portfolio-2#anchor--gatsby-node">Gatsby: gatsby-node.js (creating pages from data)</AniLink>
            <AniLink to="mailchimp#anchor--gatsby-mailchimp">Gatsby: Mailchimp Plugin</AniLink>
            <AniLink to="seo-meta#anchor--og-plugin">Gatsby: Open Graph Plugin</AniLink>
            <AniLink to="seo-gtm#anchor--gatsby-plugin-gtm">Gatsby: Plugin Google Tag Manager</AniLink>
            <AniLink to="seo-robots#anchor--gatsby-robots">Gatsby: robots.txt Generation</AniLink>
            <AniLink to="sass-101#anchor--gatsby-sass">Gatsby: Sass Setup</AniLink>
            <AniLink to="seo-sitemap#anchor--gatsby-sitemap">Gatsby: Sitemap Generation</AniLink>
            <AniLink to="build-services#anchor--gatsby-static-folder">Gatsby: Static Folder</AniLink>
            <AniLink to="js-fun#anchor--geolocation">Geolocation</AniLink>
            <AniLink to="seo-ga">Google Analytics</AniLink>
            <AniLink to="seo-gtm">Google Tag Manager</AniLink>
            <AniLink to="git-101">Git 101</AniLink>
            <AniLink to="gitlab#anchor--git-flow">Basic Git Flow</AniLink>
            <AniLink to="git-101#anchor--git-windows">Git Install - Windows</AniLink>
            <AniLink to="gitlab">GitLab Setup</AniLink>
            <AniLink to="git-101#anchor--gitlab-training">GitLab Training</AniLink>
            <AniLink to="hosting-ftp#anchor--hosting-godaddy-AniLink">goDaddy YouTube Channel</AniLink>
            <AniLink to="wrapping-up-101#anchor--google-analytics">Google Analytics</AniLink>
            <AniLink to="gmb#anchor--gmb">Google My Business</AniLink>
            <AniLink to="gmb#anchor--gmb-hubspot">Google My Business Info from HubSpot</AniLink>
            <AniLink to="gsc">Google Search Console</AniLink>
            <AniLink to="graphql-101#anchor--udemy-graphql-tutorial">GraphQl: Andrew Mead Udemy Course</AniLink>
            <AniLink to="graphql-101#anchor--graphql-gatsby-docs">GraphQl Gatsby Docs</AniLink>
            <AniLink to="graphql-101#anchor--how-to-graphql-tutorial">GraphQl How to GraphQl Tutorial</AniLink>
            <AniLink to="graphql-101#anchor--graphql-intro-docs">GraphQl Intro Docs</AniLink>
            <AniLink to="graphql-101#anchor--graphql-playground">GraphQl Playground</AniLink>
            <AniLink to="seo-ga#anchor--gtm-triggers">GTM Triggers</AniLink>

            <h3>H</h3>
            <AniLink to="hosting-ftp#anchor--hosting-hostgator-AniLink">HostGator YouTube Channel</AniLink>
            <AniLink to="hotjar">Hotjar: Heatmaps</AniLink>
            <AniLink to="html-101">HTML 101</AniLink>
            <AniLink to="coming-soon-html#anchor--html-entities">HTML Entities</AniLink>
            <AniLink to="hosting-ftp#anchor--htaccess-https">HTTPS Redirect (htaccess)</AniLink>

            <h3>I</h3>
            <AniLink to="ide">IDE</AniLink>
            <AniLink to="intro">Intro</AniLink>

            <h3>J</h3>
            <AniLink to="intro#anchor--jamstack">Jamstack Explained</AniLink>

            <h3>K</h3>
            <AniLink to="js-fun#anchor--keyboard-events">Keyboard Events</AniLink>
            <AniLink to="keywords">Keywords SEO</AniLink>
            <AniLink to="keywords#anchor--keyword-brainstorm">Keyword Brainstorming</AniLink>
            <AniLink to="keywords#anchor--small-seo-tools">Keyword Ranking</AniLink>
            <AniLink to="keywords#anchor--keyword-planner">Keyword Planner</AniLink>

            <h3>L</h3>
            <AniLink to="opti-speed#anchor--lighthouse-docs">Lighthouse Optimization</AniLink>
            <AniLink to="linting">Linting</AniLink>

            <h3>M</h3>
            <AniLink to="mailchimp">Mailchimp</AniLink>
            <AniLink to="coming-soon-html#anchor--mailto">Mailto</AniLink>
            <AniLink to="coming-soon-mobile#anchor--media-query-intro">Media Query Intro</AniLink>
            <AniLink to="js-fun#anchor--mouse-detection">Mouse Detection</AniLink>

            <h3>N</h3>
            <AniLink to="netlify">Netlify</AniLink>
            <AniLink to="slack">Netlify: Slack Notifications</AniLink>
            <AniLink to="slack#anchor--email-notification">Netlify: Email Notifications</AniLink>
            <AniLink to="spa-101#anchor--install-nodejs">NodeJs: How to Install</AniLink>
            <AniLink to="spa-101#anchor--update-nodejs">NodeJs: How to Update</AniLink>

            <h3>O</h3>
            <AniLink to="seo-meta#anchor--open-graph">Open Graph</AniLink>
            <AniLink to="overview">Overview</AniLink>

            <h3>P</h3>
            <AniLink to="html-101#anchor--photopea">Photopea</AniLink>
            <AniLink to="social#anchor--pinterest-signup">Pinterest Signup</AniLink>
            <AniLink to="html-101#anchor--coming-soon-psd">PSD: Coming Soon Mockup</AniLink>
            <AniLink to="coming-soon-css#anchor--pseudo-selectors">Pseudo Selectors</AniLink>

            <h3>Q</h3>

            <h3>R</h3>
            <AniLink to="spa-101#anchor--react-devtools">React Dev Tools</AniLink>
            <AniLink to="gatsby-init#anchor--react-helmet">React Helmet (for SEO)</AniLink>
            <AniLink to="build-services#anchor--react-lazyload">React LazyLoad</AniLink>
            <AniLink to="js-fun#anchor--reverse-geocoding">Reverse Geocoding</AniLink>
            <AniLink to="seo-robots#anchor--what-is-robots">robots.txt explained</AniLink>
            <AniLink to="git-101#anchor--rys-git-tutorial">Ry's Git Tutorial</AniLink>

            <h3>S</h3>
            <AniLink to="sass-101">Sass</AniLink>
            <AniLink to="sass-101#anchor--sass-docs">Sass Docs</AniLink>
            <AniLink to="schema-markup">Schema Markup</AniLink>
            <AniLink to="schema-markup#anchor--schema-validator">Schema Validator</AniLink>
            <AniLink to="search-submit#anchor--serp-ranker">Search Engine Rank Checker</AniLink>
            <AniLink to="ide#anchor--settings-json">settings.json</AniLink>
            <AniLink to="seo-sitemap">sitemap.xml</AniLink>
            <AniLink to="graphql-101#anchor--gatsby-static-query">Static Query</AniLink>
            <AniLink to="stlye-library">Style Library</AniLink>

            <h3>T</h3>
            <AniLink to="social#anchor--twitter-cards">Twitter Cards</AniLink>
            <AniLink to="social#anchor--twitter-card-validator">Twitter: Card Validator</AniLink>
            <AniLink to="social#anchor--twitter-signup">Twitter Signup</AniLink>

            <h3>U</h3>

            <h3>V</h3>
            <AniLink to="ide#anchor--vs-code">VS Code</AniLink>
            <AniLink to="ide#anchor--vs-code-youtube">VS Code YouTube - Traversy Media</AniLink>

            <h3>W</h3>
            <AniLink to="optimize-overview#anchor--chrome-accessibility-plugin">Wave Accessibility Plugin</AniLink>
            <AniLink to="spa-101#anchor--wes-bos-react-for-beginners">Wes Bos - React For Beginners Video Series (Paid Course)</AniLink>
            <AniLink to="keywords#anchor--woo-rank">WooRank SEO Analysis</AniLink>

            <h3>X</h3>

            <h3>Y</h3>
            <AniLink to="social#anchor--youtube-signup">Youtube Signup</AniLink>
            <AniLink to="portfolio-design#anchor--xd-tutorial">Youtube: Adobe XD Course - Howard Pinsky</AniLink>
            <AniLink to="browser#anchor--css-full-course-youtube">Youtube: CSS Full Course - freeCodeCamp.org</AniLink>
            <AniLink to="browser#anchor--css-positioning-youtube">Youtube: CSS Positioning - Web Dev Simplified</AniLink>
            <AniLink to="browser#anchor--dev-tools-youtube">Youtube: Dev Tools - Google Playlist</AniLink>
            <AniLink to="graphql-101#anchor--ace-mind-graphql">Youtube: GraphQl Course - Ace Mind</AniLink>
            <AniLink to="spa-101#anchor--gatsby-andrew-mead">Youtube: Great Gatsby Bootcamp - Andrew Mead</AniLink>
            <AniLink to="git-101#anchor--git-crash-course">Youtube: Git Crash Course - Traversy Media</AniLink>
            <AniLink to="spa-101#anchor--react-hooks">Youtube: React Hooks - Acemind</AniLink>
            <AniLink to="browser#anchor--dev-tools-crash-course-youtube">Youtube: Google Chrome Developer Tools Crash Course - Traversy Media</AniLink>
            <AniLink to="browser#anchor--dev-tools-crash-course-youtube">Youtube: 14 Must know Chrome Dev Tool Tricks - Wes Bos</AniLink>
            <AniLink to="html-101#anchor--html-basics-youtube">Youtube: Learn HTML Basics - Envato</AniLink>
            <AniLink to="sass-101#anchor--sass-video">Youtube: Sass Crash Course - Dev Ed</AniLink>
            <AniLink to="javascript-101#anchor--javascript-beginner-videos">YouTube: Javascript Beginner Videos - Assorted</AniLink>

            <h3>Z</h3>
          </div>
          <br />
          <br />
        </section>
      </section>
    </Layout>
  );
};

export default Appendix;
