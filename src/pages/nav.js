import React from 'react';
import TransitionLink from 'gatsby-plugin-transition-link';
import AniLink from 'gatsby-plugin-transition-link/AniLink';

import Logo from '../../static/images/logo--nwc-white.svg';
import JamstackNav from '../pages/jamstack-jedi/JamstackNav';

const Nav = () => {
  const goBack = () => {
    window.history.back();
  };

  return (
    <nav id="nav">
      <header className="grid container font--light">
        <div className="col-6 font--50">
          <button
            type="button"
            className="font--50 padding--none bordered--none"
            onClick={() => goBack()}
          >
            <span className="font--light">
              &times;
            </span>
          </button>
        </div>

        <div className="col-6 text-right">
          <img
            src={Logo}
            style={{
              height: 20,
            }}
          />
        </div>
      </header>

      <JamstackNav />
    </nav>
  )
};

export default Nav;
