import React from 'react';
import { Link } from 'gatsby';
import Layout from '../components/layout';


const Docs = () => (
  <Layout
    title="NWC Docs"
    description="Nerds With Charisma Documentation"
    keywords="Nerds With Charisma, style docs, sass docs"
    showSchema={false}
  >
    <section className="text-center">
      <h2 className="font--42">Projects</h2>
      <br />
      <br />
      <div className="grid">
        <div className="col-6 col-sm-6 col-md-2 col-lg-2">
          <Link to="/stiles">
            <img src="/images/docs/docs--stiles.png" alt="Stiles SCSS Framework" />
            <br />
            <strong>STILES</strong>
          </Link>
        </div>
      </div>
    </section>
  </Layout>
);

export default Docs;
