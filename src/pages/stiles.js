import React, { useEffect } from "react"

import Markdown from '../components/Common/markdown';
import Layout from "../components/layout"
import CodePen from "../components/codepen"
import Swatch from "../components/swatch"

const Stiles = () => {
  return (
    <Layout title="" description="" keywords="">
      <section id="docs--container">
        <section id="docs--grid" className="grid">
          <div id="docs--main" className="col-12">
            <strong className="font--42">{"STILES"}</strong>
            <br />
            <i className="fas fa-link font--grey" />{' '}
            <a
              href="https://www.npmjs.com/package/@nerds-with-charisma/nerds-style-sass"
              target="_blank"
              rel="noopener noreferrer"
            >
              {"NPM Repo"}
            </a>
            &nbsp;&nbsp;&nbsp;&nbsp;
            <i className="fab fa-gitlab font--grey" />{" "}
            <a
              href="https://gitlab.com/nerds-with-charisma/nerds-style-sass"
              target="_blank"
              rel="noopener noreferrer"
            >
              {"Git Repo"}
            </a>
            <br />
            <br />
            <strong>{"What is it?"}</strong>
            <p>
              {
                "A lightweight, modern Sass framework that focuses on re-usability over anything else. Think SMACCS meets bootstrap. Fully commented and easy to customize."
              }
            </p>
            <br />
            <strong>{"Usage"}</strong>
            <p>{"Install with npm"}</p>
            <Markdown
              rel="Terminal"
              langugage="terminal"
              md={
                `  ## NPM\n  npm i @nerds-with-charisma/nerds-style-sass --save`
              }
            />
            <p>
              {'Import it in to your main scss file'}
            </p>
            <Markdown
              rel="Terminal"
              language="terminal"
              md={
                `  @import "~@nerds-with-charisma/nerds-style-sass/main.scss";`
              }
            />
            {
              'That\'s it, you\'re ready to start using it. Check below for what comes out of the box.'
            }
            <hr />
            <strong>{"Entry point"}</strong>
            <br />
            <p>
              {"The main file, that simply imports all the other files is  "}
              <u>{"main.scss"}</u>
              {
                ". Feel free to toss your custom styles inside of the theme folder, then import them into the main.scss file."
              }
            </p>
            <hr />
            <strong>{"Variables"}</strong>
            <p>
              {"The "}
              <u>{"variables.scss  "}</u>
              {
                "file holds our base colors and some helpers. You could go crazy here adding every little detail, but we've kept it simple."
              }
            </p>
            <br />
            <strong>{"Color"}</strong>
            <p>
              <u>{"variables.scss"}</u>
              {
                '  contains our basic color scheme. Feel free to modify this file to suit your color needs. If you notice, below the color variables, there is a comment "/* put all our colors here, we\'ll use this for looping */". What this does, is auto-loop through our colors and make helper classes around them. If you add a new color, add it to this list so that it is available for you later.'
              }
            </p>
            <br />
            <div className="grid">
              <div className="col-12 col-md-6 col-lg-6">
                <Markdown
                  rel="Scss"
                  language="css"
                  md={
                    `  $text: #484848;       /* standard text color */
  $dark: #000;          /* our actual black */
  $offDark: #2c2c2c;    /* our not actual black */
  $light: #fff;         /* our actual white */
  $offLight: #f7f7f7;   /* our not actual white */
  $grey: #d7d7d7;       /* grey, mostly borders */

  $primary: #9012FE;    /* our primary color */
  $primaryAlt: #C212FE; /* our alt primary color, accent */
  $cta: #F20028;        /* our secondary, or call to action color */
  $ctaAlt: #ff0084;     /* our alt cta color, accent */
  $success: #85DD33;    /* successful things */
  $successAlt: #d2ff52; /* our alt success color */
  $info: #4dd8ea;       /* info color */
  $infoAlt: #33d671;    /* info alt color */
  $error: #F20028;      /* error color */
  $errorAlt: #ff0084;   /* error alt color */
  $links: #f233b9;      /* our STANDARD link color */
  $linksHover: #02e2ca; /* our hover link color */

  /* put all our colors here, we'll use this for looping */
  $color_names: text, dark, offDark, light, offLight, grey, primary, primaryAlt, cta, ctaAlt, success, successAlt, info, infoAlt, error, errorAlt, links, linksHover;
  $color_vars: $text, $dark, $offDark, $light, $offLight, $grey, $primary, $primaryAlt, $cta, $ctaAlt, $success, $successAlt, $info, $infoAlt, $error, $errorAlt, $links, $linksHover;
                    `
                  }
                />
              </div>
              <div className="col-12 col-md-6 col-lg-6">
                <br />
                <Swatch name="text" />
                <Swatch name="dark" />
                <Swatch name="offDark" />
                <Swatch name="light" />
                <Swatch name="offLight" />
                <Swatch name="grey" />
                <Swatch name="primary" />
                <Swatch name="primaryAlt" />
                <Swatch name="cta" />
                <Swatch name="ctaAlt" />
                <Swatch name="success" />
                <Swatch name="successAlt" />
                <Swatch name="info" />
                <Swatch name="infoAlt" />
                <Swatch name="error" />
                <Swatch name="errorAlt" />
                <Swatch name="links" />
                <Swatch name="linksHover" />
              </div>
            </div>
            <br className="col-12 col-sm-12 col-md-12 col-lg-12" />
            <strong>{"Border Radius"}</strong>
            <p>
              {"Also, inside our  "}
              <u>{"variables.scss"} </u>
              {
                "file is our border radius helpers. You can use this to round (or remove rounding) from any element. We have set these xs, sm, md, and lg radius' here, but if you feel like you would like to set different radii you can."
              }
            </p>
            <br />
            <div className="grid">
              <div className="col-12 col-md-6 col-lg-6">
              <Markdown
                rel="Scss"
                language="css"
                md={
                  `  /* border radius */
  $borderRadiusXS: 3px;
  $borderRadiusSM: 5px;
  $borderRadiusMD: 10px;
  $borderRadiusLG: 300px;` }
              />
              </div>
              <div className="col-12 col-md-6 col-lg-6 text-center font--light">
                <br />
                <div className="radius--xs bg--primary">{"xs"}</div>
                <br />
                <div className="radius--sm bg--primary">{"sm"}</div>
                <br />
                <div className="radius--md bg--primary">{"md"}</div>
                <br />
                <div className="radius--lg bg--primary">{"lg"}</div>
              </div>
            </div>
            <br />
            <strong>{"Grid Setup"}</strong>
            <p>
              {
                "We also have some grid helpers, incase you don't like working with the standard 12 col grid, feel free to modify the  "
              }
              <b>{"$gridColumns"}</b>
              {"  and "}
              <b>{"$gridGap"}</b>
              {"variables to fulfill your needs."}
            </p>
            <hr className="col-12 col-sm-12 col-md-12 col-lg-12" />
            <strong>{"Reset & Base"}</strong>
            <p>
              <u>{"reset.scss "}</u>
              {
                "put all browsers on an even artboard. But they can also be extremely bloated and balloon in size. We have added a VERY simple reset that will make most things look good in most modern browsers. To read more about the reset, visit "
              }
              <a
                href="https://alligator.io/css/minimal-css-reset/"
                target="_blank"
                rel="noopener noreferrer"
              >
                {"alligator.io"}
              </a>
              {"."}
            </p>
            <br />
            <p>
              <u>{"base.scss "}</u>
              {
                "has some basic styles for our body content, links, and highlighting. Again, this is kept simple so you can customize to your needs without worrying about styles conflicting."
              }
            </p>
            <hr />
            <strong>{"Borders"}</strong>
            <p>
              <u>{"borders.scss "}</u>
              {
                "has our border classes, as well radius classes, and our horizontal line styles. Check the codepen and examples below to see what each does."
              }
            </p>
            <div className="grid">
              <div className="col-12 col-md-6 col-lg-6">
              <Markdown
              rel="Scss"
              language="css"
              md={
                `  .bordered { border: 1px solid $grey; } /* adds a 1px border all the way around an element */

  .bordered--none { border: none; } /* removes the border from an element */

  /* removes the default border from an element, to be used with one of the below classes */
  .bordered---top, .bordered--bottom, .bordered--right, .bordered--left { border: none; }

  .bordered--top { border-top: 1px solid $grey; }
  .bordered--bottom { border-bottom: 1px solid $grey; }
  .bordered--right { border-right: 1px solid $grey; }
  .bordered--left { border-left: 1px solid $grey; }

  /* change the thickness of a border */
  .bordered--xs { border-width: 2px; }
  .bordered--sm { border-width: 5px; }
  .bordered--md { border-width: 10px; }
  .bordered--lg { border-width: 25px; }

  /* round the edges of an element */
  .radius--xs { border-radius: 3px; }
  .radius--sm { border-radius: 5px; }
  .radius--md { border-radius: 10px; }
  .radius--lg { border-radius: 300px; }

  /* style our horizontal line to be simple and clean */
  hr  {
    border: none;
    height: 0;
    border-bottom: 1px solid $grey;

    /* makes a fancy shorty hr for decoration */
    &.shorty {
      width: 60px;
    }
  }`}
              />
              </div>
              <div className="col-12 col-md-6 col-lg-6 text-center">
                <br />
                <div className="bordered">{"bordered"}</div>
                <br />
                <div className="border--none">{"border--none"}</div>
                <br />
                <span className="bordered--top">{"bordered--top"}</span>
                &nbsp;&nbsp;
                <span className="bordered--bottom">{"bordered--bottom"}</span>
                &nbsp;&nbsp;
                <span className="bordered--left">{"bordered--left"}</span>
                &nbsp;&nbsp;
                <span className="bordered--right">{"bordered--right"}</span>
                <br />
                <br />
                <div className="bordered bordered--xs">
                  {"bordered.bordered--xs"}
                </div>
                &nbsp;&nbsp;
                <div className="bordered bordered--sm">
                  {"bordered.bordered--sm"}
                </div>
                &nbsp;&nbsp;
                <div className="bordered bordered--md">
                  {"bordered.bordered--md"}
                </div>
                &nbsp;&nbsp;
                <div className="bordered bordered--lg">
                  {"bordered.bordered--lg"}
                </div>
                &nbsp;&nbsp;
                <br />
                <div className="radius--xs bg--primary">{"radius--xs"}</div>
                <br />
                <div className="radius--sm bg--primary">{"radius--sm"}</div>
                <br />
                <div className="radius--md bg--primary">{"radius--md"}</div>
                <br />
                <div className="radius--lg bg--primary">{"radius--lg"}</div>
                <hr />
                <hr className="shorty" />
              </div>
            </div>
            <hr />
            <strong>{"Buttons"}</strong>
            <p>
              <u>{"buttons.scss"}</u>
              {
                " has been kept simple, but is highly customizable with other helper classes like bacground, padding, and font color classes."
              }
            </p>
            <br />
            <div className="grid">
              <div className="col-12 col-md-6 col-lg-6">
                <Markdown
                  rel="Scss"
                  language="css"
                  md={
                    `  /* reset button styles */
  button:focus, .btn {
      outline: none;
  }

  /* a simple, base to build on */
  button, [type=submit], .btn {
      border: 1px solid $grey;
      padding: 7px 20px;
      background: none;
      color: $light;
      transition: all 0.3s linear;
  }

  /* make a standard rounded button */
  .round {
      border-radius: $borderRadiusLG;
  }

  /* make a button without a border */
  .noBorder {
      border: 0;
  }

  /* remove the text shadow from the button */
  .noShadow {
      text-shadow: none;
  }

  /* loop through all our colors and make buttons for each variable we export in variables.scss */
  /* use with the class .button.{color_name} ex: button primary */
  @each $name in $color_names {
      $i: index($color_names, $name);

      button.#{$name}, [type=submit].#{$name}, .btn.#{$name} {
          border-color: nth($color_vars, $i);
          text-shadow: 0 1px 2px nth($color_vars, $i);
          background: nth($color_vars, $i);
          background: linear-gradient(135deg, nth($color_vars, $i) 0%, lighten(nth($color_vars, $i), 10%) 100%);
      }
  }`}
                />

              </div>
              <div className="col-12 col-md-6 col-lg-6 text-center">
                <button type="button" className="font--text">
                  {"Button"}
                </button>
                <br />
                <br />
                <button type="button" className="font--text round">
                  {"round"}
                </button>
                <br />
                <br />
                <button type="button" className="font--text noBorder">
                  {"noBorder"}
                </button>
                <br />
                <br />
                <button type="button" className="font--text noShadow">
                  {"noShadow"}
                </button>
                <br />
                <br />
                <button
                  type="button"
                  className="font--text primary font--light"
                >
                  {"primary.font--light"}
                </button>
                <br />
                <br />
                <button
                  type="button"
                  className="font--text cta font--light round"
                >
                  {"cta.font--light.round"}
                </button>
                <br />
                <br />
                <button
                  type="button"
                  className="font--text success font--light noBorder"
                >
                  {"success.font--light.noBorder"}
                </button>
                <br />
                <br />
                <button type="button" className="font--text font--24">
                  {"font--text.font--24"}
                </button>
                <br />
                <br />
                <button type="button" className="font--text bordered--md">
                  {"font--text.bordered--md"}
                </button>
                <br />
                <br />
                {
                  "There's nearly an unlimited number of combinations you can make here. So feel free to experiment and play around."
                }
                <br />
                <br />
                {
                  "It might also make sense to create a single component, and pass the classes directly to it, with some default props for consistency."
                }
              </div>
            </div>
            <hr />
            <strong>{"Colors"}</strong>
            <p>
              <u>{"colors.scss"}</u>
              {
                " are meant to be used with other classes to apply reusable styling like background colors, font colors, and border colors."
              }
            </p>
            <p>
              {"To modify a background color, use the class "}
              <b>{'bg--"color_name"'}</b>

              {"Use "}
              <b>{'font--"color_name"'}</b>
              {" for text color. And use "}
              <b>{'bordered--"color_name"'}</b>
              {" for border colors."}
            </p>
            <br />
            <div className="padding-md font--light bg--primary">
              {"font--light.bg--primary"}
            </div>
            <div className="padding-md font--primary bg--grey">
              f{"ont--primary.bg--grey"}
            </div>
            <div className="padding-md font--primary bordered bordered--primary">
              {"font--primary.bordered.bordered--primary"}
            </div>
            <hr />
            <strong>{"Grid"}</strong>
            <br />
            <p>
              <u>{"grid.scss"}</u>
              {
                " mimics the bootstrap grid. One difference is this grid needs explicit cols for each viewport size. It does not inherit up. All start at base 12. So if you want to size differently for laptop and desktop you need to use col-md-6 col-lg-6, you cannot use col-md-6 and expect it to inherit up like Bootstrap does."
              }
            </p>
            <br />
            <p>
              {"To use a grid layout, simply wrap your divs in a "}
              <b>{".grid"}</b>
              {
                " class. You can also use the optional container sizes (container, container-xs, container-sm, container-md) to constrain them as a wrapper to your grid."
              }
            </p>
            <br />
            <Markdown
              rel="HTML"
              language="html"
              md={`  <div class="grid">
    <div class="col-12 col-sm-12 col-md-6 col-lg-3">A simple grid</div>
    <div class="col-12 col-sm-12 col-md-6 col-lg-9">A simple grid</div>
  </div>


  <div class="container">
    <br />
    <div class="grid">
      <div class="col-12 col-sm-12 col-md-12 col-lg-12">A default container</div>
    </div>
  </div>

  <div class="container-md">
    <div class="grid">
      <div class="col-12 col-sm-12 col-md-12 col-lg-12">A medium container</div>
    </div>
  </div>
            `}
            />
            <br />
            <div className="grid">
              <div className="bg--grey col-12 col-sm-12 col-md-6 col-lg-3">
                {"A simple grid"}
              </div>
              <div className="bg--grey col-12 col-sm-12 col-md-6 col-lg-9">
                {"A simple grid"}
              </div>
            </div>
            <br />
            <br />
            <div className="container">
              <div className="grid">
                <div className="bg--grey col-12 col-sm-12 col-md-12 col-lg-12">
                  {"A default container"}
                </div>
              </div>
            </div>
            <br />
            <div className="container-md">
              <div className="grid">
                <div className="bg--grey col-12 col-sm-12 col-md-12 col-lg-12">
                  {"A medium container"}
                </div>
              </div>
            </div>
            <hr />
            <strong>{"Helpers"}</strong>
            <p>
              <u>{"helpers.scss"}</u>
              {
                " contains a mostly layout helpers for adding padding, margins, line-height, etc to your formatting. These can be applied to any block level element."
              }
            </p>
            <br />
            <div className="grid">
              <div className="col-12 col-md-6 col-lg-6">
                <Markdown
                  rel="Scss"
                  language="css"
                  md={`  /* animations - adds a subtle animation to anything you apply it to */
  .transition--all {
      transition: all 0.2s;
  }

  /* padding - adds some breathing room inside a block element */
  .padding-none { padding: 0; }
  .padding-xs { padding: 2px; }
  .padding-sm { padding: 5px; }
  .padding-md { padding: 15px; }
  .padding-lg { padding: 30px; }

  /* margins - adds some breathing room outside an element */
  .margin-auto { margin: auto; }
  .margin-none { margin: 0; }
  .margin-xs { margin-bottom: 2px; }
  .margin-sm { margin-bottom: 5px; }
  .margin-md { margin-bottom: 15px; }
  .margin-lg { margin-bottom: 30px; }

  /* positions - incase you need to position things inside a wrapper, you can use these classes on the parent */
  .position--relative { position: relative; }
  .position--absolute { position: absolute; }

  /* floats - for moving things out of the flow and to the left, right, or clearing */
  .float-right{ float: right; }
  .float-left{ float: left; }
  .clear{ clear: both; }

  /* display - make an inline element block */
  .block {
      display: block;
      width: 100%;
  }

  /* don't allow something to break to the next line */
  .no--wrap {
      white-space: nowrap;
  }

  /* line height helpers */
  .lh-xs { line-height: 0.5em; }
  .lh-sm { line-height: 1em; }
  .lh-md { line-height: 2em; }
  .lh-lg { line-height: 3em; }

  /* display helpers - used to hide things on different devices */
  @media only screen and (max-width: 767px) {
      .hidden-xs { display: none; }
  }

  @media only screen and (min-width: 768px) and (max-width: 1023px) {
      .hidden-sm { display: none; }
  }

  @media only screen and (min-width: 1024px) and (max-width: 1299px) {
      .hidden-md { display: none; }
  }

  @media only screen and (min-width: 1300px) {
      .hidden-lg { display: none; }
  }

  /* hide something on all devices */
  .hide { display: none; }`}
                />
              </div>
              <div className="col-12 col-md-6 col-lg-6 text-center">
                <strong>{"Animations"}</strong>
                <br />
                <a
                  href="/"
                  className="btn transition--all font--text font--16 radius--lg"
                >
                  {"Hover over me!"}
                </a>
                <br />
                <br />
                <strong>{"Padding"}</strong>
                <br />
                <div className="bordered padding-none">{"padding-none"}</div>
                <div className="bordered padding-xs">{"padding-xs"}</div>
                <div className="bordered padding-sm">{"padding-sm"}</div>
                <div className="bordered padding-md">{"padding-md"}</div>
                <div className="bordered padding-lg">{"padding-lg"}</div>
                <br />
                <br />
                <strong>{"Margins"}</strong>
                <br />
                <div className="bordered margin-auto" style={{ width: "50%" }}>
                  {"margin-auto"}
                </div>
                <br />
                <div className="bordered margin-none">{"margin-none"}</div>
                <br />
                <div className="bordered margin-xs">{"margin-xs"}</div>
                <br />
                <div className="bordered margin-sm">{"margin-sm"}</div>
                <br />
                <div className="bordered margin-md">{"margin-md"}</div>
                <br />
                <div className="bordered margin-lg">{"margin-lg"}</div>
                <br />
                <br />
                <strong>{"Floats"}</strong>
                <br />
                <span className="float-left">{"float-left"}</span>
                <span className="float-right">{"float-right"}</span>
                <span className="clear">{"clear"}</span>
                <br />
                <br />
                <strong>{"Display"}</strong>
                <br />
                <span className="block bg--grey">
                  {"I'm a span, but I'm block!"}
                </span>
                <br />
                <br />
                <strong>{"Line Height"}</strong>
                <br />
                <div className="lh-xs">{"lh-xs"}</div>
                <div className="lh-sm">{"lh-sm"}</div>
                <div className="lh-md">{"lh-md"}</div>
                <div className="lh-lg">{"lh-lg"}</div>
                <br />
                <br />
                <strong>{"Display Helpers"}</strong>
                <br />
                <span className="hidden-xs padding-sm bg--grey">{"xs"}</span>
                &nbsp;&nbsp;
                <span className="hidden-sm padding-sm bg--grey">{"sm"}</span>
                &nbsp;&nbsp;
                <span className="hidden-md padding-sm bg--grey">{"md"}</span>
                &nbsp;&nbsp;
                <span className="hidden-lg padding-sm bg--grey">{"lg"}</span>
              </div>
            </div>
            <hr />
            <strong>{"Images"}</strong>
            <p>
              <u>{"images.scss"}</u>
              {
                " is the responsive helper to shrink images down as the viewport shrinks. Simply add a class of "
              }
              <b>{".responsive"}</b>
              {
                " to any image tag and it will constrain to the max-width of it's parent."
              }
            </p>
            <br />
            <hr />
            <strong>{"Opacity"}</strong>
            <p>
              <u>{"opacity.scss"}</u>
              {
                " controls the alpha of an element. Simply choose a number between 0 and 9, 9 being 90% visile, 0 being 0% visible."
              }
            </p>
            <br />
            <div className="grid">
              <div className="col-12 col-md-6 col-lg-6">
                <Markdown
                  rel="Scss"
                  language="css"
                  md={`  .opacity0 { opacity: 0; }
  .opacity1 { opacity: 0.1; }
  .opacity2 { opacity: 0.2; }
  .opacity3 { opacity: 0.3; }
  .opacity4 { opacity: 0.4; }
  .opacity5 { opacity: 0.5; }
  .opacity6 { opacity: 0.6; }
  .opacity7 { opacity: 0.7; }
  .opacity8 { opacity: 0.8; }
  .opacity9 { opacity: 0.9; }`}
                />

              </div>
              <div className="col-12 col-md-6 col-lg-6 text-center">
                <div className="padding-sm bg--grey opacity0">
                  {".opacity0"}
                </div>
                <div className="padding-sm bg--grey opacity1">
                  {".opacity1"}
                </div>
                <div className="padding-sm bg--grey opacity2">
                  {".opacity2"}
                </div>
                <div className="padding-sm bg--grey opacity3">
                  {".opacity3"}
                </div>
                <div className="padding-sm bg--grey opacity4">
                  {".opacity4"}
                </div>
                <div className="padding-sm bg--grey opacity5">
                  {".opacity5"}
                </div>
                <div className="padding-sm bg--grey opacity6">
                  {".opacity6"}
                </div>
                <div className="padding-sm bg--grey opacity7">
                  {".opacity7"}
                </div>
                <div className="padding-sm bg--grey opacity8">
                  {".opacity8"}
                </div>
                <div className="padding-sm bg--grey opacity9">
                  {".opacity9"}
                </div>
              </div>
            </div>
            <hr />
            <strong>{"Tables"}</strong>
            <p>
              <u>{"tables.scss"}</u>
              {
                " are kept very simple. There's a reset, and some helper classes. You can use "
              }
              <b>{".table-bordered"}</b>
              {" to give the table a border."}
              <b>{".table-striped"}</b>
              {" to give the table alternating colored rows."}
              {"And "}
              <b>{".table-hover"}</b>
              {" which will make any row hovered over highlight."}
            </p>
            <br />
            <hr />
            <strong>{"Typography"}</strong>
            <p>
              <u>{"typography.scss"}</u>
              {
                " file contains the font sizing helpers, as well as our positioning helpers."
              }
            </p>
            <br />
            <div className="grid">
              <div className="col-12 col-md-6 col-lg-6">
                <Markdown
                  rel="Scss"
                  language="css"
                  md={`  h1, h2, h3, h4, h5, h6 { margin: 0; }

  @for $i from 1 through 200 {
    .font--#{$i} { font-size: #{$i}px; }
  }

  .text-center { text-align: center; }
  .text-right { text-align: right; }
  .text-left { text-align: left; }
                  `}
                />
              </div>
              <div className="col-12 col-md-6 col-lg-6 text-center">
                <strong>Choose any number between 1-200</strong>
                <br />
                <div className="font--10">font--10</div>
                <div className="font--20">font--20</div>
                <div className="font--50">font--50</div>
                <br />
                <br />
                <div className="padding-sm bg--grey text-center">
                  text-center
                </div>
                <br />
                <div className="padding-sm bg--grey text-left">text-left</div>
                <br />
                <div className="padding-sm bg--grey text-right">text-right</div>
              </div>
            </div>
          </div>
        </section>
      </section>
      <br />
      <br />
      <br />
      <br />
    </Layout>
  )
}

export default Stiles
