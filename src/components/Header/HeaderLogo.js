import React from 'react';

const HeaderLogo = () => (
  <a
    href="/"
    id="logo"
    className="font--light transition--all"
    style={{
      zIndex: 999,
      position: 'fixed',
      left: 15,
      top: 15,
      fontWeight: 800,
      display: 'flex',
      height: 32,
      lineHeight: '34px',
      zoom: 1.2,
    }}
  >
    {'nerds   '}
    <img
      width="30"
      style={{
        margin: '0px 5px',
      }}
      src="/images/logo--nwc-purple.svg"
      alt="Nerds With Charisma Bitchin Websites"
    />
    {'  charisma'}
  </a>
);

export default HeaderLogo;
