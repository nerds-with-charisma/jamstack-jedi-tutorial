import React from 'react';

export default class HeaderNav extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isActive: false,
      activeBurger: false,
    };
  }

  componentDidMount() {
    window.addEventListener('scroll', this.handleScroll);
  }

  handleScroll = () => {
    if (window.scrollY > 1000) {
      this.setState({ activeBurger: true });
    } else {
      this.setState({ activeBurger: false });
    }
  }

  toggleNav() {
    const { isActive } = this.state;

    this.setState({ isActive: !isActive });
  }

  scrollTo(id) {
    this.setState({ isActive: false });
    document.getElementById(id).scrollIntoView({ behavior: 'smooth' });
  }

  render() {
    const { isActive, activeBurger } = this.state;

    return (
      <section id="headerNav">
        <button
          onClick={() => this.toggleNav()}
          className={(isActive) ? 'hamburger hamburger--collapse font--light is-active' : 'hamburger hamburger--collapse opacity7'}
          type="button"
          style={{
            zIndex: 999999999,
            position: 'fixed',
            right: 15,
            top: 15,
          }}
        >
          <span className={(activeBurger === true) ? 'hamburger-box active' : 'hamburger-box'}>
            <span className="hamburger-inner ir">
              Menu.
            </span>
          </span>
        </button>
        <nav id="nav" className={(isActive) ? 'in' : 'out'}>
          <br />
          <h1 className="font--50 font--light">
            <strong className="crazy-shadow bordered padding-sm bordered--md bordered--light font--heavy">
              {' NWC '}
            </strong>
          </h1>
          <br />
          <br />
          <button
            type="button"
            onClick={() => this.scrollTo('hero')}
            className="nav--item font--light border--none font--18"
          >
            <strong>HOME</strong>
          </button>

          <br />

          <button
            type="button"
            onClick={() => this.scrollTo('services')}
            className="nav--item font--light border--none font--18"
          >
            <strong>SERVICES</strong>
          </button>

          <br />

          <button
            type="button"
            onClick={() => this.scrollTo('portfolio')}
            className="nav--item font--light border--none font--18"
          >
            <strong>PORTFOLIO</strong>
          </button>

          <br />

          <button
            type="button"
            onClick={() => this.scrollTo('about')}
            className="nav--item font--light border--none font--18"
          >
            <strong>ABOUT</strong>
          </button>

          <br />

          <button
            type="button"
            onClick={() => this.scrollTo('contact')}
            className="nav--item font--light border--none font--18"
          >
            <strong>CONTACT</strong>
          </button>
        </nav>
      </section>
    );
  }
}
