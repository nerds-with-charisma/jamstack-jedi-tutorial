import React from 'react';

import HeaderLogo from './HeaderLogo';
import HeaderNav from './HeaderNav';

const Header = () => (
  <header>
    <HeaderLogo />
    <HeaderNav />
  </header>
);

export default Header;
