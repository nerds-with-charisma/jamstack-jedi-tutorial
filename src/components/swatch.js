import React from 'react';
import { PropTypes } from 'prop-types';

const swatch = ({ name }) => (
  <div className={`bg--${name} padding-md font--light font--10 text-center`}>{name}</div>
);

swatch.propTypes = {
  name: PropTypes.string.isRequired,
};

export default swatch;
