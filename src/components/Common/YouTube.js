
import React, { useEffect } from 'react';

import { PropTypes } from 'prop-types';

const YouTube = ({ url }) => {
  return (
    <iframe
      style={{ display: 'block', margin: 'auto' }}
      className="marginAuto"
      width="560"
      height="315"
      src={url}
      frameborder="0"
      allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
      allowfullscreen
    />
  );
};

export default YouTube;

YouTube.propTypes = {
  url: PropTypes.string.isRequired,
};
