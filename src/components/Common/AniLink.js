import React from 'react';
import { PropTypes } from 'prop-types';
import AniLink from 'gatsby-plugin-transition-link/AniLink';

const Header = ({ children, className, hex, to }) => (
  <AniLink
    paintDrip
    hex={hex}
    duration={0.2}
    direction="left"
    to={`/${to}`}
    className={className}
  >
    {children}
  </AniLink>
);

Header.defaultProps = {
  className: null,
  hex: '#fff',
};

Header.propTypes = {
  children: PropTypes.object.isRequired,
  className: PropTypes.string,
  hex: PropTypes.string,
  to: PropTypes.string.isRequired,
};

export default Header;
