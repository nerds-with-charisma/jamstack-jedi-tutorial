import React from 'react';

import Logo from '../../../static/images/logo--nwc-white.svg';
import MenuIcon from '../../../static/images/icon--menu.svg';

import AniLink from '../Common/AniLink';

const Header = () => (
  <header className="grid container font--light">
    <div className="col-6">
      <AniLink to="nav">
        <img
          src={MenuIcon}
          style={{
            height: 20,
          }}
        />
      </AniLink>
    </div>

    <div className="col-6 text-right">
      <AniLink to="/">
      <img
          src={Logo}
          style={{
            height: 20,
          }}
        />
      </AniLink>
    </div>
  </header>
);

export default Header;
