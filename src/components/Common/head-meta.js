import React from 'react';
import { Helmet } from 'react-helmet';

const HeadMeta = ({ description, keywords, title }) => (
    <Helmet>
      <html lang="en" />
      <title>{title}</title>

      <link rel="shortcut icon" href="favicon.ico" />

      <meta charSet="utf-8" />
      <meta
        name="viewport"
        content="width=device-width, initial-scale=1, shrink-to-fit=no"
      />
      <meta name="theme-color" content="#9012fe" />
      <meta name="p:domain_verify" content="6b1bf384b5f509e1ffd1e8cf6307730c"/>
      <meta name="apple-mobile-web-app-capable" content="yes" />
      <meta name="robots" content="index, follow" />

      <meta name="description" content={description} />
      <meta name="keywords" content={keywords} />
      <link rel="canonical" href="https://nerdswithcharisma.com/" />

      <meta property="og:title" content="Nerds With Charisma" />
      <meta property="og:site_name" content="Nerds With Charisma" />
      <meta property="og:url" content="https://nerdswithcharisma.com" />
      <meta property="og:type" content="website" />
      <meta
        property="og:description"
        content="Nerds With Charisma is a cutting edge digital media agency specializing in awesome websites."
      />
      <meta
        property="og:image"
        content="https://nerdswithcharisma.com/images/nwc-tile.png"
      />

      <meta name="twitter:card" content="summary_large_image" />
      <meta name="twitter:site" content="https://nerdswithcharisma.com" />
      <meta name="twitter:creator" content="Brian Dausman" />
      <meta name="twitter:title" content="Nerds With Charisma" />
      <meta
        name="twitter:description"
        content="Nerds With Charisma is a cutting edge digital media agency specializing in awesome websites."
      />
      <meta name="twitter:image:src" content="/images/nwc-tile.png" />

      <meta name="google-site-verification" content="VNLuFr6uYVgjRt5AipwVIwD4ZP6wx6toZPoupfUoyvA" />
    </Helmet>
);

export default HeadMeta;
