import React, { useState } from 'react';

const Tooltip = ({ children, theme, title }) => {
  const [hover, hoverSetter] = useState(false);

  return (
    <span className={`tooltip--wrap position--relative ${theme}`}>
      &nbsp;
      <span
        className="tooltip--title"
        onMouseEnter={() => hoverSetter(true)}
        onMouseLeave={() => hoverSetter(false)}
        style={{
          background: '#02e2ca',
          cursor: 'help',
        }}
      >
        {` ${title} `}
      </span>
      { (hover === true) && (
        <div
          className="tooltip--dropdown position--absolute bg--light z99 padding-sm font--14 bordered radius--md text-center lh-md"
          style={{
            width: 250,
            margin: 'auto',
            left: '0px',
            boxShadow: '0 3px 9px rgba(0, 0, 0, 0.1)',
          }}
        >
          {children}
        </div>
      )}
      &nbsp;
    </span>
  )
};

export default Tooltip;
