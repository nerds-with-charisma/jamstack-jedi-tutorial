
import React, { useEffect, useState } from 'react';
import { PropTypes } from 'prop-types';
import Prism from 'prismjs';

const Markdown = ({ rel, language, md }) => {
  const [showCopied, setShowCopied] = useState(false);

  useEffect(() => {
    Prism.highlightAll();
  }, []);

  const copyText = (str) => {
    const self = this;

    // Create new element
    var el = document.createElement('textarea');

    // Set value (string to be copied)
    el.value = str;

    // Set non-editable to avoid focus and move outside of view
    el.setAttribute('readonly', '');
    el.style = {position: 'absolute', left: '-9999px'};
    document.body.appendChild(el);

    // Select text inside element
    el.select();

    // Copy text to clipboard
    document.execCommand('copy');

    // Remove temporary element
    document.body.removeChild(el);

    setShowCopied(true);

    setTimeout(() => {
      setShowCopied(false);
    }, 3000);
  }

  return (
    <pre rel={rel}>
      <div
        className={(showCopied === true) ? "bg--primary transition--all opacity9" : "bg--primary transition--all opacity0"}
        style={{
          position: 'absolute',
          right: 0,
          padding: '5px 25px',
        }}
      >
        Copied to Clipboard
      </div>
      <button
        type="button"
        className="bg--primary font--light bordered--none"
        onClick={(e) => { copyText(md)}}
        style={{
          position: 'absolute',
          top: 0,
          right: 0,
        }}
      >
        Copy
      </button>
      <code className={`language-${language}`}>
        {md}
      </code>
    </pre>
  );
};

export default Markdown;

Markdown.propTypes = {
  rel: PropTypes.string.isRequired,
  language: PropTypes.string.isRequired,
  md: PropTypes.string.isRequired,
};
