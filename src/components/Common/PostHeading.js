import React, { useState } from 'react';
import { PropTypes } from 'prop-types';

const Layout = ({ author, gitRepo, title, time }) => {
  const [showDarkside, setShowDarkside] = useState(false);

  const tldrOn = () => {
    setShowDarkside(true);
    for(var i = 0; i < document.getElementsByClassName('tldr').length; i++) {
      document.getElementsByClassName('tldr')[i].className = "tldr hide";
    }
  };

  const tldrOff = () => {
    setShowDarkside(false);
    for(var i = 0; i < document.getElementsByClassName('tldr').length; i++) {
      document.getElementsByClassName('tldr')[i].className = "tldr";
    }
  };

  return (
    <div className="container">
      { title && (
        <strong className="font--30 container lh-md">
          {title}
        </strong>
      )}

      <br />

      { author && (
        <span className="bg--light padding-sm font--dark radius--lg font--11 inline--block lh-lg">
          <img
            src={author.gravatar}
            className="radius--lg padding-xs float-left"
            style={{ height: 35 }}
          />
          <strong>
            &nbsp;
            {` ${time} min read`}
            &nbsp;&nbsp;&nbsp;&nbsp;
          </strong>
        </span>
      )}

      <span className="float-right">
        { gitRepo && (
          <a
            href={gitRepo}
            target="_blank"
            className="bg--light font--dark radius--lg font--25 shadow-xs"
            style={{ display: 'inline-block', width: 40, height: 40, textAlign: 'center', lineHeight: '45px', marginRight: 10, boxShadow: '0px 3px 20px rgba(0,0,0,0.5)' }}
          >
            <i className="fab fa-gitlab" />
          </a>
        )}

        { (author && showDarkside === true) && (
          <button
            type="button"
            className="bg--linksHover font--light radius--lg font--25 shadow-xs padding-none bordered--none"
            style={{ display: 'inline-block', width: 40, height: 40, textAlign: 'center', lineHeight: '45px', marginRight: 10, boxShadow: '0px 3px 20px rgba(0,0,0,0.5)' }}
            onClick={() => tldrOff() }
          >
            <i className="fas fa-jedi" />
          </button>
        )}

        { (author && showDarkside !== true) && (
          <button
            type="button"
            className="bg--cta font--light radius--lg font--25 shadow-xs padding-none bordered--none"
            style={{ display: 'inline-block', width: 40, height: 40, textAlign: 'center', lineHeight: '37px', marginRight: 10, boxShadow: '0px 3px 20px rgba(0,0,0,0.5)' }}
            onClick={() => tldrOn() }
          >
            <i className="fab fa-galactic-republic" />
          </button>
        )}
      </span>
    </div>
  );
};

Layout.defaultProps = {
  author: null,
  gitRepo: null,
  title: null,
};

Layout.propTypes = {
  author: PropTypes.object,
  gitRepo: PropTypes.string,
  title: PropTypes.string,
};

export default Layout;
