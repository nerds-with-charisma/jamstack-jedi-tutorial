import React from 'react';
import AniLink from '../Common/AniLink';

const Header = () => (
  <footer className="container">
    <AniLink to="jamstack-jedi/intro">
      <span className="btn block font--21 radius--lg padding-md text-center">
        Get Started
      </span>
    </AniLink>

    <br />
    <div className="grid font--16 opacity7">
      <div className="col-6">
        <AniLink to="appendix">
          <span className="font--light">
            Appendix
          </span>
        </AniLink>
      </div>

      <div className="col-6 text-right">
        <AniLink to="checklist">
          <span className="font--light">
            Checklist
          </span>
        </AniLink>
      </div>
    </div>

    <br />

    <div className="text-right font--12 opacity4">
        <small>
          {'v 1.0.0 • Oct. 2019'}
        </small>
    </div>
  </footer>
);

export default Header;
