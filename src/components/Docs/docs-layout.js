import React from 'react';

import DocsNavigation from './docs-navigation';
import Footer from '../Footer/Footer';

const docsH3 = {
  lineHeight: '130px',
  display: 'block',
  width: '100%',
};

const DocsLayout = ({ children }) => (
  <main>
    <section id="hero" className="font--light text-center">
      <h3
        className="font--150"
        style={docsH3}
      >
        <strong>
          <span className="hidden-xs hidden-sm">
            {'NWC '}
          </span>
          {'Docs'}
        </strong>
        <br />
      </h3>
    </section>

    <section id="docs--container" className="container">
      <section id="docs--grid" className="grid">
        <DocsNavigation />

        <div id="docs--main" className="col-12 col-sm-6 col-md-8 col-lg-9">
          <br />
          {children}
        </div>
      </section>
    </section>
    <br />
    <br />
    <Footer />
  </main>
);

export default DocsLayout;
