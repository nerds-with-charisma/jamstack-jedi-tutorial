import React from 'react';
import { Link } from 'gatsby';

const docsNavigation = () => (
  <div id="docs--nav" className="col-12 col-sm-6 col-md-4 col-lg-3">
    <br />
    <strong>Navigation</strong>
    <br />
    <br />
    <Link to="/docs" activeClassName="font--primary" className="font--dark">
      Docs Home
    </Link>
    <br />
    <Link to="/stiles" activeClassName="font--primary" className="font--dark">
      Stiles
    </Link>
  </div>
);

export default docsNavigation;
