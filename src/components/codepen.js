import React from 'react';
import { PropTypes } from 'prop-types';

const CodePen = ({ height, src, url }) => (
  <iframe
    height={height}
    width="100%"
    scrolling="no"
    title="Zdyxor"
    src={src}
    frameBorder="no"
    allowTransparency="true"
    allowFullScreen="true"
  >
    See the Pen
    {' '}
    <a href={url}>Zdyxor</a>
    {' '}
by Brian
    (
    <a href="https://codepen.io/nerdswithcharisma">@nerdswithcharisma</a>
) on
    {' '}
    <a href="https://codepen.io">CodePen</a>
.
  </iframe>
);

CodePen.propTypes = {
  height: PropTypes.number.isRequired,
  src: PropTypes.string.isRequired,
  url: PropTypes.string.isRequired,
};

export default CodePen;
