import React from 'react';
import { PropTypes } from 'prop-types';
import Helmet from 'react-helmet';

import Header from '../components/Common/Header';
import PostHeading from '../components/Common/PostHeading';

const Layout = ({
  author,
  children,
  gitRepo,
  title,
  time,
  description,
  keywords
}) => (
  <main id="main" className="font--light">
    <Header />
    <div id="container" style={{ display: 'flex', flexDirection: 'column' }}>
      { author && (
        <PostHeading
          author={author}
          gitRepo={gitRepo}
          title={`JamstackJedi - ${title} :: Nerds with Charisma`}
          time={time}
        />
      )}

      { author && (
        <section className="post--body bg--light font--dark">
          <div className="container">
            {children}
          </div>
          <br />
          <br />
        </section>
      )}

      { !author && (
        <>
          {children}
        </>
      )}
    </div>

    <Helmet>
      <title>
       {`Jamstack Jedi - ${title} :: Front-end developer roadmap`}
      </title>
      <meta name="description" content={description} />
      <meta name="keywords" content={keywords} />
      <meta name="theme-color" content="#02e2ca" />

      <link
        rel="alternate stylesheet"
        href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
        integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr"
        crossOrigin="anonymous"
        onload="this.rel='stylesheet'"
      />

      <script data-ad-client="ca-pub-3838260189413684" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    </Helmet>
  </main>
);

Layout.defaultProps = {
  author: null,
  gitRepo: null,
  title: null,
};

Layout.propTypes = {
  author: PropTypes.object,
  children: PropTypes.element.isRequired,
  gitRepo: PropTypes.string,
  title: PropTypes.string,
};

export default Layout;
