import React from 'react';
import { Link } from 'gatsby';

const Footer = () => (
  <section
    id="contact"
    className="container text-center padding-lg lh-md font--16"
    style={{ position: 'relative' }}
  >
    <strong>
      Designed &amp; Developed By
      <br />
      <a href="/docs/brian-dausman-resume-2019.pdf" className="font--primary">
        <h2 className="font--16" style={{ fontWeight: 'bold', display: 'inline' }}>
          {'brian dausman'}
        </h2>
      </a>
      {' + '}
      <a href="http://www.andrewbieganski.com/images/andrewbieganski2018.pdf" className="font--primary" target="_blank" rel="noopener noreferrer">
        <h2 className="font--16" style={{ fontWeight: 'bold', display: 'inline' }}>
          {'andrew bieganski'}
        </h2>
      </a>
    </strong>
    <br />
    <br />
    <a
      href="https://gitlab.com/nerds-with-charisma"
      target="_blank"
      rel="noopener noreferrer"
      className="font--grey"
    >
      <i className="fab fa-gitlab font--24">
        <div className="ir">Nerds With Charisma GitLab Link</div>
      </i>
    </a>
    &nbsp;&nbsp;&nbsp;
    <a
      href="https://twitter.com/nerdswcharisma"
      target="_blank"
      rel="noopener noreferrer"
      className="font--grey"
    >
      <i className="fab fa-twitter font--24">
        <div className="ir">Nerds With Charisma Twitter</div>
      </i>
    </a>
    &nbsp;&nbsp;&nbsp;
    <a
      href="https://www.npmjs.com/~nerds-with-charisma"
      target="_blank"
      rel="noopener noreferrer"
      className="font--grey"
    >
      <i className="fab fa-npm font--28">
        <div className="ir">Nerds With Charisma NPM</div>
      </i>
    </a>
    &nbsp;&nbsp;&nbsp;
    <a
      href="https://medium.com/@nerdswithcharisma"
      target="_blank"
      rel="noopener noreferrer"
      className="font--grey"
    >
      <i className="fab fa-medium-m font--28">
        <div className="ir">Nerds With Charisma Medium</div>
      </i>
    </a>
    &nbsp;&nbsp;&nbsp;
    <Link
      to="/docs"
      className="font--grey"
    >
      <i className="fas fa-code font--24">
        <div className="ir">Nerds With Charisma Docs</div>
      </i>
    </Link>
    <button
      type="button"
      style={{
        color: '#999',
        border: 'none',
        position: 'absolute',
        bottom: 5,
        right: -15,
        fontSize: 17,
      }}
      onClick={() => console.info(`
      __.-._
      '-._"7'
      /'.-c
      |  /T
     _)_/LI

     Do or do not. There is no try.

      We honestly can't thank you enough for checking out our site.
      The fact you're even digging in here is pretty neat.

      Are you a non-for profit? We love working with them and are totally willing to work with your budget. Just drop us a line.

      Be excellent to each other
        -Bill S. Preston, Esq.
      `)} // eslint-disable-line
    >
      &#960;
    </button>

    <br />
    <br />

    <small className="font--text">
      {`copyright © ${new Date().getFullYear()} nerds with charisma`}
      <br />
      <span className="font--light">NWC making bitchin websites since the 90s!</span>
    </small>
  </section>
);

export default Footer;
